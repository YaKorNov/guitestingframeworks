﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{
    public abstract class DbSettings
    {
        /// <summary>
        /// Адрес БД UserManager
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Название БД UserManager
        /// </summary>
        public string Database { get; set; }

        /// <summary>
        /// Пользователь БД UserManager
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль пользователя БД UserManager
        /// </summary>
        public string Password { get; set; }
    }
}
