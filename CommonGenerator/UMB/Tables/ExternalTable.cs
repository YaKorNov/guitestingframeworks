﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.UMB
{

    [TableName(Name = "um_ExternalLogins")]
    public partial class um_ExternalLogins
    {
        public Guid id { get; set; }
        public Guid user_id { get; set; }
        public int type_id { get; set; }
        public string login { get; set; }
    }

}


