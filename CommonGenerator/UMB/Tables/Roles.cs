﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.UMB
{

    [TableName(Name = "um_Roles")]
    public partial class um_Roles
    {
        public Guid id { get; set; }
        public string name { get; set; }
        [Nullable]
        public string description { get; set; }
        public int type_id { get; set; }
        public Guid parent_client_system_id { get; set; }
    }


}


