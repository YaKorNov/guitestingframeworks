﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.UMB
{

    [TableName(Name = "um_ManagedObjectsRoles")]
    public partial class um_ManagedObjectsRoles
    {
        public Guid managed_object_id { get; set; }
        public Guid role_id { get; set; }
    }


}

