﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.UMB
{
    [TableName(Name = "um_Users")]
    public partial class um_Users
    {
        [PrimaryKey(1)]
        public Guid id { get; set; }
        public string name { get; set; }
        public string login { get; set; }
        [Nullable]
        public string pass { get; set; }
        [Nullable]
        public string description { get; set; }
        public byte disabled { get; set; }
        [Nullable]
        public Guid? user_group_id { get; set; }
    }
}
