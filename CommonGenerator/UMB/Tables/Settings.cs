﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.UMB
{
    [TableName(Name = "um_Settings")]
    public partial class um_Settings
    {
        public Guid id { get; set; }
        [Nullable]
        public Guid? user_id { get; set; }
        public Guid settings_meta_id { get; set; }
        [Nullable]
        public string scalar_value { get; set; }
        [Nullable]
        public byte[] binary_value { get; set; }
    }
}
