﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.UMB
{

    [TableName(Name = "um_UserGroups")]
    public partial class um_UserGroups
    {
        public Guid id { get; set; }
        public string name { get; set; }
    }
}
