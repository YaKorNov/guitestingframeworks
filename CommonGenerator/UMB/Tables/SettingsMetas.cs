﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.UMB
{

    [TableName(Name = "um_SettingsMetas")]
    public partial class um_SettingsMetas
    {
        public Guid id { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public Guid parent_client_system_id { get; set; }
    }

}