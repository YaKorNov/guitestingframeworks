﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.UMB
{

    [TableName(Name = "um_ClientSystems")]
    public partial class um_ClientSystems
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string version { get; set; }
        [Nullable]
        public Guid? auth_role_id { get; set; }
        [Nullable]
        public Guid? access_role_id { get; set; }
    }
}
