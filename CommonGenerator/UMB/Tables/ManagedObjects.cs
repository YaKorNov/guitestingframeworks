﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.UMB
{
    [TableName(Name = "um_ManagedObjects")]
    public partial class um_ManagedObjects
    {
        public Guid id { get; set; }
        public int type_id { get; set; }
        public int security_id { get; set; }
    }

}
