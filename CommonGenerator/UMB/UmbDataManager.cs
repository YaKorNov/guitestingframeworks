﻿using System;
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using Microsoft.Xrm.Client;



namespace CommonGenerator.UMB
{
    public  class UMBDataManager : DbManager
    {

        private const string ConnectionStrPattern = "Data Source={0};Initial Catalog={1};User ID={2};Password={3}";

        public UMBDataManager(DataProviderBase dataProvider, UmbSettings settings)
            : base(
                dataProvider,
                ConnectionStrPattern.FormatWith(settings.Address, settings.Database, settings.Login, settings.Password))
        {
        }

        public Table<um_ClientSystems> um_ClientSystems { get { return GetTable<um_ClientSystems>(); } }
        //public Table<um_ExternalLogins> um_ExternalLogins { get { return GetTable<um_ExternalLogins>(); } }
        public Table<um_ManagedObjects> um_ManagedObjects { get { return GetTable<um_ManagedObjects>(); } }
        public Table<um_ManagedObjectsRoles> um_ManagedObjectsRoles { get { return GetTable<um_ManagedObjectsRoles>(); } }
        public Table<um_Roles> um_Roles { get { return GetTable<um_Roles>(); } }
        public Table<um_Settings> um_Settings { get { return GetTable<um_Settings>(); } }
        public Table<um_SettingsMetas> um_SettingsMetas { get { return GetTable<um_SettingsMetas>(); } }
        public Table<um_UserGroups> um_UserGroups { get { return GetTable<um_UserGroups>(); } }
        public Table<um_Users> um_Users { get { return GetTable<um_Users>(); } }
        //public Table<um_UserSessions> um_UserSessions { get { return GetTable<um_UserSessions>(); } }
        public Table<um_ExternalLogins> um_ExternalLogins { get { return GetTable<um_ExternalLogins>(); } }
    }
}
