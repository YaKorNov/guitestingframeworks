﻿using System;
using System.Data;
using BLToolkit.Data.Linq;
using BLToolkit.Data;
using BLToolkit.DataAccess;
using System.Text;
using System.Collections.Generic;
using System.Linq;



namespace CommonGenerator.UMB
{
    public class UmbDataBuilder : IDisposable
    {
        private readonly UMBDataManager _umb;
        private readonly ManagedObjectsDataAccessor _mda;


        public UmbDataBuilder(UMBDataManager umbDbManager)
        {
            if (umbDbManager == null) throw new ArgumentNullException("umbDbManager");

            _umb = umbDbManager;
            _mda = DataAccessor.CreateInstance<ManagedObjectsDataAccessor>(umbDbManager);
            
        }


        #region Roles

        public um_Roles SetCustomRole(string new_role_name, IEnumerable<string> default_role_names, bool if_exists_update_role_info=true)
        {
            foreach (var role_name in default_role_names)
            {
                if (FindRoleByName(role_name) == null)
                    throw new Exception(String.Format("Ошибка при добавлении роли {0}, т.к.  в БД не существует включаемой дефолтной роли {1}", 
                        new_role_name, role_name));
            }


            um_Roles _ro = null;


            var new_role_object = FindRoleByName(new_role_name);

            
            if (new_role_object == null) // add new 
            {

                try
                {
                    _umb.BeginTransaction();


                    _ro = new um_Roles()
                    {
                        id = Guid.NewGuid(),
                        name = new_role_name,
                        description = new_role_name,
                        type_id = 1,
                        parent_client_system_id = GetParentSystemGuidByName("Центр управления пользователями")

                    };

                    Insert(_ro);


                    var _mo = new um_ManagedObjects()
                    {
                        id = _ro.id,
                        type_id = (int)ManagedObjectTypes.Role,
                        security_id = 0
                    };

                    Insert(_mo);



                    foreach (var role_name in default_role_names)
                    {
                        var _mor = new um_ManagedObjectsRoles()
                        {
                            managed_object_id = _mo.id,
                            role_id = GetRoleGuidByName(role_name)
                        };

                        Insert(_mor);
                    }

                    _umb.CommitTransaction();

                }
                catch (Exception ex)
                {
                    _umb.RollbackTransaction();
                    throw new Exception("Ошибка при добавлении роли " + new_role_name + "" + ex.Message);
                }




            }
            else if (new_role_object != null && if_exists_update_role_info) //update
            {

                _ro = new_role_object;

                try
                {
                    _umb.BeginTransaction();


                    //delete old links
                    foreach  (var obj_role in GetObjectRolesLinks(new_role_object.id))
                    {
                        Delete(obj_role);
                    }


                    foreach (var role_name in default_role_names)
                    {
                        var _mor = new um_ManagedObjectsRoles()
                        {
                            managed_object_id = new_role_object.id,
                            role_id = GetRoleGuidByName(role_name)
                        };

                        Insert(_mor);
                    }

                    _umb.CommitTransaction();

                }
                catch (Exception ex)
                {
                    _umb.RollbackTransaction();
                    throw new Exception("Ошибка при обновлении данных роли " + new_role_name + "" + ex.Message);
                }


            }
            else if (new_role_object != null && !if_exists_update_role_info) //return existent
            {
                _ro = new_role_object;
            }



            return _ro;
            


        }

        public um_Roles FindRoleByName(string role_name)
        {
            return _umb.um_Roles.Where(role => role.name == role_name).FirstOrDefault();
        }

        public um_Roles FindRoleByGuid(Guid role_guid)
        {
            return _umb.um_Roles.Where(role => role.id == role_guid).FirstOrDefault();
        }

        

        public Guid GetRoleGuidByName(string role_name)
        {
            var role_obj = _umb.um_Roles.Where(role => role.name == role_name).FirstOrDefault();
            if ( role_obj != null)
            {
                return role_obj.id;
            }
            else
            {
                throw new Exception("Не существует роли с именем" + role_name);
            }


        }



        #endregion



        #region ObjectRoles

        public IEnumerable<um_ManagedObjectsRoles> GetObjectRolesLinks(Guid m_object_guid)
        {

            return
                (from obj_roles
                  in _umb.um_ManagedObjectsRoles
                  where obj_roles.managed_object_id == m_object_guid
                  select obj_roles).ToArray();

        }


        public IEnumerable<um_Roles> GetLinkedObjectRoles(Guid m_object_guid)
        {
            
            return  
                (from role 
                in _umb.um_Roles
                where
                (from obj_roles 
                 in _umb.um_ManagedObjectsRoles
                 where obj_roles.managed_object_id == m_object_guid
                 select obj_roles.role_id).Contains(role.id)
                select role).ToArray();

        }


        public IEnumerable<um_Roles> GetLinkedObjectRoles(um_Roles role)
        {

            return
                (from rle
                in _umb.um_Roles
                 where
                 (from obj_roles
                  in _umb.um_ManagedObjectsRoles
                  where obj_roles.managed_object_id == role.id
                  select obj_roles.role_id).Contains(rle.id)
                 select rle).ToArray();

        }


        public IEnumerable<um_Roles> GetLinkedObjectRoles(um_Users usr)
        {

            return
                (from rle
                in _umb.um_Roles
                 where
                 (from obj_roles
                  in _umb.um_ManagedObjectsRoles
                  where obj_roles.managed_object_id == usr.id
                  select obj_roles.role_id).Contains(rle.id)
                 select rle).ToArray();

        }


        #endregion



        #region Users

        public um_Users SetUser(string username, string pasword, IEnumerable<string> assigned_roles, bool disabled=false, bool if_exists_update_user_info=true)
        {
            foreach (var role_name in assigned_roles)
            {
                if (FindRoleByName(role_name) == null)
                    throw new Exception(String.Format("Невозможно назначить роль {0}, т.к. ее не существует",
                       role_name));
            }

            um_Users _uo = null;

            var new_user_object = FindUserByName(username);

            var encrypted_pass = SimpleCryptoService.HashToString( new SimpleCryptoService().ComputeHash(pasword));

            //var encrypted_login = SimpleCryptoService.HashToString(new SimpleCryptoService().ComputeHash(username));



            if (new_user_object == null)// add
            {
                try
                {
                    _umb.BeginTransaction();


                    _uo = new um_Users()
                    {
                        id = Guid.NewGuid(),
                        name = username,
                        login = username,
                        pass = encrypted_pass,
                        description = username,
                        disabled = (byte)(disabled == true ? 1 : 0),
                        user_group_id = null

                    };

                    Insert(_uo);


                    //var _ext_login = new um_ExternalLogins()
                    //{
                    //    id = Guid.NewGuid(),
                    //    user_id = _uo.id,
                    //    type_id = 20,
                    //    login = encrypted_login
                    //};

                    //Insert(_ext_login);


                    var _mo = new um_ManagedObjects()
                    {
                        id = _uo.id,
                        type_id = (int)ManagedObjectTypes.User,
                        security_id = 0
                    };

                    Insert(_mo);



                    foreach (var role_name in assigned_roles)
                    {
                        var _mor = new um_ManagedObjectsRoles()
                        {
                            managed_object_id = _mo.id,
                            role_id = GetRoleGuidByName(role_name)
                        };

                        Insert(_mor);
                    }

                    _umb.CommitTransaction();

                }
                catch (Exception ex)
                {
                    _umb.RollbackTransaction();
                    throw new Exception("Ошибка при добавлении пользователя " + username + "" + ex.Message);
                }


            }
            else if (new_user_object != null && if_exists_update_user_info) //update
            {

                _uo = new_user_object;

        
                try
                {
                    _umb.BeginTransaction();


                    //update user properties
                    _uo.pass = encrypted_pass;
                    _uo.disabled = (byte)(disabled == true ? 1 : 0);
                    InsertOrUpdate(_uo);

                    //delete old links
                    foreach (var obj_role in GetObjectRolesLinks(new_user_object.id))
                    {
                        Delete(obj_role);
                    }


                    foreach (var role_name in assigned_roles)
                    {
                        var _mor = new um_ManagedObjectsRoles()
                        {
                            managed_object_id = new_user_object.id,
                            role_id = GetRoleGuidByName(role_name)
                        };

                        Insert(_mor);
                    }

                    _umb.CommitTransaction();

                }
                catch (Exception ex)
                {
                    _umb.RollbackTransaction();
                    throw new Exception("Ошибка при обновлении данных роли " + username + "" + ex.Message);
                }


            }
            else if (new_user_object != null && !if_exists_update_user_info) //return existent
            {
                _uo = new_user_object;
            }


            

            return _uo;


        }


        public um_Users  FindUserByName(string user_name)
        {
            return _umb.um_Users.Where(usr => usr.name == user_name).FirstOrDefault();
        }




        public IEnumerable<um_Roles> Get_Default_User_Roles(um_Users user)
        {
            var def_roles = new List<um_Roles>();

            foreach (var rle in GetLinkedObjectRoles(user))
            {
                def_roles.AddRange(GetLinkedObjectRoles(rle).ToList());
            }

            return def_roles.Distinct().ToArray();
        }




        #endregion



        public Guid GetParentSystemGuidByName(string system_name)
        {
            var sys_obj = _umb.um_ClientSystems.Where(sys => sys.name == system_name).FirstOrDefault();
            if (sys_obj != null)
            {
                return sys_obj.id;
            }
            else
            {
                throw new Exception("В БД не существует записи для программы комплекса МАГ" + system_name);
            }
        }


        

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        private void Insert<T>(T tableRowDto)
        {
            _umb.Insert(tableRowDto);
        }


        private void InsertOrUpdate<T>(T tableRowDto)
        {
            _umb.InsertOrReplace(tableRowDto);
        }

        private void Delete<T>(T tableRowDto)
        {
            _umb.Delete(tableRowDto);
        }

        private int InsertWithIdentity<T>(T tableRowDto)
        {
            return Convert.ToInt32(_umb.InsertWithIdentity(tableRowDto));
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
                _umb.Dispose();
        }

        ~UmbDataBuilder()
        {
            Dispose(false);
        }


    }
}





        
