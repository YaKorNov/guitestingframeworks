﻿using System;
using System.Collections.Generic;
using System.Data;
using BLToolkit.DataAccess;


namespace CommonGenerator.UMB
{
    
    public abstract class ManagedObjectsDataAccessor : DataAccessor<um_ManagedObjects, ManagedObjectsDataAccessor>
    {

        /// <summary>
        /// Получить объекты по типу
        /// </summary>
        [SqlQuery(@"
                    SELECT  * FROM um_ManagedObjects
			        WHERE type_id = @type_id
                ;")]
        public abstract List<um_ManagedObjects> FindTasksByShifrList(string type_id);
    }

}
