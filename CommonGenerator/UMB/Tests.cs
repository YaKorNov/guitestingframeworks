﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using BLToolkit.Data.DataProvider;




namespace CommonGenerator.UMB
{

    [TestFixture]
    public class CommonGeneratorTests
    {
        private readonly UmbSettings _umbSettings;
        private readonly UmbDataBuilder _umbDataBuilder;

        public CommonGeneratorTests()
        {

            _umbSettings = new UmbSettings()
            {
                Address = @"gui-test",
                Database = "UserManagerBase",
                Login = "sa",
                Password = "m1m2m30-="
            };
            _umbDataBuilder = new UmbDataBuilder(new UMBDataManager(new SqlDataProvider(), _umbSettings));

        }

        [Test]
        public void Set_Some_Role()
        {
            var new_role = _umbDataBuilder.SetCustomRole("Custom role 1", 
                new List<string>() { "Mag.UsersManager.AccessToApplication", "Mag.TaskDistributor.AccessToApplication" });

           var included_roles = _umbDataBuilder.GetLinkedObjectRoles(new_role).Select(rle => rle.name).ToList();
            
            Assert.Contains("Mag.UsersManager.AccessToApplication", included_roles);
            Assert.Contains("Mag.TaskDistributor.AccessToApplication", included_roles);
        }


        [Test]
        //[Ignore ("проблема,, если есть одноименные роли")]
        public void Set_Some_User()
        {
            var new_role = _umbDataBuilder.SetCustomRole("Custom role 1",
               new List<string>() { "Mag.UsersManager.AccessToApplication", "Mag.TaskDistributor.AccessToApplication", "Mag.RIM.AccessToApplication", "Mag.UsersManager.DeleteUser" });

            var new_role2 = _umbDataBuilder.SetCustomRole("Custom role 2",
               new List<string>() { "Signatec.Mag.Abonents.AccessToApplication", "Mag.UsersManager.DeleteUser" });

            var new_user = _umbDataBuilder.SetUser("Fedya", "mag1991", new List<string>() { new_role.name, new_role2.name }
                );

            //будет возвращено 6 объектов, т.к. возвращаются уникальные объекты
            var default_roles = _umbDataBuilder.Get_Default_User_Roles(new_user).Select(rle => rle.name).Distinct().ToList();

            Assert.AreEqual(5, default_roles.Count);


        }


        


    }

}




