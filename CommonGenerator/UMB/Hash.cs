﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;


//вязто из Signatec.Mag.UsersManager.ImplementationCore.Storage
namespace CommonGenerator.UMB
{
    /// <summary>
    /// Пока функции по шифрованию пароля просто вынес сюда
    /// </summary>
    public class SimpleCryptoService
    {
        private const string Key = "RqjBbxOACq8=";
        private const string Iv = "1qaC8vw0JvE=";

        public byte[] ComputeHash(string str)
        {
            return string.IsNullOrEmpty(str)
                ? new byte[0]
                : new SHA1CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(str));
        }

        public static string HashToString(byte[] hash)
        {
            return (null == hash) || (0 == hash.Length)
                ? ""
                : Convert.ToBase64String(hash);
        }

        public static byte[] StringToHash(string pass)
        {
            return string.IsNullOrEmpty(pass)
                ? new byte[0]
                : Convert.FromBase64String(pass);
        }

        public static string Encrypt(string value)
        {
            value = value ?? "";
            // Добавление немного мусора в строку чтобы она получилась пооригинальнее
            var str = (DateTime.Now.Hour % 10).ToString(CultureInfo.InvariantCulture)
                + (DateTime.Now.Minute % 10).ToString(CultureInfo.InvariantCulture)
                + (DateTime.Now.Second % 10).ToString(CultureInfo.InvariantCulture)
                + value + value + value;
            str = StrToStr(str
                , Encoding.UTF8.GetBytes
                , (des, key, iv) => des.CreateEncryptor(key, iv)
                , Convert.ToBase64String);
            var index1 = str[0] % 10;
            index1 = index1 < 5 ? 8 : index1;

            var str2 = StrToStr(str
                , Encoding.UTF8.GetBytes
                , (des, key, iv) => des.CreateEncryptor(key, iv)
                , Convert.ToBase64String);
            var index2 = str2[0] % 20;
            index2 = index2 < 5 ? 9 : index2;

            var str3 = str.Insert(index1, str2.Substring(0, index2));
            return StrToStr(str3
                , Encoding.UTF8.GetBytes
                , (des, key, iv) => des.CreateEncryptor(key, iv)
                , Convert.ToBase64String);
        }

        public static string Decrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            value = StrToStr(value
                , Convert.FromBase64String
                , (des, key, iv) => des.CreateDecryptor(key, iv)
                , Encoding.UTF8.GetString);
            var index1 = value[0] % 10;
            index1 = index1 < 5 ? 8 : index1;

            var index2 = value[index1] % 20;
            index2 = index2 < 5 ? 9 : index2;

            value = value.Remove(index1, index2); // Убран мусор

            value = StrToStr(value
                , Convert.FromBase64String
                , (des, key, iv) => des.CreateDecryptor(key, iv)
                , Encoding.UTF8.GetString);
            return value.Substring(3, (value.Length - 3) / 3);
        }

        private static string StrToStr(string value, Func<string, byte[]> strToByte, Func<DES, byte[], byte[], ICryptoTransform> transform, Func<byte[], string> byteToStr)
        {
            using (var fout = new MemoryStream())
            {
                using (var fin = new MemoryStream(strToByte(value)))
                {
                    var bin = new byte[100];
                    long rdlen = 0;
                    var totlen = fin.Length;

                    DES des = new DESCryptoServiceProvider();
                    byte[] key = Convert.FromBase64String(Key);
                    byte[] iv = Convert.FromBase64String(Iv);
                    using (var encStream = new CryptoStream(fout, transform(des, key, iv), CryptoStreamMode.Write))
                    {
                        while (rdlen < totlen)
                        {
                            var len = fin.Read(bin, 0, 100);
                            encStream.Write(bin, 0, len);
                            rdlen = rdlen + len;
                        }

                        encStream.Flush();
                        encStream.FlushFinalBlock();
                        fout.Position = 0;
                        return byteToStr(fout.ToArray());
                    }

                }
            }
        }
    }
}