﻿using System;
using System.Data;
using BLToolkit.Data.Linq;
using BLToolkit.Data;
using BLToolkit.DataAccess;
using DataGenerator.Mtb.Tables;
using System.Linq;
using System.Collections.Generic;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Mtb;
using CommonGenerator.Mtb.Tables;
using System.Text;
using CommonGenerator;
using System.Xml.Linq;
using CommonGenerator.Moj.Enums;


namespace DataGenerator.Mtb.MtbBuilders
{
    public class MtbDataBuilder : IDisposable
    {
        #region Fields

        private readonly MtbDbManager _mtb;
        private string  _autotask_file_path;
        private readonly CommonDataAccessor _cda;
        private readonly CommonDataAccessor _cleanup_da;
        private readonly MagCommonDataAccessor _mcda;
        private readonly MagVersion _mag_version;

        public const string DefaultSmsMtbGuidString = "69801926-C0F3-4C63-A6C7-A41EB053612E";
        public const string DefaultTalkMtbGuidString = "D509398F-D043-447F-ADB7-B9A2C134AA16";
        public const string DefaultPlaceMtbGuidString = "290cd8e1-dafe-4c04-b464-6928357ec4f5";
        public const string DefaultTalkMarkGuidString = "d64a6827-4d3d-41a0-84f7-8f00f4f5c4f6";
        public const string DefaultPhoneNumber = "79131111111";

        //source_name, device_id, channel_id
        public const SourceTypes DefaultSourceName = SourceTypes.MagFlowBroker;
        public const int DefaultDeviceId = 0;
        public const int DefaultChannelId = 256;

        //код страны
        public int def_mcc = (int)MCC.Russia; //Россия
        //код оператора
        public int def_mnc = (int)MNC.MTS; //МТС
        //код локальной зоны
        public int def_lac = 16;
        //идентификатор соты 
        public int def_cl = 16;
           
        public const string AnotherSmsMtbGuidString = "DCF9CCFE-1DF2-4C5A-8C5F-A55043AAFBA3";
        public const string AnotherTalkMtbGuidString = "2B9A70B1-EBAB-41B1-BA8C-9264871DEE83";
        public const string AnotherPhoneNumber = "79139999999";

        public static DateTime DefaultDateTime = new DateTime(2015, 04, 01, 11, 55, 58);
        public static DateTime DefaultBeginDateTime = new DateTime(2015, 04, 01, 11, 45, 47);
        public static DateTime DefaultEndDateTime = new DateTime(2015, 04, 01, 11, 55, 58);

        #endregion


        #region Properties

        private string AutoTaskFilePath {
            get
            {

                if (string.IsNullOrWhiteSpace(_autotask_file_path))
                {
                    _autotask_file_path = SelectUserOptionByName("beginsDefMagFilePath");
                    _autotask_file_path = !string.IsNullOrWhiteSpace(_autotask_file_path) ? _autotask_file_path : "default_path";
                }

                return _autotask_file_path;
            }
            
        }

        #endregion



        public MtbDataBuilder(MtbDbManager mtbDbManager, MagVersion mag_version )
        {
            if (mtbDbManager == null) throw new ArgumentNullException("mtbDbManager");

            _mtb = mtbDbManager;
            _cda = DataAccessor.CreateInstance<CommonDataAccessor>(mtbDbManager);
            _mcda = DataAccessor.CreateInstance<MagCommonDataAccessor>(mtbDbManager);

            _mag_version = mag_version;
    
        }


        public MtbDataBuilder(MtbDbManager cleanupMtbDbManager, MtbDbManager mtbDbManager, MagVersion mag_version)
        {
            if (mtbDbManager == null) throw new ArgumentNullException("mtbDbManager");
            if (cleanupMtbDbManager == null) throw new ArgumentNullException("cleanupMtbDbManager");

            _mtb = mtbDbManager;
            _cda = DataAccessor.CreateInstance<CommonDataAccessor>(mtbDbManager);
            _cleanup_da = DataAccessor.CreateInstance<CommonDataAccessor>(cleanupMtbDbManager);

            _mag_version = mag_version;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void FullCleanUpMtb()
        {

            try
            {
                _mtb.BeginTransaction();

                _cda.FullCleanUp();

                _mtb.CommitTransaction();

            }
            catch (Exception ex)
            {
                _mtb.RollbackTransaction();
                throw new Exception("Ошибка при очистке базы MTB " + ex.Message);
            }

        }

        private void ClearDefaultParams(params string[] paramNames)
        {
            foreach (var paramName in paramNames)
            {
                _mtb.DefaultParams
                    .Update(o => o.NameParam == paramName, o => new DefaultParam { NameParam = o.NameParam, Param = "" });
            }
        }

        private void ClearUserOptions(params string[] optionNames)
        {
            foreach (var optionName in optionNames)
            {
                _mtb.UserOptions
                    .Update(o => o.FieldName == optionName, o => new UserOption { FieldName = o.FieldName, FieldValue = "" });
            }
        }


        private string SelectUserOptionByName(string optionName)
        {
            //TODO DataAccessor
            //string op_name =  (string)_mtb.SetCommand(@"
            //   SELECT UserOption.field_value
            //   FROM UserOption
            //   WHERE UserOption.field_name = :field_name",
            //         _mtb.Parameter("field_name", optionName))
            //     .ExecuteScalar();

            foreach (var _us_op in _mtb.UserOptions)
            {
                if (_us_op.FieldName == optionName)
                    return _us_op.FieldValue;
            }

            return String.Empty;
        }


        public void Update<T>(T dto)
        {
            _mtb.Update(dto);
        }


        #region Autotask

        private TaskAuto WriteTaskAuto(TaskAuto taskAuto)
        {
            taskAuto.TaskAutoId = InsertWithIdentity(taskAuto);
            return taskAuto;
        }


        public TaskAuto CreateTaskAuto(string reg_number, DateTime CreationTime)
        {
            return CreateTaskAuto(reg_number, DefaultSourceName, DefaultDeviceId, DefaultChannelId, true, CreationTime);
        }


        public TaskAuto CreateTaskAuto(string reg_number, int device_id, int channel_id, DateTime CreationTime)
        {
            return CreateTaskAuto(reg_number, DefaultSourceName, device_id, channel_id, true, CreationTime);
        }


        public TaskAuto CreateTaskAuto(string reg_number, SourceTypes source_name, int device_id, int channel_id, DateTime CreationTime)
        {
            //создавать по дефолту активные автозадания
            return CreateTaskAuto(reg_number, source_name, device_id, channel_id, true, CreationTime);
        }

        
        //наиболее общий случай (все значимые атрибуты)
        public TaskAuto CreateTaskAuto(string reg_number, SourceTypes source_name, int device_id, int channel_id, bool active, DateTime CreationTime )
        {

            var taskAuto = TaskAuto.CreatePrototype();

            //данные атрибуты устройство/канал, идентификатор(reg_number), источник
            //отображаются при подключении задания к автозаданию в УЗ(управлении заданиями)
            taskAuto.RegNumber = reg_number;
            taskAuto.DeviceId = device_id;
            taskAuto.Channel = channel_id;
            //по дефолту - mag flow broker
            taskAuto.SourceId = (int)source_name;
            //var matchedSources = TaskAuto.SourceTypes.Where(x => x.Value == source_name);
            //if (matchedSources.Any())
            //{
            //    taskAuto.SourceId = matchedSources.First().Key;
            //}

            //активно ли автозадание (устанавливается = 1, когда подключаем таск и автотаск)
            taskAuto.ActiveFlag = (byte)(active == true ? 1 : 0);

            //дата создания автозадания
            taskAuto.CreationTime = CreationTime;
            taskAuto.LastModify = CreationTime;

            //путь сохранения файла автозадания
            taskAuto.FilePath  = AutoTaskFilePath + @"\" + reg_number;


            taskAuto.TaskAutoGuid = System.Guid.NewGuid();

            return WriteTaskAuto(taskAuto);

        }


        public TaskAuto GetAutoTaskById(int Id)
        {
            SqlQuery<TaskAuto> query = new SqlQuery<TaskAuto>();

            return  query.SelectByKey(_mtb, Id);
            
        }


        public List<TaskAuto> GetAutoTasksByIdList(List<int> autotask_ids)
        {
            return _mtb.TasksAuto.Where(t_auto => autotask_ids.Contains(t_auto.TaskAutoId)).ToList();
        }


        public List<TaskAuto> SelectAutoTasksByRegName(string reg_name)
        {
            return _mtb.TasksAuto.Where(x => x.RegNumber == reg_name).Select(x => x).ToList();
        }


        #endregion


        #region Task

        //основные атрибуты задания
        public TaskMtb CreateTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName)
        {
            return CreateTaskMtb(taskBegin, taskSrok,  meropr,  vid_object,  task_number,  task_year, categoryName, "", "", "", DataTypes.Unsigned, new List<TaskTypes>(), SecurityLevel.Normal);
        }

        //основные атрибуты задания + уровень секретности
        public TaskMtb CreateTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName, SecurityLevel securityLevel)
        {
            return CreateTaskMtb(taskBegin, taskSrok,  meropr,  vid_object,  task_number,  task_year, categoryName, "", "", "", DataTypes.Unsigned, new List<TaskTypes>(), securityLevel);
        }

        //не указываем значения для атрибутов initiator, orient, goal, dataType (присутствуют в таске из MOJ )
        public TaskMtb CreateTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName, List<TaskTypes> taskTypes, SecurityLevel securityLevel)
        {
            return CreateTaskMtb(taskBegin, taskSrok,  meropr,  vid_object,  task_number,  task_year, categoryName, "", "", "", DataTypes.Unsigned, taskTypes, securityLevel);
        }

        //общий случай
        public TaskMtb CreateTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName, string initiator, string orient, string goal, DataTypes dataType, List<TaskTypes> taskTypes, SecurityLevel securityLevel  )
        {
            var task = TaskMtb.CreatePrototype();

            try
            {
                
                task.TaskBegin = taskBegin;
                task.TaskSrok = taskSrok;
                task.ObjName = String.Format(@"{0}-{1}-{2}-{3}", meropr, vid_object, task_number, task_year);
                task.ObjShifr = String.Format(@"{0}-{1}-{2}-{3}", meropr, vid_object, task_number, task_year);

                task.TaskGuid = Guid.NewGuid();
                //по дефолту - категория не определена
                task.CategoryId = (int)categoryName;

                task.Orientation = orient;
                task.Goal = goal;
                task.Initiator = initiator;

                task.DataType = (int)dataType;

                task.SecurityId = (int)securityLevel;

                //виды сеансов, связанных с заданием
                if (taskTypes.Count > 0)
                {
                    int result_ttype_id = 0;
                    foreach (var tt in taskTypes)
                    {
                        if (tt == TaskTypes.All)
                            continue;
                        result_ttype_id += (int)tt;
                    }
                    task.TaskType = result_ttype_id;
                }
                //все виды сеансов
                else
                    task.TaskType = (int)TaskTypes.All;

                _mtb.BeginTransaction();
                //task.TaskId = InsertWithIdentity(task);
                _cda.AddTask(task);

                //cipher parts

                var task_cp = new TaskCipherParts();
                //начало и конец устанавливаются в ХП
                task_cp.TaskBegin = taskBegin;
                task_cp.TaskEnd = task_cp.TaskBegin.Value.AddDays(taskSrok);
                task_cp.TaskId = task.TaskId;
                task_cp.TaskNumber = task_number;
                task_cp.TaskObject = vid_object;
                task_cp.TaskType = meropr;
                task_cp.TaskYear = task_year;
                
                //_mtb.TaskCipherParts.Insert(() => task_cp);
                
                _cda.SetCipherParts(task_cp);

                _mtb.CommitTransaction();
                
            }
            catch
            {
                _mtb.RollbackTransaction();
                throw new Exception("Ошибка при добавлении задания "+ task.ObjShifr);
            }

            return task;


        }



        public TaskMtb SetTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName, string initiator, string orient, string goal, DataTypes dataType, List<TaskTypes> taskTypes, SecurityLevel securityLevel, Guid task_guid,
            int ArcKeepPlus, int ArcKeepMinus, int ArcKeepNotMark)
        {
            var task = TaskMtb.CreatePrototype();

            try
            {

                task.TaskBegin = taskBegin;
                task.TaskSrok = taskSrok;
                task.ObjName = String.Format(@"{0}-{1}-{2}-{3}", meropr, vid_object, task_number, task_year);
                task.ObjShifr = String.Format(@"{0}-{1}-{2}-{3}", meropr, vid_object, task_number, task_year);

                task.TaskGuid = task_guid;
                //по дефолту - категория не определена
                task.CategoryId = (int)categoryName;

                task.Orientation = orient;
                task.Goal = goal;
                task.Initiator = initiator;

                task.DataType = (int)dataType;

                task.SecurityId = (int)securityLevel;

                //виды сеансов, связанных с заданием
                if (taskTypes.Count > 0)
                {
                    int result_ttype_id = 0;
                    foreach (var tt in taskTypes)
                    {
                        if (tt == TaskTypes.All)
                            continue;
                        result_ttype_id += (int)tt;
                    }
                    task.TaskType = result_ttype_id;
                }
                //все виды сеансов
                else
                    task.TaskType = (int)TaskTypes.All;


                task.ArcKeepPlus = ArcKeepPlus;
                task.ArcKeepMinus = ArcKeepMinus;
                task.ArcKeepNotMark = ArcKeepNotMark;


                _mtb.BeginTransaction();

                var f_task = _cda.FindTaskByShifr(task.ObjShifr);

                if (f_task != null)
                {
                    task.TaskId = f_task.TaskId;
                    _cda.UpdateTask(task);
                }
                else
                    _cda.AddTask(task);

                _cda.UpdateArcPlisMinusMarks(task.ArcKeepPlus.Value, task.ArcKeepMinus.Value, task.ArcKeepNotMark.Value, task.TaskId);

                //cipher parts

                var task_cp = new TaskCipherParts();
                task_cp.TaskBegin = taskBegin;
                task_cp.TaskEnd = task_cp.TaskBegin.Value.AddDays(taskSrok);
                task_cp.TaskId = task.TaskId;
                task_cp.TaskNumber = task_number;
                task_cp.TaskObject = vid_object;
                task_cp.TaskType = meropr;
                task_cp.TaskYear = task_year;

                //_mtb.TaskCipherParts.Insert(() => task_cp);

                _cda.SetCipherParts(task_cp);

                _mtb.CommitTransaction();
            }
            catch
            {
                _mtb.RollbackTransaction();
                throw new Exception("Ошибка при добавлении задания " + task.ObjShifr);
            }

            return task;
        }

        public TaskMtb SetTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName, string initiator, string orient, string goal, DataTypes dataType, List<TaskTypes> taskTypes, SecurityLevel securityLevel)
        {
            var task_guid = new Guid();
            return SetTaskMtb(taskBegin,  taskSrok,  meropr,  vid_object,  task_number,  task_year,  categoryName,  initiator,  orient,  goal, dataType, taskTypes, securityLevel, task_guid, 10, 20, 20);
        }


        //основные атрибуты задания
        public TaskMtb SetTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName)
        {
            return SetTaskMtb(taskBegin, taskSrok, meropr, vid_object, task_number, task_year, categoryName, "", "", "", DataTypes.Unsigned, new List<TaskTypes>(), SecurityLevel.Normal);
        }


        //основные атрибуты задания + минусовые/плюсовые метки
        public TaskMtb SetTaskMtb(DateTime? taskBegin, int taskSrok, string meropr, string vid_object, string task_number, string task_year, Categories categoryName, int ArcKeepPlus, int ArcKeepMinus, int ArcKeepNotMark)
        {
            var task_guid = new Guid();
            return SetTaskMtb(taskBegin, taskSrok, meropr, vid_object, task_number, task_year, categoryName, "", "", "", DataTypes.Unsigned, new List<TaskTypes>(), SecurityLevel.Normal, task_guid, ArcKeepPlus, ArcKeepMinus, ArcKeepNotMark);
        }



        public List<TaskMtb> GetTaskList()
        {
            return _cda.FindTask();
        }


        public List<TaskMtb> GetTasksByShifrList(string shifr_list)
        {
            return _cda.FindTaskByShifrList(shifr_list);
        }


        public List<string> GetAutotasksRegNumberList()
        {
            return _cda.GetAutotasksRegNumberList();
        }


        public DataTable GetAutotasksWithTaskGuids(List<string> shifr_list)
        {
            return _cda.GetAutotasksWithTaskGuids(String.Join(";",shifr_list));
        }


        public TaskMtb GetTaskById(int Id)
        {
            SqlQuery<TaskMtb> query = new SqlQuery<TaskMtb>();

            return query.SelectByKey(_mtb, Id);

        }

        public TaskMtb GetTaskByShifr(string task_shifr)
        {
            return _cda.FindTaskByShifr(task_shifr);

        }

        public List<TaskMtb> FindTasksByShifrList(List<string> task_shifr_list)
        {
            var in_clause = String.Join(",", task_shifr_list.Select(task_shifr => String.Format("'{0}'", task_shifr)).ToList());
            return _cda.FindTasksByShifrList(in_clause);

        }

        #endregion



        #region TaskGrant
        public TaskGrant CreateTaskGrant(TaskAuto taskAuto, TaskMtb taskMtb, DateTime autoStart, DateTime? autoEnd)
        {
            var tg = TaskGrant.CreatePrototype();
            tg.TaskAutoId = taskAuto.TaskAutoId;
            tg.TaskOperId = taskMtb.TaskId;
            tg.AutoStart = autoStart;
            tg.AutoEnd = autoEnd;
            tg.TaskGrantId = InsertWithIdentity(tg);
            

            return tg;
        }


        public TaskGrant CreateTaskGrant(int taskAutoId, TaskMtb taskMtb, DateTime autoStart, DateTime? autoEnd)
        {
            var tg = TaskGrant.CreatePrototype();
            tg.TaskAutoId = taskAutoId;
            tg.TaskOperId = taskMtb.TaskId;
            tg.AutoStart = autoStart;
            tg.AutoEnd = autoEnd;
            tg.TaskGrantId = InsertWithIdentity(tg);


            return tg;
        }

        public TaskGrant CreateTaskGrant(int taskAutoId, int taskMtbId, DateTime autoStart, DateTime? autoEnd)
        {
            var tg = TaskGrant.CreatePrototype();
            tg.TaskAutoId = taskAutoId;
            tg.TaskOperId = taskMtbId;
            tg.AutoStart = autoStart;
            tg.AutoEnd = autoEnd;
            tg.TaskGrantId = InsertWithIdentity(tg);


            return tg;
        }


        public List<TaskGrant> SelectTaskGrants()
        {
            return _mtb.TasksGrant.ToList();

        }


        public List<TaskAuto> SelectAutoTasks()
        {
            return  _mtb.TasksAuto.ToList();

        }

       


        public IDictionary<int, string> GetControlPhonesList()
        {
            return _cda.GetControlPhonesList();
        }



        public List<TaskAuto> SelectAutoTasksTiedWithTask(TaskMtb task)
        {
            //List<TaskAuto> _ta_list = _mtb.TasksGrant.Where(x => x.TaskOperId == task.TaskId).Select(x => x.AutoTask).ToList();
            List<int> _aut_list = _mtb.TasksGrant.Where(x => x.TaskOperId == task.TaskId).Select(x => x.TaskAutoId).ToList();
            List<TaskAuto> _ta_list = _mtb.TasksAuto
                  .Where(x => _aut_list.Contains(x.TaskAutoId)).ToList();

            return _ta_list;

        }

        public List<TaskGrant> SelectsTasksGrantsTiedWithTask(TaskMtb task)
        {
            //List<TaskAuto> _ta_list = _mtb.TasksGrant.Where(x => x.TaskOperId == task.TaskId).Select(x => x.AutoTask).ToList();
            return _mtb.TasksGrant.Where(x => x.TaskOperId == task.TaskId).Select(x => x).ToList();
            

        }


        public List<TaskMtb> SelectTasksTiedWithAutoTask(TaskAuto taskAuto)
        {

            List<int> _ta_list = _mtb.TasksGrant.Where(x => x.TaskAutoId == taskAuto.TaskAutoId).Select(x => x.TaskOperId).ToList();
            List<TaskMtb> _t_list = _mtb.Tasks
                  .Where(x => _ta_list.Contains(x.TaskId)).ToList();

            return _t_list;
            
        }


        public int CountAutoTasksTiedWithTask(TaskMtb task)
        {
            return SelectAutoTasksTiedWithTask(task).Count;
        }

        #endregion


        #region Sms

        private SmsMtb Write(SmsMtb sms, TaskAuto taskAuto)
        {
            sms.TaskAutoId = taskAuto.TaskAutoId;
            sms.SmsId = InsertWithIdentity(sms);
            
            return sms;
        }

        //public SmsMtb CreateSmsMtb(TaskAuto taskAuto, string guid)
        //{
        //    return CreateSmsMtb(taskAuto, DefaultDateTime, guid);
        //}

        public SmsMtb CreateSmsMtb(TaskAuto taskAuto, string phoneControlObject, string phoneInterlocutor, DateTime sendRecvTime, PhoneDirection Direction)
        {
            return CreateSmsMtb(taskAuto, phoneControlObject, phoneInterlocutor, sendRecvTime, sendRecvTime, Direction, false, "sms_default_text");
        }

        public SmsMtb CreateSmsMtb(TaskAuto taskAuto, string phoneControlObject, string phoneInterlocutor, DateTime sendRecvTime, PhoneDirection Direction, bool archived)
        {
            return CreateSmsMtb(taskAuto, phoneControlObject, phoneInterlocutor, sendRecvTime, sendRecvTime, Direction, archived, "sms_default_text");
        }


        public SmsMtb CreateSmsMtb(TaskAuto taskAuto, string phoneControlObject, string phoneInterlocutor, DateTime sendTime, DateTime recvTime, PhoneDirection Direction, bool archived, string sms_text)
        {
            
            if (taskAuto == null) throw new ArgumentNullException("taskAuto");

            var sms = SmsMtb.CreatePrototype();
            sms.SmsGuid = Guid.NewGuid();
           
            sms.TelBTel = phoneControlObject;
            sms.TelBSelTy = (byte)SEL_TY.SEL_A;
            sms.TelBSelId = (byte)TEL_ID.TEL_ID_ATS;

            sms.TelATel = phoneInterlocutor;
            sms.TelASelTy = (byte)SEL_TY.SEL_B;
            sms.TelASelId = (byte)TEL_ID.TEL_ID_ATS;

            sms.SendDateTime = sendTime;
            sms.RecvDateTime = recvTime;
            sms.Direction = Direction == PhoneDirection.Incoming ? false : true;
            sms.ArcFlag = (byte)(archived ? 1 : 0);

            //
            sms.DecodeText = sms_text;
			
            return Write(sms, taskAuto);
        }

       
        public List<SmsPhone> CreateSmsPhones(SmsMtb sms)
        {
            if (sms == null)
                throw new ArgumentNullException("sms");

            //2 записи  на основании SMS
            List<SmsPhone> smsPhones = new List<SmsPhone>();

            //номер объекта контроля
            var smsPhone = SmsPhone.CreatePrototype();
            
            smsPhone.SmsId = sms.SmsId;
            smsPhone.PhoneDir = (byte)(sms.Direction == true ? 1 : 0);

            smsPhone.Phone = sms.TelATel;
            smsPhone.SelTy = sms.TelASelTy;
            smsPhone.TelId = sms.TelASelId;
            smsPhone.PhoneGuid = Guid.NewGuid();
            
            smsPhone.PhoneId = InsertWithIdentity(smsPhone);
            smsPhones.Add(smsPhone);


            //номер собеседника
            smsPhone = SmsPhone.CreatePrototype();

            smsPhone.SmsId = sms.SmsId;
            //противоположно направлению номера объекта контроля
            smsPhone.PhoneDir = (byte)(sms.Direction == true ? 0 : 1);

            smsPhone.Phone = sms.TelBTel;
            smsPhone.SelTy = sms.TelBSelTy;
            smsPhone.TelId = sms.TelBSelId;
            smsPhone.PhoneGuid = Guid.NewGuid();

            smsPhone.PhoneId = InsertWithIdentity(smsPhone);
            smsPhones.Add(smsPhone);


            return smsPhones;
        }


        public List<SmsMtb> GetSmsLinkedToTask(TaskMtb task)
        {
            if (task == null)
                throw new ArgumentNullException("Не задан таск");


            // _mtb.TasksGrant.Where(t => t.TaskOperId == task.TaskId).Select(t => t.TaskAutoId);

            List<SmsMtb> sms_list = new List<SmsMtb>();
          
            var query =
            from tg in _mtb.TasksGrant
            join sms in _mtb.Sms
                on tg.TaskAutoId equals sms.TaskAutoId
            where tg.TaskOperId == task.TaskId 
            select sms;

            foreach ( var sms in query )
            {
                sms_list.Add(sms);
            }

            return sms_list;

            
        }
        
        #endregion


        #region talk

        public TalkMtb CreateTalkMtb(TaskAuto taskAuto, DateTime begin, DateTime end, Arc_flags arc_flag, TalkPhoneDirection dir, byte suppress_remove, byte[] sound_file_data )
        {
            if (taskAuto == null) throw new ArgumentNullException("taskAuto");

            var talk = TalkMtb.CreatePrototype();

            //дефолтный тип файла - "wsd"

            talk.TalkGuid = Guid.NewGuid();
            talk.TaskAutoId = taskAuto.TaskAutoId;
            talk.BeginDateTime = begin;
            talk.EndDateTime = end;
            //int arcFlagValue = talk.IsSounded ? 10 : 50;
            talk.ArcFlag = (byte)arc_flag;
            talk.Direction = (byte)dir;
            talk.CreatorId = 1;
            talk.SuppressRemove = suppress_remove;

            talk.TalkId = InsertWithIdentity(talk);


            if (arc_flag == Arc_flags.FileWrited)
                //разговор со звуком
            {
                //записать сэмпл файла разговора по указанному пути
                InsertFileStream(talk.TalkId, sound_file_data);
                //_cda.NewfasSetIsComplete(talk.TalkId);

            }


            return talk;
            
            
        }


        public TalkMtb CreateTalkMtb(int taskAutoId, DateTime begin, DateTime end, Arc_flags arc_flag, TalkPhoneDirection dir, byte suppress_remove, byte[] sound_file_data)
        {
            if (taskAutoId <= 0) throw new ArgumentNullException("taskAutoId");

            var talk = TalkMtb.CreatePrototype();

            //дефолтный тип файла - "wsd"

            talk.TalkGuid = Guid.NewGuid();
            talk.TaskAutoId = taskAutoId;
            talk.BeginDateTime = begin;
            talk.EndDateTime = end;
            //int arcFlagValue = talk.IsSounded ? 10 : 50;
            talk.ArcFlag = (byte)arc_flag;
            talk.Direction = (byte)dir;
            talk.CreatorId = 1;
            talk.SuppressRemove = suppress_remove;

            talk.TalkId = InsertWithIdentity(talk);


            if (arc_flag == Arc_flags.FileWrited)
            //разговор со звуком
            {
                //записать сэмпл файла разговора по указанному пути
                InsertFileStream(talk.TalkId, sound_file_data);
                //_cda.NewfasSetIsComplete(talk.TalkId);

            }


            return talk;


        }


        public TalkMtb CreateSoundedTalkMtb(TaskAuto taskAuto, DateTime begin, DateTime end, TalkPhoneDirection dir, byte suppress_remove, byte[] sound_file_data)
        {
            return CreateTalkMtb(taskAuto, begin, end, Arc_flags.FileCreated, dir, suppress_remove, sound_file_data);

        }


        public TalkMtb CreateStaticTalkMtb(TaskAuto taskAuto, DateTime begin, DateTime end, TalkPhoneDirection dir, byte suppress_remove)
        {
            return CreateTalkMtb(taskAuto, begin, end, Arc_flags.Statcontrol, dir, suppress_remove, null);

        }

        private void InsertFileStream(int talkId, byte[] fileData)
        {
            // Смотри код ниже а также содержимое хранимок
            // [MagTalksBase].[dbo].[newfas_GetContext]
            // [MagTalksBase].[dbo].[newfas_SetIsComplete]

            string path = String.Empty;

            try
            {
                path = _cda.NewfasGetContext(talkId);
                string[] dir_array = path.Split('\\');
                string dir_path = string.Join(@"\", dir_array.Take(dir_array.Length - 1));
                System.IO.Directory.CreateDirectory(dir_path);
                System.IO.File.WriteAllBytes(path, fileData);

            }
            catch (Exception ex)
            {
                throw new Exception("Не удалось записать файл разговора " + path + " " + ex.Message);
            }
        }

       

        public List<TalkPhone> CreateTalkPhones(TalkMtb talk, string phoneControlObject, string phoneInterlocutor)
        {
            if (talk == null) throw new ArgumentNullException("talk");
            
            List<TalkPhone> talkPhones = new List<TalkPhone>();

            //номер объекта контроля

            var talkPhone = TalkPhone.CreatePrototype();
            talkPhone.TalkId = talk.TalkId;
            talkPhone.Phone = phoneControlObject;
            talkPhone.PhoneTime = talk.BeginDateTime;
            talkPhone.SelTy = (byte)SEL_TY.SEL_B;
            talkPhone.TelId = (byte)TEL_ID.TEL_ID_ATS;
            
            if (talk.Direction == 1) //исходящий
            {
                //вызывающий
                talkPhone.TelTy = (byte)TEL_TY.TEL_TY_A;
            }
            else
            {
                //вызываемый
                talkPhone.TelTy = (byte)TEL_TY.TEL_TY_B;
            }

            talkPhone.PhoneDir = talk.Direction;
            talkPhone.PhoneGuid = Guid.NewGuid();
            talkPhone.PhoneId = InsertWithIdentity(talkPhone);

            talkPhones.Add(talkPhone);



            //номер собеседника

            talkPhone = TalkPhone.CreatePrototype();
            talkPhone.TalkId = talk.TalkId;
            talkPhone.Phone = phoneInterlocutor;
            talkPhone.PhoneTime = talk.BeginDateTime;
            talkPhone.SelTy = (byte)SEL_TY.SEL_A;
            talkPhone.TelId = (byte)TEL_ID.TEL_ID_ATS;

            if (talk.Direction == 1) //исходящий
            {
                //вызываемый
                talkPhone.TelTy = (byte)TEL_TY.TEL_TY_B;
            }
            else
            {
                //вызывающий
                talkPhone.TelTy = (byte)TEL_TY.TEL_TY_A;
            }

            talkPhone.PhoneDir = (byte)(1 - talk.Direction);
            talkPhone.PhoneGuid = Guid.NewGuid();
            talkPhone.PhoneId = InsertWithIdentity(talkPhone);

            talkPhones.Add(talkPhone);


            return talkPhones;
        }



        public List<object> GetTalksInfoLinkedToTask(TaskMtb task)
        {
            if (task == null)
                throw new ArgumentNullException("Не задан таск");

            // _mtb.TasksGrant.Where(t => t.TaskOperId == task.TaskId).Select(t => t.TaskAutoId);

            List<object> talk_list = new List<object>();

            var query =
            from tg in _mtb.TasksGrant
            join talk in _mtb.Talk
                on tg.TaskAutoId equals talk.TaskAutoId
            join talk_phone in _mtb.TalkPhone
                on talk.TalkId equals talk_phone.TalkId
            where tg.TaskOperId == task.TaskId
            orderby talk.BeginDateTime, talk_phone.Phone
            select new { talk.BeginDateTime, talk.EndDateTime,  talk.ArcFlag, talk_phone.Phone, talk_phone.PhoneDir, talk_phone.TelId };

            foreach (var talk_info in query)
            {
                //обрезаем с точностью до секунды, т.к. даты хранятся в MS SQL Servere (datetime values are rounded to increments of .000, .003, or .007 seconds) 
                talk_list.Add(new
                {
                    BeginDateTime = CommonGenerator.Utils.TruncateMiliseconds(talk_info.BeginDateTime),
                    EndDateTime = CommonGenerator.Utils.TruncateMiliseconds(talk_info.EndDateTime),
                    talk_info.ArcFlag,
                    talk_info.Phone,
                    talk_info.PhoneDir,
                    talk_info.TelId
                });
            }

            return talk_list;


        }



        #endregion

       
        #region dvo
        public Dvo CreateDVOMtb(TaskAuto taskAuto, TalkMtb talk, CommonGenerator.Moj.Enums.DVOCodes _dvo_code)
        {
            if (taskAuto == null) throw new ArgumentNullException("taskAuto");
            if (talk == null) throw new ArgumentNullException("talk");

            var dvo = Dvo.CreatePrototype();
            dvo.TalkId = talk.TalkId;
            dvo.TaskAutoId = taskAuto.TaskAutoId;
            dvo.Code = (byte)_dvo_code;
            dvo.CodeDvo = (byte)_dvo_code;
            dvo.InDate = talk.BeginDateTime;
            
            dvo.DvoId = InsertWithIdentity(dvo);

            return dvo;
            
        }


        public List<Dvo> GetDVOLinkedToTask(TaskMtb task)
        {
            if (task == null)
                throw new ArgumentNullException("Не задан таск");

            // _mtb.TasksGrant.Where(t => t.TaskOperId == task.TaskId).Select(t => t.TaskAutoId);

            List<Dvo> dvo_list = new List<Dvo>();

            var query =
            from tg in _mtb.TasksGrant
            join dvo in _mtb.Dvo
                on tg.TaskAutoId equals dvo.TaskAutoId
            where tg.TaskOperId == task.TaskId
            select dvo;

            foreach (var dvo_info in query)
            {
                dvo_list.Add(dvo_info);
            }

            
            return dvo_list;


        }

        #endregion
        //public TalkPhoneBuilder BuildIss()
        //{
        //    return new TalkPhoneBuilder(_mtb);
        //}

        #region Places
        //местоположение (общий случай)
        public Place CreatePlace(TaskAuto taskAuto, TalkMtb talk, int mcc, int mnc, int lac, int cl)
        {
            if (taskAuto == null) throw new ArgumentNullException("taskAuto");
            if (talk == null) throw new ArgumentNullException("talk");

            var place = Place.CreatePrototype();

            place.TaskAutoId = taskAuto.TaskAutoId;
            place.TalkId = talk.TalkId;
            place.PlaceGuid = Guid.NewGuid();
            //код страны
            place.Mcc = mcc;
            //код сети
            place.Mnc = mnc;
            //зона действия ЦКП
            place.Lac = lac;
            //номер соты
			place.Cl = cl;
            place.InDate = talk.BeginDateTime;
              
            place.PlaceId = InsertWithIdentity(place);

            return place;
        }


        public Place CreatePlace(TaskAuto taskAuto, TalkMtb talk)
        {
            return CreatePlace(taskAuto, talk, def_mcc, def_mnc, def_lac, def_cl);
        }


        public List<Place> GetPlacesByTalk(TalkMtb talk)
        {
            if (talk == null)
                throw new ArgumentNullException("Не задан разговор");
            
            List<Place> places_list = new List<Place>();

            var query =
            from place in _mtb.Place
            where place.TalkId == talk.TalkId
            select place;

            foreach (var place in query)
            {
                places_list.Add(place);
            }

            return places_list;

        }

        #endregion


        #region Video

        public Video CreateVideo(TaskAuto taskAuto,  DateTime begin, DateTime end, bool proccessed, byte supr_remove, byte[] fileData, bool createtVideoFile)
        {
            if (taskAuto == null) throw new ArgumentNullException("taskAuto");

            var video = Video.CreatePrototype();
            video.TaskAutoId = taskAuto.TaskAutoId;
            video.BeginDateTime = begin;
            video.EndDateTime = end;
            //0 - Не обработан, 1 - сделана осциллограмма

            //MAG 8.8.0.208 attribute deleted
            //video.FileProcessing = (byte)(proccessed == true ? 1 : 0);
            video.SuppressRemove = supr_remove;

            //file_type	file_ext	description
            //1	avi	Юрион видео (AVI)
            //2	mpg	МКМ-2006 видео (MPEG2)
            //3	avi	Импортированный файл (AVI)
            //4	mpg	Импортированный файл (MPG)
            //5	ts	Импортированный файл (TS)
            //6	mkv	mkv

            video.FileType = 1;

            video.VideoId = InsertWithIdentity(video);


            if (createtVideoFile)
            {
                InsertVideoStream(video.VideoId, fileData);
            }

            return video;
        }


        //TODO VideoStream

        public List<Video>  GetVideoByTask(TaskMtb task)
        {

            if (task == null)
                throw new ArgumentNullException("Не задан таск");
           

            List<Video> video_list = new List<Video>();

            var query =
            from tg in _mtb.TasksGrant
            join video in _mtb.Video
                on tg.TaskAutoId equals video.TaskAutoId
            where tg.TaskOperId == task.TaskId
            select video;

            foreach (var video in query)
            {
                video_list.Add(video);
            }

            return video_list;
        }


        #endregion

        //VideoStream

        public void InsertVideoStream(int videoId, byte[] fileData)
        {

            string path = String.Empty;

            try
            {
                path = _cda.NewfasVideoFileName(videoId);
                string[] dir_array = path.Split('\\');
                string dir_path = string.Join(@"\", dir_array.Take(dir_array.Length - 1));
                System.IO.Directory.CreateDirectory(dir_path);
                System.IO.File.WriteAllBytes(path, fileData);

            }
            catch (Exception ex)
            {
                throw new Exception("Не удалось записать файл видео " + path + " " + ex.Message);
            }



        }




        #region  Стенограммы

        public Texts8 CreateTexts8(TaskMtb taskMtb, TalkMtb talkMtb, Texts8.Texts8State state, string steno_text)
        {
            return CreateTexts8(taskMtb.TaskId, talkMtb.TalkId, Texts8.TypeSeance.Talk, state, steno_text);
        }

        public Texts8 CreateTexts8(TaskMtb taskMtb, SmsMtb smsMtb, Texts8.Texts8State state, string steno_text)
        {
            return CreateTexts8(taskMtb.TaskId, smsMtb.SmsId, Texts8.TypeSeance.SMS, state, steno_text);
        }

        public Texts8 CreateTexts8(TaskMtb taskMtb, Dvo dvo, Texts8.Texts8State state, string steno_text)
        {
            return CreateTexts8(taskMtb.TaskId, dvo.DvoId, Texts8.TypeSeance.DVO, state, steno_text);
        }

        public Texts8 CreateTexts8(TaskMtb taskMtb, Place place, Texts8.Texts8State state, string steno_text)
        {
            return CreateTexts8(taskMtb.TaskId, place.PlaceId, Texts8.TypeSeance.Place, state, steno_text);
        }

        public Texts8 CreateTexts8(TaskMtb taskMtb, Video vid, Texts8.Texts8State state, string steno_text)
        {
            return CreateTexts8(taskMtb.TaskId, vid.VideoId, Texts8.TypeSeance.Video, state, steno_text);
        }

        private Texts8 CreateTexts8(int taskId, int seanceId, Texts8.TypeSeance seanceType, Texts8.Texts8State state, string steno_text)
        {
            var st = Texts8.CreatePrototype();
            st.TaskId = taskId;
            st.SeanceId = seanceId;
            st.SeanceType = (int)seanceType;
            st.State = (byte)state;
            st.TextValue = steno_text;
            st.Id = InsertWithIdentity(st);

            return st;
        }


        public List<Texts8> GetStenogrammsByTask(TaskMtb task)
        {
            if (task == null) throw new ArgumentNullException("Не задан таск для поиска стенограмм");

            var st_list = new List<Texts8>();

            var query =
            from text8 in _mtb.Texts8
            where text8.TaskId == task.TaskId
            orderby text8.TextValue
            select text8;

            foreach (var steno in query)
            {
                st_list.Add(steno);
            }

            return st_list;

        }


        #endregion

        #region Метки
        public SmsMark CreateSmsMark(SmsMtb smsMtb, TaskMtb task, MarkDescription markDescr)
        {
            if (task == null) throw new ArgumentNullException("taskMtb");
            if (smsMtb == null) throw new ArgumentNullException("smsMtb");
            if (markDescr == null) throw new ArgumentNullException("markDescr");

            var mark = SmsMark.CreatePrototype();
            mark.SmsId = smsMtb.SmsId;
            mark.MarkDescrId = markDescr.MarkDescriptionId;
            mark.Descr = markDescr.Text;
            mark.TaskId = task.TaskId;

            mark.MarkId = InsertWithIdentity(mark);

            return mark;
        }

        public TalkMark CreateTalkMark(TalkMtb talk, TaskMtb task, MarkDescription markDescr)
        {
            if (task == null) throw new ArgumentNullException("taskMtb");
            if (talk == null) throw new ArgumentNullException("talk");
            if (markDescr == null) throw new ArgumentNullException("markDescr");

            var mark = TalkMark.CreatePrototype();
            mark.TalkId = talk.TalkId;
            mark.MarkDescrId = markDescr.MarkDescriptionId;
            mark.Descr = markDescr.Text;
            mark.TaskId = task.TaskId;

            mark.MarkId = InsertWithIdentity(mark);

            return mark;
        }

       
        public PlaceMark CreatePlaceMark(Place place, MarkDescription markDescr, TaskMtb task)
        {
            if (place == null) throw new ArgumentNullException("place");
            if (markDescr == null) throw new ArgumentNullException("markDescr");
            if (task == null) throw new ArgumentNullException("task");

            var mark = PlaceMark.CreatePrototype();

            mark.PlaceId = place.PlaceId;
            mark.MarkDescrId = markDescr.MarkDescriptionId;
            mark.Descr = markDescr.Text;
            mark.TaskId = task.TaskId;

            mark.MarkId = InsertWithIdentity(mark);

            return mark;
        }



        public VideoMark CreateVideoMark(Video video, MarkDescription markDescr, TaskMtb task)
        {
            if (video == null) throw new ArgumentNullException("video");
            if (markDescr == null) throw new ArgumentNullException("markDescr");
            if (task == null) throw new ArgumentNullException("task");

            var mark = VideoMark.CreatePrototype();

            mark.VideoId = video.VideoId;
            mark.MarkDescrId = markDescr.MarkDescriptionId;
            mark.Descr = markDescr.Text;
            mark.TaskId = task.TaskId;

            mark.MarkId = InsertWithIdentity(mark);

            return mark;
        }


        public MarkDescription GetMarkDescription(string text, string descr, bool saveTalk)
        {

            var md = MarkDescription.CreatePrototype();

            md.Text = text;
            md.Description = descr;
            md.SaveTalk = saveTalk;
            md = _cda.GetMarkDescription(md);

            var md8 = MarkDescription8.CreatePrototype();
            md8.Id = md.MarkDescriptionId;
            _cda.SetMarkDescription8(md8);

            
            //_mtb.Insert(md8);

            return md;
        }


        public List<DataGenerator.Mtb.Tables.MarkDescription> getMarksByTask(TaskMtb task)
        {

            if (task == null)
                throw new ArgumentNullException("Не задан таск");

            //List<Mark> marks_list = new List<CommonGenerator.Mtb.Tables.Mark>();

            //var query =
            //(from tm in _mtb.TalkMarks
            //where tm.TaskId == task.TaskId
            //select tm.MarkDescrId)
            //.Union
            //(from sm in _mtb.SmsMarks
            // where sm.TaskId == task.TaskId
            // select sm.MarkDescrId)
            // .Union
            //(from pm in _mtb.PlaceMarks
            // where pm.TaskId == task.TaskId
            // select pm.MarkDescrId)
            // .Union
            //(from vm in _mtb.VideoMarks
            // where vm.TaskId == task.TaskId
            // select vm.MarkDescrId);

            //foreach (var mark in query)
            //{
            //    video_list.Add(video);
            //}

            List<DataGenerator.Mtb.Tables.MarkDescription> marks_list = _cda.GetDescrByTask(task);
            
            return marks_list;
        }


        public void DeleteMarksByTask(TaskMtb task)
        {
            if (task == null)
                throw new ArgumentNullException("Не задан таск");

            try
            {
                _mtb.BeginTransaction();
                _mtb.TalkMarks.Delete(tm => tm.TaskId == task.TaskId);
                _mtb.PlaceMarks.Delete(tm => tm.TaskId == task.TaskId);
                _mtb.VideoMarks.Delete(tm => tm.TaskId == task.TaskId);
                _mtb.SmsMarks.Delete(tm => tm.TaskId == task.TaskId);
                _mtb.CommitTransaction();
                
            }
            catch
            {
                _mtb.RollbackTransaction();
                throw new Exception("Не удалось удалить метки, связанные с таском "+ task.ObjShifr);
            }
            
            
        }


        #endregion


        #region Reports
        public Report CreateReport(TaskMtb task, string rep_number, DateTime rep_date, DateTime? print_date, DateTime? DeliveryDate, bool SeancesDeleted)
        {
            //в таблице dt_rim_Reports еще в 2013 году была закоменчена логика обновления полей SeanceDateStart и SeanceDateEnd,
            //см. sp rim_SeanceIncludedReportSet
            //поэтому не рассчитываем эти даты, а тупо используем ХП

            Report report = null;

            
            if ((int)_mag_version >= 87)
            {
                Report87 report87 = Report87.CreateProtorype();
                report87.DeliveryDate = DeliveryDate;
                report87.SeancesStatusId = SeancesDeleted == true ? 1 : 0;
                //не указываем, будем обновлять на основании включенных сеансов
                //report87.IntervalDateStart =
                //report87.IntervalDateEnd =
                report = report87;
            }
            else
            {
                Report86 report86 = Report86.CreateProtorype();
                report = report86;
            }


            report.TaskId = task.TaskId;
            report.ReportNumber = rep_number;
            report.ReportDate = rep_date;
            report.PrintTime = print_date;
          
            //не указываем, будем обновлять на основании включенных сеансов
            //report.Detail = "31 разговор, 5 СМС",

            //можно было бы использовать и просто InsertWithIdentity, выигрыш от использования ХП только в том,
            //что есть валидация номера сводки, можно использовать оба объекта для передачи в параметры ХП, указываются дефолтные значения для нек. полей 
            //report.ReportId = InsertWithIdentity(report);
            _cda.SetRimReport(report);

            if ((int)_mag_version >= 87)
            {
                _cda.SetRimReportDeliveryDate(report);
            }
               

            return report;
        }

        //включить в сводку сеансы, относящиеся к таску, по определенному алгоритму
        public void SetReportSeancesForReport2(Report report, ReportSeancesAlgoritm alg)
        {
            //получим все сеансы по таску
            //можно было бы использовать  [rim_SeanceForReportGet], но он возвращает много излишней доп. информации
            
            if (report == null)
                throw new ArgumentNullException("Не задана сводка");
            if (report.TaskId == null)
                throw new ArgumentNullException("Не задан таск, по которому фрмируется сводка ");


            var seances_type_ids_builder = new StringBuilder();
            var seances_ids_builder = new StringBuilder();


            var earlier_incl_seances =
                   from rs in _mtb.ReportSeances
                   join rep in _mtb.Reports
                       on rs.ReportId equals rep.ReportId
                   where rep.TaskId == report.TaskId && rep.ReportId != report.ReportId
                   select rs;


            var query_talks =
                from tg in _mtb.TasksGrant
                join talk in _mtb.Talk
                    on tg.TaskAutoId equals talk.TaskAutoId 
                where tg.TaskOperId == report.TaskId && talk.BeginDateTime >= tg.AutoStart && (tg.AutoEnd == null || talk.BeginDateTime <= tg.AutoEnd )
                select new { talk.TalkId, talk.BeginDateTime, talk.EndDateTime };

           var query_sms =
               from tg in _mtb.TasksGrant
               join sms in _mtb.Sms
                   on tg.TaskAutoId equals sms.TaskAutoId
               where tg.TaskOperId == report.TaskId && sms.SendDateTime >= tg.AutoStart && (tg.AutoEnd == null || sms.SendDateTime <= tg.AutoEnd)
               select new { sms.SmsId, sms.RecvDateTime };

            var query_place =
                from tg in _mtb.TasksGrant
                join place in _mtb.Place
                    on tg.TaskAutoId equals place.TaskAutoId
                where tg.TaskOperId == report.TaskId && place.InDate >= tg.AutoStart && (tg.AutoEnd == null || place.InDate <= tg.AutoEnd)
                select new { place.PlaceId,  place.InDate };

            var query_video =
                from tg in _mtb.TasksGrant
                join video in _mtb.Video
                    on tg.TaskAutoId equals video.TaskAutoId
                where tg.TaskOperId == report.TaskId && video.BeginDateTime >= tg.AutoStart && (tg.AutoEnd == null || video.BeginDateTime <= tg.AutoEnd)
                select new { video.VideoId };


            if (alg == ReportSeancesAlgoritm.RandomExcludeEarlierSeances)
            {
                query_talks =
                from talk_info in query_talks
                where !(from seance in earlier_incl_seances
                        where seance.SeanceTypeId == (int)Texts8.TypeSeance.Talk
                        select seance.SeanceId).Contains(talk_info.TalkId)
                select new { talk_info.TalkId, talk_info.BeginDateTime, talk_info.EndDateTime };

                query_sms =
                from sms_info in query_sms
                where !(from seance in earlier_incl_seances
                        where seance.SeanceTypeId == (int)Texts8.TypeSeance.SMS
                        select seance.SeanceId).Contains(sms_info.SmsId)
                select new { sms_info.SmsId,  sms_info.RecvDateTime };

                query_place =
                from place_info in query_place
                where !(from seance in earlier_incl_seances
                        where seance.SeanceTypeId == (int)Texts8.TypeSeance.Place
                        select seance.SeanceId).Contains(place_info.PlaceId)
                select new { place_info.PlaceId,  place_info.InDate };


                query_video =
                from video_info in query_video
                where !(from seance in earlier_incl_seances
                        where seance.SeanceTypeId == (int)Texts8.TypeSeance.Video
                        select seance.SeanceId).Contains(video_info.VideoId)
                select new { video_info.VideoId  };
            }

            
            foreach (var talk_info in query_talks)
            {
                seances_type_ids_builder.Append((int)Texts8.TypeSeance.Talk).Append(";");
                seances_ids_builder.Append(talk_info.TalkId).Append(";");
            }

            
            foreach (var sms_info in query_sms)
            {
                seances_type_ids_builder.Append((int)Texts8.TypeSeance.SMS).Append(";");
                seances_ids_builder.Append(sms_info.SmsId).Append(";");
            }
            
            foreach (var p_info in query_place)
            {
                seances_type_ids_builder.Append((int)Texts8.TypeSeance.Place).Append(";");
                seances_ids_builder.Append(p_info.PlaceId).Append(";");
            }
            
            foreach (var video_info in query_video)
            {
                seances_type_ids_builder.Append((int)Texts8.TypeSeance.Video).Append(";");
                seances_ids_builder.Append(video_info.VideoId).Append(";");
            }

            
            _cda.SetRimReportSeances(report.ReportId, seances_type_ids_builder.ToString(), seances_ids_builder.ToString());


            //TODO установить report.IntervalDateStart, report.IntervalDateEnd


        }



        //включить в сводку сеансы, относящиеся к таску, по определенному алгоритму
        public void SetReportSeancesForReport(Report report, ReportSeancesAlgoritm alg)
        {
            //получим все сеансы по таску
            //можно было бы использовать  [rim_SeanceForReportGet], но он возвращает много излишней доп. информации

            if (report == null)
                throw new ArgumentNullException("Не задана сводка");
            if (report.TaskId == null)
                throw new ArgumentNullException("Не задан таск, по которому фрмируется сводка ");
            if ((int)_mag_version >= 87 && !(report is Report87))
            {
                throw new Exception("Передана версия сводки ниже, чем есть в МАГ 8.7 ");
            }

            var seances_type_ids_builder = new StringBuilder();
            var seances_ids_builder = new StringBuilder();


            //List<TalkMtb> talks_list = new List<TalkMtb>();
            //List<SmsMtb> sms_list = new List<SmsMtb>();
            //List<Video> video_list = new List<Video>();
            //List<Place> place_list = new List<Place>();

            //
            var talks_list = alg == ReportSeancesAlgoritm.RandomExcludeEarlierSeances ? _cda.GetTalkByTaskExceptEarlier((int)report.TaskId) : _cda.GetTalkByTask((int)report.TaskId);
            var sms_list = alg == ReportSeancesAlgoritm.RandomExcludeEarlierSeances ? _cda.GetSmsByTaskExceptEarlier((int)report.TaskId) : _cda.GetSmsByTask((int)report.TaskId);
            var video_list = alg == ReportSeancesAlgoritm.RandomExcludeEarlierSeances ? _cda.GetVideoByTaskExceptEarlier((int)report.TaskId) : _cda.GetVideoByTask((int)report.TaskId);
            var place_list = alg == ReportSeancesAlgoritm.RandomExcludeEarlierSeances ? _cda.GetPlaceByTaskExceptEarlier((int)report.TaskId) : _cda.GetPlaceByTask((int)report.TaskId);


            //var _rand = new Random();
            var seances_list = new List<List<int>>();
          
            if (alg == ReportSeancesAlgoritm.RandomExcludeEarlierSeances || alg == ReportSeancesAlgoritm.RandomIncludeEarlierSeances)
            {
                talks_list = Utils.RandomListEntries(talks_list);
                sms_list = Utils.RandomListEntries(sms_list);
                video_list = Utils.RandomListEntries(video_list);
                place_list = Utils.RandomListEntries(place_list);
                
            }

            List<DateTime> mins = new List<DateTime>();
            List<DateTime> maxs = new List<DateTime>();

            if (talks_list.Count > 0)
            {
                mins.Add(talks_list.Select(tl => tl.BeginDateTime).Min());
                maxs.Add(talks_list.Select(tl => tl.EndDateTime).Max());
            }
            if (sms_list.Count > 0)
            {
                mins.Add(sms_list.Select(tl => tl.RecvDateTime).Min());
                maxs.Add(sms_list.Select(tl => tl.RecvDateTime).Max());
            }
            if (place_list.Count > 0)
            {
                mins.Add(place_list.Select(tl => tl.InDate).Min());
                maxs.Add(place_list.Select(tl => tl.InDate).Max());
            }
                
           
            seances_list.AddRange(talks_list.Select(x => new List<int>() { x.TalkId, (int)Texts8.TypeSeance.Talk }).ToList());
            seances_list.AddRange(sms_list.Select(x => new List<int>() { x.SmsId, (int)Texts8.TypeSeance.SMS }).ToList());
            seances_list.AddRange(video_list.Select(x => new List<int>() { x.VideoId, (int)Texts8.TypeSeance.Video }).ToList());
            seances_list.AddRange(place_list.Select(x => new List<int>() { x.PlaceId, (int)Texts8.TypeSeance.Place }).ToList());

            foreach (var seance_info in seances_list)
            {
                seances_type_ids_builder.Append(seance_info[1]).Append(";");
                seances_ids_builder.Append(seance_info[0]).Append(";");
            }
            
            
            _cda.SetRimReportSeances(report.ReportId, seances_type_ids_builder.ToString(), seances_ids_builder.ToString());


            //обновить поля самой сводки

            XDocument xdoc = new XDocument(new XElement("Detail", new XElement("SeancesDetail",
                        new XElement("SeanceByType",
                            new XElement("SeanceType", "Talk"),
                            new XElement("Count", talks_list.Count)),
                        new XElement("SeanceByType",
                            new XElement("SeanceType", "Sms"),
                            new XElement("Count", sms_list.Count)),
                        new XElement("SeanceByType",
                            new XElement("SeanceType", "Place"),
                            new XElement("Count", place_list.Count)),
                        new XElement("SeanceByType",
                            new XElement("SeanceType", "Video"),
                            new XElement("Count", video_list.Count))
                            )));


            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            using (System.IO.TextWriter writer = new System.IO.StringWriter(builder))
            {
                xdoc.Save(writer);
            }

            report.Detail = builder.ToString();

            if ((int)_mag_version >= 87)
            {
                Report87 report87 = report as Report87;
                report87.IntervalDateStart = mins.Count > 0 ? (DateTime?)mins.Min() : null;
                report87.IntervalDateEnd = maxs.Count > 0 ? (DateTime?)maxs.Max() : null;
                report = report87;
                
            }


            _cda.UpdateRimReport(report);

            //TODO установить report.IntervalDateStart, report.IntervalDateEnd


        }


        public void UpdateRimReport(Report report)
        {
            _cda.UpdateRimReport(report);
        }



        public ReportSeance SetReportSeances(int seanceId, int reportId, Texts8.TypeSeance seanceType)
        {
            var reportSeance = new ReportSeance
            {
                ReportId = reportId,
                SeanceId = seanceId,
                SeanceTypeId = (int)seanceType
            };

            _mtb.Insert(reportSeance);

            return reportSeance;
        }
        #endregion



        #region Users


        private Dictionary<string, int> RoleToIdMapping()
        {
            return new Dictionary<string, int>
            {
                { "Mag.TaskDistributor.AccessToApplication", 2 },
                { "Mag.Quintessence.AccessToApplication",3 },
                { "Mag.Quintessence.RemoveAutotask",4 },
                { "Signatec.Mag.Playback.ViewAllTasks",8 },
                { "Signatec.Mag.Playback.AccessChangeSeance",9 },
                { "Signatec.Mag.Playback.AccessChangeMarkDescriptions",10 },
                { "Mag.UsersManager.CreateUser",15 },
                { "Mag.UsersManager.EditUser",15 },
                { "Mag.UsersManager.DeleteUser",15 },
                { "Mag.RIM.ComposeInformationalArchive",22},
                { "Signatec.Mag.Playback.Common.AccessToFaxApplication",23},
                { "Mag.Logger.AccessToApplication",24},
                { "Signatec.Mag.Playback.AccessEditPlaceDictionary",26},
                { "Signatec.Mag.Playback.ViewPlacesOnMap",28},
                { "Signatec.Mag.Playback.ViewAllTaskSeances",50},
                { "Signatec.Mag.Playback.Common.AccessToServiceApplication",101},
                {"Signatec.Mag.Playback.Common.AccessToEditCodes",102},
                { "Signatec.Mag.Playback.Common.AccessToReturnArchive",107},
                { "Mag.RIM.AccessToApplication",109},
                { "Signatec.Mag.Playback.Common.AccessToUmoAdminApplication",110},
                { "Signatec.Mag.Playback.AllowViewVideo3GCall",111},
                { "Signatec.Mag.Playback.AccessToApplication",113},
                { "Signatec.Mag.Abonents.AccessToApplication",115},
                { "Signatec.Mag.Playback.AccessToBiya",116},
                {  "Signatec.Mag.Playback.AllowConfigureSpeechRecognition",117},
                { "Signatec.Mag.Playback.AllowSendForSpeakerRecognition",118},
                { "Signatec.Mag.Playback.AllowViewSpeakerRecognitionInfo",119}
            };
        }



        public void SetUser(string username, string pass, bool disabled, IEnumerable<string> rights, bool isMinSecLevel = true)
        {

            //CreateOrUpdateLogin(username, pass);
            //CreateOrUpdateUser(username);
            _cda.SetDBLogin(username, pass);
            _cda.SetMTBUser(username);
            _cda.AddMagRole(username);


            var all_right_set = String.Join(";",
                RoleToIdMapping().Select(kv_pair => kv_pair.Value));


            var rght_id = 0;
            var right_sets = new List<string>();

            foreach (var rght in rights)
            {
                if (RoleToIdMapping().TryGetValue(rght, out rght_id))
                {
                    right_sets.Add(rght_id.ToString());
                }
                //else
                //    throw new Exception("Failed to find default role name " + rght);
            }
            
            
            try
            {
                _cda.CreateOrUpdateMtbUser(username, username, "", disabled, null, isMinSecLevel, all_right_set, String.Join(";", right_sets));
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при обновлении данных о пользователе " + username + "в MTB " + ex.Message);
            }

        }


        private string PasswordToDbHash(string password)
        {
            //var pass = Convert.FromBase64String(password);
            var pass = Encoding.ASCII.GetBytes(password);
            var passwordStr = 0 < pass.Length
                                  ? "0x" + BitConverter.ToString(pass).Replace("-", string.Empty)
                                  : "0x0";
            return passwordStr;
        }


        /// <summary>
        /// Создать логин уровня сервера БД 
        public void CreateOrUpdateLogin(string login, string pass)
        {

            //var cmd = _mtb.Command;
            //cmd.CommandText = string.Format("USE [master]");
            //cmd.ExecuteNonQuery();
            //cmd.CommandText = string.Format(" IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'{0}')" +
            //                                " BEGIN" +
            //                                " ALTER LOGIN [{0}] WITH PASSWORD='{1}', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF" +
            //                                " END" +
            //                                " ELSE" +
            //                                " BEGIN" +
            //                                " CREATE LOGIN [{0}] WITH PASSWORD='{1}', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF" +
            //                                " END"
            //    , login, pass);
            //cmd.ExecuteNonQuery();

            try
            {
                _mcda.SetDBLogin(login, pass, "MagTalksBase");
            }
            catch (Exception ex)
            {
                throw new Exception("Error " + ex.Message);
            }
            

        }

        /// <summary>
        /// Создать пользователя уровня БД
        public void CreateOrUpdateUser(string login)
        {
            //var cmd = _mtb.Command;
            //cmd.CommandText = string.Format("USE [{0}]", "MagTalksBase");
            //cmd.ExecuteNonQuery();
            //cmd.CommandText = string.Format(" IF NOT EXISTS (SELECT * FROM dbo.sysusers where name = '{0}')" +
            //                                " BEGIN" +
            //                                " CREATE USER [{0}] FOR LOGIN [{0}]" +
            //                                " END", login);
            //cmd.ExecuteNonQuery();
            //cmd.CommandText = string.Format("EXEC sp_addrolemember N'db_datareader', N'{0}'", login);
            //cmd.ExecuteNonQuery();
            //cmd.CommandText = string.Format("EXEC sp_addrolemember N'db_datawriter', N'{0}'", login);
            //cmd.ExecuteNonQuery();
            ////if (isMag8)
            ////{
            ////    cmd.CommandText = string.Format("EXEC sp_addrolemember N'mag8_role', N'{0}'", login);
            ////    cmd.ExecuteNonQuery();
            ////}
            ////if (isMag)
            ////{
            //    cmd.CommandText = string.Format("EXEC sp_addrolemember N'mag_role', N'{0}'", login);
            //    cmd.ExecuteNonQuery();
            ////}

            try
            {
                _mcda.SetMTBUser(login, "MagTalksBase");
            }
            catch(Exception ex)
            {
                throw new Exception("Error " + ex.Message);
            }
            
        }


        #endregion



        #region ClenUp

        public void UpdateDefaultUserOption(string option, string value)
        {
            try
            {
                _cda.SetUserOption(option, value);
            }
            catch (Exception ex)
            {
                throw new Exception("Не удалось записать дефолтную настройку для юзеров " + ex.Message);
            }

        }


        public string GetDefaultUserOption(string option)
        {
            try
            {
                return _cda.GetUserOption(option);
            }
            catch (Exception ex)
            {
                throw new Exception("Не удалось прочитать дефолтную настройку для юзеров " + ex.Message);
            }

        }


        public void InvokeCleanup()
        {
            try
            {
                _cleanup_da.CleanupInvoke();
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка расчистки " + ex.Message);
            }

        }



        public List<FasWorkDelete> GetFasWorkDelete()
        {
           return  _mtb.FasWorkDelete.Select(table_str => table_str).ToList();

        }


        #endregion

        private int InsertWithIdentity<T>(T tableRowDto)
        {
            return Convert.ToInt32(_mtb.InsertWithIdentity(tableRowDto));
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
                _mtb.Dispose();
        }

        ~MtbDataBuilder()
        {
            Dispose(false);
        }
    }
}