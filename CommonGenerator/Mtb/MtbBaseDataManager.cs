﻿using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.Data.Linq;
using DataGenerator.Mtb.Tables;
using Microsoft.Xrm.Client;
using CommonGenerator.Mtb;


namespace DataGenerator.Mtb
{
    public class MtbDbManager : DbManager
    {
        private const string ConnectionStrPattern = "Data Source={0};Initial Catalog={1};User ID={2};Password={3}";

        public MtbDbManager(DataProviderBase dataProvider, MtbSettings settings)
            : base(
                dataProvider,
                ConnectionStrPattern.FormatWith(settings.Address, settings.Database, settings.Login, settings.Password))
        {
        }

        public Table<DefaultParam> DefaultParams
        {
            get { return GetTable<DefaultParam>(); }
        }

        public Table<UserOption> UserOptions
        {
            get { return GetTable<UserOption>(); }
        }

        public Table<TaskMtb> Tasks
        {
            get { return GetTable<TaskMtb>(); }
        }

        public Table<TaskArcCompleted> TasksArcCompleted
        {
            get { return GetTable<TaskArcCompleted>(); }
        }

        public Table<TaskAuto> TasksAuto
        {
            get { return GetTable<TaskAuto>(); }
        }

        public Table<TaskGrant> TasksGrant
        {
            get { return GetTable<TaskGrant>(); }
        }

        public Table<TalkMtb> Talk
        {
            get { return GetTable<TalkMtb>(); }
        }


        public Table<TalksStream> TalksStreams
        {
            get { return GetTable<TalksStream>(); }
        }

        public Table<TalkPhone> TalkPhone
        {
            get { return GetTable<TalkPhone>(); }
        }

        public Table<Texts8> Texts8
        {
            get { return GetTable<Texts8>(); }
        }


        public Table<Texts7> Texts7
        {
            get { return GetTable<Texts7>(); }
        }

        public Table<TalkPhase> TalkPhase
        {
            get { return GetTable<TalkPhase>(); }
        }

        public Table<TalkMark> TalkMarks
        {
            get { return GetTable<TalkMark>(); }
        }

        public Table<Video> Video
        {
            get { return GetTable<Video>(); }
        }

        public Table<VideoExt> VideoExt
        {
            get { return GetTable<VideoExt>(); }
        }

        public Table<VideoMark> VideoMarks
        {
            get { return GetTable<VideoMark>(); }
        }

        public Table<SmsMtb> Sms
        {
            get { return GetTable<SmsMtb>(); }
        }

        public Table<SmsMark> SmsMarks
        {
            get { return GetTable<SmsMark>(); }
        }

        public Table<SmsPhone> SmsPhones
        {
            get { return GetTable<SmsPhone>(); }
        }

        public Table<Place> Place
        {
            get { return GetTable<Place>(); }
        }

        public Table<PlaceMark> PlaceMarks
        {
            get { return GetTable<PlaceMark>(); }
        }

        public Table<Dvo> Dvo
        {
            get { return GetTable<Dvo>(); }
        }

        public Table<MarkDescription> MarkDescriptions
        {
            get { return GetTable<MarkDescription>(); }
        }

        public Table<MarkDescription8> MarkDescriptions8
        {
            get { return GetTable<MarkDescription8>(); }
        }

        public Table<PhaseDescription> PhaseDescriptions
        {
            get { return GetTable<PhaseDescription>(); }
        }

        public Table<Report> Reports
        {
            get { return GetTable<Report>(); }
        }

        public Table<ReportSeance> ReportSeances
        {
            get { return GetTable<ReportSeance>(); }
        }

        public Table<MarkCategoryRelation> MarkCategoryRelations
        {
            get { return GetTable<MarkCategoryRelation>(); }
        }

        public Table<TaskPacs> TaskPacs
        {
            get { return GetTable<TaskPacs>(); }
        }

        public Table<TaskCipherParts> TaskCipherParts
        {
            get { return GetTable<TaskCipherParts>(); }
        }


        public Table<FasWorkDelete> FasWorkDelete
        {
            get { return GetTable<FasWorkDelete>(); }
        }


    }
}
