﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("mr_pb_MarksDescr8")]
	public class MarkDescription8
	{
		[MapField("id"), PrimaryKey]
		public int Id { get; set; }
	  
		[MapField("is_complete")]
		public bool IsComplite { get; set; }

		public static MarkDescription8 CreatePrototype()
		{
			return new MarkDescription8
			{
				Id = -1,
				IsComplite = true
			};
		}
	}
}
