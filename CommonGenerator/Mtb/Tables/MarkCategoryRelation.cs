﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace DataGenerator.Mtb.Tables
{
    [TableName("mr_MarkCategoryRelation")]
    public class MarkCategoryRelation
    {
        /// <summary>
		/// Идентификатор 
		/// </summary>
		[MapField("relation_id"), PrimaryKey, Identity]
        public int RelationId { get; set; }


        /// <summary>
		/// Id Описания метки (ссылка на полное описание метки)
		/// </summary>
		[MapField("marks_descr_id")]
        public int MarksDescriptionId { get; set; }


        /// <summary>
        /// Id категории метки (ссылка на категорию метки)
        /// </summary>
        [MapField("mark_category_id")]
        public int MarkCategoryId { get; set; }


    }
}
