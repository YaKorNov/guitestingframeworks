﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using CommonGenerator.Mtb.Tables;


namespace DataGenerator.Mtb.Tables
{
	[TableName("mr_Marks_sms")]
	public class SmsMark : Mark
	{
		[MapField("mark_id"), PrimaryKey, Identity]
		public override int MarkId { get; set; }

		[MapField("sms_id")]
		public int SmsId { get; set; }

		[MapField("task_id")]
		public int? TaskId { get; set; }

		[MapField("mark_descr_id")]
		public override int MarkDescrId { get; set; }

		[MapField("descr")]
		public override string Descr { get; set; }

		[MapField("user_id")]
		public int? UserId { get; set; }

		[MapField("ins_date")]
		public DateTime InsDate { get; set; }

		[MapField("mark_guid")]
		public Guid MarkGuid { get; set; }

		[MapField("proxy_user_id")]
		public int? ProxyUserId { get; set; }

		public static SmsMark CreatePrototype()
		{
			return new SmsMark
			{
				MarkId = -1,
				SmsId = -1,
				MarkDescrId = -1,
				MarkGuid = Guid.NewGuid(),
				TaskId = null,
				UserId = 1,
				Descr = "Описание метки",
				InsDate = DateTime.Now,
				ProxyUserId = 1,
			};
		}
	}
}
