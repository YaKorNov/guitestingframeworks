﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("dt_Talks")]
	public class TalkMtb
	{
		[MapField("talk_id"), PrimaryKey, Identity]
		public int TalkId { get; set; }

		[MapField("task_auto_id")]
		public int TaskAutoId { get; set; }

		[MapField("tbegin")]
		public DateTime BeginDateTime { get; set; }

		[MapField("tend")]
		public DateTime EndDateTime { get; set; }

		[MapField("direction")]
		public byte Direction { get; set; }

		[MapField("ring_count")]
		public byte RingCount { get; set; }

		[MapField("creator_id")]
		public byte CreatorId { get; set; }

		[MapField("file_type")]
		public string FileType { get; set; }

		[MapField("file_name")]
		public int? FileName { get; set; }

		[MapField("arc_flag")]
		public byte ArcFlag { get; set; }

		[MapField("arc_cd")]
		public byte? ArcCd { get; set; }

		[MapField("file_size")]
		public int FileSize { get; set; }

		[MapField("sorm_type")]
		public short SormType { get; set; }

		[MapField("sorm_disconnectCode")]
		public short SormDisconnectCode { get; set; }

		[MapField("fax")]
		public bool Fax { get; set; }

		[MapField("data_type")]
		public int DataType { get; set; }

		[MapField("bad")]
		public bool Bad { get; set; }

		[MapField("talk_guid")]
		public Guid TalkGuid { get; set; }

		[MapField("has_dvo")]
		public bool HasDvo { get; set; }

		[MapField("fax_proc_result")]
		public byte? FaxProcRes { get; set; }

		[MapField("ext_info_int")]
		public int? ExtInfoInt { get; set; }

		[MapField("ext_info_str")]
		public string ExtInfoStr { get; set; }

		[MapField("selection_criterion")]
		public byte? SelectionCriterion { get; set; }

        [MapField("suppress_remove")]
        public byte? SuppressRemove { get; set; }

        public static TalkMtb CreatePrototype()
		{
			return new TalkMtb
			{
				TalkId = -1,
				TaskAutoId = 1,
				Direction = 2,
				RingCount = 0,
                //TODO пока ставим 1, логично, что здесь должен быть реальный id usera
				CreatorId = 1,
				FileType = "wsd",
				FileName = null,
				ArcFlag = 10,
				ArcCd = 40,
				FileSize = 177336,
				SormType = 31,
				SormDisconnectCode = 1,
				Fax = false,
				DataType = 0,
				Bad = false,
				TalkGuid = Guid.Empty,
				HasDvo = false,
				FaxProcRes = 0,
				ExtInfoInt = 0,
				ExtInfoStr = null,
				SelectionCriterion = null,
                //Запрет на удаление. Движение в сторону отказа от архивации МАГ.
                SuppressRemove = 0
            };
		}
	}
}
