﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace DataGenerator.Mtb.Tables
{
	[TableName("dt_Tasks")]
	public class TaskMtb
	{
		[MapField("task_id"), PrimaryKey, Identity]
		public int TaskId { get; set; }

		[MapField("obj_shifr")]
		public string ObjShifr { get; set; }

		[MapField("obj_name")]
		public string ObjName { get; set; }

		[MapField("category_id")]
		public int CategoryId { get; set; }

		[MapField("ctrl_allowed")]
		public int CtrlAllowed { get; set; }

        [JsonIgnore]
        [MapField("sanction_end")]
		public DateTime? SanctionEnd { get; set; }

        [JsonIgnore]
        [MapField("sanction_begin")]
		public DateTime? SanctionBegin { get; set; }

        [JsonIgnore]
        [MapField("Arc_TypeDel")]
		public byte? ArcTypeDel { get; set; }

        [JsonIgnore]
        [MapField("Arc_Plus")]
		public byte? ArcPlus { get; set; }

        [JsonIgnore]
        [MapField("Arc_Minus")]
		public byte? ArcMinus { get; set; }

        [JsonIgnore]
        [MapField("Arc_NotMark")]
		public byte? ArcNotMark { get; set; }

        [JsonIgnore]
        [MapField("Arc_KeepPlus")]
		public int? ArcKeepPlus { get; set; }

        [JsonIgnore]
        [MapField("Arc_KeepMinus")]
		public int? ArcKeepMinus { get; set; }

        [JsonIgnore]
        [MapField("Arc_KeepNotMark")]
		public int? ArcKeepNotMark { get; set; }

		[MapField("initiator")]
		public string Initiator { get; set; }

		[MapField("orientation")]
		public string Orientation { get; set; }

		[MapField("goal")]
		public string Goal { get; set; }

        [JsonIgnore]
        [MapField("comments")]
		public string Comments { get; set; }

		[MapField("task_begin")]
		public DateTime? TaskBegin { get; set; }

		[MapField("task_srok")]
		public int TaskSrok { get; set; }

		[MapField("task_guid")]
		public Guid TaskGuid { get; set; }

		[MapField("data_type")]
		public int DataType { get; set; }

		[MapField("security_id")]
		public int SecurityId { get; set; }

		[MapField("task_type")]
		public int TaskType { get; set; }

        [JsonIgnore]
        [MapField("RestrictExport")]
		public bool RestrictExport { get; set; }

		public static TaskMtb CreatePrototype()
		{
			return new TaskMtb
			{
				TaskId = -1,
				ObjShifr = "1РАЗГ-10-001-2015",
				ObjName = "1РАЗГ-10-001-2015",
				CategoryId = 5,
                //Разрешить контроль задания(1), при сбросе гранты очищаются триггером
                CtrlAllowed = 1,
				SanctionEnd = null,
				SanctionBegin = null,
				ArcTypeDel = 1,
				ArcPlus = 1,
				ArcMinus = 1,
				ArcNotMark = 1,
				ArcKeepPlus = 30,
				ArcKeepMinus = 20,
				ArcKeepNotMark = 10,
				Initiator = null,
				Orientation = null,
				Goal = null,
				Comments = null,
				TaskBegin = null,
				TaskSrok = 30,
				TaskGuid = new Guid("f8de33a4-fab7-45bb-9296-000c5b182ffb"),
				DataType = 0,
				SecurityId = 3,
				TaskType = 1,
				RestrictExport = false
			};
		}


        public static Dictionary<int, string> Categories = new Dictionary<int, string>()
        {
            { 2 , "Очередное"},
            { 3 , "Исполняемое"},
            { 4 , "Приостановлено"},
            { 5 , "Завершено"},
            { 6 , "Не выполнено"},

        };

        public static Dictionary<int, string> DataTypes = new Dictionary<int, string>()
        {
            { 0 , "unsigned"},
            { 1 , "talk"},
            { 2 , "fax"},
            { 3 , "modem"},
            { 4 , "video"},

        };

        
    }


}
