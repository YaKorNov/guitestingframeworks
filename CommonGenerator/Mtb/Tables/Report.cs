﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System.ComponentModel;



namespace DataGenerator.Mtb.Tables
{

    public enum ReportType
    {
        [Description("Сводка")]
        Report = 1,
        [Description("Информационный архив")]
        Archive =2
    }


    [TableName("dt_rim_Reports")]
    public class Report86 : Report
    {
        /// <summary>
        /// Идентификатор 
        /// </summary>
        [MapField("ReportId"), PrimaryKey, Identity]
        public override int ReportId { get; set; }

        /// <summary>
        /// ГУИД
        /// </summary>
        [MapField("ReportGUID")]
        public override Guid ReportGuid { get; set; }

        /// <summary>
        /// ГУИД задания
        /// </summary>
        [MapField("TaskGUID")]
        public override Guid? TaskGuid { get; set; }

        /// <summary>
        /// id задания
        /// </summary>
        [MapField("TaskID")]
        public override int? TaskId { get; set; }


        /// <summary>
        /// Дата сводки. без времени
        /// </summary>
        [MapField("RptDate")]
        public override DateTime ReportDate { get; set; }

        /// <summary>
        /// Номер сводки
        /// </summary>
        [MapField("RptNum")]
        public override string ReportNumber { get; set; }

        /// <summary>
        /// Дата начала отбора сеансов связи
        /// </summary>
        [MapField("SeanceDateStart")]
        public override DateTime? ReportBegin { get; set; }

        /// <summary>
        /// Дата окончания отбора сеансов связи
        /// </summary>
        [MapField("SeanceDateEnd")]
        public override DateTime? ReportEnd { get; set; }

        /// <summary>
        /// Дата и время печати сводки
        /// </summary>
        [MapField("PrintedDate")]
        public override DateTime? PrintTime { get; set; }

        /// <summary>
        /// Признак что сводка была перепечатана
        /// </summary>
        [MapField("IsPrinted")]
        public override bool IsPrinted { get; set; }

        /// <summary>
        /// Количество  вошедших в сводку сеансов
        /// </summary>
        [MapField("SeanceCount")]
        public override int SeanceCount { get; set; }

        /// <summary>
        /// Количество  страниц сводки
        /// </summary>
        [MapField("PageCount")]
        public override int PageCount { get; set; }

        /// <summary>
        /// Число отправки на печать
        /// </summary>
        [MapField("PrintedCount")]
        public override int PrintedCount { get; set; }

        /// <summary>
        /// Краткое описание содержимого архива (например: 31 разговор, 5 СМС)
        /// </summary>
        [MapField("Detail")]
        public override string Detail { get; set; }

        /// <summary>
        /// Признак что сводка была перепечатана
        /// </summary>
        [MapField("IsReprinted")]
        public override bool IsReprinted { get; set; }

        /// <summary>
        /// тип 
        /// </summary>
        [MapField("ReportDocumentTypeId")]
        public override int ReportDocumentTypeId { get; set; }


        [MapField("CriteriaType")]
        public override int CriteriaType { get; set; }

        [MapField("UserIdPrinted")]
        public override int? UserIdPrinted { get; set; }



        public static Report86 CreateProtorype()
        {
            return new Report86
            {
                ReportGuid = Guid.NewGuid(),
                TaskGuid = null,
                TaskId = -1,
                ReportDate = new DateTime(2015, 9, 25),
                ReportNumber = "5",
                ReportBegin = null,
                ReportEnd = null,
                PrintTime = new DateTime(2015, 9, 25),
                IsPrinted = true,
                SeanceCount = 0,
                PageCount = 7,
                PrintedCount = 0,
                //Detail = "31 разговор, 5 СМС",
                Detail = "0 сеансов",
                IsReprinted = true,
                ReportDocumentTypeId = (int)ReportType.Report,
                //по фильтру(0) или вручную(1)
                CriteriaType = 1,
                //имеем в виду Admin
                UserIdPrinted = 1
            };
        }
    }



    [TableName("dt_rim_Reports")]
    public class Report87 : Report
    {

        /// <summary>
        /// Идентификатор 
        /// </summary>
        [MapField("ReportId"), PrimaryKey, Identity]
        public override int ReportId { get; set; }

        /// <summary>
        /// ГУИД
        /// </summary>
        [MapField("ReportGUID")]
        public override Guid ReportGuid { get; set; }

        /// <summary>
        /// ГУИД задания
        /// </summary>
        [MapField("TaskGUID")]
        public override Guid? TaskGuid { get; set; }


        /// <summary>
        /// id задания
        /// </summary>
        [MapField("TaskID")]
        public override int? TaskId { get; set; }


        /// <summary>
        /// Дата сводки. без времени
        /// </summary>
        [MapField("RptDate")]
        public override DateTime ReportDate { get; set; }

        /// <summary>
        /// Номер сводки
        /// </summary>
        [MapField("RptNum")]
        public override string ReportNumber { get; set; }

        /// <summary>
        /// Дата начала отбора сеансов связи
        /// </summary>
        [MapField("SeanceDateStart")]
        public override DateTime? ReportBegin { get; set; }

        /// <summary>
        /// Дата окончания отбора сеансов связи
        /// </summary>
        [MapField("SeanceDateEnd")]
        public override DateTime? ReportEnd { get; set; }

        /// <summary>
        /// Дата и время печати сводки
        /// </summary>
        [MapField("PrintedDate")]
        public override DateTime? PrintTime { get; set; }

        /// <summary>
        /// Признак что сводка была перепечатана
        /// </summary>
        [MapField("IsPrinted")]
        public override bool IsPrinted { get; set; }

        /// <summary>
        /// Количество  вошедших в сводку сеансов
        /// </summary>
        [MapField("SeanceCount")]
        public override int SeanceCount { get; set; }

        /// <summary>
        /// Количество  страниц сводки
        /// </summary>
        [MapField("PageCount")]
        public override int PageCount { get; set; }

        /// <summary>
        /// Число отправки на печать
        /// </summary>
        [MapField("PrintedCount")]
        public override int PrintedCount { get; set; }

        /// <summary>
        /// Краткое описание содержимого архива (например: 31 разговор, 5 СМС)
        /// </summary>
        [MapField("Detail")]
        public override string Detail { get; set; }

        /// <summary>
        /// Признак что сводка была перепечатана
        /// </summary>
        [MapField("IsReprinted")]
        public override bool IsReprinted { get; set; }


        //дата доставки
        [MapField("DeliveryDate")]
        public DateTime? DeliveryDate { get; set; }

        //1 - сеансы, связанные с данной сводкой, удалены
        [MapField("SeancesStatusId")]
        public int SeancesStatusId { get; set; }


        /// <summary>
        /// Дата и время начала периода сводки
        /// </summary>
        [MapField("IntervalDateStart")]
        public DateTime? IntervalDateStart { get; set; }


        /// <summary>
        /// Дата и время окончания периода сводки
        /// </summary>
        [MapField("IntervalDateEnd")]
        public DateTime? IntervalDateEnd { get; set; }


        /// <summary>
        /// тип 
        /// </summary>
        [MapField("ReportDocumentTypeId")]
        public override int ReportDocumentTypeId { get; set; }

        [MapField("CriteriaType")]
        public override int CriteriaType { get; set; }

        [MapField("UserIdPrinted")]
        public override int? UserIdPrinted { get; set; }

        

        public static Report87 CreateProtorype()
        {
            return new Report87
            {
                ReportGuid = Guid.NewGuid(),
                TaskGuid = null,
                TaskId = -1,
                ReportDate = new DateTime(2015, 9, 25),
                ReportNumber = "5",
                ReportBegin = null,
                ReportEnd = null,
                PrintTime = new DateTime(2015, 9, 25),
                IsPrinted = true,
                SeanceCount = 0,
                PageCount = 7,
                PrintedCount = 0,
                Detail = "0 сеансов",
                IsReprinted = true,
                DeliveryDate = DateTime.Now,
                SeancesStatusId = -1,
                IntervalDateStart = null,
                IntervalDateEnd = null,
                ReportDocumentTypeId = (int)ReportType.Report,
                //по фильтру(0) или вручную(1)
                CriteriaType = 1,
                //имеем в виду Admin
                UserIdPrinted = 1

            };
        }
    }


    [TableName("dt_rim_Reports")]
    public class Report
    {
        /// <summary>
        /// Идентификатор 
        /// </summary>
        [MapField("ReportId"), PrimaryKey, Identity]
        public virtual int ReportId { get; set; }

        /// <summary>
        /// ГУИД
        /// </summary>
        [MapField("ReportGUID")]
        public virtual Guid ReportGuid { get; set; }

        /// <summary>
        /// ГУИД задания
        /// </summary>
        [MapField("TaskGUID")]
        public virtual Guid? TaskGuid { get; set; }


        /// <summary>
        /// id задания
        /// </summary>
        [MapField("TaskID")]
        public virtual int? TaskId { get; set; }


        /// <summary>
        /// Дата сводки. без времени
        /// </summary>
        [MapField("RptDate")]
        public virtual DateTime ReportDate { get; set; }

        /// <summary>
        /// Номер сводки
        /// </summary>
        [MapField("RptNum")]
        public virtual string ReportNumber { get; set; }

        /// <summary>
        /// Дата начала отбора сеансов связи
        /// </summary>
        [MapField("SeanceDateStart")]
        public virtual DateTime? ReportBegin { get; set; }

        /// <summary>
        /// Дата окончания отбора сеансов связи
        /// </summary>
        [MapField("SeanceDateEnd")]
        public virtual DateTime? ReportEnd { get; set; }

        /// <summary>
        /// Дата и время печати сводки
        /// </summary>
        [MapField("PrintedDate")]
        public virtual DateTime? PrintTime { get; set; }

        /// <summary>
        /// Признак что сводка была перепечатана
        /// </summary>
        [MapField("IsPrinted")]
        public virtual bool IsPrinted { get; set; }

        /// <summary>
        /// Количество  вошедших в сводку сеансов
        /// </summary>
        [MapField("SeanceCount")]
        public virtual int SeanceCount { get; set; }

        /// <summary>
        /// Количество  страниц сводки
        /// </summary>
        [MapField("PageCount")]
        public virtual int PageCount { get; set; }

        /// <summary>
        /// Число отправки на печать
        /// </summary>
        [MapField("PrintedCount")]
        public virtual int PrintedCount { get; set; }

        /// <summary>
        /// Краткое описание содержимого архива (например: 31 разговор, 5 СМС)
        /// </summary>
        [MapField("Detail")]
        public virtual string Detail { get; set; }

        /// <summary>
        /// Признак что сводка была перепечатана
        /// </summary>
        [MapField("IsReprinted")]
        public virtual bool IsReprinted { get; set; }



        //1 - сеансы, связанные с данной сводкой, удалены
        [MapField("SeancesStatusId")]
        public virtual int SeancesStatusId { get; set; }


        
        /// <summary>
        /// тип 
        /// </summary>
        [MapField("ReportDocumentTypeId")]
        public virtual int ReportDocumentTypeId { get; set; }

        [MapField("CriteriaType")]
        public virtual int CriteriaType { get; set; }

        [MapField("UserIdPrinted")]
        public virtual int? UserIdPrinted { get; set; }
    }


}
