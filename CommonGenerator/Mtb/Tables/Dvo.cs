﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
    [TableName("ot_dvo")]
    public class Dvo
    {
        [MapField("dvo_id"), PrimaryKey, Identity]
        public int DvoId { get; set; }
      
        [MapField("talk_id")]
        public int TalkId { get; set; }

        [MapField("task_auto_id")]
        public int TaskAutoId { get; set; }

        [MapField("FazaDvo")]
        public byte FazaDvo { get; set; }
        
        [MapField("Ty")]
        public byte Ty { get; set; }
      
        [MapField("Code")]
        public byte Code { get; set; }
      
        [MapField("CodeDvo")]
        public byte CodeDvo { get; set; }
      
        [MapField("CodeAdd")]
        public byte CodeAdd { get; set; }
      
        [MapField("in_date")]
        public DateTime InDate { get; set; }
        
        [MapField("arc_flag")]
        public byte ArcFlag { get; set; }

        [MapField("dvo_guid")]
        public Guid DvoGuid { get; set; }


        public static Dvo CreatePrototype()
        {
            return new Tables.Dvo()
            {
                DvoId = -1,
                TalkId = -1,
                TaskAutoId = -1,
                //фаза ДВО(sorm.h, DVO_XXX) по дефолту = 1
                FazaDvo = 1,
                //тип услуги  //0xFF не имеет значения.
                Ty = Convert.ToByte(255),
                Code = (byte)CommonGenerator.Moj.Enums.DVOCodes.CONF,
                CodeDvo = (byte)CommonGenerator.Moj.Enums.DVOCodes.CONF,
                //доп. код
                CodeAdd = 0,
                InDate = DateTime.Now,
                //Состояние ДВО: 0 - создан; 15 - восстановлен из архива; 20 - скопирован в архив
                ArcFlag = 0,
                DvoGuid = Guid.NewGuid()
            };
        }

    }
}
