﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonGenerator.Mtb.Tables
{
    public class Mark
    {
        public virtual int MarkId { get; set; }
        
        public virtual int MarkDescrId { get; set; }
        
        public virtual string Descr { get; set; }
    }
}
