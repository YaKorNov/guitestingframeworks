﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using CommonGenerator.Mtb.Tables;


namespace DataGenerator.Mtb.Tables
{
    [TableName("vid_Marks")]
    public class VideoMark : Mark
    {
        [MapField("video_mark_id"), PrimaryKey, Identity]
        public override int MarkId { get; set; }

        [MapField("video_id")]
        public int VideoId { get; set; }

        [MapField("marks_descr_id")]
        public override int MarkDescrId { get; set; }

        [MapField("begin_time")]
        public DateTime BeginTime { get; set; }

        [MapField("end_time")]
        public DateTime EndTime { get; set; }

        [MapField("descr")]
        public override string Descr { get; set; }

        [MapField("user_id")]
        public int? UserId { get; set; }

        [MapField("ins_date")]
        public DateTime InsDate { get; set; }

        [MapField("marks_guid")]
        public Guid MarkGuid { get; set; }

        [MapField("task_id")]
        public int? TaskId { get; set; }



        public static VideoMark CreatePrototype()
        {
            return new VideoMark()
            {
                MarkId = -1,
                VideoId = -1,
                MarkDescrId = -1,
                BeginTime = new DateTime(1900, 01, 01, 00, 00, 00),
                EndTime = new DateTime(2100, 01, 01, 00, 00, 00),
                TaskId = -1,
                Descr = "Описание метки",
                UserId = 1,
                InsDate = DateTime.Now,
                MarkGuid = Guid.NewGuid()
            };
        }

    }
}
