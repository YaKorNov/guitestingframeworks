﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{

    public enum MCC
    {
        Russia = 250,
        Kazakhstan = 401
    }

    //наиболее распространенные операторы сотовой связи
    public enum MNC
    {
        MTS = 1,
        MegaFon = 2,
        Yota = 11,
        BWK = 12,
        Beeline = 28
    }

    [TableName("ot_place")]
	public class Place
	{
		[MapField("place_id"), PrimaryKey, Identity]
		public int PlaceId { get; set; }

		[MapField("task_auto_id")]
		public int TaskAutoId { get; set; }

		[MapField("talk_id")]
		public int? TalkId { get; set; }
	  
		[MapField("mcc")]
		public int Mcc { get; set; }
	  
		[MapField("mnc")]
		public int Mnc { get; set; }
	  
		[MapField("lac")]
		public int Lac { get; set; }
	  
		[MapField("cl")]
		public int Cl { get; set; }

		[MapField("state")]
		public byte State { get; set; }
	  
		[MapField("in_date")]
		public DateTime InDate { get; set; }
	  
		[MapField("arc_flag")]
		public byte ArcFlag { get; set; }
	  
		[MapField("place_guid")]
		public Guid PlaceGuid { get; set; }

		[MapField("dev_state")]
		public byte DevState { get; set; }
	  
		[MapField("data_label")]
		public byte DataLabel { get; set; }
	  
		[MapField("time_last_session")]
		public int TimeLastSession { get; set; }

		[MapField("raw_data")]
		public byte[] RawData { get; set; }

		[MapField("sector")]
		public byte? Sector { get; set; }

		[MapField("timing_advance")]
		public byte? TimingAdvance { get; set; }
	  
		[MapField("TETRA_area_id")]
		public Int16? TetraAreaId { get; set; }

		[MapField("TETRA_area_name")]
		public string TetraAreaName { get; set; }

		[MapField("TETRA_cell_id")]
		public Int16? TetraCellId { get; set; }

		[MapField("TETRA_cell_name")]
		public string TetraCellName { get; set; }

		[MapField("msg_code")]
		public byte? MsgCode { get; set; }

		public static Place CreatePrototype()
		{
			return new Place
			{
				PlaceId = -1,
				TaskAutoId = -1,
				TalkId = -1,
				Mcc = (int)MCC.Russia,
				Mnc = (int)MNC.MTS,
				Lac = 103,
				Cl = 2192,
                //Для сообщения 1.6:
                //05h – изменение местоположения объекта контроля в активном состоянии;
                //06h – изменение местоположения объекта контроля в пассивном состоянии.
                //пока поставим 6 по дефолту
                State = 6,
				InDate = new DateTime(2015, 04, 01, 11, 55, 47),
                //Состояние ДВО: 0 - создан; 15 - восстановлен из архива; 20 - скопирован в архив
                ArcFlag = 0,
				PlaceGuid = new Guid("290cd8e1-dafe-4c04-b464-6928357ec4f5"),
                //состояние оконечного оборудования  (из 174 приказа сорм1.1)
                DevState = 0,
				DataLabel = 131,
				TimeLastSession = 0,
				RawData = null,
				Sector = null,
				TimingAdvance = null,
				TetraAreaId = null,
				TetraAreaName = null,
				TetraCellId = null,
				TetraCellName = null,
				MsgCode = 180
			};
		}
	}
}
