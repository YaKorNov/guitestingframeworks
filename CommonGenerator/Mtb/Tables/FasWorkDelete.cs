﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using CommonGenerator.Mtb.Tables;



namespace DataGenerator.Mtb.Tables
{
    [TableName("dt_newfas_WorkDelete")]
    public class FasWorkDelete
    {
        [MapField("id"), PrimaryKey, Identity]
        public int Id { get; set; }

        [MapField("delete_type")]
        public int DeleteType { get; set; }

        [MapField("object_id")]
        public int? ObjectId { get; set; }

        [MapField("object_guid")]
        public Guid? ObjectGuid { get; set; }

        [MapField("path")]
        public string Path { get; set; }

        [MapField("is_audio")]
        public short? IsAudio { get; set; }

        [MapField("is_audio_3g")]
        public short? IsAudio3G { get; set; }

        [MapField("is_video")]
        public short? IsVideo { get; set; }


    }
}



