﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
    
    [TableName("dt_mr_TasksGrants")]
	public class TaskGrant
	{
		[MapField("task_grants_id"), PrimaryKey, Identity]
		public int TaskGrantId { get; set; }

		[MapField("oper_task_id")]
		public int TaskOperId { get; set; }

		[MapField("task_auto_id")]
		public int TaskAutoId { get; set; }

		[MapField("auto_start")]
		public DateTime AutoStart { get; set; }

		[MapField("auto_end")]
		public DateTime? AutoEnd { get; set; }

		[MapField("task_grants_guid")]
		public Guid TaskGrantGuid { get; set; }


        // FK_dt_mr_TasksGrants_dt_Tasks
        [Association(ThisKey = "oper_task_id", OtherKey = "task_id")]
        public TaskMtb Task { get; set; }

        // FK_dt_mr_TasksGrants_dt_TasksAuto
        [Association(ThisKey = "task_auto_id", OtherKey = "task_auto_id")]
        public TaskAuto AutoTask { get; set; }


        public static TaskGrant CreatePrototype()
		{
			return new TaskGrant
			{
				TaskGrantId = -1,
				TaskOperId = -1,
				TaskAutoId = -1,
				AutoStart = new DateTime(2015, 04, 01),
				AutoEnd = new DateTime(2015, 04, 03, 23, 59, 59),
				TaskGrantGuid = new Guid("9e5be87e-9010-4927-bba9-dd30884f8efe")
			};
		}
	}
}
