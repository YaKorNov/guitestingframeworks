﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using CommonGenerator.Mtb.Tables;


namespace DataGenerator.Mtb.Tables
{
	[TableName("mr_Marks")]
	public class TalkMark : Mark
	{
		[MapField("mark_id"), PrimaryKey, Identity]
		public override int MarkId { get; set; }

		[MapField("talk_id")]
		public int TalkId { get; set; }

		[MapField("task_id")]
		public int? TaskId { get; set; }

		[MapField("mark_descr_id")]
		public override int MarkDescrId { get; set; }

		[MapField("descr")]
		public override string Descr { get; set; }

		[MapField("user_id")]
		public int? UserId { get; set; }

		[MapField("ins_date")]
		public DateTime InsDate { get; set; }

		[MapField("begin_time")]
		public DateTime BeginTime { get; set; }

		[MapField("end_time")]
		public DateTime EndTime { get; set; }

		[MapField("mark_guid")]
		public Guid MarkGuid { get; set; }

		[MapField("proxy_user_id")]
		public int? ProxyUserId { get; set; }

		public static TalkMark CreatePrototype()
		{
			return new TalkMark
			{
				MarkId = -1,
				TalkId = -1,
				MarkDescrId = -1,
				TaskId = null,
				BeginTime = new DateTime(1900, 01, 01, 00, 00, 00),
				EndTime = new DateTime(2100, 01, 01, 00, 00, 00),
				Descr = "Описание метки",
				UserId = 1,
				InsDate = DateTime.Now,				
				ProxyUserId = null
			};
		}
	}
}
