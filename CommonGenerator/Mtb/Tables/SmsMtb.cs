﻿using System;
using System.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("ot_sms")]
	public class SmsMtb
	{
		[MapField("sms_id"), PrimaryKey, Identity]
		public int SmsId { get; set; }

		[MapField("task_auto_id")]
		public int TaskAutoId { get; set; }

		[MapField("send_time")]
		public DateTime? SendDateTime { get; set; }

		[MapField("recv_time")]
		public DateTime RecvDateTime { get; set; }

		[MapField("telA_SEL_TY")]
		public byte TelASelTy { get; set; }

		[MapField("telA_SEL_ID")]
		public byte TelASelId { get; set; }

		[MapField("telA_tel")]
		public string TelATel { get; set; }

		[MapField("telB_SEL_TY")]
		public byte TelBSelTy { get; set; }

		[MapField("telB_SEL_ID")]
		public byte TelBSelId { get; set; }

		[MapField("telB_tel")]
		public string TelBTel { get; set; }

		[MapField("dir")]
		public bool Direction { get; set; }

		[MapField("decode")]
		public bool Decode { get; set; }

		[MapField("decode_text")]
		public string DecodeText { get; set; }

		[MapField("raw_data")]
		public byte[] RawData { get; set; }
	  
		[MapField("mark_descr_id")]
		public int? MarkDescrId { get; set; }
  
		[MapField("mark_descr")]
		public string MarkDescr { get; set; }
	  
		[MapField("mark_user_id")]
		public int? MarkUserId { get; set; }

		[MapField("arc_flag")]
		public byte ArcFlag { get; set; }

		[MapField("sms_guid")]
		public Guid SmsGuid { get; set; }

		[MapField("has_dir")]
		public bool HasDirection { get; set; }
	  
		[MapField("delivery_code")]
		public byte? DeliveryCode { get; set; }

		[MapField("transmission_params")]
		public byte? TransmissionParams { get; set; }

		public static SmsMtb CreatePrototype()
		{
			return new SmsMtb
			{
				SmsId = -1,
				TaskAutoId = 2,
				SendDateTime = new DateTime(2015, 04, 01, 11, 55, 58),
				RecvDateTime = new DateTime(2015, 04, 01, 11, 55, 58),
				TelASelTy = 0,
				TelASelId = 0,
				TelATel = "79133333333",
				TelBSelTy = 0,
				TelBSelId = 0,
				TelBTel = "79134444444",
				Direction = false,
				Decode = true,
				DecodeText = "0123 Enter the message ~!@#$",
				RawData =
					StringToByteArray(
						"3031323320456E74657220746865206D657373616765207E214023240000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"),
				MarkDescrId = null,
				MarkDescr = null,
				MarkUserId = null,
				ArcFlag = 0,
				SmsGuid = Guid.Empty,
				HasDirection = true,
				DeliveryCode = null,
				TransmissionParams = null
			};
		}

		private static byte[] StringToByteArray(string hex)
		{
			return Enumerable.Range(0, hex.Length)
				.Where(x => x % 2 == 0)
				.Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
				.ToArray();
		}
	}
}
