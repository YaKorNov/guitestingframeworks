﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
    [TableName("mr_PhaseDescr")]
    public class PhaseDescription
    {
        [MapField("phase_descr_id"), PrimaryKey, Identity]
        public int PhaseDescrId { get; set; }

        [MapField("text")]
        public string Text { get; set; }

        [MapField("desc")]
        public string Descr { get; set; }

        [MapField("color")]
        public int Color { get; set; }

        [MapField("phase_descr_guid")]
        public Guid PhaseDescrGuid { get; set; }
    }
}
