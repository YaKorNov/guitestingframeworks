﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace DataGenerator.Mtb.Tables
{

    [TableName("dt_TalksStream")]
    public class TalksStream
    {

        [MapField("talk_id"), PrimaryKey]
        public int TalkId { get; set; }

        [MapField("MovIsComplete")]
        public bool MovIsComplete { get; set; }

        //byte[] DraftChart

        //int Decode3GIsComplete

    }
}





