﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("sm_ct_DefaultParam")]
	public class DefaultParam
	{
		[MapField("name_param")]
		public string NameParam { get; set; }

		[MapField("param")]
		public string Param { get; set; }
	}
}