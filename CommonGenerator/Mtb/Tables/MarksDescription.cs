﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace DataGenerator.Mtb.Tables
{

    [TableName("mr_MarkCategoryRelation")]
    public class MarksDescription
    {
        /// <summary>
		/// Идентификатор 
		/// </summary>
		[MapField("marks_descr_id"), PrimaryKey, Identity]
        public int MarksDescrId { get; set; }


        /// <summary>
		/// Название метки
		/// </summary>
		[MapField("text")]
        public string Text { get; set; }


        /// <summary>
		/// Описание метки
		/// </summary>
		[MapField("desc")]
        public string Desc { get; set; }



        /// <summary>
		/// shortcut
		/// </summary>
		[MapField("shortcut")]
        public int Shortcut { get; set; }


        /// <summary>
		/// Цвет метки
		/// </summary>
		[MapField("color")]
        public int Color { get; set; }


        /// <summary>
		/// Разговор с данной меткой сохранять, 0 - нет 1 - сохранять
		/// </summary>
		[MapField("save_talk")]
        public byte SaveTalk { get; set; }


        /// <summary>
		/// Guid
		/// </summary>
		[MapField("marks_descr_guid")]
        public string MarksDescrGuid { get; set; }


        /// <summary>
		///Порядок отображения меток
		/// </summary>
		[MapField("mark_order")]
        public string MarkOrder { get; set; }

    }


}
