﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
//using DataGenerator.Mtb.MagEntities;
using DataGenerator.Mtb.MtbBuilders;

namespace DataGenerator.Mtb.Tables
{
    
    public enum SEL_TY         // Тип отбора телефона (по какому принципу был сюда помещен)
    {
        SEL_UNKNOWN = 0, // тип неизвестен
        SEL_A = 1,       // собеседник
        SEL_B = 2,       // объект контроля
        SEL_REDIR = 3,   // переадресация
        SEL_IDENT = 4,   // идентификатор
        SEL_UNO_ = 15,   // УНО (условный номер объекта)
        SEL_CHAN_ = 16   // Канал
    };
    public enum TEL_ID
    {
        TEL_ID_UNKNOWN = 0, // тип неизвестен
        TEL_ID_ATS = 1,     // номер подвижного абонента данного ЦКП
        TEL_ID_LOCAL = 2,   // местный номер
        TEL_ID_ZONE = 3,    // зоновый номер (городской)
        TEL_ID_INTERTOWN = 4,// междугородный номер
        TEL_ID_INTERNATION = 5,// международный  номер
        TEL_ID_SPEC = 6,    // экстренные и справочные телефоны
        TEL_ID_IMSI = 7,    // идентификатор подвижного абонента IMSI
        TEL_ID_IMEI = 8,     // идентификатор подвижной станции IMEI
        TEL_ID_ROAMING = 9  // объект в роуминге(зарегестрирован в визитной сети) 
    };
    public enum TEL_TY
    {
        /// <summary>
        /// тип неизвестен 
        /// </summary>
        TEL_TY_UNKNOWN = 0,
        /// <summary>
        /// номер абонента A (вызывающий, я звоню, calling)
        /// </summary>
        TEL_TY_A = 1,
        /// <summary>
        /// номер абонента B (вызываемый, мне звонят, called)
        /// </summary>
        TEL_TY_B = 2,
        /// <summary>
        /// номер переадресации   (redirection)
        /// </summary>
        TEL_TY_REDIR = 3,
        /// <summary>
        /// идентификатор контролируемого абонента
        /// </summary>
        TEL_TY_IDENT = 4,
        /// <summary>
        /// номер контролируемый  (monitoring number)
        /// </summary>
        TEL_TY_MONB = 5,
        /// <summary>
        /// номер подписчика      (subscriber number)
        /// </summary>
        TEL_TY_SNB = 6,
        /// <summary>
        /// номер мобильный       (mobile station number)
        /// </summary>
        TEL_TY_MSNB = 7,
        /// <summary>
        /// номер контролирующий  (monitoring center number)
        /// </summary>
        TEL_TY_MCNB = 8,
        /// <summary>
        /// идентификатор базовой сотовой станции
        /// </summary>
        TEL_TY_CGI = 9,
        /// <summary>
        /// условный номер объекта (УНО)
        /// </summary>
        TEL_TY_UNO = 15,
        /// <summary>
        /// (ПТП) Входящий  номер (мне звонят)
        /// </summary>
        TEL_TY_IN = 16,
        /// <summary>
        /// (ПТП) Исходящий номер (я звоню) импульсный набор
        /// </summary>
        TEL_TY_OUT_IMP = 17,
        /// <summary>
        /// (ПТП) Исходящий номер (я звоню) тональный набор
        /// </summary>
        TEL_TY_OUT_TON = 18
    };
    // 0 = a->b,  1 = b->a
    public enum PhoneDirection
    {
        Incoming = 0, //входящее
        Outgoing = 1 //исходящее
    };

    //направление для разговора
    //0 неопредено, 1 вход., 2 исход.
    public enum TalkPhoneDirection
    {
        Undefined = 0,
        Incoming = 1, //входящее
        Outgoing = 2 //исходящее
    };



    [TableName("ot_SmsPhones")]
	public class SmsPhone
	{
		[MapField("phones_id"), PrimaryKey, Identity]
		public int PhoneId { get; set; }

		[MapField("sms_id")]
		public int SmsId { get; set; }

		[MapField("phone")]
		public string Phone { get; set; }

		[MapField("SEL_TY")]
		public byte SelTy { get; set; }

		[MapField("TEL_ID")]
		public byte TelId { get; set; }

		[MapField("phone_dir")]
		public byte PhoneDir { get; set; }

		[MapField("phones_guid")]
		public Guid PhoneGuid { get; set; }

		public static SmsPhone CreatePrototype()
		{
			return new SmsPhone
			{
				SmsId = -1,
				PhoneId = -1,
				SelTy = (byte)SEL_TY.SEL_UNKNOWN,
				TelId = (byte)TEL_ID.TEL_ID_UNKNOWN,
				PhoneDir = 0,
				PhoneGuid = Guid.Parse("3623070F-5442-47D0-9B33-2A27B64ED7EB"),
				Phone = "38326446677"
			};
		}
	}
}
