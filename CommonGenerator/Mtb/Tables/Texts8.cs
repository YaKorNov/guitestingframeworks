﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System.ComponentModel;


namespace DataGenerator.Mtb.Tables
{
	[TableName("dt_pb_Texts8")]
	public class Texts8
	{
		[MapField("id"), PrimaryKey, Identity]
		public int Id { get; set; }

		[MapField("seance_id")]
		public int SeanceId;

		[MapField("seance_type")]
		public int SeanceType { get; set; }

		[MapField("task_id")]
		public int TaskId { get; set; }

		[MapField("text_value")]
		public string TextValue { get; set; }

		[MapField("create_date")]
		public DateTime CreateDate { get; set; }

		[MapField("state")]
		public byte State { get; set; }

		[MapField("user_id")]
		public int UserId { get; set; }

		[MapField("text_ext")]
		public string TextExt { get; set; }

		public enum TypeSeance
		{
			Talk = 0x01,
			SMS = 0x02,
			Fax = 0x04,
			Place = 0x08,
			DVO = 0x10,
			Video = 0x20,
			All = 0xFF
		};

		public enum Texts8State
        {
            [Description("Черновик")]
            Darft = 1,
            [Description("Завершена")]
            Completed = 2,
            [Description("Включена в сводку")]
            Published = 4,
		}

		public static Texts8 CreatePrototype()
		{
			return new Texts8
			{
				Id=-1,
				SeanceId = -1,
				SeanceType = (int) TypeSeance.Talk,
				CreateDate = DateTime.Now,
				State = (byte) Texts8State.Published,
				UserId = 1,
				TextValue = new Guid("BB765A86-C7B0-42D0-9D10-0C7057C6249E").ToString(),
				TextExt = null
			};
		}
	}
}