﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
    [TableName("vid_VideoExt")]
    public class VideoExt
    {
        [MapField("video_id"), PrimaryKey]
        public int VideoId { get; set; }

        [MapField("task_id"), PrimaryKey]
        public int TaskId { get; set; }

        [MapField("important")]
        public byte Important { get; set; }

        [MapField("proc_id")]
        public int ProcId { get; set; }

        [MapField("marked")]
        public bool Marked { get; set; }

        [MapField("stenogramm_id")]
        public int? StenogrammId { get; set; }

        [MapField("comment")]
        public string Comment { get; set; }

    }
}
