﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("dt_rim_ReportSeance")]
	public class ReportSeance
	{
		/// <summary>
		/// Идентификатор 
		/// </summary>
		[MapField("ReportId")]
		public int ReportId { get; set; }

		/// <summary>
		/// Идентификатор типа сеанса
		/// </summary>
		[MapField("SeanceTypeId")]
		public int SeanceTypeId { get; set; }

		/// <summary>
		/// Идентификатор сеанса
		/// </summary>
		[MapField("SeanceId")]
		public int SeanceId { get; set; }
	}
}
