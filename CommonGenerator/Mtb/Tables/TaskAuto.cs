﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using System.Collections.Generic;
using Newtonsoft.Json;



namespace DataGenerator.Mtb.Tables
{
	[TableName("dt_TasksAuto")]
	public class TaskAuto
	{
        
        [MapField("task_auto_id"), PrimaryKey, Identity]
		public int TaskAutoId { get; set; }

		[MapField("reg_number")]
		public string RegNumber { get; set; }

		[MapField("source_id")]
		public int SourceId { get; set; }

		[MapField("file_path")]
		public string FilePath { get; set; }

		[MapField("device_id")]
		public int DeviceId { get; set; }

		[MapField("channel")]
		public int Channel { get; set; }

        [JsonIgnore]
        [MapField("control_phone")]
		public string ControlPhone { get; set; }

		[MapField("active_flag")]
		public byte ActiveFlag { get; set; }

		[MapField("task_auto_guid")]
		public Guid TaskAutoGuid { get; set; }

        [JsonIgnore]
        [MapField("creation_time")]
		public  DateTime CreationTime { get; set; }

        [JsonIgnore]
        [MapField("fs_name")]
		public string FsName { get; set; }

        [JsonIgnore]
        [MapField("last_modify")]
        public DateTime LastModify { get; set; }

        [JsonIgnore]
        [MapField("last_modify_arc")]
        public DateTime? LastModifyArc { get; set; }



        public static TaskAuto CreatePrototype()
		{
			return new TaskAuto
			{
				TaskAutoId = 1,
                //Имя задания, приходит от брокера или другого закачивальщика
                RegNumber = "0_1",
                //FK from dt_SourceTypes -опред.источник разговоров
                SourceId = 7,
                //путь до файла разговора 
				FilePath = "somefilename_0_1",
                //id устройства (приходит извне от MagBroker)
				DeviceId = 0,
                //канал
				Channel = 256,
				ControlPhone = null,
				ActiveFlag = 0,
				TaskAutoGuid = new Guid("8e791461-f3d2-47fb-a757-bb1e448500b9"),
				CreationTime = new DateTime(2015, 04, 01),
                //Используемый файл - сервер,NULL - дефолтный
                FsName = null,
                LastModify = new DateTime(2015, 04, 01),
                LastModifyArc = null
            };
		}


        //таблица dt_SourceTypes (источники разговоров)
        public static Dictionary<int, string> SourceTypes = new Dictionary<int, string>()
        {
            {1, "Источник не определён"},
            {2, "FifoSQL Конвертор"},
            {5, "Хижина"},
            {7, "MagFlow Broker"},
            {8, "Из Хаты"},
            {9, "Импортированный"},
            {11, "MagVideo"},
            {12, "YurionVideo"},
            {13, "Рейка" },
            {14, "Архивация"}

        };





    }
}
