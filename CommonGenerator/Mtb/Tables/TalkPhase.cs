﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
    [TableName("dt_Phases")]
    public class TalkPhase
    {
        [MapField("phasetalk_id"), PrimaryKey, Identity]
        public int PhaseTalkId { get; set; }

        [MapField("talk_id")]
        public int TalkId { get; set; }

        [MapField("phase_time")]
        public DateTime PhaseTime { get; set; }

        [MapField("phase_descr_id")]
        public int PhaseDescrId { get; set; }

        [MapField("phasetalk_guid")]
        public Guid PhaseTalkGuid { get; set; }
    }
}
