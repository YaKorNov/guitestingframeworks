﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace DataGenerator.Mtb.Tables
{

    [TableName("sm_dt_Texts")]
    public class Texts7
    {

        [MapField("text_id"), PrimaryKey, Identity]
        public int TextId { get; set; }

        [MapField("task_id")]
        public int TaskId;

        [MapField("talk_id")]
        public int? TalkId;

        [MapField("kOtd")]
        public int kOtd;

        [MapField("Nmr_Swd")]
        public int? NmrSwd;

        [MapField("DtSwd")]
        public DateTime? DtSwd { get; set; }

        [MapField("DtCreat")]
        public DateTime? DtCreat { get; set; }

        [MapField("DtSwdUpdate")]
        public DateTime DtSwdUpdate { get; set; }

        [MapField("users_Name")]
        public string UsersName { get; set; }

        [MapField("tbegin")]
        public DateTime TBegin { get; set; }

        [MapField("tend")]
        public DateTime TEnd { get; set; }

        [MapField("resltCall_id")]
        public int? resltCall_id { get; set; }


        [MapField("Phone")]
        public string Phone { get; set; }

        [MapField("Owner_ph")]
        public string OwnerPh { get; set; }

        [MapField("onCel_id")]
        public int OnCelId { get; set; }



        [MapField("Analit_Proc")]
        public int? AnalitProc { get; set; }


        [MapField("Cartridge")]
        public string Cartridge { get; set; }


        [MapField("OnPrt")]
        public bool OnPrt { get; set; }



        [MapField("Stenogram")]
        public string Stenogram { get; set; }


        [MapField("text_guid")]
        public Guid TextGuid { get; set; }




    }
}
