﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace DataGenerator.Mtb.Tables
{

    [TableName("dt_mr_usTskPacs")]
    public class TaskPacs
    {
        [MapField("pac_id")]
        public int PacId { get; set; }

        [MapField("task_id")]
        public int TaskId { get; set; }


    }
}
