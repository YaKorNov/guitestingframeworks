﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("mr_UserOptions")]
	public class UserOption
	{
		[MapField("field_name")]
		public string FieldName { get; set; }

		[MapField("field_value")]
		public string FieldValue { get; set; }
	}
}