﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using CommonGenerator.Mtb.Tables;


namespace DataGenerator.Mtb.Tables
{
	[TableName("mr_Marks_place")]
	public class PlaceMark : Mark
	{
		[MapField("mark_id"), PrimaryKey, Identity]
		public override int MarkId { get; set; }

		[MapField("place_id")]
		public int PlaceId { get; set; }

		[MapField("task_id")]
		public int TaskId { get; set; }

		[MapField("mark_descr_id")]
		public override int MarkDescrId { get; set; }

		[MapField("descr")]
		public override string Descr { get; set; }

		[MapField("user_id")]
		public int? UserId { get; set; }

		[MapField("ins_date")]
		public DateTime InsDate { get; set; }

		public static PlaceMark CreatePrototype()
		{
			return new PlaceMark
			{
				MarkId = -1,
				PlaceId = -1,
				MarkDescrId = -1,
				TaskId = -1,
				Descr = "Описание метки",
				UserId = 1,
				InsDate = DateTime.Now,
			};
		}
	}
}
