﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
    [TableName("dt_TasksArcCompleted")]
    public class TaskArcCompleted
    {
        [MapField("task_id"), PrimaryKey] 
        public int TaskId { get; set; }

        [MapField("DelCompleted")]
        public bool DelCompleted { get; set; }

        [MapField("ArcCompleted")]
        public bool ArcCompleted { get; set; }
    }
}
