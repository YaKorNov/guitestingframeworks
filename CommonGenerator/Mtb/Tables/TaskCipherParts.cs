﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace DataGenerator.Mtb.Tables
{

    [TableName("dt_TasksCipherParts")]
    public class TaskCipherParts
    {

        [MapField("task_id"), PrimaryKey, Identity]
        public int TaskId { get; set; }

        [MapField("TaskType")]
        public string TaskType { get; set; }

        [MapField("TaskObject")]
        public string TaskObject { get; set; }

        [MapField("TaskNumber")]
        public string TaskNumber { get; set; }

        [MapField("TaskYear")]
        public string TaskYear { get; set; }

        [MapField("TaskBegin")]
        public DateTime? TaskBegin { get; set; }

        [MapField("TaskEnd")]
        public DateTime? TaskEnd { get; set; }

    }
}
