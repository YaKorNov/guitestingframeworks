﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("mr_MarksDescr")]
	public class MarkDescription
	{
		[MapField("marks_descr_id"), PrimaryKey, Identity]
		public int MarkDescriptionId { get; set; }
		   
		[MapField("text")]
		public string Text { get; set; }

		[MapField("desc")]
		public string Description { get; set; }
	  
		[MapField("shortcut")]
		public int Shortcut { get; set; }
	  
		[MapField("color")]
		public int Color { get; set; }
	  
		[MapField("save_talk")]
		public bool SaveTalk { get; set; }

		[MapField("marks_descr_guid")]
		public Guid MarkDescriptionGuid { get; set; }

		[MapField("mark_order")]
		public int MarkOrder { get; set; }

		public static MarkDescription CreatePrototype()
		{
			return new MarkDescription
			{
				MarkDescriptionId = -1,
				Text = "Синяя",
				Description = "Описание метки",
				Shortcut = 0,
				Color = 14853748,
				SaveTalk = true,
				MarkDescriptionGuid = Guid.NewGuid(),
				MarkOrder = 1
			};
		}
	}
}
