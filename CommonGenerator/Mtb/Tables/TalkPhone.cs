﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
	[TableName("dt_Phones")]
	public class TalkPhone
	{
		[MapField("phones_id"), PrimaryKey, Identity]
		public int PhoneId { get; set; }

		[MapField("phones_guid")]
		public Guid PhoneGuid { get; set; }

		[MapField("phone_dir")]
		public byte PhoneDir { get; set; }

		[MapField("phone")]
		public string Phone { get; set; }

		[MapField("SEL_TY")]
		public byte SelTy { get; set; }

		[MapField("TEL_ID")]
		public byte TelId { get; set; }

		[MapField("talk_id")]
		public int TalkId { get; set; }

		[MapField("phone_time")]
		public DateTime PhoneTime { get; set; }

		[MapField("TEL_TY")]
		public byte TelTy { get; set; }

		[MapField("isBusy")]
		public bool IsBusy { get; set; }

		[MapField("ticket_guid")]
		public Guid? TicketGuid { get; set; }

		public static TalkPhone CreatePrototype()
		{
			return new TalkPhone
			{
				PhoneId = -1,
				PhoneTime = new DateTime(2015, 04, 01, 11, 55, 47),
				TalkId = -1,
				SelTy = 2,
				TelId = 0,
				TelTy = 1,
				IsBusy = false,
				PhoneDir = 0,
				PhoneGuid = new Guid("AF86CE65-3B3B-4C4A-89E8-5D12AFD48DA3"),
				TicketGuid = null
			};
		}
	}
}
