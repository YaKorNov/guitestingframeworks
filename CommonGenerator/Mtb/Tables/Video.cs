﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace DataGenerator.Mtb.Tables
{
    
    public enum VidProcessing : int
    {
        /// <summary>
        /// Необработанная запись
        /// </summary>
        UNPROCESSED = 0,
        /// <summary>
        /// Запись просмотрена оператором
        /// </summary>
        PROCESSED = 1,
        /// <summary>
        /// Запись нуждается в обработке
        /// </summary>
        MUST_BE_PROCESSED = 2
    }


    [TableName("vid_Video")]
    public class Video
    {
        [MapField("video_id"), PrimaryKey, Identity]
        public int VideoId { get; set; }

        [MapField("task_auto_id")]
        public int TaskAutoId { get; set; }
        
        [MapField("tbegin")]
        public DateTime BeginDateTime { get; set; }

        [MapField("tend")]
        public DateTime? EndDateTime { get; set; }

        [MapField("file_size")]
        public int FileSize { get; set; }

        [MapField("arc_flag")]
        public int ArcFlag { get; set; }

        [MapField("video_guid")]
        public Guid VideoGuid { get; set; }

        [MapField("important")]
        public byte Important { get; set; }

        [MapField("comment")]
        public string Comment { get; set; }

        [MapField("proc_id")]
        public  int ProcId { get; set; }

        [MapField("marked")]
        public bool Marked { get; set; }

        [MapField("file_type")]
        public int? FileType { get; set; }

        [MapField("stenogramm_id")]
        public int? StenogramId { get; set; }

        //[MapField("file_processing")]
        //public byte? FileProcessing { get; set; }

        [MapField("suppress_remove")]
        public byte? SuppressRemove { get; set; }

        public static Video CreatePrototype()
        {
            return new Tables.Video()
            {
                VideoId = -1,
                TaskAutoId = -1,
                BeginDateTime = DateTime.Now,
                EndDateTime = DateTime.Now,
                FileSize = 1500,
                ArcFlag = 0,
                VideoGuid = Guid.NewGuid(),
                Important = 0,
                Comment = "",
                ProcId = (int)VidProcessing.UNPROCESSED,
                Marked = false,
                FileType = null,
                StenogramId = null,
                //FileProcessing = 0,
                SuppressRemove = 0
            };
        }

    }
}
