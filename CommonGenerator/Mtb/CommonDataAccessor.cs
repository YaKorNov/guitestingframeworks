﻿using System;
using System.Collections.Generic;
using System.Data;
using BLToolkit.DataAccess;
using DataGenerator.Mtb.Tables;
//using Signatec.Mag.Abonents.Model.Domain;



namespace CommonGenerator.Mtb
{
    public abstract class CommonDataAccessor : DataAccessor<TaskCipherParts, CommonDataAccessor>
    {

        private const string SelectVideoByTaskQuery = "select tl.* from dbo.dt_mr_TasksGrants tg with(NOLOCK) " +
            " inner join dbo.vid_Video tl with(nolock) on tl.task_auto_id = tg.task_auto_id and tl.tbegin >= tg.auto_start and (tl.tbegin <= tg.auto_end or tg.auto_end is null)" +
            " where tg.oper_task_id = @taskId";
        private const string VideoFilterByEarlierSeances = " and tl.video_id not in	(select rs2.SeanceId from  dbo.dt_rim_Reports r2 " +
            " inner join dbo.dt_rim_ReportSeance rs2  on rs2.ReportId = r2.ReportId and r2.TaskId = @taskId and rs2.SeanceTypeId = @seanceTypeVideo" +
            " where (r2.IsReprinted = 0 and r2.ReportDocumentTypeId = 1 and r2.IsDeleted = 0))";

        private const string SelectTalkByTaskQuery = "select tl.* from dbo.dt_mr_TasksGrants tg with(nolock) " +
            " inner join dbo.dt_Talks tl with(nolock) on tl.task_auto_id = tg.task_auto_id and tl.tbegin >= tg.auto_start and (tg.auto_end is null or tl.tbegin<tg.auto_end)" +
            " where tg.oper_task_id = @taskId";

        private const string TalkFilterByEarlierSeances = " and tl.talk_id not in (select rs2.SeanceId from dbo.dt_rim_Reports r2" +
            " inner join dbo.dt_rim_ReportSeance rs2  on rs2.ReportId = r2.ReportId and (rs2.SeanceTypeId = @seanceTypeTalk or rs2.SeanceTypeId = 65)" +
            " where r2.TaskId = @taskId and r2.IsReprinted = 0 and r2.ReportDocumentTypeId = 1 and r2.IsDeleted = 0)";

        private const string SelectSmsByTaskQuery = "select tl.* from dbo.dt_mr_TasksGrants tg with(NOLOCK) " +
            " inner join dbo.ot_sms tl with(nolock) on tl.task_auto_id = tg.task_auto_id and tl.send_time >= tg.auto_start and (tl.send_time <= tg.auto_end or tg.auto_end is null)" +
            " where tg.oper_task_id = @taskId";


        private const string SmsFilterByEarlierSeances = " and tl.sms_id not in	(select rs2.SeanceId from  dbo.dt_rim_Reports r2 " +
            "inner join dbo.dt_rim_ReportSeance rs2  on rs2.ReportId = r2.ReportId and r2.TaskId = @taskId and rs2.SeanceTypeId = @seanceTypeSms " +
            "where (r2.IsReprinted = 0 and r2.ReportDocumentTypeId = 1 and r2.IsDeleted = 0) )";


        private const string SelectPlaceByTaskQuery = "select  tl.* from dbo.dt_mr_TasksGrants tg with(NOLOCK) " +
            " inner join dbo.ot_place tl with(nolock) on tl.task_auto_id = tg.task_auto_id and tl.in_date >= tg.auto_start and (tl.in_date <= tg.auto_end or tg.auto_end is null)" +
            " where tg.oper_task_id = @taskId";


        private const string PlaceFilterByEarlierSeances = " and tl.place_id not in	(select rs2.SeanceId from  dbo.dt_rim_Reports r2 " +
            "inner join dbo.dt_rim_ReportSeance rs2  on rs2.ReportId = r2.ReportId and r2.TaskId = @TaskId and rs2.SeanceTypeId = @seanceTypePlace " +
            "where (r2.IsReprinted = 0 and r2.ReportDocumentTypeId = 1 and r2.IsDeleted = 0) )";


        //Changelist
        //v. 1.8.0.852. Существовала таблица dt_TalksStream
        //v. 1.8.0.965. таблица dt_TalksStream удалена 
        [SqlQuery(@"delete from dt_ProxyUserTaskTalkFilter;
                    delete from dt_talksExtHata;
                    delete from mr_BriefcaseTalksList;
                    delete from dt_rim_ReportSeance;
                    delete from dt_rim_Reports;
                    delete from dt_TasksCipherParts;
                    delete from mr_Marks_sms;
                    delete from mr_Marks;
                    delete from mr_Marks_place;
                    delete from vid_Marks;

                    delete from mr_MarkCategoryRelation;
                    delete from mr_MarksDescr;
                    delete from dt_pb_Texts8;
                    delete from sm_dt_Texts;

                    delete from dt_Phases;
                    delete from  dt_pb_AbonentSeancePhonesLink;
                    --delete from dt_TalksStream;
                    delete from dt_Phones;

                    delete from ot_dvo;
                    delete from ot_place;
                    delete from  dt_Talks;
                    delete from ot_sms;
                    delete from vid_Video;

                    delete from dt_mr_UsersGrants;
                    delete from dt_mr_TasksGrants;
                    delete from  dt_mr_usTskPacs;
                    delete from dt_Tasks;
                    delete from dt_TasksAuto;

                    ;")]
        public abstract void FullCleanUp();


        //SET IDENTITY_INSERT sl_DivisionGuid ON;
        //INSERT INTO sl_DivisionGuid(id, title, caption)
        //            VALUES(
        //                1,
        //                'default',
        //                'default'
        //            );
        //SET IDENTITY_INSERT sl_DivisionGuid OFF;
        [SqlQuery(@"
                    INSERT INTO dt_TasksCipherParts
                               ([task_id]
                               ,[TaskType]
                               ,[TaskObject]
                               ,[TaskNumber]
                               ,[TaskYear]
                               ,[TaskBegin]
                               ,[TaskEnd])
                         VALUES
                               (@task_id,
                                @TaskType,
                                   @TaskObject, 
                                   @TaskNumber,
                                   @TaskYear,
                                   @TaskBegin,
                                   @TaskEnd)
                    ;")]
        public abstract void InsertTaskCipherParts(TaskCipherParts tcp);


        [SqlQuery(@"
                    select
                    mr_MarksDescr.*
                    from
                    (select
                    distinct(mark_descr_id) 
                    from
                    (select
                    mark_descr_id as mark_descr_id
                    from
                    mr_Marks
                    where mr_Marks.task_id = @task_id
                    union all
                    select
                    mark_descr_id as mark_descr_id
                    from
                    mr_Marks_place
                    where mr_Marks_place.task_id = @task_id
                    union all
                    select
                    mark_descr_id as mark_descr_id
                    from
                    mr_Marks_sms
                    where mr_Marks_sms.task_id = @task_id
                    union all
                    select
                    marks_descr_id as mark_descr_id
                    from
                    vid_Marks
                    where vid_Marks.task_id = @task_id) as Marks) as MarksD
                    inner join
                    mr_MarksDescr
                    on MarksD.mark_descr_id= mr_MarksDescr.marks_descr_id
                    ;")]
        public abstract List<MarkDescription> GetDescrByTask(TaskMtb tsk);





       

        [SqlQuery(@" DECLARE @mdIdCount   INT = 0
                    SELECT
                    @mdIdCount = count(*)
                    FROM
                    mr_MarksDescr
                    WHERE
                    text = @Text;

                    if @mdIdCount = 0
                      begin
                        INSERT INTO[dbo].[mr_MarksDescr]
                               ([text]
                               ,[desc]
                               ,[shortcut]
                               ,[color]
                               ,[save_talk])
                         VALUES
                               (@Text
                               , @Desc
                               , @Shortcut
                               , @Color
                               , @save_talk);
                       end

                    SELECT
                    *
                    FROM
                    mr_MarksDescr
                    WHERE
                    text = @Text;
                ;")]
        public abstract MarkDescription GetMarkDescription(MarkDescription md);



        [SqlQuery(@" DECLARE @TCount   INT = 0
                    SELECT
                    @TCount = count(*)
                    FROM
                    mr_pb_MarksDescr8
                    WHERE
                    id = @id;

                    if @TCount = 0
                      begin
                        INSERT INTO[dbo].[mr_pb_MarksDescr8]
                               ([id]
                               ,[is_complete]
                                )
                         VALUES
                               (@id
                               , @is_complete);
                       end

                ;")]
        public abstract MarkDescription8 SetMarkDescription8(MarkDescription8 md);



        /// <summary>
        /// Найти задание
        /// </summary>
        [SqlQuery(@"
                    SELECT TOP 1 * FROM dt_Tasks
			        WHERE obj_shifr=@taskShifr
                ;")]
        public abstract TaskMtb FindTaskByShifr(string taskShifr);



        /// <summary>
        /// Найти задания по шифру
        /// </summary>
        [SqlQuery(@"
                    SELECT * FROM dt_Tasks
                    WHERE obj_shifr IN (select f.value from dbo.f_Playback_TextToListText(@taskShifrList) f)
                ;")]
        public abstract List<TaskMtb> FindTaskByShifrList(string taskShifrList);



        [SqlQuery(@"
                    SELECT reg_number FROM dt_TasksAuto
                ;")]
        public abstract List<string> GetAutotasksRegNumberList();



        [SqlQuery(@"
                    SELECT dt_mr_TasksGrants.task_auto_id, dt_Tasks.task_guid FROM dt_Tasks
                    inner join dt_mr_TasksGrants on dt_Tasks.task_id = dt_mr_TasksGrants.oper_task_id
                    WHERE obj_shifr IN (select f.value from dbo.f_Playback_TextToListText(@shifr_list) f);
                ;")]
        public abstract DataTable GetAutotasksWithTaskGuids(string shifr_list);


        /// <summary>
        /// Получить задания
        /// </summary>
        [SqlQuery(@"
                    SELECT  * FROM dt_Tasks
			        WHERE obj_shifr IN (@taskShifrList)
                ;")]
        public abstract List<TaskMtb> FindTasksByShifrList(string taskShifrList);


        /// <summary>
        /// Добавить задание
        /// </summary>
        [SprocName("usp_TasksAdd")]
        public abstract void AddTask([Direction.Output("task_id")] TaskMtb task);


         [SqlQuery(@"
                    UPDATE
                    dt_tasks
                    SET
                    Arc_keepPlus = @ArcKeepPlus,
                     Arc_keepMinus = @ArcKeepMinus,
                     Arc_keepNotMark = @ArckeepNotMark
                    WHERE
                    task_id = @taskId
                ;")]
        public abstract void UpdateArcPlisMinusMarks(int ArcKeepPlus, int ArcKeepMinus, int ArckeepNotMark, int taskId);



        /// <summary>
        /// Найти задание
        /// </summary>
        [SprocName("usp_TasksSearch")]
        public abstract List<TaskMtb> FindTask(int? task_id =null, Guid? task_guid = null);


        /// <summary>
        /// Обновить задание
        /// </summary>
        [SprocName("usp_TasksUpdate")]
        public abstract void UpdateTask(TaskMtb task);


        /// Шифр по частям
        /// </summary>
        [SprocName("usp_TasksCipherPartsSet")]
        public abstract void SetCipherParts(TaskCipherParts taskCP);


        [SprocName("rim_ReportSet")]
        public abstract void UpdateRimReport(Report rep);


        [SprocName("rim_ReportSet")]
        public abstract void SetRimReport([Direction.Output("ReportId")] Report rep);


        [SprocName("rim_SetDeliveryDate")]
        public abstract void SetRimReportDeliveryDate( Report rep);


        [SprocName("rim_SeanceIncludedReportSet")]
        public abstract void SetRimReportSeances(int ReportId, string SeanceTypeId, string SeanceId);



        //Sp SeanceForReportGet содержит многие поля, но не все необходимые

        [SqlQuery(SelectVideoByTaskQuery)]
        public abstract List<Video> GetVideoByTask(int taskId);


        [SqlQuery(SelectVideoByTaskQuery + VideoFilterByEarlierSeances)]
        public abstract List<Video> GetVideoByTaskExceptEarlier(int taskId, int seanceTypeVideo = (int)Texts8.TypeSeance.Video);


        [SqlQuery(SelectTalkByTaskQuery)]
        public abstract List<TalkMtb> GetTalkByTask(int taskId);


        [SqlQuery(SelectTalkByTaskQuery + TalkFilterByEarlierSeances)]
        public abstract List<TalkMtb> GetTalkByTaskExceptEarlier(int taskId, int seanceTypeTalk = (int)Texts8.TypeSeance.Talk);


        [SqlQuery(SelectSmsByTaskQuery)]
        public abstract List<SmsMtb> GetSmsByTask(int taskId);


        [SqlQuery(SelectSmsByTaskQuery + SmsFilterByEarlierSeances)]
        public abstract List<SmsMtb> GetSmsByTaskExceptEarlier(int taskId, int seanceTypeSms = (int)Texts8.TypeSeance.SMS);


        [SqlQuery(SelectPlaceByTaskQuery)]
        public abstract List<Place> GetPlaceByTask(int taskId);


        [SqlQuery(SelectPlaceByTaskQuery + PlaceFilterByEarlierSeances)]
        public abstract List<Place> GetPlaceByTaskExceptEarlier(int taskId, int seanceTypePlace = (int)Texts8.TypeSeance.Place);






        [SqlQuery(@"select
                    dt_tasksAuto.task_auto_id as task_auto_id, MAx(dt_Phones.Phone) as Phone
                    from
                    dt_tasksAuto
                    inner join
                    dt_Talks
                    on dt_tasksAuto.task_auto_id = dt_Talks.task_auto_id
                    inner
                    join dt_Phones
                    on dt_Talks.talk_id = dt_Phones.talk_id
                    where
                    dt_Phones.SEL_TY = 2
                    group by dt_tasksAuto.task_auto_id")]
        [Index("task_auto_id")]
        [ScalarFieldName("Phone")]
        public abstract IDictionary<int, string> GetControlPhonesList();

        


        [SprocName("newfas_GetContext")]
        public abstract string NewfasGetContext(int pTalkId, int pWrite = 1);


        [SprocName("newfas_SelectVideoFileName")]
        public abstract string NewfasVideoFileName(int videoId);

        //Changelist
        //v. 1.8.0.449.
        //v. 1.8.0.965 ХП удалена, т.к. удалена таблица Talks_Stream
        [SprocName("newfas_SetIsComplete")]
        public abstract void NewfasSetIsComplete(int pTalkId);



        //обновление настройки по дефолту для всех юзеров (user_id = -1)
        [SqlQuery(@" DECLARE @TCount   INT = 0
                    SELECT
                    @TCount = count(*)
                    FROM
                    mr_userOptions
                    WHERE
                    field_name = @field_name AND [user_id] = -1;

                    if @TCount = 0
                      begin
                        INSERT INTO[dbo].[mr_userOptions]
                               ([field_name]
                               ,[field_value]
                               ,[user_id]
                                )
                         VALUES
                               (@field_name
                               , @field_value,
                                -1);
                       end
                    else
                        begin
                                UPDATE [dbo].[mr_userOptions]
						        SET [field_value] =  @field_value
						        WHERE [field_name] = @field_name
						        AND [user_id] = -1
                        end

                ;")]
        public abstract void SetUserOption(string field_name, string field_value);



        [SqlQuery(@" 
                    SELECT
                    field_value
                    FROM
                    mr_userOptions
                    WHERE
                    field_name = @field_name AND [user_id] = -1;
                ")]
        public abstract string GetUserOption(string field_name);



        //вызов общей процедуры расчистки
        [SprocName("newfas_CleanupByDevices_Job")]
        public abstract void CleanupInvoke();



        /// <summary>
        /// Создать запись о пользователе в таблицах Воспроизведения
        /// </summary>
        /// <param name="login"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="isDisabled"></param>
        /// <param name="secLevel"></param>
        /// <param name="isMinSecLevel"></param>
        /// <param name="rightAll"></param>
        /// <param name="rightSets"></param>
        [SprocName("sp_um_CreateOrUpdateUser")]
        public abstract void CreateOrUpdateMtbUser(string login, string name, string description, bool isDisabled, int? secLevel, bool isMinSecLevel, string rightAll, string rightSets);





        [SqlQuery(@" 
                USE master;
			    declare @q nvarchar(4000);
                IF EXISTS (SELECT * FROM sys.server_principals WHERE name = @login)
                BEGIN
					set @q = 'alter login ['+@login+'] with password = '''+@pass+''', default_database=[master], check_expiration=off, check_policy=off'
					exec ( @q )
                END 
				ELSE
				BEGIN
					set @q = 'create login ['+@login+'] with password = '''+@pass+''', default_database=[master], check_expiration=off, check_policy=off'
					exec ( @q )
				END;
                USE MagTalksBase;
                ")]
        public abstract void SetDBLogin(string login, string pass);



        //cmd.ExecuteNonQuery();
        //cmd.CommandText = string.Format("EXEC sp_addrolemember N'db_datareader', N'{0}'", login);
        //cmd.ExecuteNonQuery();
        //cmd.CommandText = string.Format("EXEC sp_addrolemember N'db_datawriter', N'{0}'", login);
        //cmd.ExecuteNonQuery();
        ////if (isMag8)
        ////{
        ////    cmd.CommandText = string.Format("EXEC sp_addrolemember N'mag8_role', N'{0}'", login);
        ////    cmd.ExecuteNonQuery();
        ////}
        ////if (isMag)
        ////{
        //    cmd.CommandText = string.Format("EXEC sp_addrolemember N'mag_role', N'{0}'", login);
        //    cmd.ExecuteNonQuery();
        ////}



        [SqlQuery(@" 
            USE MagTalksBase;
            IF NOT EXISTS (SELECT * FROM dbo.sysusers where name = @login)
            BEGIN
			   declare @q nvarchar(4000) = 'create USER ['+@login+']  FOR LOGIN ['+@login+']'
			   exec ( @q )
            END;
			EXEC sp_addrolemember db_datareader, @login;
			EXEC sp_addrolemember db_datawriter, @login;
			EXEC sp_addrolemember mag_role, @login;
            ")]
        public abstract void SetMTBUser(string login);



        [SqlQuery(@" 
			EXEC sp_addrolemember mag_role, @login;
            ")]
        public abstract void AddMagRole(string login);

    }
}
