﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Mtb.Enums
{
    public enum ReportSeancesAlgoritm
    {
        [Description("Включить в сводку все сеансы по таску")]
        All,
        [Description("Random Не включая ранее включенные в другие сводки")]
        RandomExcludeEarlierSeances,
        [Description("Random Включая ранее включенные в другие сводки")]
        RandomIncludeEarlierSeances,
    }
}
