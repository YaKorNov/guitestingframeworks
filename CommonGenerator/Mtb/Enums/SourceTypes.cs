﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Mtb.Enums
{
    public enum SourceTypes
    {
        
        [Description("Источник не определён")]
        Undefined = 1,
        [Description("FifoSQL Конвертор")]
        FifoSQlConvertor = 2,
        [Description("Хижина")]
        Hizina = 5,
        [Description("MagFlow Broker")]
        MagFlowBroker = 7,
        [Description("Из Хаты")]
        FromHata = 8,
        [Description("Импортированный")]
        Imported = 9,
        [Description("MagVideo")]
        MagVideo = 11,
        [Description("YurionVideo")]
        YurionVideo = 12,
        [Description("Рейка")]
        Reika = 13,
        [Description("Архивация")]
        Archivation = 14
    }
}
