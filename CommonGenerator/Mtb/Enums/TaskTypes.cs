﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CommonGenerator.Mtb.Enums
{

    //TODO сделать перечисление по аналогии с VkNet.Enums.SafetyEnums.SafetyEnum
    public enum TaskTypes
    {
        Talk = 1,
        Sms = 2,
        Fax = 4,
        Place = 8,
        DVO = 16,
        Video = 32,
        All = 63
    };
}
