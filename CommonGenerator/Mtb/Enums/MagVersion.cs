﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonGenerator.Mtb.Enums
{
    public enum MagVersion
    {
        ver_8_6 = 86,
        ver_8_7 = 87,
        ver_8_8 = 88

    }
}
