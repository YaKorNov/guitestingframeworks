﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Mtb.Enums
{
    public enum Categories
    {
        [Description("Очередное")]
        Ocherednoe = 2,
        [Description("Исполняемое")]
        Executed = 3,
        [Description("Приостановлено")]
        Stopped = 4,
        [Description("Завершено")]
        Completed = 5,
        [Description("Не выполнено")]
        NotCompleted = 6,
    }

}
