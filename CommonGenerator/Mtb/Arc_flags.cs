﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;



namespace CommonGenerator.Mtb.Enums
{
    public enum Arc_flags
    {
        //0  файла нет, 
        //5  файл создан 
        //10 файл записан, 
        //15 файл восстановлен из архива, 
        //20 файл скопирован в архив, 
        //30 файл перемещен, 
        //40 Файл удален, 
        //50 Статконтроль, 
        //60 Статконтроль скопирован в архив,
        //70 Отложеный разговор,

        [Description("файла нет")]
        FileDoesNotExist = 0,
        [Description("файл создан")]
        FileCreated = 5,
        [Description("файл записан")]
        FileWrited = 10,
        [Description("Файл удален")]
        FileDeleted = 40,
        [Description("Статконтроль")]
        Statcontrol = 50,
        [Description("Статконтроль скопирован в архив")]
        StatcontrolArchived = 60,

    }







}
