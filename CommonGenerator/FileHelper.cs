﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;



namespace CommonGenerator
{
    public static class FileHelper
    {

        public static string GetFullName(string shortName)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "files", shortName);
        }

        public static byte[] LoadFile(string fileFullName)
        {
            byte[] result = null;

            string fullName = fileFullName;

            try
            {
                if (File.Exists(fullName))
                {
                    FileInfo fi = new FileInfo(fullName);
                    FileStream fs = new FileStream(fullName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    result = br.ReadBytes((int)fi.Length);
                }
                else
                {
                    throw new Exception("File was not found: " + fullName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception while loading file: " + fullName);
            }

            return result;
        }

        public static long FileSize(string file)
        {
            long result = -1;
            try
            {
                string fileName = Path.Combine();
                FileInfo fi = new FileInfo(GetFullName(file));
                result = fi.Length;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return result;
        }
    }


}





