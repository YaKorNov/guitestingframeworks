﻿using System;
using System.Collections.Generic;
using System.Data;
using BLToolkit.DataAccess;


namespace CommonGenerator
{
    public abstract class MagCommonDataAccessor : DataAccessor<int, MagCommonDataAccessor>
    {

        [SqlQuery(@" 
                USE master;
			    declare @q nvarchar(4000);
                IF EXISTS (SELECT * FROM sys.server_principals WHERE name = @login)
                BEGIN
					set @q = 'alter login ['+@login+'] with password = '''+@pass+''', default_database=[master], check_expiration=off, check_policy=off'
					exec ( @q )
                END 
				ELSE
				BEGIN
					set @q = 'create login ['+@login+'] with password = '''+@pass+''', default_database=[master], check_expiration=off, check_policy=off'
					exec ( @q )
				END;
                set @q = 'USE '+ @db_name
                exec ( @q )
                ")]
        public abstract void SetDBLogin(string login, string pass, string db_name);


        [SqlQuery(@" 
            declare @q nvarchar(4000);
            set @q = 'USE '+ @db_name
            exec ( @q )
            IF NOT EXISTS (SELECT * FROM dbo.sysusers where name = @login)
            BEGIN
			   set @q = 'create USER ['+@login+']  FOR LOGIN ['+@login+']'
			   exec ( @q )
            END;
			EXEC sp_addrolemember db_datareader, @login;
			EXEC sp_addrolemember db_datawriter, @login;
            ")]
        public abstract void SetMTBUser(string login, string db_name);


        [SqlQuery(@" 
            declare @q nvarchar(4000);
            set @q = 'USE '+ @db_name
            exec ( @q )
			EXEC sp_addrolemember mag_role, @login;
            ")]
        public abstract void AddMagRole(string login, string db_name);


        [SqlQuery(@" 
            declare @q nvarchar(4000);
            set @q = 'USE '+ @db_name
            exec ( @q )
			EXEC sp_addrolemember mag8_role, @login;
            ")]
        public abstract void AddMag8Role(string login, string db_name);


        [SqlQuery(@" 
            declare @q nvarchar(4000);
            set @q = 'USE '+ @db_name
            exec ( @q )
            IF NOT EXISTS (SELECT * FROM dbo.sysusers where name = @login)
            BEGIN
			   set @q = 'create USER ['+@login+']  FOR LOGIN ['+@login+']'
			   exec ( @q )
            END;
			EXEC sp_addrolemember db_datareader, @login;
			EXEC sp_addrolemember db_datawriter, @login;
            ")]
        public abstract void SetMOJUser(string login, string db_name);

    }
}
