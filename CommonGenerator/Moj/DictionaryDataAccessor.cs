﻿using System;
using System.Collections.Generic;
using System.Data;
using BLToolkit.DataAccess;
//using Signatec.Mag.Abonents.Model.Domain;
using CommonGenerator.Moj.Models;



namespace CommonGenerator.Moj
{
    public abstract class DictionaryDataAccessor : DataAccessor<DictionaryItem, DictionaryDataAccessor>
    {
       

        /// <summary>
        /// Получить элементы заданного словаря.
        /// </summary>
        [SprocName("usp_DictionarySearch")]
        public abstract IList<DictionaryItem> Get(string table_name, string value = null, string specification = null);

        /// <summary>
        /// Получить элементы заданного словаря без конвертации в объект.
        /// </summary>
        [SprocName("usp_DictionarySearch")]
        public abstract IDataReader GetRaw(string table_name, int? id = null, string title = null, string caption = null);

        /// <summary>
        /// Добавить новый элемент или вернуть существующий
        /// </summary>
        [SprocName("usp_DictionaryInsert")]
        public abstract void Set(string table_name, [Direction.Output("id")] DictionaryItem item);


        /// <summary>
        /// Обновить элемент словаря по id
        /// </summary>
        [SprocName("usp_DictionaryUpdate")]
        public abstract void Update(string table_name, DictionaryItem item);

        /// <summary>
        /// Удалить элемент словаря с заданным идентификатором.
        /// </summary>
        [SprocName("usp_DictionaryDelete")]
        public abstract void Delete(string table_name, int id);


    }
}


