﻿using System;
using System.Data;
using BLToolkit.Data.Linq;
using BLToolkit.Data;
using BLToolkit.DataAccess;
using CommonGenerator.Moj.Tables;
using System.Linq;
using System.Collections.Generic;
using CommonGenerator.Moj.Enums;
using CommonGenerator.Moj;
using CommonGenerator.Moj.Models;
using CommonGenerator;
using AutoMapper;


namespace DataGenerator.Moj.MojBuilders
{
    public class MojDataBuilder : IDisposable
    {
        #region Fields

        private readonly MojDbManager _moj;
        private readonly DictionaryDataAccessor _dda;
        private readonly TasksDataAccessor _tda;
        private readonly MagCommonDataAccessor _mcda;

        public const string DefaultPhoneNumber = "79131111111";

        public const TaskTypeControl DefaultTypeControl = TaskTypeControl.All;
        public DateTime? DefaultDateBirth = new DateTime(1960, 3 ,12);
        public Sex? DefaultSex = CommonGenerator.Moj.Enums.Sex.Male;
        public string DefaultCountry = "Россия";
        public string DefaultNationality = "русский";
        public ExpertORD? DefaultORDExp = ExpertORD.Not;
        public string DefaultHavings =  "Открытое акционерное общество";
        public string DefaultSphere = "Торговля";
        
        public static DateTime DefaultDateTime = new DateTime(2015, 04, 01, 11, 55, 58);
        public static DateTime DefaultBeginDateTime = new DateTime(2015, 04, 01, 11, 45, 47);
        public static DateTime DefaultEndDateTime = new DateTime(2015, 04, 01, 11, 55, 58);

        #endregion


        
        public MojDataBuilder(MojDbManager mojDbManager)
        {
            if (mojDbManager == null) throw new ArgumentNullException("mojDbManager");

            _moj = mojDbManager;
            _dda  = DataAccessor.CreateInstance<DictionaryDataAccessor>(mojDbManager);
            _tda = DataAccessor.CreateInstance<TasksDataAccessor>(mojDbManager);
            _mcda = DataAccessor.CreateInstance<MagCommonDataAccessor>(mojDbManager);
            //установим англ.язык, иначе возникнут проблемы с сохранением/преобразованием даты в ХП
            _tda.SetLang();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void FullCleanUpMoj()
        {
            try
            {
                _moj.BeginTransaction();
             
                _tda.FullCleanUp();
                ////добавить дефолтное значение организации в sl_DivisionGuid с id=1
                ////требование для УЗ(управления заданиями) - при сохранении его параметров 
                //_moj
                //.SetCommand(@"
                //    SET IDENTITY_INSERT sl_DivisionGuid ON;
                //    INSERT INTO sl_DivisionGuid (id,title,caption)
                //    VALUES (
                //        1,
                //        'default',
                //        'default'
                //    );
                //    SET IDENTITY_INSERT sl_DivisionGuid OFF;").ExecuteNonQuery();
                
                _moj.CommitTransaction();

            }
            catch (Exception ex)
            {
                _moj.RollbackTransaction();
                throw new Exception("Ошибка при очистке базы MOJ " + ex.Message);
            }
        }



        public void Update<T>(T dto)
        {
            _moj.Update(dto);
        }




        #region Task

        ////основные атрибуты задания
        //public TaskMoj CreateTaskMoj(DateTime? taskBegin, int taskSrok, string taskName, Categories categoryName)
        //{
        //    return CreateTaskMoj(taskBegin, taskSrok, taskName, categoryName, "", "", "", DataTypes.Unsigned, new List<TaskTypes>(), SecurityLevel.Normal);
        //}

        //таск без инициатора
       
        public TaskMoj CreateTaskMoj(string otdel,
            string meropr, string vid_object, string task_number, string task_year,
            DateTime? beginDate, DateTime? endDate, string phone_otm, string IMSI, string IMEI, CommonGenerator.Mtb.Enums.Categories taskCategory, SecurityLevel sec_level,
            string number_blank, string where_act, short? long_act, string delo, string delo_num, List<TaskTypeControl> type_controls, Guid task_guid)
        {

            return CreateTaskMoj(otdel, "", "", "", "", "",
             meropr, vid_object, task_number, task_year,
             beginDate, endDate, phone_otm, IMSI, IMEI, taskCategory, sec_level,
             number_blank, where_act, long_act, delo, delo_num, type_controls);
        }


        //общий случай
        public TaskMoj CreateTaskMoj(string otdel, string init_organization, string init_podrazd, string init_Famini, string init_phoneA, string init_phoneB, 
            string meropr, string vid_object, string task_number, string task_year,
            DateTime? beginDate, DateTime? endDate, string phone_otm, string IMSI, string IMEI, CommonGenerator.Mtb.Enums.Categories taskCategory, SecurityLevel sec_level,
            string number_blank, string where_act, short? long_act, string delo, string delo_num, List<TaskTypeControl> type_controls)
        {

            if (string.IsNullOrWhiteSpace(otdel))
            {
                throw new Exception("Невозможно создать таск с пустым значением отдела");
            }

            TaskMoj task = TaskMoj.CreatePrototype();
            
            task.otd_id =  GetDictionaryIdByValue(DictType.sl_DivisionGuid, otdel) ?? task.otd_id;
            try
            {

                if (init_Famini != "")
                {
                    Initiator _init = AddInitiator(init_organization, init_podrazd, init_Famini, init_phoneA, init_phoneB);
                    task.ini_id = _init.ini_id;
                }
                
            }
            catch
            {
                //task.ini_id = null
            }
            
            task.mer_id = string.IsNullOrWhiteSpace(meropr) ? null : GetDictionaryIdByValue(DictType.sl_Meropr, meropr);
            task.obj_id = string.IsNullOrWhiteSpace(vid_object) ? null : GetDictionaryIdByValue(DictType.sl_Object, vid_object);
            task.task_number = task_number;
            task.task_year = task_year;
            task.alias_obj = String.Format(@"{0}-{1}-{2}-{3}" ,meropr,vid_object,task_number,task_year);
            task.number_blank = number_blank;
            task.take_date = (DateTime)beginDate;
            task.begin_date = beginDate;
            task.end_date = endDate;
            task.where_act_id = string.IsNullOrWhiteSpace(where_act) ? null : GetDictionaryIdByValue(DictType.sl_Where_act, where_act);
            task.long_act = long_act;
            //для справочника указываем caption - УД
            task.delo_id = string.IsNullOrWhiteSpace(delo) ? null : GetDictionaryIdByValue(DictType.sl_Delo, delo, "УД");
            task.delo_num = delo_num;
            task.delo_alias = delo_num;
            task.phone_otm = phone_otm;
            task.IMEI = IMEI;
            task.IMSI = IMSI;
            task.category_id = (int)taskCategory;
            task.PrivacyLevelId = (byte)sec_level;


            //виды сеансов, связанных с заданием
            if (type_controls.Count > 0)
            {
                int result_ttype_id = 0;
                foreach (var tt in type_controls)
                {
                    if (tt == TaskTypeControl.All)
                        continue;
                    result_ttype_id += (int)tt;
                }
                task.type_control_id = result_ttype_id;
            }
            //все виды сеансов, если пустой список
            else
                task.type_control_id = (int)TaskTypeControl.All;


            try
            {
                _tda.AddTask(task);
                _moj.TasksCheckSystems.Insert(() => new TaskCheckSystem()
                {
                    checksystem_id = (int)GetDictionaryIdByValue(DictType.sl_checksystem, "МАГ", "MAG"),
                    task_id = task.task_id
                });
                //предполагаем, что группа обработки присутсвует в БД
                _moj.TasksGroupProcessing.Insert(() => new TasksGroupProcessing()
                {
                    GroupProcessingId = 1,
                    task_id = task.task_id
                });


                //_moj.TasksGuids.Insert(() => new TasksGuid()
                //{
                //    task_id = task.task_id,
                //    task_guid = task_guid
                //});

            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при добавлении нового задания " + ex.Message);
            }

            return task;
        }


        public void UpdateTask(TaskMoj task)
        {

            try
            {
                _moj.Tasks.InsertOrUpdate(() => task, (x) => task);
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при обновлении данных о задании " + ex.Message);
            }
           

        }


        public TaskMoj GetTaskById(int Id)
        {
            return _moj.Tasks.Where(x => x.task_id == Id).FirstOrDefault();

        }

        public Guid GetTaskGuidById(int id)
        {
            return _moj.TasksGuids.Where(x => x.task_id == id).Select(x=> x.task_guid).FirstOrDefault();
        }

        public TaskMoj GetTaskByGuid(Guid guid)
        {
            var task_id = _moj.TasksGuids.Where(tg => tg.task_guid == guid).Select(tg => tg.task_id).FirstOrDefault();
            if (task_id > 0)
            {
                return GetTaskById(task_id);
            }
            else
                return null;

        }




        public ControlObjectTypeId GetControlObjectTypeByTask(TaskMoj task, string controlObject)
        {
            int c_object_type = _moj.TaskControlObjects.Where(x => x.TaskId == task.task_id && x.ControlObjectID == controlObject).Select(x => x.ControlObjectIDTypeId).FirstOrDefault();
            if (c_object_type > 0)
            {
                return (ControlObjectTypeId)c_object_type;
            }
            else
                throw new Exception("Ошибка при получении типа контролируемого объекта "+ task.alias_obj + " "+ controlObject);

        }


        public TaskMoj GetTaskByShifrParts(string meropr, string vid_object, string task_number, string task_year)
        {
            return _tda.GetTaskByShifrParts(meropr, vid_object, task_number, task_year);
        }

        #endregion


        #region Initiators

        public Initiator AddInitiator(string organization, string podrazd, string Famini, string phoneA, string phoneB)
        {
            var init = Initiator.CreatePrototype();
            init.organ_id = string.IsNullOrWhiteSpace(organization) ? null : GetDictionaryIdByValue(DictType.sl_DivisionGuid, organization);
            init.podrazd_id = string.IsNullOrWhiteSpace(podrazd) ? null : GetDictionaryIdByValue(DictType.sl_Podrazd, podrazd);
            init.fam_id = string.IsNullOrWhiteSpace(Famini) ? null : GetDictionaryIdByValue(DictType.sl_FamIni, Famini);
            init.phoneA = phoneA;
            init.phoneB = phoneB;

            init.ini_id = InsertWithIdentity(init);
            return init;
        }

        public Initiator GetInitiatorById(int id)
        {
            return _moj.Initiators.Where(x => x.ini_id == id).FirstOrDefault();
        }


        public string InitiatorFamily(Initiator init)
        {
            if (init.fam_id == null)
                throw new Exception("невозможно получить инициатора по пустому id = null");
            return GetDictionaryValueById(DictType.sl_FamIni, (int)init.fam_id);
        }

        #endregion



        #region Физическое лицо

        public Phiz AddPhiz(string surname, string name, string otch)
        {
            return AddPhiz(surname, name, otch, DefaultDateBirth, DefaultSex, DefaultORDExp);
        }

        //основные атрибуты, доп. по дефолту (см. Phiz.CreatePrototype)
        public Phiz AddPhiz(string surname, string name, string otch, DateTime? dateBirth, Sex? sex, ExpertORD? ord_exp)
        {

            var phiz = Phiz.CreatePrototype();
            phiz.family_id  = string.IsNullOrWhiteSpace(surname) ? null : GetDictionaryIdByValue(DictType.sl_Family, surname);
            phiz.name_id = string.IsNullOrWhiteSpace(name) ? null : GetDictionaryIdByValue(DictType.sl_Name, name);
            phiz.otch_id = string.IsNullOrWhiteSpace(otch) ? null : GetDictionaryIdByValue(DictType.sl_Otch, otch);
            phiz.date_birth = dateBirth;
            phiz.sex_id = sex == null ? (int)Sex.Male : (int)sex;
            phiz.alias = string.Format("{0} {1} {2}", surname ?? "", name ?? "", otch ?? "");
            phiz.expertORD_id = ord_exp == null ? (int)ExpertORD.Not : (int)ord_exp;
            phiz.fio = phiz.alias;
            phiz.FioFull = phiz.alias;
            phiz.ob_full_fio = phiz.alias;

            phiz.phiz_id = InsertWithIdentity(phiz);

            return phiz;
        }


        //привязка физ.лица к заданию
        //public void Phiz2Task(Phiz phiz, TaskMoj task)
        //{
           
        //    try
        //    {
        //        _moj.BeginTransaction();

        //        //dt_ObjectCode
        //        var _obj_code = _moj.ObjectCodes.InsertWithIdentity(() => new ObjectCode()
        //        {
        //            object_type_id = (int)TaskObjectType.Phiz,
        //            person_id = phiz.phiz_id,
        //            date_create = DateTime.Now,
        //            object_guid = Guid.NewGuid()
        //        });

        //        int _obj_code_id = Convert.ToInt32(_obj_code);

        //        //dt_TasksObject

        //        var _t_obj = _moj.TasksObjects.Insert(() => new TasksObject()
        //        {
        //            task_id = task.task_id,
        //            type_task_object_id = (int)TaskObjectType.Phiz,
        //            object_id = _obj_code_id
        //        });
                
        //        _moj.CommitTransaction();

        //    }
        //    catch (Exception ex)
        //    {
        //        _moj.RollbackTransaction();
        //        throw new Exception("Ошибка при связывании физ.лица с заданием " + ex.Message);
        //    }



        //}


        
        //
        public bool PhizLinkedToTask(Phiz phiz, TaskMoj task)
        {

            if (phiz == null)
                throw new Exception("Не указано физическое лицо, привязку которого необходимо проверить");
            if (task == null)
                throw new Exception("Не указано задание, привязку которого необходимо проверить");
            
            var obj_code = _moj.ObjectCodes.Where(x => x.object_type_id == (int)TaskObjectType.Phiz && x.person_id == phiz.phiz_id).Select(x => x.object_id).FirstOrDefault();

            if (obj_code == 0)
                return false;

            var task_obj = _moj.TasksObjects.Where(x => x.task_id == task.task_id && x.type_task_object_id == (int)TaskObjectType.Phiz && x.object_id == obj_code).Select(x => x.task_object_id).FirstOrDefault();

            if (task_obj == 0)
                return false;


            return true;
        }


        #endregion



        #region Юридическое лицо(место работы физ.лица)

        public Jur AddJur(string name)
        {
            return AddJur(name, DefaultHavings, DefaultSphere);
        }


        public Jur AddJur(string name, string havings, string shpere)
        {

            var jur = Jur.CreatePrototype();
            jur.short_name = name;
            jur.full_name = name;
            jur.havings_id = string.IsNullOrWhiteSpace(havings) ? null : GetDictionaryIdByValue(DictType.sl_Havings, havings);
            jur.sphere_id = string.IsNullOrWhiteSpace(shpere) ? null : GetDictionaryIdByValue(DictType.sl_Sphere, shpere);

            jur.jur_id = InsertWithIdentity(jur);

            return jur;
        }



        //привязка физ.лица, а также сведений о его месте работы и проживания к заданию
        public void LinkPhizWithAdditionalInfo2Task(BaseTaskObject phiz, TaskMoj task)
        {

            try
            {
                _moj.BeginTransaction();

                int object_type_id = 0;
                int person_id = 0;


                if (phiz is Phiz)
                {
                    object_type_id = (int)TaskObjectType.Phiz;
                    person_id = (phiz as Phiz).phiz_id;
                }
                else if (phiz is Jur)
                {
                    object_type_id = (int)TaskObjectType.Yur;
                    person_id = (phiz as Jur).jur_id;
                }
                else if (phiz is PhizAddr)
                {
                    object_type_id = (int)TaskObjectType.Phiz_Addr;
                    person_id = (phiz as PhizAddr).adr_id;
                }
                else if (phiz is JurAddr)
                {
                    object_type_id = (int)TaskObjectType.Yur_Addr;
                    person_id = (phiz as JurAddr).adr_id;
                }



                //dt_ObjectCode
                var _obj_code = _moj.ObjectCodes.InsertWithIdentity(() => new ObjectCode()
                {
                    object_type_id = object_type_id,
                    person_id = person_id,
                    date_create = DateTime.Now,
                    object_guid = Guid.NewGuid()
                });

                int _obj_code_id = Convert.ToInt32(_obj_code);

                //dt_TasksObject

                var _t_obj = _moj.TasksObjects.Insert(() => new TasksObject()
                {
                    task_id = task.task_id,
                    type_task_object_id = object_type_id,
                    object_id = _obj_code_id
                });

                _moj.CommitTransaction();

            }
            catch (Exception ex)
            {
                _moj.RollbackTransaction();
                throw new Exception("Ошибка при связывании физ.лица/его места работы/адресов с заданием " + ex.Message);
            }



        }


        //public Jur GetJurLinkedToTask(TaskMoj task)
        //{
        //    if (task == null)
        //        throw new Exception("Не указано задание, по которому необходимо получить информацию о юр.лице(месте работы)");

        //    var obj_id = _moj.TasksObjects.Where(x => x.task_id == task.task_id && x.type_task_object_id == (int)TaskObjectType.Yur).Select(x => x.object_id).FirstOrDefault();

        //    if (obj_id == 0)
        //        return null;


        //    var jur_id = _moj.ObjectCodes.Where(x => x.object_id == obj_id).Select(x => x.person_id).FirstOrDefault();

        //    //если по каким-то причинам запись о юридическом лице с полученным id отсутствует(что нарушает целостность БД)
        //    if (jur_id == 0)
        //        throw new Exception("Нарушение целостности хранения данных в БД, при получении записи о юридическом лице по заданию "+task.alias_obj);

        //    //нет нарушения целостности, т.к. person_id - не внешний ключ
        //    return  _moj.Jurs.Where(x => x.jur_id == jur_id).Select(x => x).FirstOrDefault();
        
        //}

      
        public BaseTaskObject GetPhizYurAddrObjectLinkedToTask(TaskMoj task, TaskObjectType _t_type)
        {
            if (task == null)
                throw new Exception("Не указано задание, по которому необходимо получить информацию о юр.лице(месте работы)");

            var obj_id = _moj.TasksObjects.Where(x => x.task_id == task.task_id && x.type_task_object_id == (int)_t_type).Select(x => x.object_id).First();

            if (obj_id == 0)
                return null;
            
            var t_obj_id = _moj.ObjectCodes.Where(x => x.object_id == obj_id).Select(x => x.person_id).FirstOrDefault();
            
            //если по каким-то причинам запись о физ. лице с полученным id отсутствует(что нарушает целостность БД)
            if (t_obj_id == 0)
                throw new Exception("Нарушение целостности хранения данных в БД, при получении записи о физическом лице или его доп. атрибутах по заданию " + task.alias_obj);


            BaseTaskObject _t_o = null;

            //нет нарушения целостности, т.к. person_id - не внешний ключ
            switch (_t_type)
            {
                case TaskObjectType.Phiz_Addr:
                    _t_o = _moj.Addrs.Where(x => x.adr_id == t_obj_id).Select(x => x).FirstOrDefault();
                    break;
                case TaskObjectType.Yur_Addr:
                    _t_o = _moj.Addrs.Where(x => x.adr_id == t_obj_id).Select(x => x).FirstOrDefault();
                    break;
                case TaskObjectType.Yur:
                    _t_o =  _moj.Jurs.Where(x => x.jur_id == t_obj_id).Select(x => x).FirstOrDefault();
                    break;
                case TaskObjectType.Phiz:
                    _t_o = _moj.Phizs.Where(x => x.phiz_id == t_obj_id).Select(x => x).FirstOrDefault();
                    break;
            }

            return _t_o;

            
        }




        #endregion


        #region Адрес физ/юр лица

        public Addr AddAddr(string region, string section, string town, string street, string korp, string flat, string building, TaskObjectType task_obj_type)
        {

            if (task_obj_type != TaskObjectType.Phiz_Addr && task_obj_type != TaskObjectType.Yur_Addr)
            {
                throw new Exception("Не указан корректный тип адреса при добавлении адреса");
            }

            Addr addr = Addr.CreatePrototype();

            
            addr.region_id = string.IsNullOrWhiteSpace(region) ? null : GetDictionaryIdByValue(DictType.sl_Region, region);
            addr.section_id = string.IsNullOrWhiteSpace(section) ? null : GetDictionaryIdByValue(DictType.sl_Section, section);
            addr.town_id = string.IsNullOrWhiteSpace(town) ? null : GetDictionaryIdByValue(DictType.sl_Town, town);
            addr.street_id = string.IsNullOrWhiteSpace(street) ? null : GetDictionaryIdByValue(DictType.sl_Street, street);
            addr.korp = korp;
            addr.flat = flat;
            addr.building = building;
            addr.AddressFull = String.Format("{0} {1} {2} {3} {4} {5} {6}", region, section, town, street, korp, flat, building);

            addr.adr_id = InsertWithIdentity(addr);

            if (task_obj_type == TaskObjectType.Phiz_Addr)
            {
                Mapper.Initialize(cfg =>cfg.CreateMap<Addr, PhizAddr>());
                PhizAddr phiz_addr = Mapper.Map<Addr, PhizAddr>(addr);
                return phiz_addr;
            }
            else if (task_obj_type == TaskObjectType.Yur_Addr)
            {
                Mapper.Initialize(cfg =>cfg.CreateMap<Addr, JurAddr>());
                JurAddr jur_addr = Mapper.Map<Addr, JurAddr>(addr);
                return jur_addr;
            }

            return addr;


            
        }


        #endregion


        #region Orientation(ориентировка)

        public TasksOrient AddOrientation(TaskMoj task, string orient)
        {
            return AddOrientation(task, "", DefaultCountry, DefaultNationality, orient, "", "");
        }


        public TasksOrient AddOrientation(TaskMoj task, string crimact, string citizenship, string nationality, string orient, string crimcategory, string relationship_charact)
        {

            var _orient = TasksOrient.CreatePrototype();
            _orient.task_id = task.task_id;
            _orient.crimact_id  = !string.IsNullOrWhiteSpace(crimact) ? GetDictionaryIdByValue(DictType.sl_Crimact, crimact) : _orient.crimact_id;
            _orient.citizenship_id = !string.IsNullOrWhiteSpace(citizenship) ? GetDictionaryIdByValue(DictType.sl_Citizenship, citizenship) : _orient.citizenship_id;
            _orient.nationality_id = !string.IsNullOrWhiteSpace(nationality) ? GetDictionaryIdByValue(DictType.sl_Nationality, nationality) : _orient.nationality_id;
            _orient.orient = orient;
            _orient.crimcategory_id = !string.IsNullOrWhiteSpace(crimcategory) ? GetDictionaryIdByValue(DictType.sl_CrimCategories, crimcategory) : _orient.crimcategory_id;
            _orient.relationship_charact_id = !string.IsNullOrWhiteSpace(relationship_charact) ? GetDictionaryIdByValue(DictType.sl_RelationshipCharact, relationship_charact) : _orient.relationship_charact_id;


            try
            {
                _moj.BeginTransaction();
                _tda.SetTaskOrient( _orient);
                _moj.CommitTransaction();

            }
            catch (Exception ex)
            {
                _moj.RollbackTransaction();
                throw new Exception("Ошибка при добавлении ориентировки по заданию " + task.alias_obj + " " + ex.Message);
            }
            
            return _orient;
        }


        public bool OrientLinkedToTask(string orient, TaskMoj task)
        {

            if (orient == null)
                throw new Exception("Не указана ориентировка, привязку которой необходимо проверить");
            if (task == null)
                throw new Exception("Не указано задание, привязку которого необходимо проверить");


            var task_id = _moj.TasksOrients.Where(x => x.task_id == task.task_id &&  x.orient.Contains(orient)).Select(x => x.task_id).FirstOrDefault();

            if (task_id == 0)
                return false;
            
            return true;

        }


        #endregion


        #region TaskTarget
        // sl_PreparedTarget - цели проведения мероприятия (дефолтные)
        public TasksTarget AddTaskTarget(TaskMoj task)
        {
            //любое значение из справочника дефолтных вариантов
            Random _rand = new Random();
            var def_value = _rand.Next(1, Enum.GetValues(typeof(TaskTarget)).Length+1);
            string  target = Utils.GetEnumDescription((TaskTarget)def_value);

            return AddTaskTarget(task, target);
        }

        public TasksTarget AddTaskTarget(TaskMoj task, string target)
        {

            var t_target = TasksTarget.CreatePrototype();
            t_target.task_id = task.task_id;
            t_target.target = target;

            try
            {
                _moj.BeginTransaction();
                _tda.SetTaskTarget(t_target);
                _moj.CommitTransaction();

            }
            catch (Exception ex)
            {
                _moj.RollbackTransaction();
                throw new Exception("Ошибка при добавлении цели по заданию " + task.alias_obj + " " + ex.Message);
            }
            
            return t_target;
        }


        public TasksTarget SelectTaskTarget(TaskMoj task)
        {
            if (task == null)
                throw new Exception("Не указан таск, по которому необходимо узнать цель");

            string target = _moj.TasksTargets.Where(x => x.task_id == task.task_id).Select(x => x.target).FirstOrDefault();

            TasksTarget t_target = new TasksTarget();
            t_target.task_id = task.task_id;
            t_target.target = target;

            return t_target;
        }

        #endregion


        #region Users

        /// <summary>
		/// Создание пользователя в БД Учета
		/// </summary>
		/// <param name="login"></param>
		/// <param name="name"></param>
		/// <param name="password"></param>
		/// <param name="secLevel"></param>
		/// <param name="isMinSecLevel"></param>
		/// <param name="isAccUser"></param>
		public void CreateOrUpdateMojUser(string login, string name, string pass, bool isAccUser, bool isMinSecLevel =true)
        {
            // Из имени вычисляются части
            var nameParts = (name ?? "").Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray();
            var pFam = 0 < nameParts.Length ? nameParts[0] : "";
            var pNam = 1 < nameParts.Length ? nameParts[1] : "";
            var pOtch = 2 < nameParts.Length ? nameParts[2] : "";

           
            var secLevel = isMinSecLevel ? 3 : 1;


            _tda.SetDBLogin(login, pass);
            _tda.SetMOJUser(login);
            _tda.AddMag8Role(login);


            if (!isAccUser)
            {
                return;
            }
            else
            {
                _tda.AddMagRole(login);
            }


            // Поиск в БД пользователя с таким же логином
            var man = (_tda.PersonWorkManSearch(login) ?? new List<PersonDbo>(0)).FirstOrDefault();
            if (null != man)
            {
                // Пользователь есть. Проверка нужно ли у него чего то менять
                if ((man.Fam != pFam) || (man.Nam != pNam) || (man.Otch != pOtch) || (man.PrivacyLevelId != secLevel))
                {
                    // Нужно менять и меняются части имени
                    _tda.PersonWorkManUpdate(man.Id, man.Login, null == man.OtdelId ? -1 : man.OtdelId.Value, man.RoleId, pFam, pNam, pOtch, (byte)secLevel);
                }
                return;
            }

            // Пользователя в БД еще нет
            const string title = "Автопользователи ЦУП";
            const string titleOtd = "Автоотдел ЦУП";

            // Поиск роли для новых пользователей
            var role = (_tda.RoleSearch() ?? new List<RoleDbo>(0)).FirstOrDefault(r => r.Title == title);
            if (null == role)
            {
                // Роли еще нет и она создается
                role = new RoleDbo
                {
                    Id = _tda.RoleAdd(title, "Права назначаемые пользователям создаваемым в автоматическом режиме из ЦУП")
                };
                _tda.FillUmRoleDefaults(role.Id);
            }
            // Поиск отдела для новых пользователей
            var otdel = (_tda.OtdelSearch() ?? new List<OtdelDbo>(0)).FirstOrDefault(r => r.Title == titleOtd);
            if (null == otdel)
            {
                // Роли еще нет и она создается
                otdel = new OtdelDbo
                {
                    Id = _tda.OtdelAdd(titleOtd)
                };
            }

            // Добавление нового пользователя
            int id;
            _tda.PersonWorkManAdd(out id, login, otdel.Id, role.Id, pFam, pNam, pOtch, (byte)secLevel);
            if (0 >= id)
            {
                throw new ApplicationException("PersonWork not added. Invalid login: " + login);
            }
        }

        #endregion

        #region  Dictionary
        //возвращает существующее значение из словаря или создает новое 
        public int? GetDictionaryIdByValue(DictType d_type, string value, string caption ="")
        {
            DictionaryItem d_item = null;

            try
            {
                _moj.BeginTransaction();
                d_item =  string.IsNullOrWhiteSpace(caption) ?  new DictionaryItem()
                {
                    Title = value
                }: new DictionaryItem()
                {
                    Title = value,
                    Caption = caption
                };

                _dda.Set(Enum.GetName(typeof(DictType), d_type), d_item);
                _moj.CommitTransaction();
                
            }
            catch (Exception ex)
            {
                _moj.RollbackTransaction();
                throw new Exception("Ошибка при добавлении значений в словарь "+ Enum.GetName(typeof(DictType), d_type) + " " + ex.Message);
            }

            return d_item.ID;

        }


        public string GetDictionaryValueById(DictType d_type, int dict_id)
        {
            string value = "";

            using (IDataReader _dr =  _dda.GetRaw(Enum.GetName(typeof(DictType), d_type), dict_id))
            {
                while (_dr.Read())
                {
                    value = (string)_dr["title"];
                }
                 
            }

            return value;
           
        }


        #endregion




        public Dictionary<string,string> GetTasksWithControlPhones(List<string> guid_list)
        {
           return _tda.GetTasksControlPhones(String.Join(";", guid_list));
        }


        private int InsertWithIdentity<T>(T tableRowDto)
        {
            return Convert.ToInt32(_moj.InsertWithIdentity(tableRowDto));
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
                _moj.Dispose();
        }

        ~MojDataBuilder()
        {
            Dispose(false);
        }
    }
}