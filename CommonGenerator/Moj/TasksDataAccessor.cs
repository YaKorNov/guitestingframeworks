﻿using System;
using System.Collections.Generic;
using System.Data;
using BLToolkit.DataAccess;
//using Signatec.Mag.Abonents.Model.Domain;
using CommonGenerator.Moj.Models;
using CommonGenerator.Moj.Tables;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;




namespace CommonGenerator.Moj
{
   
    public abstract class TasksDataAccessor : DataAccessor<TaskMoj, TasksDataAccessor>
    {
        /// <summary>
        /// Добавить задание
        /// </summary>
        [SprocName("usp_TaskAdd")]
        public abstract void AddTask([Direction.Output("task_id")] TaskMoj task);


        [SqlQuery(@"set Language 'us_english'")]
        public abstract void SetLang();


        [SqlQuery(@"
                    select
                    TOP 1 *
                    from
                    dt_Tasks
                    inner join 
                    sl_Meropr on dt_Tasks.mer_id = sl_Meropr.id
                    inner join sl_Object on dt_Tasks.obj_id = sl_Object.id
                    where
                    task_number = @task_number
                    and task_year = @task_year
                    and sl_Meropr.title = @meropr
                    and sl_Object.title = @vid_object
                ;")]
        public abstract TaskMoj GetTaskByShifrParts(string meropr, string vid_object, string task_number, string task_year );


        [SqlQuery(@"INSERT INTO dt_TasksOrient(task_id, crimact_id, citizenship_id, nationality_id, orient, crimcategory_id, relationship_charact_id)
             VALUES(
                 @task_id,
                 @crimact_id,
                 @citizenship_id,
                 @nationality_id,
                 @orient,
                 @crimcategory_id,
                 @relationship_charact_id
             )")]
        public abstract void SetTaskOrient(TasksOrient _orient);


        [SqlQuery(@"INSERT INTO dt_TasksTarget(task_id, target)
             VALUES(
                 @task_id,
                 @target
             )")]
        public abstract void SetTaskTarget(TasksTarget t_target);




        [SqlQuery(@"select
            dt_TasksGUID.task_guid, dt_Tasks.phone_otm
            from
            dt_Tasks inner join
            dt_TasksGUID
            on dt_Tasks.task_id = dt_TasksGUID.task_id
            where dt_TasksGUID.task_guid in (select f.value from dbo.f_Playback_TextToListText(@guid_list) f)")]
        [Index("task_guid")]
        [ScalarFieldName("phone_otm")]
        public abstract Dictionary<string, string> GetTasksControlPhones(string guid_list);



            // "delete from dt_TasksGUID",
            //"delete from dt_TasksCipher",
            //"delete from dt_TasksCheckSystem",
            //"delete from dt_TasksControlObject",

            //"delete from dt_TasksTarget",
            //"delete from dt_TasksSanction",
            //"delete from dt_TasksObject",
            //"delete from dt_Tasks",
            //"delete from dt_ObjectCode",
            //"delete from dt_Phiz",

            //"delete from dt_Phiz",
            //"delete from sl_Family",
            //"delete from sl_Name",
            //"delete from sl_Otch",

            //"delete from dt_Initiator",
            //"delete from sl_DivisionGuid",
            //"delete from sl_FamIni",
            //"delete from sl_Podrazd",
            //"delete from dt_TasksGroupProcessing",
            //"delete from dt_GroupProcessing",

            //"delete from dt_TasksOrient",
            //"delete from sl_Citizenship",
            //"delete from sl_Nationality",
            //"delete from sl_Crimact",
            //"delete from sl_CrimCategories",
            //"delete from sl_RelationshipCharact"


        [SqlQuery(@" delete from dt_TasksGUID;
                    delete from dt_TasksCipher;
                    delete from dt_TasksCheckSystem;
                    delete from dt_TasksControlObject;
                    delete from dt_TasksTarget;
                    delete from dt_TasksSanction;
                    delete from dt_TasksObject;
                    delete from dt_Tasks;
                    delete from dt_ObjectCode;

                    delete from dt_Phiz;
                    
                    delete from dt_Initiator;
                    delete from sl_DivisionGuid;
                    SET IDENTITY_INSERT sl_DivisionGuid ON;
                    INSERT INTO sl_DivisionGuid (id,title,caption)
                    VALUES (
                        1,
                        'default',
                        'default'
                    );
                    SET IDENTITY_INSERT sl_DivisionGuid OFF;
                    delete from dt_TasksGroupProcessing;
                              
                    delete from dt_TasksOrient;")]
        public abstract void FullCleanUp();


        /// <summary>
        /// Получить пользователя Учета пол логину
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [SprocName("usp_PersonWorkManSearch")]
        public abstract List<PersonDbo> PersonWorkManSearch([ParamName("oper_login")]string login);



        /// <summary>
        /// Добавить пользователя Учета
        /// </summary>
        /// <param name="id"></param>
        /// <param name="login"></param>
        /// <param name="otdelId"></param>
        /// <param name="roleId"></param>
        /// <param name="fam"></param>
        /// <param name="nam"></param>
        /// <param name="otch"></param>
        /// <param name="privacyLevelId"></param>
        [SprocName("usp_PersonWorkManAdd")]
        public abstract void PersonWorkManAdd([ParamName("oper_id")]out int id, [ParamName("oper_login")]string login, [ParamName("otdel_id")]int otdelId, [ParamName("role_id")]int roleId
            , string fam, string nam, string otch, [ParamName("PrivacyLevelId")]byte privacyLevelId);

        /// <summary>
        /// Изменить пользователя Учета
        /// </summary>
        /// <param name="id"></param>
        /// <param name="login"></param>
        /// <param name="otdelId"></param>
        /// <param name="roleId"></param>
        /// <param name="fam"></param>
        /// <param name="nam"></param>
        /// <param name="otch"></param>
        /// <param name="privacyLevelId"></param>
        [SprocName("usp_PersonWorkManUpdate")]
        public abstract void PersonWorkManUpdate([ParamName("oper_id")]int id, [ParamName("oper_login")]string login, [ParamName("otdel_id")]int otdelId, [ParamName("role_id")]int? roleId
            , string fam, string nam, string otch, [ParamName("PrivacyLevelId")]byte privacyLevelId);



        /// <summary>
        /// Добавить группу в Учет
        /// </summary>
        /// <param name="title"></param>
        /// <param name="caption"></param>
        /// <returns></returns>
        [SprocName("usp_TemplateAdd")]
        public abstract int RoleAdd(string title, string caption);

        /// <summary>
        /// Получить список групп из Учета
        /// </summary>
        /// <returns></returns>
        [SprocName("usp_TemplateSearch")]
        public abstract List<RoleDbo> RoleSearch();



        /// <summary>
        /// Сразу после создания заполнить "спец группу" с которой связываются создаваемые пользователи нужными правами
        /// </summary>
        /// <param name="id"></param>
        [SprocName("usp_um_TemplateFillDefaults")]
        public abstract void FillUmRoleDefaults([ParamName("role_id")]int id);

        /// <summary>
        /// Список отделов
        /// </summary>
        /// <returns></returns>
        [SqlQuery("select id, title from sl_Doer")]
        public abstract List<OtdelDbo> OtdelSearch();

        /// <summary>
        /// Добавить отдел
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        [SqlQuery("insert into sl_Doer (title) values(@title); select SCOPE_IDENTITY();")]
        public abstract int OtdelAdd(string title);




        [SqlQuery(@" 
                USE master;
			    declare @q nvarchar(4000);
                IF EXISTS (SELECT * FROM sys.server_principals WHERE name = @login)
                BEGIN
					set @q = 'alter login ['+@login+'] with password = '''+@pass+''', default_database=[master], check_expiration=off, check_policy=off'
					exec ( @q )
                END 
				ELSE
				BEGIN
					set @q = 'create login ['+@login+'] with password = '''+@pass+''', default_database=[master], check_expiration=off, check_policy=off'
					exec ( @q )
				END;
                USE MagObjectsJournal;
                ")]
        public abstract void SetDBLogin(string login, string pass);



        [SqlQuery(@" 
            USE MagObjectsJournal;
            IF NOT EXISTS (SELECT * FROM dbo.sysusers where name = @login)
            BEGIN
			   declare @q nvarchar(4000) = 'create USER ['+@login+']  FOR LOGIN ['+@login+']'
			   exec ( @q )
            END;
			EXEC sp_addrolemember db_datareader, @login;
			EXEC sp_addrolemember db_datawriter, @login;
			EXEC sp_addrolemember mag_role, @login;
            ")]
        public abstract void SetMOJUser(string login);



        [SqlQuery(@" 
			EXEC sp_addrolemember mag_role, @login;
            ")]
        public abstract void AddMagRole(string login);


        [SqlQuery(@" 
			EXEC sp_addrolemember mag8_role, @login;
            ")]
        public abstract void AddMag8Role(string login);


    }

    public class PersonDbo
    {
        [MapField("oper_id")]
        public int Id { get; set; }
        [MapField("oper_login")]
        public string Login { get; set; }
        [MapField("otdel_id")]
        public int? OtdelId { get; set; }
        [MapField("fam")]
        public string Fam { get; set; }
        [MapField("nam")]
        public string Nam { get; set; }
        [MapField("otch")]
        public string Otch { get; set; }
        [MapField("role_id")]
        public int? RoleId { get; set; }

        public byte PrivacyLevelId { get; set; }
    }

    public class RoleDbo
    {
        [MapField("id")]
        public int Id { get; set; }
        [MapField("title")]
        public string Title { get; set; }
        [MapField("caption")]
        public string Caption { get; set; }
    }

    public class OtdelDbo
    {
        [MapField("id")]
        public int Id { get; set; }
        [MapField("title")]
        public string Title { get; set; }
    }

}