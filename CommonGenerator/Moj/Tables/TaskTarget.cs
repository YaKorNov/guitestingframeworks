﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.Moj.Tables
{
    [TableName(Name = "dt_TasksTarget")]
    public partial class TasksTarget
    {
        [PrimaryKey(1)]
        public int task_id { get; set; }
        [Nullable]
        public string target { get; set; }


        public static TasksTarget CreatePrototype()
        {
            return new TasksTarget()
            {
                task_id = -1,
                target = ""
            };
        }

    }
}




