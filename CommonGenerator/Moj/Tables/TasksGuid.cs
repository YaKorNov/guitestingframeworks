﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.Moj.Tables
{
    [TableName(Name = "dt_TasksGUID")]
    public class TasksGuid
    {
        [PrimaryKey(1)]
        public int task_id { get; set; }
        public Guid task_guid { get; set; }

        // FK_dt_TasksGUID_dt_Tasks
        [Association(ThisKey = "task_id", OtherKey = "task_id")]
        public TaskMoj dtTasksGUIDdtTasks { get; set; }
    }
}
