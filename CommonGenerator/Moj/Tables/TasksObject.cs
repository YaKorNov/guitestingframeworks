﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.Moj.Tables
{

    [TableName(Name = "dt_TasksObject")]
    public partial class TasksObject
    {
        [Identity, PrimaryKey(1)]
        public int task_object_id { get; set; }
        public int task_id { get; set; }
        public int type_task_object_id { get; set; }
        public int object_id { get; set; }

    }

}
