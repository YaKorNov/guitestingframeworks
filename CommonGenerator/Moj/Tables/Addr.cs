﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.Moj.Tables
{

    [TableName(Name = "dt_Adr")]
    public partial class Addr : BaseTaskObject
    {
        [Identity, PrimaryKey(1)]
        public int adr_id { get; set; }
        [Nullable]
        public int? region_id { get; set; }
        [Nullable]
        public int? section_id { get; set; }
        [Nullable]
        public int? town_id { get; set; }
        [Nullable]
        public int? street_id { get; set; }
        [Nullable]
        public string building { get; set; }
        [Nullable]
        public string korp { get; set; }
        [Nullable]
        public string flat { get; set; }
        public DateTime DateUpdate { get; set; }
        [Nullable]
        public string AddressFull { get; set; }


        public static Addr CreatePrototype()
        {
            return new Addr()
            {
                adr_id = -1,
                region_id = null,
                section_id = null,
                town_id = null,
                street_id = null,
                building = "",
                korp = "",
                flat = "",
                DateUpdate = DateTime.Now,
                AddressFull = ""
                
            };
        }
    }


    public class PhizAddr : Addr
    {

    }

    public class JurAddr : Addr
    {

    }






}
