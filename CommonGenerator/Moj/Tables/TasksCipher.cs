﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.Moj.Tables
{
    [TableName(Name = "dt_TasksCipher")]
    public class TasksCipher
    {
        [PrimaryKey(1)]
        public int task_id { get; set; }
        [Nullable               ] public string task_cipher { get; set; }
        public int    otd_id      { get; set; }

        // FK_dt_TasksCipher_dt_Tasks
        [Association(ThisKey="task_id", OtherKey="task_id")]
        public TaskMoj dtTasksCipherdtTasks { get; set; }
    }
}




