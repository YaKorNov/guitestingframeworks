﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.Moj.Tables
{
   
    [TableName(Name = "dt_Jur")]
    public partial class Jur : BaseTaskObject
    {
        [Identity, PrimaryKey(1)]
        public int jur_id { get; set; }
        [Nullable]
        public int? havings_id { get; set; }
        [Nullable]
        public int? sphere_id { get; set; }
        [Nullable]
        public string short_name { get; set; }
        [Nullable]
        public string full_name { get; set; }
        [Nullable]
        public DateTime? DateUpdate { get; set; }

       
        public static Jur CreatePrototype()
        {

            return new Jur()
            {
                jur_id = -1,
                havings_id = null,
                sphere_id = null,
                short_name = "",
                full_name = "",
                DateUpdate = DateTime.Now
            };
        }

    }
}
