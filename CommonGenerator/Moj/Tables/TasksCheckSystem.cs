﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.Moj.Tables
{
    [TableName(Name = "dt_TasksCheckSystem")]
    public class TasksCheckSystem
    {
        [PrimaryKey(1)] public int task_id        { get; set; }
        [PrimaryKey(2)] public int checksystem_id { get; set; }

        // FK_dt_TasksCheckSystem_dt_Tasks
        [Association(ThisKey="task_id", OtherKey="task_id")]
        public TaskMoj dtTasksCheckSystemdtTasks { get; set; }

    }
}


