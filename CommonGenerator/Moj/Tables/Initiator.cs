﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.Moj.Tables
{

    [TableName(Name = "dt_Initiator")]
    public class Initiator
    {
        		[Identity, PrimaryKey(1)] public int      ini_id      { get; set; }
        		[Nullable               ] public int?     organ_id    { get; set; }
        		[Nullable               ] public int?     podrazd_id  { get; set; }
        		[Nullable               ] public int?     fam_id      { get; set; }
        		[Nullable               ] public string   phoneA      { get; set; }
        		[Nullable               ] public string   phoneB      { get; set; }
        		                          public DateTime date_create { get; set; }
        		                          public byte     IsArchiv    { get; set; }

        public static Initiator CreatePrototype()
        {
            return new Initiator()
            {
                ini_id = -1,
                organ_id = null,
                podrazd_id = null,
                fam_id = null,
                phoneA  = "89132347846",
                phoneB = "89131357846",
                date_create = DateTime.Now,
                IsArchiv = 0
            };
        }

    }



   
}
