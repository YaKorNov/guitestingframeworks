﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.Moj.Tables
{

    [TableName(Name = "dt_ObjectCode")]
    public partial class ObjectCode
    {
        [Identity, PrimaryKey(1)]
        public int object_id { get; set; }
        public int object_type_id { get; set; }
        public int person_id { get; set; }
        public DateTime date_create { get; set; }
        public Guid object_guid { get; set; }

    }

}
