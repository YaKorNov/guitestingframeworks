﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.Moj.Tables
{
    [TableName(Name = "dt_TasksOrient")]
    public partial class TasksOrient
    {
        [PrimaryKey(1)]
        public int task_id { get; set; }
        [Nullable]
        public int? crimact_id { get; set; }
        [Nullable]
        public int? citizenship_id { get; set; }
        [Nullable]
        public int? nationality_id { get; set; }
        [Nullable]
        public string orient { get; set; }
        [Nullable]
        public int? crimcategory_id { get; set; }
        [Nullable]
        public int? relationship_charact_id { get; set; }


        public static TasksOrient CreatePrototype()
        {
            return new TasksOrient()
            {
                task_id = -1,
                crimact_id = null,
                citizenship_id = null,
                nationality_id = null,
                orient = null,
                crimcategory_id = null,
                relationship_charact_id = null
            };
        }


    }
}






