﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using CommonGenerator.Moj.Enums;


namespace CommonGenerator.Moj.Tables
{
    [TableName(Name = "dt_Tasks")]
    public class TaskMoj
    {
        [Identity, PrimaryKey(1)]
        public int task_id { get; set; }
        public int otd_id { get; set; }
        [Nullable]
        public int? ini_id { get; set; }
        [Nullable]
        public int? mer_id { get; set; }
        [Nullable]
        public int? obj_id { get; set; }
        [Nullable]
        public string task_number { get; set; }
        [Nullable]
        public string task_year { get; set; }
        public DateTime take_date { get; set; }
        [Nullable]
        public DateTime? begin_date { get; set; }
        [Nullable]
        public DateTime? begin_time { get; set; }
        [Nullable]
        public DateTime? end_date { get; set; }
        [Nullable]
        public DateTime? end_time { get; set; }
        [Nullable]
        public string number_blank { get; set; }
        [Nullable]
        public int? where_act_id { get; set; }
        [Nullable]
        public short? long_act { get; set; }
        [Nullable]
        public string phone_otm { get; set; }
        [Nullable]
        public int? delo_id { get; set; }
        [Nullable]
        public string delo_num { get; set; }
        [Nullable]
        public string delo_alias { get; set; }
        [Nullable]
        public string alias_obj { get; set; }
        public bool join_opu { get; set; }
        public DateTime date_create { get; set; }
        [Nullable]
        public int? doer_id { get; set; }
        [Nullable]
        public int? type_control_id { get; set; }
        public byte is_task { get; set; }
        [Nullable]
        public byte? task_type { get; set; }
        public byte ReadyReceive { get; set; }
        [Nullable]
        public string task_mn { get; set; }
        [Nullable]
        public string IMSI { get; set; }
        [Nullable]
        public string IMEI { get; set; }
        [Nullable]
        public int? category_id { get; set; }
        public byte CorrelationMission { get; set; }
        public byte RestrictExport { get; set; }
        [Nullable]
        public int? InfoCount { get; set; }
        public string CreatorUser { get; set; }
        public string LastUser { get; set; }
        [Nullable]
        public DateTime? ProcessingEndDate { get; set; }
        [Nullable]
        public DateTime? ProcessingBeginDate { get; set; }
        [Nullable]
        public DateTime? LastChange { get; set; }
        public byte PrivacyLevelId { get; set; }
        [Nullable]
        public string Comment { get; set; }
        [Nullable]
        public int? task_storage_mode_id { get; set; }


        public static TaskMoj CreatePrototype()
        {
            return new TaskMoj
            {
                //НАИБОЛЕЕ важные, недефолтные атрибуты 
                task_id = -1,
                //Код подразделения (для отделов, ведущих самостоятельный учет заданий) (sl_DivisionGuid)
                otd_id = 1,
                //Код инициатора(dt_initiator)
                ini_id = null,
                //вид мероприятия (sl_Meropr) ПТП
                mer_id = null,
                //вид объекта (sl_Object) 100
                obj_id = null,
                //номер задания
                task_number = "0000000001",
                task_year = "2016",
                //шифр задания формируется путем комбинации 
                //dt_Tasks.mer_id,
                //dt_Tasks.obj_id,
                //dt_Tasks.task_number,
                //dt_Tasks.task_year
                alias_obj = "ПТП-100-0000000001-2016",
                //Дата принятия задания
                take_date = DateTime.Now,
                begin_date = DateTime.Now,
                begin_time = null,
                end_date = DateTime.Now,
                end_time = null,
                phone_otm = "89130450918",
                IMSI = "99800000000001",
                IMEI = "11200000000001",
                //статус задания
                category_id = (int)TaskCategoryId.Ispolnayemoe,
                PrivacyLevelId = (int)SecurityLevel.Normal,

                //МЕНЕЕ важные, дефолтные атрибуты(с высокой вероятностью)
                //№ бланка задания
                number_blank = null,
                //место проведения (sl_Where_act) (Жительства/работы)
                where_act_id = null,
                //продолжительность мероприятия
                long_act = null,
                //FK [sl_Delo]
                delo_id = null,
                delo_num = null,
                delo_alias = null,
                //Тип контроля(использовать перечисление)
                type_control_id = (int)TaskTypeControl.All,


                //остальные атрибуты
                //Идентификатор отдела основного исполнителя
                doer_id = null,
                //совместно с ОПУ
                join_opu = false,
                date_create = DateTime.Now,
                //1 - задание; 2 - поручение или иное; 3 - запрос
                is_task = (int)TaskType.Task,
                //1-задание зарегистрировано в текущей БД;2-задание зарегистрировано для пересылки с коротким поручением(может не иметь шифра);3-входящее поручение; 4 - вх. короткое поруч; 5- задание из вх. поручения; 6 - задание из вх.короткого поручения;
                //ставим значения по умолчанию всегда
                task_type = 1,
                //Готовность к приему (default)
                ReadyReceive = 1,
                task_mn = null,
                //1 - задание без поручения; 2 - исходящее поручение со статусом < 12(принято); 3 - исходящее поручение со статусом >= 12(принято); 4 - имеет входящее поручение, в задании можно менять только даты: принято, начало, окончание.
                //(default)
                CorrelationMission = 1,
                //нельзя экспортировать
                RestrictExport = 0,
                InfoCount = null,
                CreatorUser = "sa",
                LastUser = "sa",
                ProcessingBeginDate = null,
                ProcessingEndDate = null,
                LastChange = DateTime.Now,
                Comment = "",
                task_storage_mode_id = null

            };
        }

    }




    public class HelperTaskMoj
    {
        public int task_id { get; set; }
        public int otd_id { get; set; }
      
        public int? ini_id { get; set; }

        public int? mer_id { get; set; }
     
        public int? obj_id { get; set; }
    
        public string task_number { get; set; }

        public string task_year { get; set; }
        public string take_date { get; set; }
    
        public string begin_date { get; set; }
 
        public string begin_time { get; set; }

        public string end_date { get; set; }

        public string end_time { get; set; }
   
        public string number_blank { get; set; }

        public int? where_act_id { get; set; }
 
        public short? long_act { get; set; }
   
        public string phone_otm { get; set; }

        public int? delo_id { get; set; }
        public string delo_num { get; set; }

        public string delo_alias { get; set; }
       
        public string alias_obj { get; set; }
        public bool join_opu { get; set; }
        public string date_create { get; set; }
   
        public int? doer_id { get; set; }
    
        public int? type_control_id { get; set; }
        public byte is_task { get; set; }
 
        public byte? task_type { get; set; }
        public byte ReadyReceive { get; set; }
    
        public string task_mn { get; set; }
 
        public string IMSI { get; set; }
 
        public string IMEI { get; set; }
     
        public int? category_id { get; set; }
        public byte CorrelationMission { get; set; }
        public byte RestrictExport { get; set; }
  
        public int? InfoCount { get; set; }
        public string CreatorUser { get; set; }
        public string LastUser { get; set; }
    
        public string ProcessingEndDate { get; set; }
    
        public string ProcessingBeginDate { get; set; }
        
        public string LastChange { get; set; }
        public byte PrivacyLevelId { get; set; }
        
        public string Comment { get; set; }
        
        public int? task_storage_mode_id { get; set; }

    }

}