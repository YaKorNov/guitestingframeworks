﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;



namespace CommonGenerator.Moj.Tables
{

    [TableName(Name = "dt_Phiz")]
    public partial class Phiz : BaseTaskObject
    {
        [Identity, PrimaryKey(1)]
        public int phiz_id { get; set; }
        [Nullable]
        public int? family_id { get; set; }
        [Nullable]
        public int? name_id { get; set; }
        [Nullable]
        public int? otch_id { get; set; }
        [Nullable]
        public DateTime? date_birth { get; set; }
        [Nullable]
        public int? sex_id { get; set; }
        [Nullable]
        public string alias { get; set; }
        [Nullable]
        public int? expertORD_id { get; set; }
        [Nullable]
        public bool? opg { get; set; }
        [Nullable]
        public string accents { get; set; }
        [Nullable]
        public bool? gun { get; set; }
        [Nullable]
        public bool? amt { get; set; }
        [Nullable]
        public DateTime? DateUpdate { get; set; }
        [Nullable]
        public string fio { get; set; }
        [Nullable]
        public DateTime? LastChange { get; set; }
        [Nullable]
        public string FioFull { get; set; }
        [Nullable]
        public string ob_full_fio { get; set; }


        public static Phiz CreatePrototype()
        {
            return new Phiz()
            {
                //основные атрибуты 
                phiz_id = -1,
                family_id = null,
                name_id = null,
                otch_id = null,
                date_birth = new DateTime(1900, 1, 1),
                sex_id = null,
                alias = "",
                expertORD_id = null,
                fio = "",
                FioFull = "",
                ob_full_fio = "",


                //доп. атрибуты
                opg = false,
                //окраска преступления
                accents = "",
                gun = false,
                //транспорт
                amt = false,
                DateUpdate = DateTime.Now,
                LastChange = DateTime.Now

            };
        }


    }


}














