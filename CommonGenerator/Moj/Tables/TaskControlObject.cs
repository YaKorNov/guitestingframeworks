﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;


namespace CommonGenerator.Moj.Tables
{

    [TableName(Name = "dt_TasksControlObject")]
    public class TaskControlObject
    {
        [Identity, PrimaryKey(1)]
        public int TaskControlObjectId { get; set; }
        public int TaskId { get; set; }
        public int ControlObjectIDTypeId { get; set; }
        public string ControlObjectID { get; set; }

        // FK_dt_TasksControlObject_dt_Tasks
        [Association(ThisKey = "TaskId", OtherKey = "task_id")]
        public TaskMoj dtTasksControlObjectdtTasks { get; set; }
    }
}

