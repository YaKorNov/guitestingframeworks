﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonGenerator.Moj.Models
{
    //
    // Сводка:
    //     Элемент любого словаря
    public class DictionaryItem
    {
        public DictionaryItem()
        {

        }

        public DictionaryItem(int id, string title = "", string caption = "")
        {
            ID = id;
            Title = title;
            Caption = caption;
        }

        //
        // Сводка:
        //     Идентификатор элемента.
        public int? ID { get; set; }
        //
        // Сводка:
        //     Основное нименование элемента
        public string Title { get; set; }
        //
        // Сводка:
        //Дополнительное значение или информация об элементе.
        public string Caption { get; set; }
       
        
    }
}
