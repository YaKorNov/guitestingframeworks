﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Moj.Enums
{
    public enum SecurityLevel
    {
        [Description("Совершенно секретный")]
        TopSecret = 1,
        [Description("Секретный")]
        Secret = 2,
        [Description("Обычный")]
        Normal = 3

    }
}

