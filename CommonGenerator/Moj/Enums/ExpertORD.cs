﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;



namespace CommonGenerator.Moj.Enums
{
    public enum ExpertORD
    {
        [Description("С.МВД")]
        MVD_Member = 1,
        [Description("С.ФСБ")]
        FSB_Member = 2,
        [Description("С.ФСНП")]
        FSNP_Member = 3,
        [Description("Б.С.МВД")]
        Former_MVD__Member = 4,
        [Description("Б.С.ФСБ")]
        Former_FSB__Member = 5,
        [Description("Б.С.ФСНП")]
        Former_FSNP__Member = 6,
        [Description("НЕТ")]
        Not = 7
    }
}
