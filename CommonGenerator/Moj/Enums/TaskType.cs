﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Moj.Enums
{
    public enum TaskTypeControl
    {
        Talk = 1,
        Sms = 2,
        Fax = 4,
        Place = 8,
        DVO = 16,
        Video = 32,
        All = 63
    }

    public enum TaskType
    {
        [Description("задание")]
        Task = 1,
        [Description("поручение")]
        Poruchenie = 2,
        [Description("запрос")]
        Query = 3
    }


    public enum TaskCategoryId
    {
        [Description("Очередное")]
        Ocherednoe = 2,
        [Description("Исполняемое")]
        Ispolnayemoe = 3,
        [Description("Приостановленное")]
        Stopped = 4,
        [Description("Завершенное")]
        Completed = 5,
        [Description("Не завершенное")]
        NotCompleted = 6

    }



}
