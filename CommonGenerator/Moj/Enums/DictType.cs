﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonGenerator.Moj.Enums
{
    public enum DictType
    {
        sl_DivisionGuid,
        sl_Meropr,
        sl_Object,
        sl_Where_act,
        sl_Delo,
        sl_checksystem,
        sl_FamIni,
        sl_Podrazd,

        sl_Family,
        sl_Name,
        sl_Otch,
        sl_expertORD,
        sl_Sex,

        sl_Crimact,
        sl_Citizenship,
        sl_CrimCategories,

        sl_Nationality,
        sl_RelationshipCharact,

        sl_Havings,
        sl_Sphere,

        sl_Region,
        sl_Section,
        sl_Town,
        sl_Street

    }
}
