﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Moj.Enums
{
    public enum TaskObjectType
    {
        [Description("Физическое лицо")]
        Phiz = 1,
        [Description("Юридическое лицо")]
        Yur = 2,
        [Description("Адрес Физического лица")]
        Phiz_Addr = 3,
        [Description("Адрес Юридического лица")]
        Yur_Addr = 4,

    }
}
