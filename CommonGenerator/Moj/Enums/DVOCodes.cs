﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Moj.Enums
{
    public enum DVOCodes
    {
        [Description("Все виды переадресации вызова")]
        AllCF = 32,
        [Description("Безусловная переадресация вызова")]
        CFU = 33,
        [Description("Все виды условной переадресации вызова")]
        AllCondCF = 40,
        [Description("Переадресация вызова по занятости")]
        CFB = 41,
        [Description("Переадресация вызова по неответу")]
        CFNRy = 42,
        [Description("Переадресация вызова при недоступности мобильного абонента")]
        CFNRc = 43,
        [Description("Конференц связь")]
        CONF = 80,

    }
}
