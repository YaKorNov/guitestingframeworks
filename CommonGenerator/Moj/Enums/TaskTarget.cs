﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CommonGenerator.Moj.Enums
{
    public enum TaskTarget
    {

        [Description("УСТАНОВИТЬ СВЯЗИ ФИГУРАНТА")]
        SetFigurantLinks = 1,
        [Description("УСТАНОВИТЬ МЕСТОНАХОЖДЕНИЕ ФИГУРАНТА")]
        SetFigurantPlace = 2,
        [Description("УСТАНОВИТЬ МЕСТА ХРАНЕНИЯ")]
        SetStoragePlace = 3,
        [Description("ДОКУМЕНТИРОВАТЬ ПРЕСТУПНУЮ ДЕЯТЕЛЬНОСТЬ ФИГУРАНТА")]
        SetCriminal = 4,
        [Description("ФИКСИРОВАТЬ НОМЕРА АБОНЕНТОВ")]
        FixNumber = 5,
        [Description("АУДИО/ВИДЕО НОСИТЕЛИ СОХРАНИТЬ")]
        AudioVideoSave = 6,
        [Description("ПРОВЕРИТЬ ПРИЧАСТНОСТЬ")]
        CheckImplication = 7,
        
    }
}
