﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonGenerator.Moj.Enums
{
    public enum  ControlObjectTypeId
    {
        Phone = 1,
        IMEI = 2,
        IMSI = 3
    }
}
