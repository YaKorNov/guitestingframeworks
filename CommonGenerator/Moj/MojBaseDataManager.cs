﻿using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.Data.Linq;
using DataGenerator.Mtb.Tables;
using Microsoft.Xrm.Client;
using CommonGenerator.Mtb;
using CommonGenerator.Moj.Tables;
using CommonGenerator.Moj;


namespace DataGenerator.Moj
{
    public class MojDbManager : DbManager
    {
        private const string ConnectionStrPattern = "Data Source={0};Initial Catalog={1};User ID={2};Password={3}";

        public MojDbManager(DataProviderBase dataProvider, MojSettings settings)
            : base(
                dataProvider,
                ConnectionStrPattern.FormatWith(settings.Address, settings.Database, settings.Login, settings.Password))
        {
        }

       
        public Table<TaskMoj> Tasks
        {
            get { return GetTable<TaskMoj>(); }
        }

        public Table<TaskControlObject> TaskControlObjects
        {
            get { return GetTable<TaskControlObject>(); }
        }

        public Table<TasksCipher> TasksCiphers
        {
            get { return GetTable<TasksCipher>(); }
        }

        public Table<TasksGuid> TasksGuids
        {
            get { return GetTable<TasksGuid>(); }
        }

        public Table<TaskCheckSystem> TasksCheckSystems
        {
            get { return GetTable<TaskCheckSystem>(); }
        }

        public Table<TasksGroupProcessing> TasksGroupProcessing
        {
            get { return GetTable<TasksGroupProcessing>(); }
        }

        public Table<Initiator> Initiators
        {
            get { return GetTable<Initiator>(); }
        }

        public Table<ObjectCode> ObjectCodes
        {
            get { return GetTable<ObjectCode>(); }
        }

        public Table<TasksObject> TasksObjects
        {
            get { return GetTable<TasksObject>(); }
        }

        public Table<TasksOrient> TasksOrients
        {
            get { return GetTable<TasksOrient>(); }
        }

        public Table<TasksTarget> TasksTargets
        {
            get { return GetTable<TasksTarget>(); }
        }

        public Table<Jur> Jurs
        {
            get { return GetTable<Jur>(); }
        }

        public Table<Phiz> Phizs
        {
            get { return GetTable<Phiz>(); }
        }

        public Table<Addr> Addrs
        {
            get { return GetTable<Addr>(); }
        }


    }
}
