﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using CommonGenerator.Moj.Tables;
using System.Collections;



namespace CommonGenerator
{
    public class Utils
    {
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        

        public static DateTime Truncate(DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }


        public static DateTime TruncateMiliseconds(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
        }


        
        public static List<T> RandomListEntries<T>(List<T> listItems)
        {
            var _rand_list_ex = new Random();
            int exclude_count = _rand_list_ex.Next(0, listItems.Count);



            var listItemsCopy = new List<T>(listItems);

            var include_count = listItemsCopy.Count - exclude_count;

            if (listItemsCopy.Count != include_count)
            {
                var shuffled_list = new List<T>();
                var random = new Random();

                while (listItemsCopy.Count > 0)
                {  // Get the next item at random. 
                    var randomIndex = random.Next(0, listItemsCopy.Count);
                    var randomItem = listItemsCopy[randomIndex];
                    // Remove the item from the list and push it to the top of the deck. 
                    listItemsCopy.RemoveAt(randomIndex);
                    shuffled_list.Add(randomItem);

                }


                listItemsCopy = shuffled_list.Take(include_count).ToList();
            }

            return listItemsCopy;


            //Dictionary<string, int> _dict = new Dictionary<string, int>();
            //_dict.Get
        }


        //косяк с DateTime у SqlServer - неверное преобразование формата без T (получаем строку вида 12:12:12 12:12:12 вместо 12:12:12T12:12:12)
        //_moj.SetCommand(" set Language 'us_english'").ExecuteNonQuery();

        //_moj
        //    .SetSpCommand("usp_taskadd", _moj.CreateParameters(task,
        //        new string[] { "task_guid", "task_id" }, new string[] { }, null))
        //    .ExecuteNonQuery(task);
        //поэтому выполняем логику ХП по insert'м здесь

        //task.task_id = InsertWithIdentity(task);

        public static HelperTaskMoj ConvertMOJTaskToHelperFormat(TaskMoj task)
        {
            HelperTaskMoj helper_task = new HelperTaskMoj();
            var task_prop = task.GetType().GetProperties();
            var helper_task_props = helper_task.GetType().GetProperties();

            foreach (var prop in task_prop)
            {
                //Convert.ChangeType(prop.GetValue(task), prop.GetType());

                foreach (var _h_prop in helper_task_props)
                {
                    if (_h_prop.Name == prop.Name)
                    {
                        if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                        {

                            _h_prop.SetValue(helper_task,  Convert.ToDateTime(prop.GetValue(task)).ToString("yyyyMMdd"));
                        }
                        else
                        {
                            _h_prop.SetValue(helper_task, prop.GetValue(task));
                        }
                    }
                        
                }

                
                
            }

            return helper_task;

        }

    }
}
