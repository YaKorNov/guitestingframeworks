﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.Data.DataProvider;
using NUnit.Framework;
using CommonGenerator.Moj;
using Newtonsoft.Json;
using System.Reflection;
using CommonGenerator.Moj.Enums;
using DataGenerator.Moj.MojBuilders;
using DataGenerator.Moj;
using CommonGenerator.Moj.Tables;
using CommonGenerator.Mtb.Enums;


namespace CommonGenerator.Tests
{

    [TestFixture]
    public class MojBuilderTests
    {
        private readonly MojSettings _mojSettings;
        private readonly MojDataBuilder _mojDataBuilder;

        public MojBuilderTests()
        {

            _mojSettings = new MojSettings()
            {
                Address = "Orakultestgui",
                Database = "MagObjectsJournal",
                Login = "sa",
                Password = "m1m2m3-="
            };
            _mojDataBuilder = new MojDataBuilder(new MojDbManager(new SqlDataProvider(), _mojSettings));

        }


        public MojBuilderTests(MojDataBuilder mojDataBuilder, MojSettings mojSettings)
        {

            _mojDataBuilder = mojDataBuilder;
            _mojSettings = mojSettings;

        }

        [TestFixtureSetUp]
        public void TestPrepare()
        {
            //_mojDataBuilder.FullCleanUpMoj();
        }




        [Test]
        public void CreateTaskWithIMSI()
        {
            var _ins_date = DateTime.Now;
            //organ_id ([dt_Initiator]) И  otd_id([dt_Tasks]) явл. FK на sl_DivisionGuid, по идее они должны быть равны
            //указываем 1-ый номер телефона инициатора
            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "20", "10000092", "2032",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), null, "79130620349", null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _cheked_ta = _mojDataBuilder.GetTaskById(_new_t.task_id);

            ControlObjectTypeId _co = _mojDataBuilder.GetControlObjectTypeByTask(_new_t, "79130620349");

            AssertEx.PropertyValuesAreEquals(_cheked_ta, new Dictionary<string, object>()
            {
                { "alias_obj", "ПТП-20-10000092-2032"},
                //{ "CreationTime", _ins_date},
            });
            Assert.AreEqual(_co, ControlObjectTypeId.IMSI);

            //инициатор
            Initiator _init = _mojDataBuilder.GetInitiatorById((int)_cheked_ta.ini_id);
            AssertEx.PropertyValuesAreEquals(_init, new Dictionary<string, object>()
            {
                { "phoneA", "89130233457"},
                //{ "CreationTime", _ins_date},
            });

            //ФИО инициатора
            Assert.AreEqual("Петров Иван Анатольевич", _mojDataBuilder.InitiatorFamily(_init));

        }


        [Test]
        public void CreateTaskWithPhone()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "40", "10000092", "2032",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _cheked_ta = _mojDataBuilder.GetTaskById(_new_t.task_id);

            ControlObjectTypeId _co = _mojDataBuilder.GetControlObjectTypeByTask(_new_t, "79130620349");

            AssertEx.PropertyValuesAreEquals(_cheked_ta, new Dictionary<string, object>()
            {
                { "alias_obj", "ПТП-40-10000092-2032"},
                //{ "CreationTime", _ins_date},
            });
            Assert.AreEqual(_co, ControlObjectTypeId.Phone);
            
        }


        [Test]
        public void CreateCompletedTask()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "40", "10000092", "2014",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _cheked_ta = _mojDataBuilder.GetTaskById(_new_t.task_id);



            Assert.AreEqual(Utils.GetEnumDescription((TaskCategoryId)_cheked_ta.category_id), "Завершенное");

        }


        [Test]
        public void CreateNotFullAttributesTask()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "", "", "10000092", "2014",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _cheked_ta = _mojDataBuilder.GetTaskById(_new_t.task_id);


            AssertEx.PropertyValuesAreEquals(_cheked_ta, new Dictionary<string, object>()
            {
                { "alias_obj", "--10000092-2014"},
                //{ "CreationTime", _ins_date},
            });

        }



        [Test]
        public void CreateInitiator()
        {
            var _new_ini = _mojDataBuilder.AddInitiator("ФСБ по г.Искитиму", "подразделение К", "Петров Иван Петрович", "89130345634", "89130545634");
            var _cheked_ini = _mojDataBuilder.GetInitiatorById(_new_ini.ini_id);
            Assert.AreEqual("Петров Иван Петрович", _mojDataBuilder.InitiatorFamily(_cheked_ini));

        }

        [Test]
        public void CreateInitiatorWithoutOrg()
        {
            var _new_ini = _mojDataBuilder.AddInitiator(null, "подразделение Y", "Петров Иван Петрович", "89130345634", "89130545634");
            var _cheked_ini = _mojDataBuilder.GetInitiatorById(_new_ini.ini_id);
            Assert.AreEqual("Петров Иван Петрович", _mojDataBuilder.InitiatorFamily(_cheked_ini));

        }


        [Test]
        public void CreatePhizAndConnectToTask()
        {

            Phiz _phiz = _mojDataBuilder.AddPhiz("Петросян", "Евгений", "Евгеньевич", null, Sex.Male, ExpertORD.MVD_Member);

            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "60", "14000092", "2015",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

           // _mojDataBuilder.Phiz2Task(_phiz, _new_t);

            
            _mojDataBuilder.LinkPhizWithAdditionalInfo2Task(_phiz, _new_t);

            Assert.AreEqual(true, _mojDataBuilder.PhizLinkedToTask(_phiz, _new_t));

        }


        [Test]
        public void CreateOrient()
        {

            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "90", "14000092", "2015",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _orient = _mojDataBuilder.AddOrientation(_new_t, "Алименты", "Китай", "тувинец", "рост средний", "Преступления в топливно-энергетическом комплексе", "Менеджер");

            
            Assert.AreEqual(true, _mojDataBuilder.OrientLinkedToTask(_orient.orient, _new_t));

        }


        [Test]
        public void CreateDefaultTaskTarget()
        {

            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "10", "14000092", "2015",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _t_target = _mojDataBuilder.AddTaskTarget(_new_t);

            var _act_t = _mojDataBuilder.SelectTaskTarget(_new_t);

         
            var myEnumDescriptions = from TaskTarget n in Enum.GetValues(typeof(TaskTarget))
                                     select  Utils.GetEnumDescription(n);

            
            Assert.IsTrue(myEnumDescriptions.ToList().Contains(_act_t.target));
        }


        [Test]
        public void CreateDefaultJur()
        {

            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "20", "15600092", "2015",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _jur = _mojDataBuilder.AddJur("Рога и копыта");
            _mojDataBuilder.LinkPhizWithAdditionalInfo2Task(_jur, _new_t);
            var _act_jur =  (Jur)_mojDataBuilder.GetPhizYurAddrObjectLinkedToTask(_new_t, TaskObjectType.Yur);

            Assert.AreEqual("Рога и копыта", _act_jur.full_name);
        }


        [Test]
        public void TestCheckLinkBetweenJurAndTask()
        {

            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "20", "15678092", "2015",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _jur = _mojDataBuilder.AddJur("Сигнатек");
            //не связываем
            //_mojDataBuilder.LinkPhizWithAdditionalInfo2Task(_jur, _new_t);
            var _act_jur = (Jur)_mojDataBuilder.GetPhizYurAddrObjectLinkedToTask(_new_t, TaskObjectType.Yur);
            
            Assert.AreEqual(null, _act_jur);
        }


        [Test]
        public void TestCreatePhizAddr()
        {

            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "20", "17988092", "2015",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _phiz_addr = _mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Phiz_Addr);
           
            _mojDataBuilder.LinkPhizWithAdditionalInfo2Task(_phiz_addr, _new_t);
            
            var _act_phiz_addr = (Addr)_mojDataBuilder.GetPhizYurAddrObjectLinkedToTask(_new_t, TaskObjectType.Phiz_Addr);

            Assert.AreEqual("Новосибирская область Новсиб. рпайон Новосибирск Инженерная 12  3", _act_phiz_addr.AddressFull);
        }


        [Test]
        public void TestCreateYurAddr()
        {

            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "20", "28988092", "2015",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620349", null, null, Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            var _yur_addr = _mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Yur_Addr);

            _mojDataBuilder.LinkPhizWithAdditionalInfo2Task(_yur_addr, _new_t);

            var _act_yur_addr = (Addr)_mojDataBuilder.GetPhizYurAddrObjectLinkedToTask(_new_t, TaskObjectType.Yur_Addr);

            Assert.AreEqual("Новосибирская область Новсиб. рпайон Новосибирск Инженерная 12  3", _act_yur_addr.AddressFull);
        }


        [Test]
        public void CreateTaskWithAllAdditionalAttrs()
        {
            var _ins_date = DateTime.Now;
            //organ_id ([dt_Initiator]) И  otd_id([dt_Tasks]) явл. FK на sl_DivisionGuid, по идее они должны быть равны

            _mojDataBuilder.FullCleanUpMoj();

            //создаем задание 
            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130645634", "89130745634", "ПТП", "40", "15000092", "2022",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), null, "79130620349", null, Categories.Executed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.All });

            //инициатора 
            //var _new_init = _mojDataBuilder.AddInitiator("МВД", "отдел К", "Петров Иван Анатольевич", "89130645634", "89130745634");

            //Операция явялется из-за текущего состояния объекта ?
            //_new_t.ini_id = _new_init.ini_id;
            //_mojDataBuilder.UpdateTask(_new_t);

            //BaseTaskObjects
            List<BaseTaskObject> _bt_objects = new List<BaseTaskObject>();

            _bt_objects.Add(_mojDataBuilder.AddPhiz("Никифоров", "Андрей", "Евгеньевич", DateTime.Now, Sex.Male, ExpertORD.MVD_Member));
            _bt_objects.Add(_mojDataBuilder.AddJur("Рога и копыта"));
            _bt_objects.Add(_mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Phiz_Addr));
            _bt_objects.Add(_mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Yur_Addr));


            foreach (var bto in _bt_objects)
            {
                _mojDataBuilder.LinkPhizWithAdditionalInfo2Task(bto, _new_t);
            }
            
            //ориентировку
            var _orient = _mojDataBuilder.AddOrientation(_new_t, "рост средний");

            //цель проведения
            var _t_target = _mojDataBuilder.AddTaskTarget(_new_t);



        }



        [Test]
        [Ignore]
        public void ConvertTest()
        {
            var _new_t = _mojDataBuilder.CreateTaskMoj("МВД", "МВД", "отдел К", "Петров Иван Анатольевич", "89130233457", "", "ПТП", "20", "10000092", "2016",
            new DateTime(2016, 1, 1), new DateTime(2016, 12, 1), "79130620345", "79130620349", "79130620356", Categories.Completed, SecurityLevel.Normal,
            "", "ЖИТЕЛЬСТВА", 2, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.Place, TaskTypeControl.Sms });

            HelperTaskMoj _conv_t = Utils.ConvertMOJTaskToHelperFormat(_new_t);
            Assert.AreEqual(_conv_t.task_id, 1);
        }

    }

}