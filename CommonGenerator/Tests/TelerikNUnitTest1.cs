using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using WinForms = System.Windows.Forms;

using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.TestAttributes;
using ArtOfTest.WebAii.TestTemplates;
using ArtOfTest.WebAii.Win32.Dialogs;

using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Wpf;

using NUnit.Framework;
using Core = NUnit.Core;



namespace CommonGenerator
{
    /// <summary>
    /// Summary description for TelerikNUnitTest1
    /// </summary>
    [TestFixture]
    public class TelerikNUnitTest1 : BaseWpfTest
    {

        #region [Setup / TearDown]

        /// <summary>
        /// Initialization for each test.
        /// </summary>
        [SetUp]
        public void MyTestInitialize()
        {
            #region WebAii Initialization

            // Initializes WebAii manager to be used by the test case.
            // If a WebAii configuration section exists, settings will be
            // loaded from it. Otherwise, will create a default settings
            // object with system defaults.
            //
            // Note: We are passing in a delegate to the NUnit's TestContext.Out.
            // WriteLine() method. This way any logging
            // done from WebAii (i.e. Manager.Log.WriteLine()) is
            // automatically logged to same output as NUnit.
            //
            // If you do not care about unifying the log, then you can simply
            // initialize the test by calling Initialize() with no parameters;
            // that will cause the log location to be picked up from the config
            // file if it exists or will use the default system settings.
            // You can also use Initialize(LogLocation) to set a specific log
            // location for this test.

            //Initialize(new TestContextWriteLine(Console.Out.WriteLine));

            // If you need to override any other settings coming from the
            // config section or you don't have a config section, you can
            // comment the 'Initialize' line above and instead use the
            // following:

            /*

            // This will get a new Settings object. If a configuration
            // section exists, then settings from that section will be
            // loaded

            Settings settings = GetSettings();

            // Override the settings you want. For example:
            settings.WaitCheckInterval = 10000;

            // Now call Initialize again with your updated settings object
            Initialize(settings, new TestContextWriteLine(Console.Out.WriteLine));

            */


            Settings settings = GetSettings();
            settings.LogLocation = @"E:\TelerikWPFLogs\logExample.log";
            Initialize(settings, new TestContextWriteLine(Console.Out.WriteLine));


            #endregion

            //
            // Place any additional initialization here
            //
        }

        /// <summary>
        /// Clean up after each test.
        /// </summary>
        [TearDown]
        public void MyTestCleanUp()
        {
            //
            // Place any additional cleanup here
            //

            #region WebAii CleanUp

            // Shuts down WebAii manager and closes all applications currently running
            this.CleanUp();

            #endregion
        }

        /// <summary>
        /// Called after all tests in this class are executed.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureCleanup()
        {
            // This will shut down all applications
            ShutDown();
        }

        #endregion


        [Test]
        public void LaunchTest()
        {
            // Launch the application instance from its location in file system
            WpfApplication wpfApp = Manager.LaunchNewApplication(@"C:\OtdevTools\GenerateTools\GeneratorGUIApplication\bin\Debug\GeneratorGUIApplication.exe");
            
            //WpfApplication wpfApp = null;
            
            //try
            //{
            //    wpfApp = Manager.LaunchNewApplication(@"C:\Windows\System32\Calc.exe");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("error  "+ex.Message);
            //}
            
            

            // Validate the title of the main window
            Assert.IsTrue(wpfApp.MainWindow.Window.Caption.Equals("��������� �������"));

            Manager.Log.WriteLine("Successfully launch application");


            
            //get the combobox item

            
            var _tb_1 = wpfApp.MainWindow.Find.ByAutomationId<TextBox>("username_textbox");
            _tb_1.SetText(true, "Admin1", 1, 1, true);


            var _tb_2 = wpfApp.MainWindow.Find.ByAutomationId<TextBox>("password_textbox");
            _tb_2.SetText(true, "mag1", 1, 1, true);

            //Thread.Sleep(5000);

            ComboBox _cb = wpfApp.MainWindow.Find.ByAutomationId<ArtOfTest.WebAii.Controls.Xaml.Wpf.ComboBox>("SQLServersList");
            Manager.Log.WriteLine("Cb items = " + String.Join(",", _cb.Items.Select(item => item.Text).ToList()));

            //var first_cb_item = _cb.Items.Count;

            //first_cb_item.Wait.ForExists(7000);

            //_cb.Find.Strategy = FindStrategy.WhenNotVisibleReturnElementProxy;
            
            //_cb.Wait.For(new System.Predicate<FrameworkElement>((fe) => ((ComboBox)fe).Items.Count() > 0 ));
            //_cb.Wait.ForExists(2000);
            Manager.Log.WriteLine("Cb items = " + String.Join(",", _cb.Items.Select(item => item.Text).ToList()));


            //ComboBox _cb = wpfApp.MainWindow.Find.ByAutomationId<ArtOfTest.WebAii.Controls.Xaml.Wpf.ComboBox>("SQLServersList");
            _cb.SelectItemByText(true, "KORNIENKO", true);
            Manager.Log.WriteLine("Selected SQL Server path " + _cb.SelectedValuePath);
            Manager.Log.WriteLine("Selected SQL Server path " + _cb.SelectedIndex);
            

            var _conn_button = wpfApp.MainWindow.Find.ByAutomationId<Button>("CheckConnectionButton");
            _conn_button.User.Click();


            var _connect_string_res = wpfApp.MainWindow.Find.ByAutomationId<Label>("ConnectionPropertiesString");
            string pretest_textbox_value =   _connect_string_res.Text;

            Manager.Log.WriteLine("Conn_label text " + _connect_string_res.Text);
            Manager.Log.WriteLine("Conn_label textBlock " + _connect_string_res.TextBlockContent);
            //System.Threading.Thread.Sleep(2000);

            _connect_string_res.Wait.For((fe) => ((Label)fe).Text != pretest_textbox_value );
            Assert.IsTrue(_connect_string_res.Text.Equals("��� ����������"));
            


            //_cb.SelectItemByText(true, "MK-SHAR");
            //_tb_1.SetText(true, "Admin", 1, 1, true);
            //_tb_1.SetText(true, "mag", 1, 1, true);
            //_conn_button.User.Click();
            //Assert.IsTrue(_connect_string_res.Text.Equals("���������� ���������"));



        }


        [Test]
        public void LaunchTest2()
        {
            // Launch the application instance from its location in file system
            WpfApplication wpfApp = Manager.LaunchNewApplication(@"C:\OtdevTools\GenerateTools\GeneratorGUIApplication\bin\Debug\GeneratorGUIApplication.exe");

            Manager.Log.WriteLine("Successfully launch application");


            var _tb_1 = wpfApp.MainWindow.Find.ByAutomationId<TextBox>("username_textbox");
            _tb_1.SetText(false, "Admin1", 1, 1, true);


            var _tb_2 = wpfApp.MainWindow.Find.ByAutomationId<TextBox>("password_textbox");
            _tb_2.SetText(false, "mag1", 1, 1, true);

            Thread.Sleep(500);

            //var x_cont = new XamlElementContainer();
            //x_cont.Get<ComboBox>("XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]/Button[0]")
            ComboBox _cb = wpfApp.MainWindow.Find.ByExpression(new XamlFindExpression("XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]/TabControl[0]/Grid[0]/Border[0]/ContentPresenter[0]/Grid[0]/ComboBox[0]")).As<ComboBox>();

            //ComboBox _cb = wpfApp.MainWindow.Find.ByAutomationId<ArtOfTest.WebAii.Controls.Xaml.Wpf.ComboBox>("SQLServersList");
            _cb.OpenDropDown(true);
            _cb.Wait.For((fe) => ((ComboBox)fe).Items.Any(), 20000, "Wait timeout for CB items");
            Thread.Sleep(500);
            _cb.SelectItemByText(true, "KORNIENKO");

            Thread.Sleep(1000);

            var _conn_button = wpfApp.MainWindow.Find.ByAutomationId<Button>("CheckConnectionButton");
            _conn_button.User.Click();

            var _connect_string_res = wpfApp.MainWindow.Find.ByAutomationId<Label>("ConnectionPropertiesString");

            string pretest_textbox_value = _connect_string_res.Text;
            //_connect_string_res.Find.WaitOnElementsTimeout = 1000;
            _connect_string_res.Wait.For((fe) => ((Label)fe).Text != pretest_textbox_value, 20000, "Wait conn message timeout");

            //Thread.Sleep(3000);
            Assert.IsTrue(_connect_string_res.Text.Equals("��� ����������"));



            //////right string
            _cb.OpenDropDown(true);
            _cb.Wait.For((fe) => ((ComboBox)fe).Items.Any(), 20000, "Wait timeout for CB items");
            Thread.Sleep(500);
            _cb.SelectItemByText(true, @"MATVEENKO7\MAGDBSERVER");
            //Thread.Sleep(1000);
            _tb_1.SetText(true, "Admin", 1, 1, true);
            _tb_2.SetText(true, "mag", 1, 1, true);
            Thread.Sleep(1000);
            _conn_button.User.Click();

            pretest_textbox_value = _connect_string_res.Text;
            _connect_string_res.Wait.For((fe) => ((Label)fe).Text != pretest_textbox_value, 20000, "Wait conn message timeout");
            
            //Thread.Sleep(3000);
            Assert.IsTrue(_connect_string_res.Text.Equals("���������� ���������"));


            ////wrong string
            _cb.OpenDropDown(true);
            _cb.Wait.For((fe) => ((ComboBox)fe).Items.Any(), 20000, "Wait timeout for CB items");
            Thread.Sleep(500);
            _cb.SelectItemByText(true, "KLYAPOV");
            //Thread.Sleep(1000);
            _tb_1.SetText(true, "Admin", 1, 1, true);
            _tb_2.SetText(true, "mag", 1, 1, true);
            Thread.Sleep(1000);
            _conn_button.User.Click();

            pretest_textbox_value = _connect_string_res.Text;
            //_connect_string_res.Find.WaitOnElementsTimeout = 25000;
            _connect_string_res.Wait.For((fe) => ((Label)fe).Text != pretest_textbox_value, 20000, "Wait conn message timeout");
            //_connect_string_res.Wait.ForVisible(20000);
            //Thread.Sleep(15000);
            Assert.IsTrue(_connect_string_res.Text.Equals("��� ����������"));



            //////right string
            _cb.OpenDropDown(true);
            _cb.Wait.For((fe) => ((ComboBox)fe).Items.Any(), 20000, "Wait timeout for CB items");
            Thread.Sleep(500);
            _cb.SelectItemByText(true, "MK-SHAR");
            _tb_1.SetText(true, "Admin", 1, 1, true);
            _tb_2.SetText(true, "mag", 1, 1, true);
            Thread.Sleep(1000);
            _conn_button.User.Click();

            pretest_textbox_value = _connect_string_res.Text;

            _connect_string_res.Wait.For((fe) => ((Label)fe).Text != pretest_textbox_value, 20000, "Wait conn message timeout");
            Assert.IsTrue(_connect_string_res.Text.Equals("���������� ���������"));


        }


        [Test]
        public void LaunchTest3()
        {
            WpfApplication wpfApp = Manager.LaunchNewApplication(@"C:\OtdevTools\GenerateTools\GeneratorGUIApplication\bin\Debug\GeneratorGUIApplication.exe");

            Manager.Log.WriteLine("Successfully launch application");


            var _tb_1 = wpfApp.MainWindow.Find.ByAutomationId<TextBox>("username_textbox");
            _tb_1.SetText(false, "Admin", 1, 1, true);


            var _tb_2 = wpfApp.MainWindow.Find.ByAutomationId<TextBox>("password_textbox");
            _tb_2.SetText(false, "mag", 1, 1, true);

            Thread.Sleep(500);

            ComboBox _cb = wpfApp.MainWindow.Find.ByAutomationId<ArtOfTest.WebAii.Controls.Xaml.Wpf.ComboBox>("SQLServersList");
            _cb.OpenDropDown(false);
            _cb.SelectItemByText(false, "MK-SHAR");

            Thread.Sleep(1000);

            var _conn_button = wpfApp.MainWindow.Find.ByAutomationId<Button>("CheckConnectionButton");
            _conn_button.User.Click();

            var _connect_string_res = wpfApp.MainWindow.Find.ByAutomationId<Label>("ConnectionPropertiesString");
            string pretest_textbox_value = _connect_string_res.Text;

            _connect_string_res.Wait.For((fe) => ((Label)fe).Text != pretest_textbox_value);
            Assert.IsTrue(_connect_string_res.Text.Equals("���������� ���������"));




            ////wrong string
            _cb.OpenDropDown(true);
            Thread.Sleep(1000);
            _cb.SelectItemByText(true, "KORNIENKO");
            //Thread.Sleep(1000);
            _tb_1.SetText(true, "Admin1", 1, 1, true);
            _tb_2.SetText(true, "mag1", 1, 1, true);
            Thread.Sleep(1000);
            _conn_button.User.Click();

            pretest_textbox_value = _connect_string_res.Text;

            _connect_string_res.Wait.For((fe) => ((Label)fe).Text != pretest_textbox_value);
            Assert.IsTrue(_connect_string_res.Text.Equals("��� ����������"));


        }



    }
}
