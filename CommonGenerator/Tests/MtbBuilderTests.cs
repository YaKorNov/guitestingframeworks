﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.Data.DataProvider;
using DataGenerator.Mtb;
using DataGenerator.Mtb.MtbBuilders;
using NUnit.Framework;
using CommonGenerator.Mtb;
using Newtonsoft.Json;
using System.Reflection;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;


namespace CommonGenerator.Tests
{

    [TestFixture]
    public class MtbBuilderTests
    {
        private readonly MtbSettings _mtbSettings;
        private readonly MtbDataBuilder _mtbDataBuilder;
        private string _talkFileName;


        public MtbBuilderTests()
        {

            _mtbSettings = new MtbSettings()
            {

                //Address = "WIN-7OCS0QPH6KE",
                //Database = "MagTalksBase",
                //Login = "sa",
                //Password = "mag"
                Address = "GUI-TEST",
                Database = "MagTalksBase",
                Login = "sa",
                Password = "m1m2m30-="
            };
            
            _mtbDataBuilder = new MtbDataBuilder(new MtbDbManager(new SqlDataProvider(), _mtbSettings), MagVersion.ver_8_6);
            _talkFileName = @"C:\OtdevTools\GenerateTools\GenerateLibrary\Files\60.wsd";

        }


        public MtbBuilderTests(MtbDataBuilder mtbDataBuilder, MtbSettings mtbSettings, string TalkFileName)
        {

            _mtbDataBuilder = mtbDataBuilder;
            _mtbSettings = mtbSettings;
            _talkFileName = TalkFileName;
        }

        [TestFixtureSetUp]
        public void TestPrepare()
        {
            _mtbDataBuilder.FullCleanUpMtb();
        }


        [Test]
        public void CheckSelect()
        {
            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_144", DateTime.Now);

            var _ta_list = _mtbDataBuilder.SelectAutoTasksByRegName(_new_ta_1.RegNumber);

            Assert.AreEqual("1_144", _ta_list[0].RegNumber);

        }


        [Test]
        public void CreateAutoTask()
        {
            var _ins_date = DateTime.Now;
            var _new_ta = _mtbDataBuilder.CreateTaskAuto("1_123", _ins_date);
            var _cheked_ta = _mtbDataBuilder.GetAutoTaskById(_new_ta.TaskAutoId);

            string expectedJson = JsonConvert.SerializeObject(_new_ta);
            string actualJson = JsonConvert.SerializeObject(_cheked_ta);

            //проверка на равенство всех полей объектов, кроме тех, у которых установлен JsonIgnore
            Assert.AreEqual(expectedJson, actualJson);
            AssertEx.PropertyValuesAreEquals(_cheked_ta, new Dictionary<string, object>()
            {
                { "RegNumber", "1_123"},
                //{ "CreationTime", _ins_date},
            });

        }


        [Test]
        public void CreateAutoTaskWithDeviceAttributes()
        {
            var _ins_date = DateTime.Now;
            var _new_ta = _mtbDataBuilder.CreateTaskAuto("1_124", 12, 0, _ins_date);
            var _cheked_ta = _mtbDataBuilder.GetAutoTaskById(_new_ta.TaskAutoId);

            Assert.AreEqual(JsonConvert.SerializeObject(_new_ta), JsonConvert.SerializeObject(_cheked_ta));
            AssertEx.PropertyValuesAreEquals(_cheked_ta, new Dictionary<string, object>()
            {
                { "RegNumber", "1_124"},
                //{ "CreationTime", _ins_date},
                { "DeviceId", 12},
                { "Channel", 0}
            });

        }


        [Test]
        public void CreateAutoTaskWithDeviceAttributesFull()
        {
            var _ins_date = DateTime.Now;
            var _new_ta = _mtbDataBuilder.CreateTaskAuto("1_125", SourceTypes.Hizina, 12, 0, _ins_date);
            var _cheked_ta = _mtbDataBuilder.GetAutoTaskById(_new_ta.TaskAutoId);

            Assert.AreEqual(JsonConvert.SerializeObject(_new_ta), JsonConvert.SerializeObject(_cheked_ta));
            AssertEx.PropertyValuesAreEquals(_cheked_ta, new Dictionary<string, object>()
            {
                { "RegNumber", "1_125"},
                { "SourceId", (int)SourceTypes.Hizina},
                //{ "CreationTime", _ins_date},
                { "DeviceId", 12},
                { "Channel", 0}
            });

        }



        [Test]
        public void CreateTask()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "10", "138", "2013", Categories.Ocherednoe);
            //обрезаем с точностью до секунды, т.к. сравниваем с данными с MS SQL Serverа
            _new_t.TaskBegin = _new_t.TaskBegin.HasValue ? Utils.TruncateMiliseconds(_new_t.TaskBegin.Value) : _new_t.TaskBegin;

            var _cheked_t = _mtbDataBuilder.GetTaskById(_new_t.TaskId);
            _cheked_t.TaskBegin = _cheked_t.TaskBegin.HasValue ? Utils.TruncateMiliseconds(_cheked_t.TaskBegin.Value) : _cheked_t.TaskBegin;

            string expectedJson = JsonConvert.SerializeObject(_new_t);
            string actualJson = JsonConvert.SerializeObject(_cheked_t);

            //проверка на равенство всех полей объектов, кроме тех, у которых установлен JsonIgnore
            Assert.AreEqual(expectedJson, actualJson);
            Assert.AreEqual("Очередное", Utils.GetEnumDescription((Categories)_cheked_t.CategoryId));
            AssertEx.PropertyValuesAreEquals(_cheked_t, new Dictionary<string, object>()
            {
                { "ObjShifr", "ПТП-10-138-2013"},
                { "ObjName", "ПТП-10-138-2013"},
                { "CategoryId", (int)Categories.Ocherednoe},
            });

        }



        [Test]
        public void ConnectTaskAndAutotask()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "10", "138", "2013", Categories.Ocherednoe);
            //обрезаем с точностью до секунды, т.к. сравниваем с данными с MS SQL Serverа
            _new_t.TaskBegin = _new_t.TaskBegin.HasValue ? Utils.TruncateMiliseconds(_new_t.TaskBegin.Value) : _new_t.TaskBegin;

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_127", SourceTypes.FromHata, 14, 0, _ins_date);
            var _new_ta_2 = _mtbDataBuilder.CreateTaskAuto("1_129", SourceTypes.Imported, 11, 1, _ins_date);


            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);
            var tg2 = _mtbDataBuilder.CreateTaskGrant(_new_ta_2, _new_t, _ins_date.AddHours(5), null);


            int founded_count = _mtbDataBuilder.CountAutoTasksTiedWithTask(_new_t);
            Assert.AreEqual(2, founded_count);

            AssertEx.PropertyValuesAreEquals(tg1, new Dictionary<string, object>()
            {
                { "TaskOperId", _new_t.TaskId},
                { "TaskAutoId", _new_ta_1.TaskAutoId},
                { "AutoStart", _ins_date.AddHours(4)},
            });

        }



        [Test]
        public void CreateTaskWithSms()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "20", "138", "2013", Categories.Ocherednoe);
            //обрезаем с точностью до секунды, т.к. сравниваем в asserte с данными в формате MS SQL Serverа
            //_new_t.TaskBegin = _new_t.TaskBegin.HasValue ? Utils.TruncateMiliseconds(_new_t.TaskBegin.Value) : _new_t.TaskBegin;

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_131", SourceTypes.FromHata, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);

            var _sms_ins_date = DateTime.Now;
            var _sms1 = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, "79130878924", "79830878924", _sms_ins_date, DataGenerator.Mtb.Tables.PhoneDirection.Incoming);
            List<DataGenerator.Mtb.Tables.SmsPhone> _sms_phones = _mtbDataBuilder.CreateSmsPhones(_sms1);

            var _sms2 = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, "79130878924", "79830678924", _sms_ins_date, DataGenerator.Mtb.Tables.PhoneDirection.Outgoing);
            List<DataGenerator.Mtb.Tables.SmsPhone> _sms_phones2 = _mtbDataBuilder.CreateSmsPhones(_sms2);

            var _sms_list = _mtbDataBuilder.GetSmsLinkedToTask(_new_t);

            Assert.AreEqual(2, _sms_list.Count);

            AssertEx.PropertyValuesAreEquals(_sms_list[0], new Dictionary<string, object>()
            {
                { "TelATel",  "79130878924"},
                { "TelBTel", "79830878924"},
                { "Direction", false},
            });

            AssertEx.PropertyValuesAreEquals(_sms_list[1], new Dictionary<string, object>()
            {
                { "TelATel",  "79130878924"},
                { "TelBTel", "79830678924"},
                { "Direction", true},
            });


        }


        [Test]
        public void CreateTaskWithTalk()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "20", "147", "2013", Categories.Ocherednoe);
            //обрезаем с точностью до секунды, т.к. даты хранятся в MS SQL Servere (datetime values are rounded to increments of .000, .003, or .007 seconds) 
            _new_t.TaskBegin = _new_t.TaskBegin.HasValue ? Utils.TruncateMiliseconds(_new_t.TaskBegin.Value) : _new_t.TaskBegin;

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_138", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);

            var _talk_ins_date1 = DateTime.Now;


            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);


            var talk1 = _mtbDataBuilder.CreateSoundedTalkMtb(_new_ta_1, _talk_ins_date1, _talk_ins_date1, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_1 = _mtbDataBuilder.CreateTalkPhones(talk1, "79130878924", "79830878924");

            var _talk_ins_date2 = DateTime.Now;

            var talk2 = _mtbDataBuilder.CreateStaticTalkMtb(_new_ta_1, _talk_ins_date2, _talk_ins_date2, DataGenerator.Mtb.Tables.TalkPhoneDirection.Outgoing, 1);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_2 = _mtbDataBuilder.CreateTalkPhones(talk2, "79130878924", "79830648924");

            var _talk_info_list = _mtbDataBuilder.GetTalksInfoLinkedToTask(_new_t);

            Assert.AreEqual(4, _talk_info_list.Count);

            // talk.BeginDateTime, talk.EndDateTime,  talk.ArcFlag, talk_phone.Phone, talk_phone.PhoneDir, talk_phone.TelId

            _talk_ins_date1 = CommonGenerator.Utils.TruncateMiliseconds(_talk_ins_date1);
            _talk_ins_date2 = CommonGenerator.Utils.TruncateMiliseconds(_talk_ins_date2);

            AssertEx.PropertyValuesAreEquals(_talk_info_list[0], new Dictionary<string, object>()
            {
                { "BeginDateTime",  _talk_ins_date1},
                { "EndDateTime", _talk_ins_date1},
                { "ArcFlag",  (byte)Arc_flags.FileCreated},
                { "Phone", "79130878924"},
                { "PhoneDir", (byte)DataGenerator.Mtb.Tables.PhoneDirection.Incoming}
            });

            AssertEx.PropertyValuesAreEquals(_talk_info_list[1], new Dictionary<string, object>()
            {
                { "BeginDateTime",  _talk_ins_date1},
                { "EndDateTime", _talk_ins_date1},
                { "ArcFlag",  (byte)Arc_flags.FileCreated},
                { "Phone", "79830878924"},
                { "PhoneDir",  (byte)DataGenerator.Mtb.Tables.PhoneDirection.Outgoing}
            });


            AssertEx.PropertyValuesAreEquals(_talk_info_list[2], new Dictionary<string, object>()
            {
                { "BeginDateTime",  _talk_ins_date2},
                { "EndDateTime", _talk_ins_date2},
                { "ArcFlag",  (byte)Arc_flags.Statcontrol},
                { "Phone", "79130878924"},
                { "PhoneDir",  (byte)DataGenerator.Mtb.Tables.PhoneDirection.Outgoing}
            });

            AssertEx.PropertyValuesAreEquals(_talk_info_list[3], new Dictionary<string, object>()
            {
                { "BeginDateTime",  _talk_ins_date2},
                { "EndDateTime", _talk_ins_date2},
                { "ArcFlag",  (byte)Arc_flags.Statcontrol},
                { "Phone", "79830648924"},
                { "PhoneDir",  (byte)DataGenerator.Mtb.Tables.PhoneDirection.Incoming}
            });


        }



        [Test]
        public void CreateTaskWithDVO()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "20", "157", "2014", Categories.Ocherednoe);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_149", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);


            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);

            var _talk_ins_date1 = DateTime.Now;
            var talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, _talk_ins_date1, _talk_ins_date1, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);

            //2 сеанса ДВО на один разговор
            var dvo_seance1 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, talk, Moj.Enums.DVOCodes.AllCF);
            var dvo_seance2 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, talk, Moj.Enums.DVOCodes.CONF);

            var _dvo_list = _mtbDataBuilder.GetDVOLinkedToTask(_new_t);

            Assert.AreEqual(2, _dvo_list.Count);


            _talk_ins_date1 = CommonGenerator.Utils.TruncateMiliseconds(_talk_ins_date1);

            AssertEx.PropertyValuesAreEquals(_dvo_list[0], new Dictionary<string, object>()
            {
                { "Code",  (byte)Moj.Enums.DVOCodes.AllCF}
            });

            AssertEx.PropertyValuesAreEquals(_dvo_list[1], new Dictionary<string, object>()
            {
                { "Code", (byte)Moj.Enums.DVOCodes.CONF},
            });


        }


        [Test]
        public void CreateTaskWithPlace()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "50", "177", "2014", Categories.Ocherednoe);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_173", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);


            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);

            var _talk_ins_date1 = DateTime.Now;
            var talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, _talk_ins_date1, _talk_ins_date1, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);

            //2 местоположения на один разговор
            var place1 = _mtbDataBuilder.CreatePlace(_new_ta_1, talk);
            var place2 = _mtbDataBuilder.CreatePlace(_new_ta_1, talk);

            var _pl_list = _mtbDataBuilder.GetPlacesByTalk(talk);

            Assert.AreEqual(2, _pl_list.Count);

            AssertEx.PropertyValuesAreEquals(_pl_list[0], new Dictionary<string, object>()
            {
                { "Mcc",  (int)DataGenerator.Mtb.Tables.MCC.Russia},
                { "Mnc",  (int)DataGenerator.Mtb.Tables.MNC.MTS},
                { "Lac",  16},
            });

            AssertEx.PropertyValuesAreEquals(_pl_list[1], new Dictionary<string, object>()
            {
                { "Mcc",  (int)DataGenerator.Mtb.Tables.MCC.Russia},
                { "Mnc",  (int)DataGenerator.Mtb.Tables.MNC.MTS},
                { "Lac",  16},
            });


        }




        [Test]
        public void CreateTaskWithVideo()
        {


            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "70", "178", "2014", Categories.Ocherednoe);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_183", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);

            //чтение файла
            var video_file = FileHelper.LoadFile(_talkFileName);

            var _talk_ins_date1 = DateTime.Now;
            var talk = _mtbDataBuilder.CreateVideo(_new_ta_1, DateTime.Now, DateTime.Now, true, 1, video_file, true);


            var _video_list = _mtbDataBuilder.GetVideoByTask(_new_t);

            Assert.AreEqual(1, _video_list.Count);

            AssertEx.PropertyValuesAreEquals(_video_list[0], new Dictionary<string, object>()
            {
                { "FileProcessing",  (byte?)1},
                { "SuppressRemove",  (byte?)1},
            });

        }



        [Test]
        public void CreateTaskWithSteno()
        {


            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "40", "158", "2014", Categories.Ocherednoe);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_187", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);


            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);

            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);

            var _sms_ins_date = DateTime.Now;
            var _sms1 = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, "79130878924", "79830878924", _sms_ins_date, DataGenerator.Mtb.Tables.PhoneDirection.Incoming);


            var steno1 = _mtbDataBuilder.CreateTexts8(_new_t, _talk, DataGenerator.Mtb.Tables.Texts8.Texts8State.Published, "Текст стенограммы");
            var steno2 = _mtbDataBuilder.CreateTexts8(_new_t, _talk2, DataGenerator.Mtb.Tables.Texts8.Texts8State.Darft, "Текст стенограммы2");
            var steno3 = _mtbDataBuilder.CreateTexts8(_new_t, _sms1, DataGenerator.Mtb.Tables.Texts8.Texts8State.Darft, "Текст стенограммы3");


            var _steno_list = _mtbDataBuilder.GetStenogrammsByTask(_new_t);

            Assert.AreEqual(3, _steno_list.Count);

            AssertEx.PropertyValuesAreEquals(_steno_list[0], new Dictionary<string, object>()
            {
                { "State",  (byte)DataGenerator.Mtb.Tables.Texts8.Texts8State.Published},
                { "TextValue",  "Текст стенограммы"},
            });

            AssertEx.PropertyValuesAreEquals(_steno_list[1], new Dictionary<string, object>()
            {
                { "State",  (byte)DataGenerator.Mtb.Tables.Texts8.Texts8State.Darft },
                { "TextValue",  "Текст стенограммы2"},
            });

            AssertEx.PropertyValuesAreEquals(_steno_list[2], new Dictionary<string, object>()
            {
                { "State",  (byte)DataGenerator.Mtb.Tables.Texts8.Texts8State.Darft },
                { "TextValue",  "Текст стенограммы3"},
            });

        }



        [Test]
        public void CreateTaskWithMarks()
        {


            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "60", "128", "2015", Categories.Ocherednoe);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_117", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);

            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);

            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);

            var video = _mtbDataBuilder.CreateVideo(_new_ta_1, DateTime.Now, DateTime.Now, true, 1, new byte[]{}, false);

            var _marck_descr1 = _mtbDataBuilder.GetMarkDescription("Важный", "Важный", true);
            var _marck_descr2 = _mtbDataBuilder.GetMarkDescription("Аналитику", "Аналитику", true);

            _mtbDataBuilder.CreateTalkMark(_talk, _new_t, _marck_descr1);
            _mtbDataBuilder.CreateTalkMark(_talk, _new_t, _marck_descr2);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr1, _new_t);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr2, _new_t);
            //_mtbDataBuilder.CreateTalkMark(_talk, _new_t, _marck_descr2);


            var _m_list = _mtbDataBuilder.getMarksByTask(_new_t);

            Assert.AreEqual(2, _m_list.Count);

            AssertEx.PropertyValuesAreEquals(_m_list[0], new Dictionary<string, object>()
            {
                { "Text",  "Важный"},
            });

            AssertEx.PropertyValuesAreEquals(_m_list[1], new Dictionary<string, object>()
            {
                { "Text",  "Аналитику"},
            });


        }




        [Test]
        public void DeleteMarksByTask()
        {
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.CreateTaskMtb(_ins_date, 30, "ПТП", "70", "128", "2016", Categories.Ocherednoe);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_159", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(4), null);

            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);

            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);

            var sms = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, "89130345676", "89130345677", DateTime.Now, DataGenerator.Mtb.Tables.PhoneDirection.Incoming);

            var _marck_descr1 = _mtbDataBuilder.GetMarkDescription("Важный", "Важный", true);
            var _marck_descr2 = _mtbDataBuilder.GetMarkDescription("Аналитику", "Аналитику", true);

            _mtbDataBuilder.CreateTalkMark(_talk, _new_t, _marck_descr1);
            _mtbDataBuilder.CreateTalkMark(_talk, _new_t, _marck_descr2);
            _mtbDataBuilder.CreateSmsMark(sms, _new_t, _marck_descr1);
            _mtbDataBuilder.CreateSmsMark(sms, _new_t, _marck_descr2);

            var _m_list = _mtbDataBuilder.getMarksByTask(_new_t);
            Assert.AreEqual(2, _m_list.Count);

            _mtbDataBuilder.DeleteMarksByTask(_new_t);


            _m_list = _mtbDataBuilder.getMarksByTask(_new_t);
            Assert.AreEqual(0, _m_list.Count);

        }



        [Test]
        public void SetTasks()
        {

            _mtbDataBuilder.FullCleanUpMtb();
            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "80", "128", "2014", Categories.Ocherednoe, "Иванов", "ориентировка", "цель", DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.All }, SecurityLevel.Normal);
            var _new_t2 = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "80", "138", "2014", Categories.Ocherednoe, "Иванов", "ориентировка", "цель", DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.All }, SecurityLevel.Normal);
            //таск с тем же шифром
            var _new_t3 = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "80", "128", "2014", Categories.Ocherednoe, "Иванов", "ориентировка", "цель", DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.All }, SecurityLevel.Normal);
            var _new_t4 = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "80", "148", "2014", Categories.Ocherednoe, "Иванов", "ориентировка", "цель", DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.All }, SecurityLevel.Normal);
            //таск с тем же шифром
            var _new_t5 = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "80", "148", "2014", Categories.Completed);

            var tasks = _mtbDataBuilder.GetTaskList();

            Assert.AreEqual(3, tasks.Count);


            AssertEx.PropertyValuesAreEquals(_new_t5, new Dictionary<string, object>()
            {
                { "CategoryId",  (int)Categories.Completed },
                 { "Initiator", "" }
            });

        }




        [Test]
        public void CreateReport()
        {

            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "80", "128", "2014", Categories.Ocherednoe, "Иванов", "ориентировка", "цель", DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.All }, SecurityLevel.Normal);
           
            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_179", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(-4), null);

            var _new_ta_2 = _mtbDataBuilder.CreateTaskAuto("1_194", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg2 = _mtbDataBuilder.CreateTaskGrant(_new_ta_2, _new_t, _ins_date.AddHours(-4), null);

            var talk_file = FileHelper.LoadFile(_talkFileName);

            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, DateTime.Now, DateTime.Now, Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);

            var sms = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, "89130345676", "89130345677", DateTime.Now, DataGenerator.Mtb.Tables.PhoneDirection.Incoming);

            var _place1 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk);
            var _place2 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk2);


            

            var video = _mtbDataBuilder.CreateVideo(_new_ta_2, DateTime.Now, DateTime.Now, false, 1, new byte[]{}, false);



            var report = _mtbDataBuilder.CreateReport(_new_t, "1", DateTime.Now, null, DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report, ReportSeancesAlgoritm.All);



        }



        [Test]
        public void CreateReportWithEarlierIncludedSances()
        {

            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "80", "138", "2015", Categories.Ocherednoe, "Иванов", "ориентировка", "цель", DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.All }, SecurityLevel.Normal);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_119", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(-4), null);

            var _new_ta_2 = _mtbDataBuilder.CreateTaskAuto("1_177", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg2 = _mtbDataBuilder.CreateTaskGrant(_new_ta_2, _new_t, _ins_date.AddHours(-4), null);

            var talk_file = FileHelper.LoadFile(_talkFileName);

            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, _ins_date.AddHours(3), _ins_date.AddHours(4), Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, _ins_date.AddHours(3), _ins_date.AddHours(5), Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);

            var sms = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, "89130345676", "89130345677", _ins_date.AddHours(6), DataGenerator.Mtb.Tables.PhoneDirection.Incoming);

            var _place1 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk);
            var _place2 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk2);

            var video = _mtbDataBuilder.CreateVideo(_new_ta_2, _ins_date.AddHours(6), _ins_date.AddHours(7), false, 1, new byte[] { }, false);



            var report = _mtbDataBuilder.CreateReport(_new_t, "1", DateTime.Now, null, DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report, ReportSeancesAlgoritm.RandomIncludeEarlierSeances);

            var report2 = _mtbDataBuilder.CreateReport(_new_t, "2", DateTime.Now, null, DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report2, ReportSeancesAlgoritm.RandomExcludeEarlierSeances);



        }


        [Test]
        public void CreateAllMTBMainEntities()
        {

            var _ins_date = DateTime.Now;
            var _new_t = _mtbDataBuilder.SetTaskMtb(_ins_date, 30, "ПТП", "90", "138", "2015", Categories.Ocherednoe, "Иванов", "ориентировка", "цель", DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.All }, SecurityLevel.Normal);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_128", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t, _ins_date.AddHours(-4), null);

            var _new_ta_2 = _mtbDataBuilder.CreateTaskAuto("1_136", SourceTypes.MagFlowBroker, 14, 0, _ins_date);
            var tg2 = _mtbDataBuilder.CreateTaskGrant(_new_ta_2, _new_t, _ins_date.AddHours(-4), null);

            var talk_file = FileHelper.LoadFile(_talkFileName);

            //разг.
            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, _ins_date.AddHours(3), _ins_date.AddHours(4), Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_1 = _mtbDataBuilder.CreateTalkPhones(_talk, "89130345676", "79830878924");

            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, _ins_date.AddHours(3), _ins_date.AddHours(5), Arc_flags.FileCreated, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_2 = _mtbDataBuilder.CreateTalkPhones(_talk, "89130345676", "79830878926");

            //дво
            //2 сеанса ДВО на один разговор
            var dvo_seance1 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, _talk, Moj.Enums.DVOCodes.AllCF);
            var dvo_seance2 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, _talk, Moj.Enums.DVOCodes.CONF);

            //смс
            var sms = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, "89130345676", "89130345677", _ins_date.AddHours(6), DataGenerator.Mtb.Tables.PhoneDirection.Incoming);

            //место
            var _place1 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk);
            var _place2 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk2);

            //видео
            var video = _mtbDataBuilder.CreateVideo(_new_ta_2, _ins_date.AddHours(6), _ins_date.AddHours(7), false, 1, new byte[]{}, false);


            //стенограммы
            var steno1 = _mtbDataBuilder.CreateTexts8(_new_t, _talk, DataGenerator.Mtb.Tables.Texts8.Texts8State.Published, "Текст стенограммы");
            var steno2 = _mtbDataBuilder.CreateTexts8(_new_t, _talk2, DataGenerator.Mtb.Tables.Texts8.Texts8State.Darft, "Текст стенограммы2");

            //метки
            var _marck_descr1 = _mtbDataBuilder.GetMarkDescription("Важный", "Важный", true);
            var _marck_descr2 = _mtbDataBuilder.GetMarkDescription("Аналитику", "Аналитику", true);

            _mtbDataBuilder.CreateTalkMark(_talk, _new_t, _marck_descr1);
            _mtbDataBuilder.CreateTalkMark(_talk, _new_t, _marck_descr2);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr1, _new_t);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr2, _new_t);

            //сводки
            var report = _mtbDataBuilder.CreateReport(_new_t, "1", DateTime.Now, null, DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report, ReportSeancesAlgoritm.RandomIncludeEarlierSeances);

            var report2 = _mtbDataBuilder.CreateReport(_new_t, "2", DateTime.Now, null, DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report2, ReportSeancesAlgoritm.RandomExcludeEarlierSeances);


            

        }




        [Test]
        public void TestShuffleList()
        {
            List<int> int_list = new List<int>()
            {
                1 , 2, 3, 4 , 5, 6
            };

            List<string> str_list = new List<string>()
            {
                "1" , "2", "3", "4", "5"
            };


            var res_int_list = Utils.RandomListEntries(int_list );
            var res_int_list2 = Utils.RandomListEntries(int_list );
            var res_str_list = Utils.RandomListEntries(str_list );

            //Assert.AreEqual(4, res_int_list.Count);
            //Assert.AreEqual(4, res_int_list2.Count);
            //Assert.AreNotEqual(res_int_list, res_int_list2);
        }





        [Test]
        public void GetControlPhones()
        {
           var list =  _mtbDataBuilder.GetControlPhonesList();
            Assert.AreEqual(1, 1);
        }




    }


    public static class AssertEx
    {
        public static void PropertyValuesAreEquals(object actual, Dictionary<string, object> expected )
        {
            PropertyInfo[] properties = actual.GetType().GetProperties();

            foreach (var exp_kv in expected)
            {
                object expectedValue = exp_kv.Value;
                object actualValue = properties.FirstOrDefault(x => x.Name == exp_kv.Key).GetValue(actual, null);

                if (!Equals(expectedValue, actualValue))
                    Assert.Fail("Property {0} does not match. Expected: {1} but was: {2}", exp_kv.Key, expectedValue, actualValue);
            }

            
        }

    }
}
