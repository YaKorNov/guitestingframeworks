﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.Data.DataProvider;
using DataGenerator.Mtb;
using DataGenerator.Mtb.MtbBuilders;
using NUnit.Framework;
using CommonGenerator.Mtb;
using Newtonsoft.Json;
using System.Reflection;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;
using DataGenerator.Moj.MojBuilders;
using DataGenerator.Moj;
using CommonGenerator.Moj.Tables;
using CommonGenerator.Moj;
using CommonGenerator.UMB;



namespace CommonGenerator.Tests
{

    [TestFixture]
    public class CommonGeneratorTests
    {
        private readonly MtbSettings _mtbSettings;
        private readonly MtbDataBuilder _mtbDataBuilder;
        private readonly MojSettings _mojSettings;
        private readonly MojDataBuilder _mojDataBuilder;
        private readonly UmbSettings _umbSettings;
        private readonly UmbDataBuilder _umbDataBuilder;
        private readonly string _talkFileName;
        private readonly string _videoFileName;

        public CommonGeneratorTests()
        {

            _mtbSettings = new MtbSettings()
            {
                Address = @"gui-test",
                Database = "MagTalksBase",
                Login = "sa",
                Password = "m1m2m30-="
                //Address = @"integration2",
                //Database = "MagTalksBase",
                //Login = "sa",
                //Password = "m1m2m30-="
            };
            _mojSettings = new MojSettings()
            {
                Address = @"gui-test",
                Database = "MagObjectsJournal",
                Login = "sa",
                Password = "m1m2m30-="
                //Address = @"integration2",
                //Database = "MagObjectsJournal",
                //Login = "sa",
                //Password = "m1m2m30-="
            };
            _umbSettings = new UmbSettings()
            {
                Address = @"gui-test",
                Database = "UserManagerBase",
                Login = "sa",
                Password = "m1m2m30-="
                //Address = @"integration2",
                //Database = "UserManagerBase",
                //Login = "sa",
                //Password = "m1m2m30-="
            };

            _mtbDataBuilder = new MtbDataBuilder(new MtbDbManager(new SqlDataProvider(), _mtbSettings), MagVersion.ver_8_6);
            _mojDataBuilder = new MojDataBuilder(new MojDbManager(new SqlDataProvider(), _mojSettings));
            _umbDataBuilder = new UmbDataBuilder(new UMBDataManager(new SqlDataProvider(), _umbSettings));

            //TODO убрать хардкодинг
            _talkFileName = @"C:\SampleFiles\sample.wsd";
            _videoFileName = @"C:\SampleFiles\sample.wsd";
        }


        public CommonGeneratorTests(MtbDataBuilder mtbDataBuilder, MtbSettings mtbSettings, MojDataBuilder mojDataBuilder, MojSettings mojSettings, string TalkFileName)
        {

            _mtbDataBuilder = mtbDataBuilder;
            _mojDataBuilder = mojDataBuilder;
            _mtbSettings = mtbSettings;
            _mojSettings = mojSettings;
            _talkFileName = TalkFileName;

        }


        [SetUp]
        public void TestPrepare()
        {
            //_mtbDataBuilder.FullCleanUpMtb();
            //_mojDataBuilder.FullCleanUpMoj();
        }



        [Test]
        public void CreateTaskInMTBandMOJ()
        {

            _mtbDataBuilder.FullCleanUpMtb();
            _mojDataBuilder.FullCleanUpMoj();
            
            //organ_id ([dt_Initiator]) И otd_id([dt_Tasks]) явл. FK на sl_DivisionGuid, по идее они должны быть равны
            var initiator = "МВД";
            var otdel = "МВД";
            var podrazd = "отдел К";
            //var init_Famini = "Петров Иван Анатольевич";
            var init_Famini = "Семенов Семен Анатольевич";
            string meropr = "ПТП";
            string vid_object = "40";
            string task_number = "15000092";
            string task_year = "2022";
            DateTime taskBegin = new DateTime(2016, 1, 1);
            DateTime taskGrantAutoStart = taskBegin.AddHours(-4);
            short? task_srok = 60;
            //исходим из предположения, что номер объекта контроля - это тот же номер, что и контролируемый объект в учете
            string control_object_phone = "89143787936";
            List<string> interceptor_phones = new List<string>() { "79830878924", "79830878926" };
            string orient_string = "рост средний";
            SecurityLevel sec_level = SecurityLevel.Normal;
            string goal = Utils.GetEnumDescription(TaskTarget.CheckImplication);



            /////// MOJ //////////////

            //создаем задание c видом постановки на контроль по номеру телефона
            //статус - исполняемое, для отображения задания в РИМе, например.
            //задание продолжительностью 60 дней
            //тип контроля - МЕСТО | СМС | ТЕЛЕФОН | ВИДЕО | ДВО (без ФАКСА)
            TaskMoj _new_t_moj = _mojDataBuilder.CreateTaskMoj(otdel, initiator, podrazd, init_Famini, "89130645634", "89130745634", meropr, vid_object, task_number, task_year,
            taskBegin, null, control_object_phone, null, null, Categories.Executed, sec_level,
            "", "ЖИТЕЛЬСТВА", task_srok, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.DVO, TaskTypeControl.Place, TaskTypeControl.Sms, TaskTypeControl.Talk, TaskTypeControl.Video });

            //создаем и подключаем различные реквизиты задания
            List<BaseTaskObject> _bt_objects = new List<BaseTaskObject>();

            //физическое лицо
            _bt_objects.Add(_mojDataBuilder.AddPhiz("Никифоров", "Андрей", "Евгеньевич", DateTime.Now, Sex.Male, ExpertORD.MVD_Member));
            //место работы
            _bt_objects.Add(_mojDataBuilder.AddJur("Рога и копыта"));
            //адрес проживания
            _bt_objects.Add(_mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Phiz_Addr));
            //место работы
            _bt_objects.Add(_mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Yur_Addr));


            foreach (var bto in _bt_objects)
            {
                _mojDataBuilder.LinkPhizWithAdditionalInfo2Task(bto, _new_t_moj);
            }

            //ориентировку
            var _orient = _mojDataBuilder.AddOrientation(_new_t_moj, orient_string);

            //цель проведения - проверить причастность
            var _t_target = _mojDataBuilder.AddTaskTarget(_new_t_moj, Utils.GetEnumDescription(TaskTarget.CheckImplication));


           
             ////// MTB ///////////////////////

             var _new_t_mtb = _mtbDataBuilder.SetTaskMtb(taskBegin, (int)task_srok, meropr, vid_object, task_number, task_year, Categories.Executed,
                init_Famini, orient_string, goal, DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.DVO, TaskTypes.Place, TaskTypes.Sms, TaskTypes.Talk, TaskTypes.Video }, sec_level, _mojDataBuilder.GetTaskGuidById(_new_t_moj.task_id)
                ,10 , 20, 30);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_128", SourceTypes.MagFlowBroker, 14, 0, taskBegin);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t_mtb, taskGrantAutoStart, null);

            var _new_ta_2 = _mtbDataBuilder.CreateTaskAuto("1_136", SourceTypes.MagFlowBroker, 14, 0, taskBegin);
            var tg2 = _mtbDataBuilder.CreateTaskGrant(_new_ta_2, _new_t_mtb, taskGrantAutoStart, null);


            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);
            var video_file = FileHelper.LoadFile(_videoFileName);

            //разг.
            //для попадания в период, добавляем несколько часов
            //TODO для разговоров файлы пишутся, но в ЖС не воспроизводятся(в поле звуковой файл нет пути до файла, пустое поле)
            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, taskBegin.AddHours(3), taskBegin.AddHours(4), Arc_flags.FileWrited, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_1 = _mtbDataBuilder.CreateTalkPhones(_talk, control_object_phone, interceptor_phones[0]);

            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, taskBegin.AddHours(3), taskBegin.AddHours(5), Arc_flags.FileWrited, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_2 = _mtbDataBuilder.CreateTalkPhones(_talk2, control_object_phone, interceptor_phones[1]);

            //дво
            //2 сеанса ДВО на один разговор
            var dvo_seance1 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, _talk, Moj.Enums.DVOCodes.AllCF);
            var dvo_seance2 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, _talk, Moj.Enums.DVOCodes.CONF);

            //проверили
            //смс
            var sms = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, control_object_phone, interceptor_phones[0], taskBegin.AddHours(6), taskBegin.AddHours(7), DataGenerator.Mtb.Tables.PhoneDirection.Incoming, false, "Это текст смс сообщения");

            //место
            var _place1 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk);
            var _place2 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk2);


            //видео
            var video = _mtbDataBuilder.CreateVideo(_new_ta_2, taskBegin.AddHours(6), taskBegin.AddHours(7), false, 1, video_file, true);


            //стенограммы
            var steno1 = _mtbDataBuilder.CreateTexts8(_new_t_mtb, _talk, DataGenerator.Mtb.Tables.Texts8.Texts8State.Published, "Текст стенограммы");
            var steno2 = _mtbDataBuilder.CreateTexts8(_new_t_mtb, _talk2, DataGenerator.Mtb.Tables.Texts8.Texts8State.Darft, "Текст стенограммы2");

            //метки
            var _marck_descr1 = _mtbDataBuilder.GetMarkDescription("Важный", "Важный", true);
            var _marck_descr2 = _mtbDataBuilder.GetMarkDescription("Аналитику", "Аналитику", true);

            _mtbDataBuilder.CreateTalkMark(_talk, _new_t_mtb, _marck_descr1);
            _mtbDataBuilder.CreateTalkMark(_talk, _new_t_mtb, _marck_descr2);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr1, _new_t_mtb);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr2, _new_t_mtb);

            //сводки (обязательно указываем дату печати сводки, для ее включения в список журнала сводок)
            var report = _mtbDataBuilder.CreateReport(_new_t_mtb, "1", taskBegin.AddDays(3), taskBegin.AddDays(4), DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report, ReportSeancesAlgoritm.RandomIncludeEarlierSeances);

            var report2 = _mtbDataBuilder.CreateReport(_new_t_mtb, "2", taskBegin.AddDays(4), taskBegin.AddDays(5), DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report2, ReportSeancesAlgoritm.RandomExcludeEarlierSeances);
            
            
            
        }

        [Test]
        public void testRaiseException()
        {
            //
            var i = 0;
            Assert.Throws<System.DivideByZeroException>(() => { var b = 1 / i; });
        }


        [Test]
        public void SetCustomUser_With_Rim_Rights()
        {
            var playback_rights = _umbDataBuilder.SetCustomRole("Playback rights",
               new List<string>()
               {
                   "Signatec.Mag.Playback.ViewAllTasks",
                   "Signatec.Mag.Playback.ViewAllTaskSeances",
               });

            var rim_access = _umbDataBuilder.SetCustomRole("Rim Access",
               new List<string>()
                   { 
                      "Mag.RIM.AccessToApplication"
                   }
               );


            var rim_journal_page = _umbDataBuilder.SetCustomRole("Rim Journal Page",
               new List<string>()
                   {
                      "Mag.RIM.ViewOtherUserJournal",
                      "Mag.RIM.DeleteSummary",
                   }
               );

            var rim_summ_page = _umbDataBuilder.SetCustomRole("Rim Summary Page",
               new List<string>()
                   {
                      "Mag.RIM.PrintSummary",

                   }
               );

            var rim_archive_page = _umbDataBuilder.SetCustomRole("Rim Archive Page",
               new List<string>()
                   {
                      "Mag.RIM.ComposeInformationalArchive",
                      "Mag.RIM.ExportToMsWord",
                   }
               );

            

            var name = "GOGA";
            var pass = "1";

            var new_user = _umbDataBuilder.SetUser(name, pass,
                new List<string>() {
                    playback_rights.name,
                    rim_access.name,
                    rim_journal_page.name,
                    //rim_summ_page.name,
                    //rim_archive_page.name
                }
                );
            
            var base_default_roles = _umbDataBuilder.Get_Default_User_Roles(new_user).Select(rle => rle.name).Distinct().ToList();

           
            _mtbDataBuilder.SetUser(name, pass, new_user.disabled == 1 ? true : false, base_default_roles);
            _mojDataBuilder.CreateOrUpdateMojUser(new_user.login, new_user.name, pass, base_default_roles.Contains("Mag.Accounting.AccessToApplication"));

        }



        [Test]
        public void SetCustomUser_With_ViewOtherUserJournal()
        {
            var playback_rights = _umbDataBuilder.SetCustomRole("Playback rights",
               new List<string>()
               {
                   "Signatec.Mag.Playback.ViewAllTasks",
                   "Signatec.Mag.Playback.ViewAllTaskSeances",
               });

            var rim_access = _umbDataBuilder.SetCustomRole("Rim Access",
               new List<string>()
                   {
                      "Mag.RIM.AccessToApplication"
                   }
               );


            var rim_journal_page = _umbDataBuilder.SetCustomRole("Rim Journal Page",
               new List<string>()
                   {
                      "Mag.RIM.ViewOtherUserJournal",
                      "Mag.RIM.DeleteSummary",
                   }
               );

            var rim_summ_page = _umbDataBuilder.SetCustomRole("Rim Summary Page",
               new List<string>()
                   {
                      "Mag.RIM.PrintSummary",

                   }
               );

            var rim_archive_page = _umbDataBuilder.SetCustomRole("Rim Archive Page",
               new List<string>()
                   {
                      "Mag.RIM.ComposeInformationalArchive",
                      "Mag.RIM.ExportToMsWord",
                   }
               );



            var name = "PETYA";
            var pass = "1";

            var new_user = _umbDataBuilder.SetUser(name, pass,
                new List<string>() {
                    playback_rights.name,
                    rim_access.name,
                    //rim_journal_page.name,
                    rim_summ_page.name,
                    //rim_archive_page.name
                }
                );

            var base_default_roles = _umbDataBuilder.Get_Default_User_Roles(new_user).Select(rle => rle.name).Distinct().ToList();


            _mtbDataBuilder.SetUser(name, pass, new_user.disabled == 1 ? true : false, base_default_roles);
            _mojDataBuilder.CreateOrUpdateMojUser(new_user.login, new_user.name, pass, base_default_roles.Contains("Mag.Accounting.AccessToApplication"));


        }



        [Test]
        public void SetCustomUser_2()
        {
            var playback_rights = _umbDataBuilder.SetCustomRole("Playback rights",
               new List<string>()
               {
                   "Signatec.Mag.Playback.ViewAllTasks",
                   "Signatec.Mag.Playback.ViewAllTaskSeances",
               });

            var rim_access = _umbDataBuilder.SetCustomRole("Rim Access",
               new List<string>()
                   {
                      "Mag.RIM.AccessToApplication"
                   }
               );


            var rim_journal_page = _umbDataBuilder.SetCustomRole("Rim Journal Page",
               new List<string>()
                   {
                      "Mag.RIM.ViewOtherUserJournal",
                      "Mag.RIM.DeleteSummary",
                   }
               );

            var rim_summ_page = _umbDataBuilder.SetCustomRole("Rim Summary Page",
               new List<string>()
                   {
                      "Mag.RIM.PrintSummary",

                   }
               );

            var rim_archive_page = _umbDataBuilder.SetCustomRole("Rim Archive Page",
               new List<string>()
                   {
                      "Mag.RIM.ComposeInformationalArchive",
                      "Mag.RIM.ExportToMsWord",
                   }
               );



            var name = "VANYA";
            var pass = "1";

            var new_user = _umbDataBuilder.SetUser(name, pass,
                new List<string>() {
                    playback_rights.name,
                    rim_access.name,
                    //rim_journal_page.name,
                    //rim_summ_page.name,
                    rim_archive_page.name
                }
                );

            var base_default_roles = _umbDataBuilder.Get_Default_User_Roles(new_user).Select(rle => rle.name).Distinct().ToList();


            _mtbDataBuilder.SetUser(name, pass, new_user.disabled == 1 ? true : false, base_default_roles);
            _mojDataBuilder.CreateOrUpdateMojUser(new_user.login, new_user.name, pass, base_default_roles.Contains("Mag.Accounting.AccessToApplication"));

        }


        [Test]
        public void SetCustomUser_3()
        {
            var playback_rights = _umbDataBuilder.SetCustomRole("Playback rights",
               new List<string>()
               {
                   "Signatec.Mag.Playback.ViewAllTasks",
                   "Signatec.Mag.Playback.ViewAllTaskSeances",
               });

            var rim_access = _umbDataBuilder.SetCustomRole("Rim Access",
               new List<string>()
                   {
                      "Mag.RIM.AccessToApplication"
                   }
               );


            var rim_journal_page = _umbDataBuilder.SetCustomRole("Rim Journal Page",
               new List<string>()
                   {
                      "Mag.RIM.ViewOtherUserJournal",
                      "Mag.RIM.DeleteSummary",
                   }
               );

            var rim_summ_page = _umbDataBuilder.SetCustomRole("Rim Summary Page",
               new List<string>()
                   {
                      "Mag.RIM.PrintSummary",

                   }
               );

            var rim_archive_page = _umbDataBuilder.SetCustomRole("Rim Archive Page",
               new List<string>()
                   {
                      "Mag.RIM.ComposeInformationalArchive",
                      "Mag.RIM.ExportToMsWord",
                   }
               );

            var rim_archive_page_cut = _umbDataBuilder.SetCustomRole("Rim Archive Page CUT",
               new List<string>()
                   {
                      "Mag.RIM.ComposeInformationalArchive",
                   }
               );



            var name = "EGOR";
            var pass = "1";

            var new_user = _umbDataBuilder.SetUser(name, pass,
                new List<string>() {
                    playback_rights.name,
                    rim_access.name,
                    //rim_journal_page.name,
                    //rim_summ_page.name,
                    rim_archive_page_cut.name
                }
                );

            var base_default_roles = _umbDataBuilder.Get_Default_User_Roles(new_user).Select(rle => rle.name).Distinct().ToList();


            _mtbDataBuilder.SetUser(name, pass, new_user.disabled == 1 ? true : false, base_default_roles);
            _mojDataBuilder.CreateOrUpdateMojUser(new_user.login, new_user.name, pass, base_default_roles.Contains("Mag.Accounting.AccessToApplication"));

        }






    }
}
