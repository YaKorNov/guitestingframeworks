﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ArtOfTest.WebAii.Wpf;
using ArtOfTest.WebAii.TestTemplates;


namespace CommonGenerator.Tests
{

    [TestFixture]
    public class TryingTelerikGUIToolkit : BaseWpfTest
    {
        [Test]
        public void LaunchTest()
        {
            // Launch the application instance from its location in file system
            WpfApplication wpfApp = Manager.LaunchNewApplication(@"C:\OtdevTools\GenerateTools\GeneratorGUIApplication\bin\Debug\GeneratorGUIApplication.exe");

            // Validate the title of the main window
            Assert.IsTrue(wpfApp.MainWindow.Window.Caption.Equals("Генератор сеансов"));

            ////Get the button
            //Button b = wpfApp.MainWindow.Find.ByName<Button>("button1");

            ////Click the button 
            //b.User.Click();

            ////Get the text box
            //TextBox tb = wpfApp.MainWindow.Find.ByName<TextBox>("textBlock1");

            ////Verify the text
            //Assert.IsTrue(tb.Text.Equals("Hello World!"));


        }

    }
}
