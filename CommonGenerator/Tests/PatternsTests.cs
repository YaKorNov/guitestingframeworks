﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.Data.DataProvider;
using NUnit.Framework;


namespace CommonGenerator.Tests
{

    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return string.Format("Id = {0}, Name = {1}", Id, Name);
        }

        //public int CompareTo(object obj)
        //{
        //    Employee temp = (Employee)obj;
        //    if (this.name.Length < temp.name.Length)
        //        return -1;
        //    else return 0;
        //}
    }

    class EmployeeByIdComparer : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        { return x.Id.CompareTo(y.Id); }
    }



    public class EventSampleClass
    {

        public delegate void SomeEventDelegate(int a);

        public int a;
        public int b;

        public event SomeEventDelegate sed;
        public SomeEventDelegate some_f;
 
        public EventSampleClass()
        {
            sed += new SomeEventDelegate(SomeEventDelgatehandler);
            sed += AnotherEventDelgatehandler;

            some_f += new SomeEventDelegate(AnotherEventDelgatehandler);
            some_f += SomeEventDelgatehandler;
        }


        public void raiseEventHandler(int input_value)
        {
            sed(input_value);
        }

        public void raiseSomeF(int input_value)
        {
            some_f(input_value);
        }

        private void SomeEventDelgatehandler(int a)
        {
            b = a * (a + 2);
        }

        private void AnotherEventDelgatehandler(int a)
        {
            b = b*10;
        }



    }




    [TestFixture]
    public class PatternsTests
    {

        [Test]
        public void EventsTest()
        {

            var es = new EventSampleClass();
            EventSampleClass.SomeEventDelegate ev_del = (a) => { es.b = 40; };

            
            es.raiseEventHandler(5);
            Assert.AreEqual(350, es.b);

            es.raiseSomeF(5);
            Assert.AreEqual(35, es.b);

            es.some_f(5);
            Assert.AreEqual(35, es.b);
            es.sed += ev_del;

            es.raiseEventHandler(5);
            Assert.AreEqual(40, es.b);

            es.sed -= ev_del;

            es.raiseEventHandler(5);
            Assert.AreEqual(350, es.b);


        }


        [Test, Description("Strategy Pattern")]
        public void ComparerStrategyTest()
        {
            
            List<Employee> list = new List<Employee>()
                    {
                        new Employee()
                        {
                            Id = 1,
                            Name = "Петров"
                        },
                        new Employee()
                        {
                            Id = 3,
                            Name = "Семенов"
                        },
                        new Employee()
                        {
                            Id = 2,
                            Name = "Фролов"
                        },
                        new Employee()
                        {
                            Id = 4,
                            Name = "Иванов"
                        }
                    };
            // Используем "функтор"    
            list.Sort(new EmployeeByIdComparer());
            // Используем делегат    
            list.Sort((x, y) => x.Name.CompareTo(y.Name));

            Assert.AreEqual(4, list[0].Id);
            Assert.AreEqual("Иванов", list[0].Name);


        }

    }


}




