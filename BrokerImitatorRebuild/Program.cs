﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using CommonGenerator;
using CommonGenerator.Mtb;
using CommonGenerator.Moj;
using DataGenerator.Mtb;
using DataGenerator.Moj;
using BLToolkit.Data.DataProvider;
using DataGenerator.Mtb.MtbBuilders;
using DataGenerator.Moj.MojBuilders;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;
using DataGenerator.Mtb.Tables;
using log4net;
    
    
[assembly: log4net.Config.XmlConfigurator(Watch = true)]



namespace BrokerImitator
{
    class Program
    {

        static bool exitSystem = false;
        
        private DbDataModel _dbDataModel;

        private Random _rand;

        private byte[] talk_file = new byte[] { };
        private byte[] video_file = new byte[] { };

        private string mag_talks_dir;

        private List<string> task_shifrs = new List<string>();

        protected static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);



        #region Trap application termination
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        public static void Exit()
        {
            Handler(CtrlType.CTRL_SHUTDOWN_EVENT);
        }

        private static bool Handler(CtrlType sig)
        {
            Notify("Exiting ...");

            exitSystem = true;

            //shutdown right away so there are no lingering threads
            Environment.Exit(-1);

            return true;
        }
        #endregion

        static void Main(string[] args)
        {
            // Some biolerplate to react to close window event, CTRL-C, kill, etc
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
            
            if (args.Length < 1)
            {
                Notify("Файл конфигурации не указан в качестве аргумента");
                Exit();
            }

            string cfgFile = args[0];
            string fullName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config", cfgFile);

           
            Notify(String.Format("Файл конфигурации : {0}\n ", fullName));
            
            if (!File.Exists(fullName))
            {
                Notify("Файл конфигурации не существует. Проверте правильность указания файла.");
                Exit();
            }

            BrokerSettings Settings = BrokerSettings.Create(fullName);
            Settings.Validate();

            Notify("Используем конфигурацию \n");
            Notify(Settings.ToString());
            
            Console.WriteLine("\nДля продолжения нажмите любую клавишу. Для выхода ESC.");
            ConsoleKeyInfo key = Console.ReadKey();
            if (key.Key == ConsoleKey.Escape)
            {
                return;
            }
            Console.WriteLine("");

            //start your multi threaded program here
            Program p = new Program();
            p.Start(Settings);

            //hold the console so it doesn’t run off the end
            //while (!exitSystem)
            //{
            //    Thread.Sleep(500);
            //}
        }


        public static void Notify(string msg)
        {
            //Console.WriteLine(msg);
            log.Info(msg);
        }

        
        public void MagTalksDirCleanup()
        {

            try
            {
                Utils.ClearDirectory(mag_talks_dir);
            }
            catch (Exception ex)
            {
                Notify(String.Format("Неудачная попытка очистить директорию MagTalks {0}. {1}",
                mag_talks_dir, ex.Message));
                Exit();
            }

            //log.Info("Test step. Полностью очистили директорию MagTalks ");


        }


        private void GenerateTasksShifrs_LinearDistribution(int TasksCount, List<string> MeroprList, List<string> VidObjectList, DateTime FirstTaskDate)
        {
            
            var meropr = MeroprList[_rand.Next(0, MeroprList.Count)];
            var vid_object = VidObjectList[_rand.Next(0, MeroprList.Count)];

            for (var i = 1; i <= TasksCount; i++)
            {
                var task_shifr = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, i.ToString(), FirstTaskDate.Year.ToString());
                
                task_shifrs.Add(task_shifr);
            }
        }




        public void GenerateTalks(List<string> tasks_shifrs, int talks_generation_period, int seances_count, int stat_talks_count, List<string> phone_numbers, byte[] talk_file)
        {

            var connected_autotasks_with_control_phones = _dbDataModel.GetAutotasksWithControlPhone(tasks_shifrs);

            _dbDataModel.GetAutoTasksByIdList(connected_autotasks_with_control_phones.Keys.ToList());

            //1 час
            var seance_duration = 1;



            //Dictionary<AutoTask, DateTime> autotasks_dates = new Dictionary<AutoTask, DateTime>();

            ////первоначальная инициализация 
            //foreach (var _a_task in connected_autotasks)
            //{
            //    //дата первого сеанса = дата подклоючения автотаска + 1 сек
            //    autotasks_dates.Add(_a_task, _a_task.creation_date.AddHours(4).AddSeconds(1));
            //}


            Notify(String.Format("Разговоры генерируются для {0} подключенных автотасков\n", connected_autotasks_with_control_phones.Count
                ));


            System.Diagnostics.Stopwatch talks_timer = new System.Diagnostics.Stopwatch();
            talks_timer.Start();


            var sounded_talk_generated_count = 0;
            var stat_talk_generated_count = 0;


            for (var number = 1; number <= seances_count; number++)
            {
                System.Diagnostics.Stopwatch talks_step_timer = new System.Diagnostics.Stopwatch();
                talks_step_timer.Start();


                var generate_stat_talk = _rand.Next(0, 100) % 2 == 0;
                //проверим, какой вариант разговора генерить
                if (generate_stat_talk && (stat_talk_generated_count + 1) > stat_talks_count)
                {
                    generate_stat_talk = false;
                }
                if (!generate_stat_talk && (sounded_talk_generated_count + 1) > seances_count - stat_talks_count)
                {
                    generate_stat_talk = true;
                }
                if (generate_stat_talk)
                {
                    stat_talk_generated_count++;
                }
                else
                {
                    sounded_talk_generated_count++;
                }


                foreach (var a_task in connected_autotasks_with_control_phones)
                {
                    var begin_date = _dbDataModel.AutoTasks.First(a_t => a_t.id == a_task.Key).creation_date.AddSeconds(4 * 3600 + 1).AddSeconds((number - 1) * talks_generation_period);
                    var end_date = begin_date.AddHours(_rand.Next(1, seance_duration));
                    var direction = _rand.Next(0, 100) % 2 == 0 ? TalkPhoneDirection.Incoming : TalkPhoneDirection.Outgoing;
                    var interlocutor_phone = phone_numbers[_rand.Next(0, phone_numbers.Count)];


                    //var talk = new Talk()
                    //{
                    //    autotask = a_task_date.Key,
                    //    begin = begin_date,
                    //    end = end_date,
                    //    dir = _rand.Next(0, 100) % 2 == 0 ? TalkPhoneDirection.Incoming : TalkPhoneDirection.Outgoing,
                    //    InterlocutorPhone = PhoneNumbers[_rand.Next(0, PhoneNumbers.Count)],

                    //};
                    
                    if (generate_stat_talk)
                    {
                        _dbDataModel.CreateStatTalkWithPhones(a_task.Key, begin_date, end_date, direction, a_task.Value, interlocutor_phone);
                    }
                    else
                    {
                        _dbDataModel.CreateTalkWithPhones(a_task.Key, begin_date, end_date, direction, talk_file, a_task.Value, interlocutor_phone);
                    }


                    //если будет расширение в сторону генерации стенограм/ и.т.д
                    //talk.seanceId = _talk.TalkId;

                    //if (talk.Steno != null)
                    //{
                    //    _mtbDataBuilder.CreateTexts8(t_mtb, _talk, talk.Steno.State, talk.Steno.text);

                    //}


                }


                //
                if (talks_step_timer.ElapsedMilliseconds < talks_generation_period)
                {
                    Thread.Sleep(talks_generation_period - (int)talks_step_timer.ElapsedMilliseconds);
                }
                else
                {
                    Notify(String.Format("Превышено время интервала генерации разговоров на {0} мсек", (talks_generation_period - (int)talks_step_timer.ElapsedMilliseconds)));
                }

                talks_step_timer.Stop();


                if (number % 5 == 0)
                {
                    Notify(String.Format("Записали {0} разговоров", number * connected_autotasks_with_control_phones.Count));
                }




            }

            talks_timer.Stop();

            Notify(String.Format("Общее время генерации разговоров - {0} мин {1} сек {2} мсек, в среднем - {3} мсек/разговор ", talks_timer.Elapsed.Minutes
                , talks_timer.Elapsed.Seconds, talks_timer.Elapsed.Milliseconds, talks_timer.Elapsed.TotalMilliseconds / (seances_count * connected_autotasks_with_control_phones.Count)));


        }



        public void Start(BrokerSettings settings)
        {
            Console.WriteLine("Для прерывания нажмите Ctrl+C.");

            //must to do
            
            var generated = 0;

            //чтение файла разговора
            var _talkFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Samples", "sample_talk.wsd");
            talk_file = FileHelper.LoadFile(_talkFileName);
            
            _rand = new Random();
            _dbDataModel = new DbDataModel(settings.Server, settings.User, settings.Password);

            mag_talks_dir = _dbDataModel.GetMagFullPath();
            if (mag_talks_dir == "" || !Directory.Exists(mag_talks_dir))
            {
                Notify("В настройках МАГа не указан путь до директории MagTalks или она недоступна");
                Exit();
            }

            try
            {

                System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

                timer.Start();

                if (settings.CleanEnv)
                {
                    //очистка директории magtalks
                    Notify("[INIT] Расчистка директории MagTalks...");
                    MagTalksDirCleanup();
                    Notify("[DONE] Завершено\n");

                    //очистить базу
                    Notify("[INIT] Очистка таблиц в БД...");
                    _dbDataModel.FullDBCleanup();
                    Notify("[DONE] Завершено\n");
                }


                
                if (settings.GenerationType == "tasks_linear_distrbution")
                {
                    GenerateTasksShifrs_LinearDistribution(settings.TasksCount, settings.MeroprList, settings.VidObjectList, settings.FirstTaskDate);

                }
                else if (settings.GenerationType == "tasks_ciphers")
                {
                    task_shifrs = settings.OtmList;
                }
                Notify(String.Format("Разговоры будут генерироваться по списку заданий :\n {0} \n ", String.Join(",", task_shifrs)));



                Notify("[INIT] Проверка существования в БД заданий из данного списка ...");
                var non_existed_tasks = _dbDataModel.GetNotExistedTasksInDB(task_shifrs);
                Notify("[DONE] Завершено\n");


                if (non_existed_tasks.Count > 0)
                {
                    Notify(String.Format("[INIT] Будут сгенерированы задания для списка шифров ...\n {0}", string.Join(", ", non_existed_tasks)));
                   
                    _dbDataModel.GenerateTasks(non_existed_tasks, settings.FirstTaskDate, settings.TasksInterval, settings.TaskSrok, settings.PhoneNumbers);
                    _dbDataModel.GenerateAutoTasks_LinearDistribution(settings.AutoTasksCount, settings.AutoTasksInterval,
                        settings.FirstAutoTaskCreationDate, settings.DevicesList, settings.ChannelList);
                    Notify(String.Format("Сгенерированы автозадания с номерами ...\n{0}", string.Join(", ", _dbDataModel.AutoTasks.Select(a_tsk => a_tsk.autotask_number))));
                    
                    _dbDataModel.TaskConnect(settings.TaskToAutotaskGrantedCountMin, settings.TaskToAutotaskGrantedCountMax);
                    _dbDataModel.Save();
                    //_dbDataModel.GenerateNonExistedTasksAndSave();
                    Notify("[DONE] Завершено\n");
                }
                else
                {
                    Notify("Задания уже существуют в БД MagTalks, используем подключенные к ним автозадания \n");
                }


                Notify("[INIT] Генерация разговоров...");
                GenerateTalks(task_shifrs, settings.TalksGenerationPeriod, settings.SeancesCount, settings.StatTalksCount, settings.PhoneNumbers, talk_file);
                Notify("[DONE] Завершено\n");

                Notify( String.Format("{0, -35} {1, 40}", "Время работы генератора", timer.Elapsed));

                timer.Stop();

                Console.WriteLine();


            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION]");
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
