﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.IO;


namespace BrokerImitator
{
    public class Utils
    {
        public static List<T> RandomListEntries<T>(IList<T> listItems, Random _rand)
        {
            //var _rand_list_ex = new Random();
            int exclude_count = _rand.Next(0, listItems.Count);



            var listItemsCopy = new List<T>(listItems);

            var include_count = listItemsCopy.Count - exclude_count;

            if (listItemsCopy.Count != include_count)
            {
                var shuffled_list = new List<T>();
                //var random = new Random();

                while (listItemsCopy.Count > 0)
                {  // Get the next item at random. 
                    var randomIndex = _rand.Next(0, listItemsCopy.Count);
                    var randomItem = listItemsCopy[randomIndex];
                    // Remove the item from the list and push it to the top of the deck. 
                    listItemsCopy.RemoveAt(randomIndex);
                    shuffled_list.Add(randomItem);

                }


                listItemsCopy = shuffled_list.Take(include_count).ToList();
            }

            return listItemsCopy;


            //Dictionary<string, int> _dict = new Dictionary<string, int>();
            //_dict.Get
        }


        public static T[] ConcatArrays<T>(params T[][] list)
        {
            var result = new T[list.Sum(a => a.Length)];
            int offset = 0;
            for (int x = 0; x < list.Length; x++)
            {
                list[x].CopyTo(result, offset);
                offset += list[x].Length;
            }
            return result;
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static Enum FindEnumByDescription(string desc, Type enum_type)
        {

            foreach (var value in Enum.GetValues(enum_type))
            {
                if (Utils.GetEnumDescription((Enum)value) == desc)
                {
                    return (Enum)value;
                }
            }


            return null;

        }


        //удаляя суб-директории
        public static void ClearDirectory(string folder, int level = 1)
        {
            // Delete all files and sub-folders?
            // Yep... Let's do this
            var subfolders = Directory.GetDirectories(folder);
            foreach (var s in subfolders)
            {
                ClearDirectory(s, level + 1);
            }


            // Get all files of the folder
            var files = Directory.GetFiles(folder);
            foreach (var f in files)
            {
                // Get the attributes of the file
                var attr = File.GetAttributes(f);

                // Is this file marked as 'read-only'?
                if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    // Yes... Remove the 'read-only' attribute, then
                    File.SetAttributes(f, attr ^ FileAttributes.ReadOnly);
                }

                // Delete the file
                File.Delete(f);
            }

            // When we get here, all the files of the folder were
            // already deleted, so we just delete the empty folder
            //уровень вложенности
            if (level > 1)
                Directory.Delete(folder);


        }


       

    }
}
