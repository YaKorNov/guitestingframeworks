﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;



namespace BrokerImitator
{
    public class BrokerSettings
    {
        public const string DATETIME_FORMAT = "dd:MM:yyyy HH:mm:ss";

        //Строка подключения

        public string Server { get; set; }

        public string Port { get; set; }
        public string User { get; set; }

        public string Password { get; set; }



        public List<string> PhoneNumbers { get; set; }

        public bool CleanEnv { get; set; }


        /// <summary>
        // Генерировать разговоры по заданиям  - 
        /// 1.С указанными шифрами (tasks_ciphers)
        /// 2.По указанному диапазону (tasks_linear_distrbution)
        /// </summary>
        public string GenerationType { get; set; }


        ////////настройки, для генерации с типом tasks_ciphers///////////////////////////
        public List<string> OtmList { get; set; }



        ////////настройки, для генерации с типом tasks_linear_distrbution///////////////////////////
        public int TasksCount { get; set; }

        public List<string> MeroprList { get; set; }

        public List<string> VidObjectList { get; set; }




        //атрибуты генерируемых заданий
        public DateTime FirstTaskDate { get; set; }
        //дней
        public int TasksInterval { get; set; }

        public short TaskSrok { get; set; }





        //атрибуты генерируемых автотасков

        public int AutoTasksCount { get; set; }

        //дата создания первого автозадания
        public DateTime FirstAutoTaskCreationDate { get; set; }

        public int AutoTasksInterval { get; set; }

        public List<int> DevicesList { get; set; }

        public List<int> ChannelList { get; set; }




        //параметры подключения тасков к автотаскам
        public int TaskToAutotaskGrantedCountMin { get; set; }

        public int TaskToAutotaskGrantedCountMax { get; set; }

        


        //параметры разговоров (общие для указанных выше типов генерации)
        //интервал генерации
        public int TalksGenerationPeriod {get; set;}

        public int SeancesCount { get; set; }

        public int StatTalksCount { get; set; }




        public static BrokerSettings Create(string cfgFile)
        {
            var cfg = new BrokerSettings();

            string content = File.ReadAllText(cfgFile, Encoding.UTF8);
            var js = new JsonSerializerSettings();
            js.MissingMemberHandling = MissingMemberHandling.Ignore;
            js.NullValueHandling = NullValueHandling.Ignore;
            cfg = JsonConvert.DeserializeObject<BrokerSettings>(content, js);

            return cfg;
        }


        public void Validate()
        {

            PropertyInfo[] p_infos = this.GetType().GetProperties();

            foreach (var p_info in p_infos)
            {
                if (p_info.GetValue(this) == null)
                {
                    Program.Notify(String.Format("Не указана или неверно указана настройка генерации {0}", p_info.Name));
                    Program.Exit();
                }
                
            }

            foreach (var otm_shifr in OtmList)
            {
                if (otm_shifr.Split('-').Count<string>() < 4)
                {
                   
                    Program.Notify(String.Format("неверно указаны части шифра для задания {0}", otm_shifr));
                    Program.Exit();
                }
            }



        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            var summary = new Dictionary<string, Dictionary<string, string>>();
            var CommonInfo = new Dictionary<string, string>();
            var TaskInfo = new Dictionary<string, string>();
            var AutoTaskInfo = new Dictionary<string, string>();
            var TaskGrantsInfo = new Dictionary<string, string>();
            var TalkInfo = new Dictionary<string, string>();
            var only_talks_otm_List  = new Dictionary<string, string>();


            CommonInfo.Add("Строка подключения к БД", Server + " " + Port + " " + User + " " + Password);
            CommonInfo.Add("Очищать ли БД и директорию MagTalks перед стартом?", CleanEnv == true ? "да" : "нет");
            CommonInfo.Add("Вариант генерации", GenerationType == "tasks_linear_distrbution" ? "С линейным распределением" : "С указанными шифрами");
            CommonInfo.Add("Номера телефонов (объектов контроля и собеседников)", String.Join(",", PhoneNumbers));

            summary.Add("Общие параметры", CommonInfo);


            if (GenerationType == "tasks_ciphers")
            {
                TaskInfo.Add("Список заданий, по которым генерируются разговоры ", string.Join(",", OtmList));

                TaskInfo.Add("Дата начала первого задания", FirstTaskDate.ToString(DATETIME_FORMAT));
                TaskInfo.Add("Интервал между заданиями (в днях)", TasksInterval.ToString());
                TaskInfo.Add("Срок проведения задания (в днях)", TaskSrok.ToString());

                summary.Add("Настройки генерации заданий ", TaskInfo);
            }
            else if (GenerationType == "tasks_linear_distrbution")
            {
                
                TaskInfo.Add("Количество заданий", TasksCount.ToString());
                TaskInfo.Add("Список мероприятий, используемый для генерации частей шифра заданий", String.Join(",", MeroprList));
                TaskInfo.Add("Список видов объектов, используемый для генерации частей шифра заданий", String.Join(",", VidObjectList));

                TaskInfo.Add("Дата начала первого задания", FirstTaskDate.ToString(DATETIME_FORMAT));
                TaskInfo.Add("Интервал между заданиями (в днях)", TasksInterval.ToString());
                TaskInfo.Add("Срок проведения задания (в днях)", TaskSrok.ToString());

                summary.Add("Настройки генерации заданий ", TaskInfo);
                
            }


            AutoTaskInfo.Add("Количество автозаданий", AutoTasksCount.ToString());
            AutoTaskInfo.Add("Дата начала первого автозадания", FirstAutoTaskCreationDate.ToString(DATETIME_FORMAT));
            AutoTaskInfo.Add("Интервал между автозаданиями (в часах)", AutoTasksInterval.ToString());
            AutoTaskInfo.Add("Список устройств, используемый для генерации автозаданий", String.Join(",", DevicesList));
            AutoTaskInfo.Add("Список каналов, используемый для генерации автозаданий", String.Join(",", ChannelList));

            summary.Add("Настройки генерации автозаданий ", AutoTaskInfo);


            TaskGrantsInfo.Add("Минимальное количество тасков привзяываемых к автотаску", TaskToAutotaskGrantedCountMin.ToString());
            TaskGrantsInfo.Add("Иаксимальное количество тасков привзяываемых к автотаску", TaskToAutotaskGrantedCountMax.ToString());

            summary.Add("Настройки подключения заданий и автозаданий ", TaskGrantsInfo);




            TalkInfo.Add("Интервал в милисекундах, используемый для генерации партии разговоров по подключенным автотаскам", TalksGenerationPeriod.ToString());
            TalkInfo.Add("Количество разговоров, генерируемых по каждому автотаску", SeancesCount.ToString());
            TalkInfo.Add("Из них - стат. разговоров", StatTalksCount.ToString());

            summary.Add("Настройки генерации разговоров ", TalkInfo);


            int max = -1;
            foreach (var pair in summary)
            {
                foreach (string key in pair.Value.Keys)
                {
                    if (key.Length > max)
                    {
                        max = key.Length;
                    }
                }
            }

            max += 2;
            foreach (var pair in summary)
            {
                sb.AppendLine("");
                sb.AppendLine(pair.Key);
                foreach (KeyValuePair<string, string> item in pair.Value)
                {
                    string line = String.Format("{0," + max + "}: {1}", item.Key, item.Value);
                    sb.AppendLine(line);
                }
            }


            return sb.ToString();
        }


    }
}






