﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Data;
using System.Xml;
using System.ServiceProcess;
using NUnit.Framework;
using NUnit.Core;
//генератор
using CommonGenerator;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;
using DataGenerator.Moj.MojBuilders;
using DataGenerator.Moj;
using CommonGenerator.Moj.Tables;
using CommonGenerator.Moj;
using CommonGenerator.Mtb;
using CommonGenerator;
using DataGenerator.Mtb.MtbBuilders;
using DataGenerator.Mtb;
using BLToolkit.Data.DataProvider;
using DataGenerator.Mtb.Tables;
using log4net;
using System.Security.Principal;


[assembly: log4net.Config.XmlConfigurator(Watch = true)]




namespace CleanUpTests
{
    public class BaseTest
    {

        protected readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private string mag_talks_dir;
        private MtbSettings _mtbSettings;
        private MtbDataBuilder _mtbDataBuilder;
        private MojSettings _mojSettings;
        private MojDataBuilder _mojDataBuilder;
        private string _talkFileName;
        private string _videoFileName;
        protected Random _random = new Random();

        protected DbDataModel DbDataModel { get; set; }


        #region MainSettings


        //TODO перенести MainSettings в конфиг

        protected string Get_ENV()
        {
            if (Environment.MachineName == "gui-test")
            {
                return "DEV";
            }
            else if (Environment.MachineName == "gui-test")
            {
                return "TEST";
            }

            return "TEST";
        }

        
        //на случай, если понадобится указывать настройки расчистки через конфигуратор, а не через БД
        //путь до конфигуратора
        protected static Dictionary<string, string> Configurator_Application_Path
        {
            get
            {

                return new Dictionary<string, string>()
                    {
                        {"DEV", @"D:\MAG\Configurator\"},
                        {"TEST", @"D:\MAG\Configurator\"}
                    };

            }
        }


        protected static Dictionary<string, string> FAS_Service_Config_Path
        {
            get
            {
                return new Dictionary<string, string>()
                    {
                        {"DEV", @"C:\MAG\FAS\Signatec.FAS.Service.exe.config"},
                        {"TEST", @"C:\MAG\FAS\Signatec.FAS.Service.exe.config"}
                    };

            }
        }


        protected static Dictionary<string, Dictionary<string, string>> Rim_Seances_Templates_File_Path
        {
            get
            {

                return new Dictionary<string, Dictionary<string, string>>()
                    {
                        {"DEV",  new Dictionary<string, string>(){
                             {"talk_file" ,  @"C:\SampleFiles\sample.wsd"},
                             {"video_file",  @"C:\SampleFiles\sample.wsd"}
                        }},
                        {"TEST", new Dictionary<string, string>(){
                             {"talk_file" ,  @"C:\SampleFiles\sample.wsd"},
                             {"video_file",  @"C:\SampleFiles\sample.wsd"}
                        }}
                    };

            }
        }


        protected static Dictionary<string, Dictionary<string, object>> MAG_DB_Settings
        {
            get
            {

                return new Dictionary<string, Dictionary<string, object>>()
                    {
                        {"DEV", new Dictionary<string, object>(){
                            {"MTB", new MtbSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagTalksBase",
                                    Login = "sa",
                                    Password = "m1m2m30-="
                                }},
                            {"MOJ", new MojSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagObjectsJournal",
                                    Login = "sa",
                                    Password = "m1m2m30-="

                                }}
                        }},
                        {"TEST", new Dictionary<string, object>(){
                            {"MTB", new MtbSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagTalksBase",
                                    Login = "sa",
                                    Password = "m1m2m30-="
                                }},
                            {"MOJ", new MojSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagObjectsJournal",
                                    Login = "sa",
                                    Password = "m1m2m30-="

                                }}
                        }}
                    };

            }
        }


        #endregion 




        #region DataGeneration

        protected void DataGeneratorInit()
        {
            _mtbSettings = (MtbSettings)MAG_DB_Settings[Get_ENV()]["MTB"];
            _mojSettings = (MojSettings)MAG_DB_Settings[Get_ENV()]["MOJ"];


            MtbDbManager cleanupMtbDbManager = new MtbDbManager(new SqlDataProvider(), _mtbSettings);
            //ХП расчистки может выполняться длительное время
            cleanupMtbDbManager.CommandTimeout = 24 * 3600;
            MtbDbManager mtbDbManager = new MtbDbManager(new SqlDataProvider(), _mtbSettings);

            _mtbDataBuilder = new MtbDataBuilder(cleanupMtbDbManager, mtbDbManager, MagVersion.ver_8_8);
            _mojDataBuilder = new MojDataBuilder(new MojDbManager(new SqlDataProvider(), _mojSettings));

            _talkFileName = Rim_Seances_Templates_File_Path[Get_ENV()]["talk_file"];
            _videoFileName = Rim_Seances_Templates_File_Path[Get_ENV()]["video_file"];

            mag_talks_dir = _mtbDataBuilder.GetDefaultUserOption("beginsDefMagFilePath");
            if (mag_talks_dir == "" || !Directory.Exists(mag_talks_dir))
            {
                Assert.Fail("В настройках базы данных не найдена опция - путь до директории MagTalks ");
            }
        }


        protected void DataBasesCleanUp()
        {
            _mtbDataBuilder.FullCleanUpMtb();
            _mojDataBuilder.FullCleanUpMoj();

            log.Info("Test step. Успешно откатили состояние баз данных к первоначальному состоянию");
        }

        //откат модели БД в ОЗУ к первоначальному состоянию 
        protected void GeneratedDataCleanUp()
        {

            DbDataModel = new DbDataModel();

        }


        //TODO Также вынести в конфиг, и в CommonGenerator
        //также и в RimGUITests
        private IList<string> GetStringParametrByName(TaskGeneratorStringParameterType param_type)
        {
            List<string> string_list = new List<string>();

            switch (param_type)
            {
                case TaskGeneratorStringParameterType.Initiator:
                    string_list = new List<string> { "МВД", "ФСБ" };
                    break;
                case TaskGeneratorStringParameterType.Otdel:
                    string_list = new List<string> { "МВД", "ФСБ" };
                    break;
                case TaskGeneratorStringParameterType.Podrazd:
                    string_list = new List<string> { "Отдел Н", "Отдел К" };
                    break;
                case TaskGeneratorStringParameterType.InitFamIni:
                    string_list = new List<string> { "Петров Иван Анатольевич", "Иванов Дмитрий Сергеевич" };
                    break;
                case TaskGeneratorStringParameterType.Phone:
                    string_list = new List<string> { "89130645634", "89130745634" };
                    break;
                case TaskGeneratorStringParameterType.Meropr:
                    string_list = new List<string> { "ПТП", "ОТП" };
                    break;
                case TaskGeneratorStringParameterType.VidObject:
                    string_list = new List<string> { "30", "40", "50" };
                    break;
                case TaskGeneratorStringParameterType.Orient:
                    string_list = new List<string> { "рост средний", "волосы рыжие" };
                    break;
                case TaskGeneratorStringParameterType.SmsText:
                    string_list = new List<string> { "привет, все готово", "когда начинаем?" };
                    break;
                case TaskGeneratorStringParameterType.StenoText:
                    string_list = new List<string> { "ты меня слышишь?", "я полковник на белом коне" };
                    break;
            }

            return string_list;
        }


        public string GetRandomStringParametrValueByName(TaskGeneratorStringParameterType param_type)
        {
            var string_list = GetStringParametrByName(param_type);
            return string_list[_random.Next(0, string_list.Count)];
        }


        //извлечь 2 значения 
        public List<string> GetTwoRandomStringParametrValueByName(TaskGeneratorStringParameterType param_type)
        {
            var string_list = GetStringParametrByName(param_type);
            var rand_index = _random.Next(0, string_list.Count);

            List<string> res_list = new List<string>();
            res_list.Add(string_list[rand_index]);


            var next_element = string_list.ElementAtOrDefault(rand_index + 1);

            if (next_element != null)
            {
                res_list.Add(next_element);
                return res_list;
            }


            var prev_element = string_list.ElementAtOrDefault(rand_index - 1);

            if (prev_element != null)
            {
                res_list.Add(prev_element);
                return res_list;
            }



            if (res_list.Count < 2)
                throw new Exception(String.Format("Не удалось получить 2 различных значения для параметра {0}, проверьте конфиг", param_type.ToString()));

            return res_list;

        }


        public int GetRandomIntegerParametrValueByName(TaskGeneratorIntegerParameterType param_type)
        {
            //Random rnd = new Random();

            switch (param_type)
            {
                case TaskGeneratorIntegerParameterType.TaskSrok:
                    return _random.Next(1, 121);
                case TaskGeneratorIntegerParameterType.DeviceId:
                    return _random.Next(10, 50);
                case TaskGeneratorIntegerParameterType.ChannelId:
                    return _random.Next(15, 28);
            }

            return 0;

        }


        protected TaskTarget GetRandomTaskTarget()
        {
            var tt = Enum.GetValues(typeof(TaskTarget));
            //Random random = new Random();
            return (TaskTarget)tt.GetValue(_random.Next(tt.Length));
        }




        //генреировать таски с линейным распределением
        protected void GenerateOnlyTasks_LinearDistribution(string meropr_p = null, string vid_object_p = null, int first_task_number = 1, int count_tasks = 10,
            DateTime? first_task_date_p = null, int task_interval = 2, short task_srok = 30, bool generate_completed_tasks = false)
        {

            var meropr = meropr_p ?? GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            var vid_object = vid_object_p ?? GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            //var count_tasks = count_tasks_p;
            var first_task_date = first_task_date_p.HasValue ? first_task_date_p.Value : new DateTime(2016, 1, 1);
            var task_year = first_task_date.Year.ToString();
            //var task_interval = task_interval_p; //суток
            //var task_srok = task_srok_p;


            for (int i = 1; i <= count_tasks; i++)
            {


                var taskBegin = first_task_date.AddDays((i - 1) * task_interval);

                string task_number = (i - 1 + first_task_number).ToString();

                var init_Famini = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.InitFamIni);
                var orient = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Orient);
                string goal = Utils.GetEnumDescription(GetRandomTaskTarget());

                var task = new Task();
                task.Otdel = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Otdel);
                task.Init_organization = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Initiator);
                task.Init_podrazd = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Podrazd);
                task.Init_Famini = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.InitFamIni);
                task.Init_phoneA = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.Init_phoneB = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.Meropr = meropr;
                task.VidObject = vid_object;
                task.Task_number = task_number;
                task.Task_year = task_year;
                task.BeginDate = taskBegin;
                task.TaskSrok = task_srok;
                task.PhoneControl = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.IMSI = null;
                task.IMEI = null;
                task.Orient = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Orient);
                task.Goal = GetRandomTaskTarget();

                if (generate_completed_tasks)
                    task.IsCompleted = _random.Next(0, 100) % 2 == 0 ? true : false;

               

                DbDataModel.Tasks.Add(task);

            }


            log.Info(String.Format("Test step. В ОЗУ сгенерировали {0} заданий с маской шифра {1}, дата начала первого задания - {2}, интервал - {3} суток",
                count_tasks, String.Format("{0}-{1}-number-{2}", meropr, vid_object, task_year), first_task_date, task_interval));

        }


        protected void GeneratePlusMinusMarksForTasks(DateTime dt)
        {

            // расчет +/-  меток равномерно относительно
            //заданной даты

            var task_ends = DbDataModel.Tasks.Select(task => task.BeginDate.AddDays(task.TaskSrok));

            if (task_ends.Max() < DateTime.Now || task_ends.Min() > DateTime.Now)
            {
                //берем среднее значение диапазона дат сеансов, берем разницу как срок хранения
                var date_diff = task_ends.Max() - task_ends.Min();
                var average_date = task_ends.Min().AddDays(date_diff.Days / 2);
                var task_end_bias  = (DateTime.Now - average_date).Days;

                foreach (var task in DbDataModel.Tasks)
                {
                    task.ArcKeepMinus = task_end_bias;
                    task.ArcKeepPlus = task_end_bias;
                    task.ArcKeepNotMark = task_end_bias;
                }


            }



        }


        //генерировать по таскам сеансы (распределение случайным образом)
        protected void GenerateSeancesForTasks_ByRandom(
                int seance_max_duration_in_hours = 12, int talk_per_task_max_count = 10, int talk_per_task_min_count = 8,
                int sms_per_task_max_count = 8, int sms_per_task_min_count = 6,
                int place_per_task_max_count = 1, int place_per_task_min_count = 1,
                int video_per_task_max_count = 8, int video_per_task_min_count = 6,
                bool generate_audio_video_files = false)
        {


            foreach (Task task in DbDataModel.Tasks)
            {
                //var _rand = new Random();

                var task_interval_in_hours = (task.BeginDate.AddDays(task.TaskSrok) - task.BeginDate).TotalHours;

                //даты сеансов линейно по возрастанию в порядке, как в превью сводки
                //разговоры->смс->место->видео
                //для возможности смерджить в дальнейшем список сеансов в общий
                var seances_interval_in_hours = (int)(task_interval_in_hours /
                    (place_per_task_max_count + sms_per_task_max_count + place_per_task_max_count + video_per_task_max_count) / 2);

                DateTime seance_begin_date = task.BeginDate;

                //Talks
                for (int i = 1; i <= talk_per_task_max_count; i++)
                {

                    if (_random.Next(0, 20) % 2 == 1 && i > talk_per_task_min_count)
                        continue;

                    //var begin_date = task.BeginDate.AddHours(_rand.Next(1, (int)task_interval_in_hours - seance_max_duration_in_hours));

                    seance_begin_date = seance_begin_date.AddHours(seances_interval_in_hours);
                    var end_date = seance_begin_date.AddHours(_random.Next(1, seance_max_duration_in_hours));


                    var talk = new Talk()
                    {
                        task = task,
                        begin = seance_begin_date,
                        end = end_date,
                        dir = _random.Next(0, 100) % 2 == 0 ? TalkPhoneDirection.Incoming : TalkPhoneDirection.Outgoing,
                        InterlocutorPhone = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone),
                        //т.к. тест-кейсы в РИМе не предусматривают проигрывание файлов,
                        //для ускорения генерации генерим статические разговоры
                        createAudioFile = generate_audio_video_files
                    };

                    task.Talks.Add(talk);

                }

                //Sms
                for (int i = 1; i <= sms_per_task_max_count; i++)
                {
                    if (_random.Next(0, 20) % 2 == 1 && i > sms_per_task_min_count)
                        continue;

                    //var begin_date = task.BeginDate.AddHours(_rand.Next(1, (int)task_interval_in_hours - seance_max_duration_in_hours));
                    seance_begin_date = seance_begin_date.AddHours(seances_interval_in_hours);
                    var end_date = seance_begin_date.AddHours(_random.Next(1, seance_max_duration_in_hours));


                    var sms = new Sms()
                    {
                        task = task,
                        sendTime = seance_begin_date,
                        recvTime = end_date,
                        Direction = _random.Next(0, 100) % 2 == 0 ? PhoneDirection.Incoming : PhoneDirection.Outgoing,
                        InterlocutorPhone = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone),
                        text = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.SmsText),
                        archived = false
                    };

                    task.Sms.Add(sms);

                }


                if (task.Talks.Count > 0)
                {

                    //Place
                    for (int i = 1; i <= place_per_task_max_count; i++)
                    {
                        if (_random.Next(0, 20) % 2 == 1 && i > place_per_task_min_count)
                            continue;


                        var place = new Place()
                        {
                            task = task,
                            talk = task.Talks[0],
                            //код страны
                            mcc = (int)MCC.Russia,
                            mnc = (int)MNC.MTS,
                            lac = _random.Next(10, 26),
                            cl = _random.Next(15, 26)
                        };

                        task.Places.Add(place);

                    }


                }



                //Video
                for (int i = 1; i <= video_per_task_max_count; i++)
                {
                    if (_random.Next(0, 20) % 2 == 1 && i > video_per_task_min_count)
                        continue;


                    //var begin_date = task.BeginDate.AddHours(_rand.Next(1, (int)task_interval_in_hours - seance_max_duration_in_hours));

                    seance_begin_date = seance_begin_date.AddHours(seances_interval_in_hours);
                    var end_date = seance_begin_date.AddHours(_random.Next(1, seance_max_duration_in_hours));

                    var video = new Video()
                    {
                        task = task,
                        begin = seance_begin_date,
                        end = end_date,
                        proccessed = false,
                        createtVideoFile = generate_audio_video_files
                    };

                    task.Video.Add(video);

                }



            }


            log.Info(String.Format("Test step. В ОЗУ сгенерировали сеансы для заданий: {0} разговоров, {1} смс, {2} видео, {3} местоположений,",
                DbDataModel.Tasks.SelectMany(task => task.Talks).Sum(talk => 1),
                DbDataModel.Tasks.SelectMany(task => task.Sms).Sum(sms => 1),
                DbDataModel.Tasks.SelectMany(task => task.Video).Sum(video => 1),
                DbDataModel.Tasks.SelectMany(task => task.Places).Sum(place => 1)
           ));


        }



        protected void GenerateStenoForTalksAndVideos(bool for_talks = true, bool for_videos = true)
        {
            foreach (Task task in DbDataModel.Tasks)
            {

                if (for_talks == true)
                {
                    foreach (Talk talk in task.Talks)
                    {
                        //var _rand = new Random();

                        talk.Steno = new Steno()
                        {
                            text = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.StenoText),
                            State = Texts8.Texts8State.Published
                        };
                    }
                }


                if (for_videos == true)
                {
                    foreach (Video vid in task.Video)
                    {
                        //var _rand = new Random();

                        vid.Steno = new Steno()
                        {
                            text = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.StenoText),
                            State = Texts8.Texts8State.Published
                        };
                    }
                }



            }


            log.Info("Test step. В ОЗУ сгенерировали стенограммы для разговоров " + (for_videos == true ? "и видео" : ""));


        }



        protected void GenerateMarksForSeances()
        {
            //hradcoded marks list
            DbDataModel.Marks = new List<Mark>()
            {
                new Mark()
                {
                    Title = "Важный",
                    Descr = "Важный"
                },
                new Mark()
                {
                    Title = "Аналитику",
                    Descr = "Аналитику"
                },
                new Mark()
                {
                    Title = "В сводку",
                    Descr = "В сводку"
                }
            };


            foreach (Task task in DbDataModel.Tasks)
            {

                //var _rand_flag = new Random();
                foreach (Talk talk in task.Talks)
                {

                    var marks_list = Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        talk.Marks.Add(mark);
                    }


                }


                foreach (var sms_info in task.Sms)
                {

                    var marks_list = Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        sms_info.Marks.Add(mark);
                    }

                }


                foreach (var place in task.Places)
                {
                    var marks_list = Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        place.Marks.Add(mark);
                    }


                }


                foreach (var video_info in task.Video)
                {
                    var marks_list = Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        video_info.Marks.Add(mark);
                    }



                }

            }


            log.Info(String.Format("Test step. В ОЗУ сгенерировали метки для сеансов: для разговоров {0}, для смс - {1}, для видео - {2}, для местоположений -{3} ",
                 DbDataModel.Tasks.SelectMany(task => task.Talks).SelectMany(seance => seance.Marks).Sum(mark => 1),
                 DbDataModel.Tasks.SelectMany(task => task.Sms).SelectMany(seance => seance.Marks).Sum(mark => 1),
                 DbDataModel.Tasks.SelectMany(task => task.Video).SelectMany(seance => seance.Marks).Sum(mark => 1),
                 DbDataModel.Tasks.SelectMany(task => task.Places).SelectMany(seance => seance.Marks).Sum(mark => 1)
            ));


        }


        //генерируем включая ранее вкюченные сеансы
        public void GenerateSummariesForTask_WithRandomSeances(string meropr, string vid_object, string task_number, string task_year,
            int summaries_count, bool set_default_report_date_now = true, DateTime? first_report_date = null, int interval_in_seconds = 24*3600, DateTime? presented_date = null,
            DateTime? deadline = null)
        {

            Task task = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number, task_year);

            for (int i = 1; i <= summaries_count; i++)
            {
                var report = new Report();
                report.task = task;
                report.reportType = ReportType.Report;
                report.IsPrinted = true;

                if (set_default_report_date_now)
                {
                    report.ReportDate = DateTime.Now;
                    report.PrintTime = DateTime.Now;
                }
                else
                {
                    var rep_date = first_report_date.Value.AddSeconds(interval_in_seconds * (i - 1));
                    report.ReportDate = rep_date;
                    report.PrintTime = rep_date;
                }

                report.ReportNumber = i.ToString();
                //TODO generete custom users
                report.UserIdPrinted = 1;
                var rep_dates = new List<DateTime>() { };

                foreach (Talk talk in task.Talks)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Talks.Add(talk);
                    rep_dates.Add(talk.begin);
                }

                foreach (Sms sms in task.Sms)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Sms.Add(sms);
                    rep_dates.Add(sms.sendTime);
                }

                foreach (Video vid in task.Video)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Video.Add(vid);
                    rep_dates.Add(vid.begin);
                }

                foreach (Place place in task.Places)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Places.Add(place);
                }

                report.SeanceCount = report.Talks.Count + report.Sms.Count +
                    report.Video.Count + report.Places.Count;
                report.ReportBegin = rep_dates.Min();
                report.ReportEnd = rep_dates.Max();

                report.PresentedMark = presented_date;

                if (deadline.HasValue)
                {
                    //
                    report.PresentedMark = deadline.Value.AddDays( _random.Next(-4, 5));

                }


                task.Reports.Add(report);
            }


            log.Info(String.Format("Test step. В ОЗУ сгенерировали {0} сводок  для задания {1}  ",
                task.Reports.Count,
                task.TaskShifr
            ));


        }



        protected void ProjectDBStructToDatabase(bool set_task_grant_end_date=false, bool not_set_grant = false)
        {

            Dictionary<string, MarkDescription> mark_d = new Dictionary<string, MarkDescription>();
            foreach (var mark in DbDataModel.Marks)
            {
                mark_d.Add(mark.Title, _mtbDataBuilder.GetMarkDescription(mark.Title, mark.Descr, true));

            }



            byte[] talk_file = new byte[] { };
            byte[] video_file = new byte[] { };
            var create_files = false;

            //генерить ли сеансы со звуковыми и видеофайлами 
            if (DbDataModel.Tasks.SelectMany(task => task.Talks).Any(talk => talk.createAudioFile == true)
                || DbDataModel.Tasks.SelectMany(task => task.Video).Any(video => video.createtVideoFile == true))
            {
                create_files = true;
                //чтение файла
                talk_file = FileHelper.LoadFile(_talkFileName);
                video_file = FileHelper.LoadFile(_videoFileName);
            }


           


            foreach (var task in DbDataModel.Tasks)
            {

                //создаем задание c видом постановки на контроль по номеру телефона
                //статус - исполняемое, для отображения задания в РИМе, например.
                //тип контроля - МЕСТО | СМС | ТЕЛЕФОН | ВИДЕО | ДВО (без ФАКСА)
                //НЕ секретное

                var t_moj = _mojDataBuilder.CreateTaskMoj(
                task.Otdel, task.Init_organization, task.Init_podrazd, task.Init_Famini,
                task.Init_phoneA, task.Init_phoneB, task.Meropr, task.VidObject, task.Task_number, task.Task_year,
                task.BeginDate, null, task.PhoneControl, null, null, task.IsCompleted ? Categories.Completed : Categories.Executed, SecurityLevel.Normal,
                "", "ЖИТЕЛЬСТВА", task.TaskSrok, "АСП", "232",
                new List<TaskTypeControl>() { TaskTypeControl.DVO, TaskTypeControl.Place, TaskTypeControl.Sms, TaskTypeControl.Talk, TaskTypeControl.Video });

                ////ориентировку
                _mojDataBuilder.AddOrientation(t_moj, task.Orient);
                ////цель проведения - проверить причастность
                _mojDataBuilder.AddTaskTarget(t_moj, Utils.GetEnumDescription(task.Goal));


                var t_mtb = _mtbDataBuilder.SetTaskMtb(task.BeginDate, task.TaskSrok, task.Meropr, task.VidObject, task.Task_number, task.Task_year, task.IsCompleted ? Categories.Completed : Categories.Executed,
                task.Init_Famini, task.Orient, Utils.GetEnumDescription(task.Goal), DataTypes.Unsigned,
                new List<TaskTypes>() { TaskTypes.DVO, TaskTypes.Place, TaskTypes.Sms, TaskTypes.Talk, TaskTypes.Video },
                SecurityLevel.Normal, _mojDataBuilder.GetTaskGuidById(t_moj.task_id),
                task.ArcKeepPlus, task.ArcKeepMinus, task.ArcKeepNotMark);

                //назначить задание
                string t_auto_number = String.Format("1_{0}", task.Task_number);

                var autotask_device = GetRandomIntegerParametrValueByName(TaskGeneratorIntegerParameterType.DeviceId); 
                var autotask_channel = GetRandomIntegerParametrValueByName(TaskGeneratorIntegerParameterType.ChannelId);

                var ta = _mtbDataBuilder.CreateTaskAuto(t_auto_number, SourceTypes.MagFlowBroker, autotask_device, autotask_channel, task.BeginDate);


                //для теста меняем нижнюю границу привязки в зависимости от входящих в задание сеансов
                //ставим нижнюю границу привязки в среднее значение между датой первого и последнего сеанса
               
                //верхняя граница всегда на 4 часа будет меньше даты начала 
                //задания, автозадания, и первого сеанса - поэтому меняем только нижнюю границу

                DateTime? auto_end = null;

                if (set_task_grant_end_date)
                {
                    var date_diff = task.Talks.Select(talk => talk.begin).Max() - task.Talks.Select(talk => talk.begin).Min();
                    auto_end = task.Talks.Select(talk => talk.begin).Min().AddDays(date_diff.Days / 2);
                }
                else
                    auto_end = null;

                bool granted = false;

                if ((not_set_grant == true && _random.Next(0, 100) % 2 == 0) || not_set_grant == false)
                {
                    var tg = _mtbDataBuilder.CreateTaskGrant(ta, t_mtb, task.BeginDate.AddHours(-4), auto_end);
                    granted = true;
                }

                //autotask DBModel
                var db_ta = new AutoTask()
                {
                    id = ta.TaskAutoId,
                    autotask_number = t_auto_number,
                    device_id = autotask_device,
                    channel_id = autotask_channel,
                    task_grant_end_date = auto_end,
                    granted = granted
                };

                DbDataModel.AutoTasks.Add(db_ta);


                ///////////////////////сеансы////////////////////////////////////
                //чтение файлов
                //var talk_file = FileHelper.LoadFile(_talkFileName);
                //var video_file = FileHelper.LoadFile(_videoFileName);
                TalkMtb first_talk = null;

                foreach (var talk in task.Talks)
                {

                    talk.autotask = db_ta;

                    var _talk = create_files == true ?
                        _mtbDataBuilder.CreateTalkMtb(ta, talk.begin, talk.end, Arc_flags.FileWrited, talk.dir, talk.suppress_remove, talk_file)
                        : _mtbDataBuilder.CreateStaticTalkMtb(ta, talk.begin, talk.end, talk.dir, talk.suppress_remove);


                    _mtbDataBuilder.CreateTalkPhones(_talk, task.PhoneControl, talk.InterlocutorPhone);
                    if (task.Talks.IndexOf(talk) == 0)
                    {
                        first_talk = _talk;
                    }

                    if (talk.Steno != null)
                    {
                        _mtbDataBuilder.CreateTexts8(t_mtb, _talk, talk.Steno.State, talk.Steno.text);

                    }

                    foreach (var mark in talk.Marks)
                    {
                        _mtbDataBuilder.CreateTalkMark(_talk, t_mtb, mark_d[mark.Descr]);
                    }

                    talk.seanceId = _talk.TalkId;

                }

                foreach (var sms in task.Sms)
                {

                    sms.autotask = db_ta;

                    var _sms = _mtbDataBuilder.CreateSmsMtb(ta, task.PhoneControl, sms.InterlocutorPhone, sms.sendTime, sms.recvTime,
                        sms.Direction, sms.archived, sms.text);


                    foreach (var mark in sms.Marks)
                    {
                        _mtbDataBuilder.CreateSmsMark(_sms, t_mtb, mark_d[mark.Descr]);
                    }

                    sms.seanceId = _sms.SmsId;
                }


                if (task.Talks.Count > 0)
                {
                    foreach (var place in task.Places)
                    {

                        place.autotask = db_ta;

                        var _place = _mtbDataBuilder.CreatePlace(ta, first_talk, place.mcc, place.mnc, place.lac, place.cl);

                        foreach (var mark in place.Marks)
                        {
                            _mtbDataBuilder.CreatePlaceMark(_place, mark_d[mark.Descr], t_mtb);
                        }

                        place.seanceId = _place.PlaceId;

                    }


                }


                foreach (var video in task.Video)
                {

                    video.autotask = db_ta;


                    var _video = create_files == true ?
                        _mtbDataBuilder.CreateVideo(ta, video.begin, video.end, false, video.suppress_remove, video_file, true)
                       : _mtbDataBuilder.CreateVideo(ta, video.begin, video.end, false, video.suppress_remove, new byte[] { }, false);



                    if (video.Steno != null)
                    {
                        _mtbDataBuilder.CreateTexts8(t_mtb, _video, video.Steno.State, video.Steno.text);

                    }


                    foreach (var mark in video.Marks)
                    {
                        _mtbDataBuilder.CreateVideoMark(_video, mark_d[mark.Descr], t_mtb);
                    }

                    video.seanceId = _video.VideoId;

                }



                List<DateTime> mins = new List<DateTime>();
                List<DateTime> maxs = new List<DateTime>();


                foreach (var report in task.Reports)
                {
                    var report_mtb = _mtbDataBuilder.CreateReport(t_mtb, report.ReportNumber, report.ReportDate, report.PrintTime, report.PresentedMark, false);

                    foreach (Talk talk in report.Talks)
                    {
                        _mtbDataBuilder.SetReportSeances(talk.seanceId, report_mtb.ReportId, Texts8.TypeSeance.Talk);
                    }
                    if (report.Talks.Count > 0)
                    {
                        mins.Add(report.Talks.Select(tl => tl.begin).Min());
                        maxs.Add(report.Talks.Select(tl => tl.end).Max());
                    }


                    foreach (Sms sms in report.Sms)
                    {
                        _mtbDataBuilder.SetReportSeances(sms.seanceId, report_mtb.ReportId, Texts8.TypeSeance.SMS);
                    }
                    if (report.Sms.Count > 0)
                    {
                        mins.Add(report.Sms.Select(sms => sms.recvTime).Min());
                        maxs.Add(report.Sms.Select(sms => sms.recvTime).Max());
                    }

                    foreach (Video vid in report.Video)
                    {
                        _mtbDataBuilder.SetReportSeances(vid.seanceId, report_mtb.ReportId, Texts8.TypeSeance.Video);
                    }
                    if (report.Video.Count > 0)
                    {
                        mins.Add(report.Video.Select(vid => vid.begin).Min());
                        maxs.Add(report.Video.Select(vid => vid.end).Max());
                    }



                    foreach (Place place in report.Places)
                    {
                        _mtbDataBuilder.SetReportSeances(place.seanceId, report_mtb.ReportId, Texts8.TypeSeance.Place);
                    }


                    Report87 report87 = report_mtb as Report87;
                    report87.ReportBegin = report.ReportBegin;
                    report87.ReportEnd = report.ReportEnd;
                    report87.IntervalDateStart = mins.Count > 0 ? (DateTime?)mins.Min() : null;
                    report87.IntervalDateEnd = maxs.Count > 0 ? (DateTime?)maxs.Max() : null;
                    

                    //обновить даты IntervalDateStart, IntervalDateEnd
                    _mtbDataBuilder.UpdateRimReport(report87);

                }



            }


            log.Info("Test step. БД. Успешно записали задания,сеансы, связанную информацию. ");

        }

        public void ProjectSettingsToDb()
        {
            //настройки
            foreach (var option in DbDataModel.DefUserOptions.GetDict())
            {
                _mtbDataBuilder.UpdateDefaultUserOption(option.Key, option.Value);

            }

            log.Info("Test step. БД. Успешно записали настройки расчистки ");

        }



        #endregion





        #region Helpers


        public void SetFasConfigParams()
        {
            var config_file_path = FAS_Service_Config_Path[Get_ENV()];

            var config_xml = new XmlDocument();
            var restart_loop_parametr = "2";


            try
            {
                config_xml.Load(config_file_path);
                //XmlAttribute formId = (XmlAttribute)config_xml.SelectSingleNode("//configuration/appSettings/@add");
                XmlNodeList xnList = config_xml.SelectNodes("//appSettings/add[@key='FileDeleteLoopMinutes']");

                if (xnList.Count == 1)
                {
                    var node = xnList[0];
                    foreach (XmlAttribute attr in node.Attributes)
                    {
                        if (attr.Name == "value")
                            attr.Value = restart_loop_parametr; //минуты
                    }
                }
                else
                {
                    Assert.Fail("Ошибка при чтении файла конфигурации FAS, не найден параметр FileDeleteLoopMinutes ");
                }

                config_xml.Save(config_file_path);


            }
            catch(Exception ex)
            {
                Assert.Fail("Ошибка при чтении/записи файла конфигурации FAS " + ex.Message);
            }


            log.Info("Test step. Set FAS config parametr FileDeleteLoopMinutes=" + restart_loop_parametr);

            var is_admin = IsAdministrator();

            RestartService("Signatec.FAS.Service", 10 * 1000);
           

        }


        private void RestartService(string serviceName, int timeoutMilliseconds)
        {

            ServiceController service = new ServiceController(serviceName);
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                log.Info("Test step. Fas.Service stopped ");

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                log.Info("Test step. Fas.Service started ");

            }
            catch (Exception ex)
            {
                Assert.Fail("Ошибка при рестарте FAS " + ex.Message);
                log.Info("Test step. Ошибка при рестарте FAS " + ex.Message);
            }

        }

        public bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();

            if (null != identity)
            {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            return false;
        }



        public void MagTalksDirCleanup()
        {
           
            try
            {
                Utils.ClearDirectory(mag_talks_dir);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Неудачная попытка очистить директорию MagTalks {0}. {1}",
                mag_talks_dir, ex.Message));
            }

            log.Info("Test step. Полностью очистили директорию MagTalks ");


        }



        public void CleanupSPInvoke()
        {
            _mtbDataBuilder.InvokeCleanup();
            log.Info("Test step. Вызов хранмой процедуры расчистки - newfas_CleanupByDevices_Job ");

        }



        public bool FasDeleteQueueNotEmpty()
        {
            //что имеем из БД
            var fas_delete_queued_work = _mtbDataBuilder.GetFasWorkDelete();
            if (fas_delete_queued_work.Count > 0)
            {
                return true;
            }

            return false;

        }


        //TODO отрефакторить, разбить на блоки по возможным вариантам 
        public string GenerateDeviceChannelList()
        {
            //список устройств
            var devices_list = "";


            //--Возможные варианты записи в @devicesList:
            //--

            //--Устройство с каналом
            //--		device3/channel1;device3/channel2
            //--		или
            //--		device3/channel1/channel2
            //--эти два варианта равнозначны

            Dictionary<List<int>, int> dev_channel_pairs = DbDataModel.AutoTasks.Select(a_task => a_task.device_id).Distinct().
                ToDictionary(device_id => DbDataModel.AutoTasks.Where(a_task => a_task.device_id == device_id).Select(a_task => a_task.channel_id).Distinct().ToList());


            var counter = 0;
            foreach (var dev_chan_pair in dev_channel_pairs)
            {
                if (counter > 1)
                    break;

                if (counter == 0)
                {
                    //--		device3/channel1;device3/channel2
                    devices_list += String.Join(";", dev_chan_pair.Key.Select(channel => String.Format(@"{0}/{1}", dev_chan_pair.Value, channel)).ToList()) + ";";

                    foreach (var _at in  DbDataModel.AutoTasks.Where(a_task => a_task.device_id == dev_chan_pair.Value && dev_chan_pair.Key.Contains(a_task.channel_id) ))
                    {
                        _at.match_cleanup_settings_by_dev_channel_pair = true;
                    }
                    
                }
                if (counter == 1)
                {
                    //--		device3/channel1/channel2
                    devices_list += dev_chan_pair.Value + @"/" + String.Join(@"/", dev_chan_pair.Key) + ";";

                    foreach (var _at in DbDataModel.AutoTasks.Where(a_task => a_task.device_id == dev_chan_pair.Value && dev_chan_pair.Key.Contains(a_task.channel_id)))
                    {
                        _at.match_cleanup_settings_by_dev_channel_pair = true;
                    }

                }

                counter++;
            }


            if (dev_channel_pairs.Count > 2)
            {
                var devices = dev_channel_pairs.Values.ToList().Skip(2).ToList();

                //--Перечисление устройств
                //--device1;device2

                devices_list += String.Join(";", devices.Take(Math.Min(devices.Count, 3))) + ";";

                foreach (var _at in DbDataModel.AutoTasks.Where(a_task =>  devices.Take(Math.Min(devices.Count, 3)).ToList().Contains(a_task.device_id)))
                {
                    _at.match_cleanup_settings_by_dev_channel_pair = true;
                }

                //--Интервал номеров устройств
                //--device5-device7
                devices = dev_channel_pairs.Values.ToList().Skip(2 + Math.Min(devices.Count, 3)).ToList();

                if (devices.Count > 0)
                {
                    devices.Sort();
                    var max_index = Math.Min(devices.Count, 3) - 1;
                    devices_list += devices[0] + "-" + devices[max_index] + ";";


                    foreach (var _at in DbDataModel.AutoTasks.Where(a_task => devices.Take(max_index+1).ToList().Contains(a_task.device_id)))
                    {
                        _at.match_cleanup_settings_by_dev_channel_pair = true;
                    }
                }

                //парметр для проверки
               //var count =  devices.Skip(Math.Min(devices.Count, 3)).ToList().Count;
            }

            log.Info(String.Format("Test step. Сгенерирован параметр devices_list, который имеет вид {0}", devices_list));


            return devices_list; 
        }


        public string GenerateDevicesListLight()
        {
            //--Перечисление устройств
            //--device1;device2

            var devices_list = DbDataModel.AutoTasks.Select(a_task => a_task.device_id).Distinct().ToList();

            string devices = String.Join(";", devices_list.Take(Math.Min(devices_list.Count, 6))) + ";";

            foreach (var _at in DbDataModel.AutoTasks.Where(a_task => devices_list.Take(Math.Min(devices_list.Count, 6)).ToList().Contains(a_task.device_id)))
            {
                _at.match_cleanup_settings_by_dev_channel_pair = true;
            }

            return devices;
        }



        #endregion



        #region AssertEx

        public void AssertEx_WorkDelete(List<Talk> expected_deleted_talks, List<Video> expected_deleted_videos) 
        {

            List<string> error_list = new List<string>();


            
            log.Info(String.Format("Test step. Должны быть расчищены - {0} разговоров, {1} видео", expected_deleted_talks.Count, expected_deleted_videos.Count));

            DataTable dt = new DataTable();

            dt.Columns.Add("object_id", typeof(int));
            dt.Columns.Add("Path", typeof(string));
            dt.Columns.Add("IsAudio", typeof(short));
            dt.Columns.Add("IsVideo", typeof(short));

            foreach (var talk in expected_deleted_talks)
            {
                var new_row = dt.NewRow();
                new_row["object_id"] = talk.seanceId;
                //\\MATVEENKO7\MAGTALKS\1_11\2016-06-18\896845.wsd
                new_row["Path"] = String.Format(@"{0}\{1}\{2}\{3}.wsd", mag_talks_dir, talk.autotask.autotask_number, talk.begin.ToString("yyyy-MM-dd"), talk.seanceId);
                new_row["IsAudio"] = 1;
                new_row["IsVideo"] = 0;
                dt.Rows.Add(new_row);
            }

            foreach (var video in expected_deleted_videos)
            {
                var new_row = dt.NewRow();
                new_row["object_id"] = video.seanceId;
                new_row["Path"] = String.Format(@"{0}\{1}\{2}\vid_{3}.avi", mag_talks_dir, video.autotask.autotask_number, video.begin.ToString("yyyy-MM-dd"), video.seanceId);
                new_row["IsAudio"] = 0;
                new_row["IsVideo"] = 1;
                dt.Rows.Add(new_row);
            }


            //что имеем из БД
            var  fas_delete_queued_work = _mtbDataBuilder.GetFasWorkDelete();


            foreach (DataRow del_row in dt.Rows)
            {
                var del_string = fas_delete_queued_work.Find(fas_string => fas_string.ObjectId == (int)del_row["object_id"]);
                if (del_string == null)
                {
                    error_list.Add(String.Format("В таблице dt_newfas_WorkDelete не был найден подлежащий удалению {0} с id {1} ",
                        (short)del_row["IsAudio"] == 1 ? "разговор" : "видео", (int)del_row["object_id"]));
                }
                else
                {
                    //сравнение полей таблицы
                    if (del_string.Path != (string)del_row["Path"])
                    {
                        error_list.Add(String.Format("В таблице dt_newfas_WorkDelete для сеанса с id {0} неверно указан путь до файла actual - {1}, expected - {2}   ",
                            (short)del_row["object_id"], del_string.Path, (string)del_row["Path"]));
                    }
                    
                
                }

            }



            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
                log.Info(String.Join("; ", error_list.ToArray()));
            }

            log.Info("Test step. Таблица dt_newfas_WorkDelete содержит сеансы, предназначенные для расчистки");

        }






        public void AsertEx_SeancesDeleted(DateTime procStartTime, List<Talk> expected_fas_deleted_talks, List<Video> expected_fas_deleted_videos)
        {
            List<string> error_list = new List<string>();

           
            var expected_remained_talks = DbDataModel.Tasks.SelectMany(task => task.Talks).Except(expected_fas_deleted_talks).ToList();
            var expected_remained_videos = DbDataModel.Tasks.SelectMany(task => task.Video).Except(expected_fas_deleted_videos).ToList();


            foreach (var talk in expected_fas_deleted_talks)
            {
                string path = String.Format(@"{0}\{1}\{2}\{3}.wsd", mag_talks_dir, talk.autotask.autotask_number, talk.begin.ToString("yyyy-MM-dd"), talk.seanceId);
                
                if (System.IO.File.Exists(path))
                {
                     error_list.Add(String.Format("Разговор не удален расчисткой {0}", path));
                }

            }

            foreach (var video in expected_fas_deleted_videos)
            {
                string path = String.Format(@"{0}\{1}\{2}\vid_{3}.avi", mag_talks_dir, video.autotask.autotask_number, video.begin.ToString("yyyy-MM-dd"), video.seanceId);

                if (System.IO.File.Exists(path))
                {
                    error_list.Add(String.Format("Видео не удалено расчисткой {0}", path));
                }

            }



            foreach (var talk in expected_remained_talks)
            {
                string path = String.Format(@"{0}\{1}\{2}\{3}.wsd", mag_talks_dir, talk.autotask.autotask_number, talk.begin.ToString("yyyy-MM-dd"), talk.seanceId);

                if (!System.IO.File.Exists(path))
                {
                    error_list.Add(String.Format("Не найден разговор {0}, возможно удаленный расчисткой", path));
                }

            }

            foreach (var video in expected_remained_videos)
            {
                string path = String.Format(@"{0}\{1}\{2}\vid_{3}.avi", mag_talks_dir, video.autotask.autotask_number, video.begin.ToString("yyyy-MM-dd"), video.seanceId);

                if (!System.IO.File.Exists(path))
                {
                    error_list.Add(String.Format("Не найдено видео {0}, возможно удаленное расчисткой", path));
                }

            }


            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
                log.Info(String.Join("; ", error_list.ToArray()));
            }


            log.Info(String.Format("Test step. Тест успешно выполнен. Расчищено разговоров - {0}, видео - {1} в соответствии с назначенными параметрами",
                expected_fas_deleted_talks.Count, expected_fas_deleted_videos.Count));



        }






        #endregion




    }
}
