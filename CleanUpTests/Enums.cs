﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace CleanUpTests
{
    public enum TaskGeneratorStringParameterType
    {
        Initiator,
        Otdel,
        Podrazd,
        InitFamIni,
        Phone,
        Meropr,
        VidObject,
        Orient,
        SmsText,
        StenoText
    }


    public enum TaskGeneratorIntegerParameterType
    {
        TaskSrok,
        DeviceId,
        ChannelId

    }


    public enum ReportType
    {
        [Description("Сводка")]
        Report = 1,
        [Description("Удаленная сводка")]
        DeletedReport = 2,
        [Description("Информационный архив")]
        Archive = 3,
    }
   
}

