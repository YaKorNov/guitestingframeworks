﻿using System;
using NUnit.Framework;
using NUnit.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace CleanUpTests
{

    [TestFixture]
    public class CleanupTests : BaseTest
    {
        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            DataGeneratorInit();

        }

        [SetUp]
        public void Initialise()
        {
            //TODO перезапуск Fas.Service
            SetFasConfigParams();
            //откат базы
            DataBasesCleanUp();
            GeneratedDataCleanUp();
            MagTalksDirCleanup();

        }


        [TearDown]
        public void CleanUp()
        {
            //Stop();
        }

        #endregion


        [Test]
        [Description("Расчистка. Вариант - по общему сроку хранения. Опция - удалять сеансы от всех устройств")]
        public void CleanUp_ByDevices_With_All_Devices()
        {
            //в teamcity description теста не отображается, поэтому добавили в логи
            log.Info("Test step. Старт расчистки. Вариант - по общему сроку хранения. Опция - удалять сеансы от всех устройств ");

            //генерация заданий + сеансов
            int count_tasks = 10;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.enabled = 1;
            DbDataModel.DefUserOptions.all_devices = 1;


            //для того, чтобы дата запуска расчистки была в интервале между наименьшей и наибольшей
            //датами начала сгенеренных сеансов
            
            if (DbDataModel.GetCleanupSeancesDates().Max() > DateTime.Now)
                //есть сеансы, превышающие текущую дату
                DbDataModel.DefUserOptions.session_storage_limit_default = 0;
            else
            {
                //берем среднее значение диапазона дат сеансов, берем разницу как срок хранения
                var date_diff = DbDataModel.GetCleanupSeancesDates().Max() - DbDataModel.GetCleanupSeancesDates().Min();
                var average_date = DbDataModel.GetCleanupSeancesDates().Min().AddDays(date_diff.Days / 2);
                DbDataModel.DefUserOptions.session_storage_limit_default = (DateTime.Now - average_date).Days;

            }

            //записали все в БД
            ProjectDBStructToDatabase();
            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            //сразу по окончанию работы ХП должны быть очищены
            //1.Соответствующие таблицы БД

            //2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению
            var expected_deleted_talks = DbDataModel.Tasks.SelectMany(task => task.Talks).Where(talk => talk.begin.AddDays(DbDataModel.DefUserOptions.session_storage_limit_default) < procStartTime && talk.suppress_remove == 0).ToList();
            var expected_deleted_videos = DbDataModel.Tasks.SelectMany(task => task.Video).Where(video => video.begin.AddDays(DbDataModel.DefUserOptions.session_storage_limit_default) < procStartTime && video.suppress_remove == 0).ToList();
            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);

            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");
            
            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);


        }


        [Test]
        [Description("Расчистка. Вариант - по общему сроку хранения. Опция - удалять сеансы от устройств с заданными номерами")]
        public void CleanUp_ByDevices_With_Some_Devices()
        {
            log.Info("Test step. Старт расчистки. Вариант - по общему сроку хранения. Опция - удалять сеансы от устройств с заданными номерами ");

            //генерация заданий + сеансов
            int count_tasks = 30;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            ProjectDBStructToDatabase();


            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.enabled = 1;
            DbDataModel.DefUserOptions.all_devices = 0;
            DbDataModel.DefUserOptions.devices_list = GenerateDeviceChannelList();


            //для того, чтобы дата запуска расчистки была в интервале между наименьшей и наибольшей
            //датами начала сгенеренных сеансов

            if (DbDataModel.GetCleanupSeancesDates().Max() > DateTime.Now)
                //есть сеансы, превышающие текущую дату
                DbDataModel.DefUserOptions.session_storage_limit_default = 0;
            else
            {
                //берем среднее значение диапазона дат сеансов, берем разницу как срок хранения
                var date_diff = DbDataModel.GetCleanupSeancesDates().Max() - DbDataModel.GetCleanupSeancesDates().Min();
                var average_date = DbDataModel.GetCleanupSeancesDates().Min().AddDays(date_diff.Days / 2);
                DbDataModel.DefUserOptions.session_storage_limit_default = (DateTime.Now - average_date).Days;

            }

           
            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            //сразу по окончанию работы ХП должны быть очищены
            //1.Соответствующие таблицы БД

            //2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению
            var expected_deleted_talks = DbDataModel.Tasks.SelectMany(task => task.Talks).Where(talk => talk.autotask.match_cleanup_settings_by_dev_channel_pair && talk.begin.AddDays(DbDataModel.DefUserOptions.session_storage_limit_default) < procStartTime && talk.suppress_remove == 0).ToList();
            var expected_deleted_videos = DbDataModel.Tasks.SelectMany(task => task.Video).Where(video => video.autotask.match_cleanup_settings_by_dev_channel_pair && video.begin.AddDays(DbDataModel.DefUserOptions.session_storage_limit_default) < procStartTime && video.suppress_remove == 0).ToList();
            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);


            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");

            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);
        }



        [Test]
        [Description("Расчистка. Вариант - по завершению ОТМ. Опция - удалять сеансы от устройств с указанными номерами")]
        public void CleanUp_ByDevices_With_Completed_OTM_Some_Devices()
        {
            log.Info("Test step. Старт расчистки.  Вариант - по завершению ОТМ. Опция - удалять сеансы от устройств с указанными номерами ");

            //генерация заданий + сеансов
            int count_tasks = 20;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok, true);
            //расчитаем плюсовые/минусовые метки
            GeneratePlusMinusMarksForTasks(DateTime.Now);
            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            ProjectDBStructToDatabase();


            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.completed_task_enabled = 1;
            DbDataModel.DefUserOptions.completed_task_devices_all = 0;
            DbDataModel.DefUserOptions.completed_task_devices_list = GenerateDevicesListLight();

            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            //сразу по окончанию работы ХП должны быть очищены
            //1.Соответствующие таблицы БД

            //2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению

            //т.к. в нашем случае ArcKeepMinus = ArcKeepPlus = ArcKeepNotMark,
            //а в логике ХП [newfas_CleanupByCompletedTask] они равнозначны
            var completed_tasks = DbDataModel.Tasks.Where(task=>task.IsCompleted);
            var plus_minus_tasks = completed_tasks.Where(task => task.BeginDate.AddDays(task.TaskSrok).AddDays(task.ArcKeepMinus) < DateTime.Now);
            log.Info(String.Format("Test step. Из {0} тасков {1} имеют статус завершенного, из них {2} отфильтрованы по плюсовым/минусовым меткам  ", DbDataModel.Tasks.Count, completed_tasks.Count<Task>(), plus_minus_tasks.Count<Task>()));

            var expected_deleted_talks = plus_minus_tasks.SelectMany(task => task.Talks).Where(talk => talk.autotask.match_cleanup_settings_by_dev_channel_pair && talk.suppress_remove == 0).ToList();
            var expected_deleted_videos = plus_minus_tasks.SelectMany(task => task.Video).Where(video => video.autotask.match_cleanup_settings_by_dev_channel_pair && video.suppress_remove == 0).ToList();
            
            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);


            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");

            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);
        }




        [Test]
        [Description("Расчистка. Вариант - по завершению ОТМ. Опция - удалять сеансы от всех устройств")]
        public void CleanUp_ByDevices_With_Completed_OTM_All_Devices()
        {
            log.Info("Test step. Старт расчистки.  Вариант - по завершению ОТМ. Опция - удалять сеансы от устройств");

            //генерация заданий + сеансов
            int count_tasks = 20;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok, true);
            //расчитаем плюсовые/минусовые метки
            GeneratePlusMinusMarksForTasks(DateTime.Now);
            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            ProjectDBStructToDatabase();


            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.completed_task_enabled = 1;
            DbDataModel.DefUserOptions.completed_task_devices_all = 1;

            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            //сразу по окончанию работы ХП должны быть очищены
            //1.Соответствующие таблицы БД

            //2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению

            //т.к. в нашем случае ArcKeepMinus = ArcKeepPlus = ArcKeepNotMark,
            //а в логике ХП [newfas_CleanupByCompletedTask] они равнозначны
            var completed_tasks = DbDataModel.Tasks.Where(task => task.IsCompleted);
            var plus_minus_tasks = completed_tasks.Where(task => task.BeginDate.AddDays(task.TaskSrok).AddDays(task.ArcKeepMinus) < DateTime.Now);
            log.Info(String.Format("Test step. Из {0} тасков {1} имеют статус завершенного, из них {2} отфильтрованы по плюсовым/минусовым меткам  ", DbDataModel.Tasks.Count, completed_tasks.Count<Task>(), plus_minus_tasks.Count<Task>()));

            //разговоры и видео без фильтрации по   - autotask.match_cleanup_settings_by_dev_channel_pair
            var expected_deleted_talks = plus_minus_tasks.SelectMany(task => task.Talks).Where(talk => talk.suppress_remove == 0).ToList();
            var expected_deleted_videos = plus_minus_tasks.SelectMany(task => task.Video).Where(video => video.suppress_remove == 0).ToList();

            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);


            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");

            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);
        }




        [Test]
        [Ignore("Must done")]
        [Description("Расчистка. Вариант - по завершению ОТМ. Опция - удалять сеансы от всех устройств, + проверка архивации по меткам MarkDescr")]
        public void CleanUp_ByDevices_With_Completed_OTM_All_Devices_Marks()
        {
            log.Info("Test step. Старт расчистки.  Вариант - по завершению ОТМ. Опция - удалять сеансы от устройств");

            //генерация заданий + сеансов
            int count_tasks = 20;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok, true);
            //расчитаем плюсовые/минусовые метки
            GeneratePlusMinusMarksForTasks(DateTime.Now);
            //GeneratePlusMinusMarksForTasks();
            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            ProjectDBStructToDatabase();


            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.completed_task_enabled = 1;
            DbDataModel.DefUserOptions.completed_task_devices_all = 1;

            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            //сразу по окончанию работы ХП должны быть очищены
            //1.Соответствующие таблицы БД

            //2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению

            //т.к. в нашем случае ArcKeepMinus = ArcKeepPlus = ArcKeepNotMark,
            //а в логике ХП [newfas_CleanupByCompletedTask] они равнозначны
            var completed_tasks = DbDataModel.Tasks.Where(task => task.IsCompleted);
            var plus_minus_tasks = completed_tasks.Where(task => task.BeginDate.AddDays(task.TaskSrok).AddDays(task.ArcKeepMinus) < DateTime.Now);
            log.Info(String.Format("Test step. Из {0} тасков {1} имеют статус завершенного, из них {2} отфильтрованы по плюсовым/минусовым меткам  ", DbDataModel.Tasks.Count, completed_tasks.Count<Task>(), plus_minus_tasks.Count<Task>()));

            //разговоры и видео без фильтрации по   - autotask.match_cleanup_settings_by_dev_channel_pair
            var expected_deleted_talks = plus_minus_tasks.SelectMany(task => task.Talks).Where(talk => talk.suppress_remove == 0).ToList();
            var expected_deleted_videos = plus_minus_tasks.SelectMany(task => task.Video).Where(video => video.suppress_remove == 0).ToList();

            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);


            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");

            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);
        }




        [Test]
        [Description("Расчистка. Вариант - включать авторасчистку по предоставленным сводкам ")]
        public void CleanUp_ByDelivered_Report()
        {
            log.Info("Test step. Старт расчистки.  Вариант - по завершению ОТМ. Опция - удалять сеансы от устройств");

            //генерация заданий + сеансов
            int count_tasks = 10;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok, false);
            
            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            
            var time_life_after_delivery = 30; //дней

            DateTime deadline = DateTime.Now.AddDays(-(time_life_after_delivery + 1));
            log.Info(String.Format("Дедлайн расчистки варианта по завершению ОТМ - {0}, срок хранения сеансов после предоставления -  {1}", deadline, time_life_after_delivery));

           
            //выбрать задания, для которых будут сгенерены сеансы
            var selected_tasks =  Utils.RandomListEntries(DbDataModel.Tasks, _random);
            //отчетов на задание
            var reports_count = 5;

            foreach (var task in selected_tasks)
            {
                GenerateSummariesForTask_WithRandomSeances(task.Meropr, task.VidObject, task.Task_number, task.Task_year, reports_count,
                    false, DateTime.Now, 24 * 3600, null, deadline);
            }

            ProjectDBStructToDatabase();


            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.enabled_by_delivered_reports = 1;
            DbDataModel.DefUserOptions.time_life_after_delivery = time_life_after_delivery;

            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            //сразу по окончанию работы ХП должны быть очищены
            //1.Соответствующие таблицы БД

            //2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению

            //
            var reports_deadline_overdued = DbDataModel.Tasks.SelectMany(task => task.Reports).Where(report => report.PresentedMark.Value <= deadline);
            var reports_deadline_overdue_talks = reports_deadline_overdued.Select(report => report.task).Distinct().SelectMany(task => task.Talks).Where(talk => talk.suppress_remove == 0).ToList();
            var reports_deadline_overdue_videos = reports_deadline_overdued.Select(report => report.task).Distinct().SelectMany(task => task.Video).Where(video => video.suppress_remove == 0).ToList();
            log.Info(String.Format("Test step. Всего сеансов, связанных с просроченными сводками: разговоров - {0}, видео - {1} ", reports_deadline_overdue_talks.Count<Talk>(), reports_deadline_overdue_videos.Count<Video>()));

            //-Пометить сеансы, которые включены в другие сводки с датой доставки не подлежащей удалению
            var reports_not_deadlined = DbDataModel.Tasks.SelectMany(task => task.Reports).Where(report => report.PresentedMark.Value > deadline);
            var reports_not_deadlined_talks = reports_not_deadlined.SelectMany(report => report.Talks).Where(talk => talk.suppress_remove == 0).ToList();
            var reports_not_deadlined_videos = reports_not_deadlined.SelectMany(report => report.Video).Where(video => video.suppress_remove == 0).ToList();


            //log.Info(String.Format("Test step. Из {0} тасков {1} имеют статус завершенного, из них {2} отфильтрованы по плюсовым/минусовым меткам  ", DbDataModel.Tasks.Count, completed_tasks.Count<Task>(), plus_minus_tasks.Count<Task>()));

            //разговоры и видео без фильтрации по   - autotask.match_cleanup_settings_by_dev_channel_pair
            var expected_deleted_talks = reports_deadline_overdue_talks.Except(reports_not_deadlined_talks).ToList();
            var expected_deleted_videos = reports_deadline_overdue_videos.Except(reports_not_deadlined_videos).ToList();

            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);


            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");

            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);
        }




        //В общем случае расчистка непривязанных сеансов  работает, когда  
        //1.Когда к автотаску не привязан ни один таск (dt_mr_Grants не содержит записей)
        //2.Когда несколько автотасков подключены к одному таску (в случае если даты сеансов не входят в совокупный интервал привязки dt_mr_Grants - auto_start, auto_end  )
        //делаем тест для частного случая, когда таск подключается к одному автотаску(для упрощения кода проверки). В этом случае границы 
        //будут равны auto_start, auto_end (dt_mr_Grants)

        [Test]
        [Description("Расчистка. Вариант - включать авторасчистку непривязанных сеансов. Для всех устройств. Вариант 2 ")]
        public void CleanUp_Unbound_Sessions_All_Devices()
        {

            log.Info("Test step. Старт расчистки. Вариант - включать авторасчистку непривязанных сеансов. Для всех устройств. Вариант 2. Автотаски подключены к таскам, некоторые сеансы не входят в интервал привязки ");

            //генерация заданий + сеансов
            int count_tasks = 10;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok, false);

            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //конец интервала будет равен текущей дате
            //расчет данного параметра приведет к неоправданному усложнению теста
            //поэтому 0
            var unbound_sessions_storage_interval = 0;
            
            //var unbound_sessions_storage_interval =  (DateTime.Now - DbDataModel.Tasks.Select(task => task.BeginDate).Max()).Days - 5;//дней

            //DateTime deadline = DateTime.Now.AddDays(-(time_life_after_delivery + 1));
            //log.Info(String.Format("Дедлайн расчистки варианта по завершению ОТМ - {0}, срок хранения сеансов после предоставления -  {1}", deadline, time_life_after_delivery));


            //в данном случае, с такими настройками генерации, часть разговоров по каждому заданию будет попадать под расчистку,
            //и будут попадать все видео
            ProjectDBStructToDatabase(true);


            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.unbound_sessions_enable = 1;
            DbDataModel.DefUserOptions.completed_task_devices_all = 1;
            DbDataModel.DefUserOptions.unbound_sessions_storage_interval = unbound_sessions_storage_interval;

            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            ////сразу по окончанию работы ХП должны быть очищены
            ////1.Соответствующие таблицы БД

            ////2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению

            //умещаем сеанс в границы 
            var expected_deleted_talks = DbDataModel.Tasks.SelectMany(task => task.Talks).Where(talk => talk.begin > talk.autotask.task_grant_end_date).ToList();

            var expected_deleted_videos = DbDataModel.Tasks.SelectMany(task => task.Video).Where(video => video.begin > video.autotask.task_grant_end_date).ToList();

            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);


            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");

            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);
        }



        [Test]
        [Description("Расчистка. Вариант - включать авторасчистку непривязанных сеансов. Для всех устройств. Вариант 1 ")]
        public void CleanUp_Unbound_Sessions_All_Devices_2()
        {
            //тест пока падает, должны быть внесены изменения в ХП CleanupUnboundSessions
            //строки where not exists(select task_auto_id from @ta t where t.auto_start = ta.creation_time)
            log.Info("Test step. Старт расчистки. Вариант - включать авторасчистку непривязанных сеансов. Для всех устройств. Вариант 2. К автотаску может быть не привязан ни один таск ");

            //генерация заданий + сеансов
            int count_tasks = 10;
            //дата первого задания и первого сеанса - за 6 месяцев до текущей даты
            //это гарантирует наличие определенного количества сеансов, попадающих под условия расчистки
            DateTime first_task_date = DateTime.Now.AddDays(-6 * 30);
            int task_interval = 2; //дней
            short task_srok = 30; //продолжительность
            //части шифра
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok, false);

            //сеансы
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 10, 6, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //конец интервала будет равен текущей дате
            //расчет данного параметра приведет к неоправданному усложнению теста
            //поэтому 0
            var unbound_sessions_storage_interval = 0;

            //отобразить таски + автотаски, для некотрых без назначений
            ProjectDBStructToDatabase(false, true);


            //опции расчистки
            DbDataModel.DefUserOptions.work_time_limit = 5 * 60; //5 МИН
            DbDataModel.DefUserOptions.unbound_sessions_enable = 1;
            DbDataModel.DefUserOptions.completed_task_devices_all = 1;
            DbDataModel.DefUserOptions.unbound_sessions_storage_interval = unbound_sessions_storage_interval;

            ProjectSettingsToDb();

            //вызвали процедуру расчистки
            //время запуска процедуры, будет различие максимум в несколько сек. со значением 
            //@procStartTime  в ХП [newfas_CleanupByDevices_Job]
            var procStartTime = DateTime.Now;
            CleanupSPInvoke();

            ////сразу по окончанию работы ХП должны быть очищены
            ////1.Соответствующие таблицы БД

            ////2.Заполена таблица newfas_WorkDelete сеансами, подлежащими удалению

            //умещаем сеанс в границы 
            var expected_deleted_talks = DbDataModel.Tasks.SelectMany(task => task.Talks).Where(talk => talk.autotask.granted == false).ToList();

            var expected_deleted_videos = DbDataModel.Tasks.SelectMany(task => task.Video).Where(video => video.autotask.granted == false).ToList();

            AssertEx_WorkDelete(expected_deleted_talks, expected_deleted_videos);


            //ждем пока сервис не отработает
            while (FasDeleteQueueNotEmpty())
            {
                Thread.Sleep(5000);
            }

            log.Info("Test step. FAS-сервис завершил расчистку,таблица dt_newfas_WorkDelete пуста ");

            //отработал - проверяем результат

            AsertEx_SeancesDeleted(procStartTime, expected_deleted_talks, expected_deleted_videos);
        }





    }







    






    //тесты с аналогичными условиями, но с большим количеством сеансов
    //для нагрузочного тестирования
    [TestFixture]
    public class CleanupLoadingTests : BaseTest
    {
        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            DataGeneratorInit();

        }

        [SetUp]
        public void Initialise()
        {
            DataBasesCleanUp();
            GeneratedDataCleanUp();
        }


        [TearDown]
        public void CleanUp()
        {
            //Stop();
        }

        #endregion


        [Test]
        [Ignore("Must done")]
        [Description("Расчистка. Вариант - по общему сроку хранения. Опция - удалять сеансы от всех устройств")]
        public void CleanUp_ByDevices_With_Drop_Files()
        {

        }




    }


}
