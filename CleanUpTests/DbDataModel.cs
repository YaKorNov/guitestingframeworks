﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using CommonGenerator.Moj.Enums;
using  DataGenerator.Mtb.Tables;


//TODO рефактор, перенос класса в CommonGenerator  (при увеличении числа тестов)
//одна и таж е струкутра БД сейчас используется в CleanUPTests и в RimGUITests
namespace CleanUpTests
{

   
    public class DbDataModel
    {
        public IList<Task> Tasks { get; set; }
        public IList<AutoTask> AutoTasks { get; set; }
        public IList<Mark> Marks { get; set; }
        public DefaultUserOptions DefUserOptions { get; set; }


        public DbDataModel()
        {
            //не установлены условия для расчистки
            DefUserOptions = new DefaultUserOptions();

            AutoTasks = new List<AutoTask>(); 
            Tasks = new List<Task>();
            Marks = new List<Mark>();


        }



        public Task GetTaskByCipherParts(string meropr, string vid_object, string task_number, string task_year)
        {
            return Tasks.Where(task => task.Meropr == meropr &&
                task.VidObject == vid_object && task.Task_number == task_number && task.Task_year == task_year).First();
        }


        public List<DateTime> GetReportsDates(Task for_task = null)
        {

            var rep_dates = new List<DateTime>();
            
            foreach (var task in Tasks)
            {
                if (for_task != null && for_task != task)
                    continue;

                foreach (var report in task.Reports)
                {
                    rep_dates.Add(report.ReportDate);
                }

            }

            return rep_dates;

        }

        //получить даты начала сеансов, подвергаемых расчистке
        public List<DateTime> GetCleanupSeancesDates()
        {
        
            List<DateTime> _dt = new List<DateTime>();

            _dt.AddRange(Tasks.SelectMany(task => task.Talks).Select(talk => talk.begin).ToList());
            _dt.AddRange(Tasks.SelectMany(task => task.Video).Select(video => video.begin).ToList());
            
           
            return _dt;

        }

    }



    public class DefaultUserOptions
    {

        //макс. вермя работы после запуска (в минутах)
        [Description("cleanup.work_time_limit")]
        public int work_time_limit {get ;set;}
        //удалять только файлы
        [Description("cleanup.files_only")]
        public int files_only {get ;set;}
        //расчистка по общему сроку хранения
        [Description("cleanup.enabled")]
        public int enabled {get ;set;}
        //удалять сеансы от всех устройств
        [Description("cleanup.all_devices")]
        public int all_devices {get ;set;}
        //список устройств
        [Description("cleanup.devices_list")]
        public string devices_list {get ;set;}
        // срок хранения сеансов в сутках
        [Description("cleanup.session_storage_limit.default")]
        public int session_storage_limit_default {get ;set;}
        //расчистка по предоставленным сводкам
        [Description("cleanup.enabled_by_delivered_reports")]
        public int enabled_by_delivered_reports {get ;set;}
        //срок хранения сеансов после предоставления (в сутках)
        [Description("cleanup.time_life_after_delivery")]
        public int time_life_after_delivery {get ;set;}
         //расчистка по завершенным ОТМ
        [Description("cleanup.completed_task_enabled")]
        public int completed_task_enabled {get ;set;}
        //удалять сеансы от всех устройств
        [Description("cleanup.completed_task_devices_all")]
        public int completed_task_devices_all {get ;set;}
        //удалять сеансы от устр-в с номерами
        [Description("cleanup.completed_task_devices_list")]
        public string completed_task_devices_list {get ;set;}
        //расчистка непривязанных сеансов
        [Description("cleanup.unbound_sessions_enable")]
        public int unbound_sessions_enable {get ;set;}
        //срок хранения непривязанных
        [Description("cleanup.unbound_sessions_storage_interval")]
        public int unbound_sessions_storage_interval {get ;set;}


        public DefaultUserOptions()
        {
            //1 минута
            work_time_limit = 60;
            files_only = 1;

            enabled = 0;
            all_devices = 0;
            devices_list = "";
            session_storage_limit_default = 0;

            enabled_by_delivered_reports = 0;
            time_life_after_delivery =  0;
            
            completed_task_enabled = 0;
            completed_task_devices_all = 0;
            completed_task_devices_all = 0;
            completed_task_devices_list = "";

            unbound_sessions_enable = 0;
            unbound_sessions_storage_interval = 0;
        }


        public Dictionary<string,string>  GetDict()
        {
            var props =  this.GetType().GetProperties();

            Dictionary<string, string> _dict = new Dictionary<string, string>();


            foreach (var prop in props)
            {
                DescriptionAttribute[] attributes =
                (DescriptionAttribute[])prop.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

                string desc = "";

                if (attributes != null && attributes.Length > 0)
                    desc =  attributes[0].Description;
                else
                    desc = prop.ToString();

                _dict.Add(desc, prop.GetValue(this).ToString() );

            }

            return _dict;

           

        }

    }




    public class Task
    {

        public Task()
        {
            Talks = new List<Talk>();
            Sms = new List<Sms>();
            Video = new List<Video>();
            Places = new List<Place>();

            Reports = new List<Report>();

            SeancesCount = "0";
            SrokInfo = "Сводок нет";

            IsCompleted = false; 

            ArcKeepPlus = 10;

            ArcKeepMinus = 20;

            ArcKeepNotMark = 30;

        }

        public bool IsCompleted { get; set; }

        //агрегируемая информация
        //включать сеансов в сводку
        public string SeancesCount { get; set; }

        //количество дней, прошедших с последней предыдущей печати сводки
        public string SrokInfo { get; set; }

        public string Otdel { get; set; }
        public string Init_organization { get; set; }
        public string Init_podrazd { get; set; }
        public string Init_Famini { get; set; }
        public string Init_phoneA { get; set; }

        public string Init_phoneB { get; set; }

        public string Meropr { get; set; }

        public string VidObject { get; set; }

        public string Task_number { get; set; }

        public string Task_year { get; set; }

        public DateTime BeginDate { get; set; }

        public short TaskSrok { get; set; }

        public string PhoneControl { get; set; }

        public string IMSI { get; set; }

        public string IMEI { get; set; }

        public string Orient { get; set; }

        public TaskTarget Goal { get; set; }


        public int ArcKeepPlus { get; set; }

        public int ArcKeepMinus { get; set; }

        public int ArcKeepNotMark { get; set; }

        public string TaskShifr { get { return String.Format("{0}-{1}-{2}-{3}", Meropr, VidObject, Task_number, Task_year); } }
            

        public IList<Talk> Talks { get; set; }
        public IList<Sms> Sms { get; set; }
        public IList<Video> Video { get; set; }
        public IList<Place> Places { get; set; }

        public IList<Report> Reports { get; set; }


        public List<DateTime>  GetSeancesDates()
        {
            List<DateTime> _dt = new List<DateTime>();

            foreach (var talk in Talks)
            {
                _dt.Add(talk.begin);
            }

            foreach (var sms in Sms)
            {
                _dt.Add(sms.sendTime);
            }

            //dates equals talks
            //foreach (var place in Places)
            //{
            //    _dt.Add(place.sendTime);
            //}

            foreach (var video in Video)
            {
                _dt.Add(video.begin);
            }


            return _dt;

        }


        public List<Talk> GetTalksIncludedInSummaries()
        {
            var incl_talks = new List<Talk>();
            foreach (Report rep in this.Reports)
            {
                incl_talks.AddRange(rep.Talks);

            };


            return incl_talks.Distinct().ToList();
        }

        public List<Sms> GetSmsIncludedInSummaries()
        {
            var incl_sms = new List<Sms>();

            foreach (Report rep in this.Reports)
            {
                incl_sms.AddRange(rep.Sms);

            };


            return incl_sms.Distinct().ToList();

        }


        public List<Place> GetPlacesIncludedInSummaries()
        {
            var incl_places = new List<Place>();

            foreach (Report rep in this.Reports)
            {
                incl_places.AddRange(rep.Places);

            };


            return incl_places.Distinct().ToList();
        }


        public List<Video> GetVideoIncludedInSummaries()
        {
            var incl_videos = new List<Video>();

            foreach (Report rep in this.Reports)
            {
                incl_videos.AddRange(rep.Video);

            };


            return incl_videos.Distinct().ToList();
        }



        public string Get_Task_Shifr()
        {
            return String.Format("{0}-{1}-{2}-{3}", Meropr, VidObject, Task_number, Task_year);
        }

    }



    public class AutoTask
    {

        public int id { get; set; }
        public string autotask_number { get; set; }
        public int device_id { get; set; }
        public int channel_id { get; set; }
        public bool match_cleanup_settings_by_dev_channel_pair { get; set; }
        public DateTime? task_grant_end_date {get ; set;}
        public bool granted { get; set; }

        public AutoTask()
        {
            match_cleanup_settings_by_dev_channel_pair = false;
            task_grant_end_date = null;
            granted = false;
        }
    }




    public interface ISeance
    {
        //public Seance() { }

    }


    public class Talk : System.Object, ISeance
    {

        public Talk()
        {
            Marks = new List<Mark>();

            suppress_remove = 0;
        }


        public int seanceId { get; set; }
        public Task task {get; set;}
        public DateTime begin { get; set; }
        public DateTime end { get; set; }
        public TalkPhoneDirection dir { get; set; }
        public string InterlocutorPhone { get; set; }
        public bool createAudioFile { get; set; }
        public IList<Mark> Marks { get; set; }

        public Steno Steno { get; set; }

        //0 - удаление разрешено. При значении > 0 - счетчик количества заданий, в рамках которых сеанс должен храниться.
        public byte suppress_remove { get; set; }

        public AutoTask autotask { get; set; }


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            Talk t = obj as Talk;
            if ((System.Object)t == null)
            {
                return false;
            }


            // учитывая, что все генерируемые даты уникальны
            return (task == t.task)
                && (begin == t.begin);
        }


        public bool Equals(Talk obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // учитывая, что все генерируемые даты уникальны
            return (task == obj.task)
                && (begin == obj.begin);
        }

        public override int GetHashCode()
        {
            //в данном случае имеем в виду, что все сгенерированные сеансы уникальны по 
            //совокупности набора полей

            byte[] task_asciiBytes = Encoding.ASCII.GetBytes(task.ToString());
            byte[] begin_asciiBytes = Encoding.ASCII.GetBytes(begin.ToString());
           
            int total = 0;

            foreach (var byte_s in task_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in begin_asciiBytes)
            {
                total += (int)byte_s;
            }

            
            return total;


        }



    }

    public class Sms : ISeance
    {

         public Sms()
        {
            Marks = new List<Mark>();
        }

         public int seanceId { get; set; }
        public Task task {get; set;}

        public DateTime sendTime {get; set;}

        public DateTime recvTime {get; set;}

        public PhoneDirection Direction {get; set;}
        public string InterlocutorPhone { get ; set;}

        public string text { get; set; }
        public bool archived { get ; set;}

        public IList<Mark> Marks { get; set; }

        public AutoTask autotask { get; set; }



        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            Sms t = obj as Sms;
            if ((System.Object)t == null)
            {
                return false;
            }


            // учитывая, что все генерируемые даты уникальны
            return (task == t.task)
                && (sendTime == t.sendTime);
        }


        public bool Equals(Sms obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // учитывая, что все генерируемые даты уникальны
            return (task == obj.task)
                && (sendTime == obj.sendTime);
        }

        public override int GetHashCode()
        {
            //в данном случае имеем в виду, что все сгенерированные сеансы уникальны по 
            //совокупности набора полей

            byte[] task_asciiBytes = Encoding.ASCII.GetBytes(task.ToString());
            byte[] begin_asciiBytes = Encoding.ASCII.GetBytes(sendTime.ToString());

            int total = 0;

            foreach (var byte_s in task_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in begin_asciiBytes)
            {
                total += (int)byte_s;
            }


            return total;


        }

    }

    public class Video : ISeance
    {
        public Video()
        {
            Marks = new List<Mark>();

            suppress_remove = 0;
        }

         public int seanceId { get; set; }
        public Task task {get; set;}
        public DateTime begin { get; set; }
        public DateTime end { get; set; }
        
        public bool proccessed  { get; set; }
        
        public bool createtVideoFile  { get; set; }

        public IList<Mark> Marks { get; set; }

        public Steno Steno { get; set; }

        public byte suppress_remove { get; set; }

        public AutoTask autotask { get; set; }



        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            Video t = obj as Video;
            if ((System.Object)t == null)
            {
                return false;
            }


            // учитывая, что все генерируемые даты уникальны
            return (task == t.task)
                && (begin == t.begin);
        }


        public bool Equals(Video obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // учитывая, что все генерируемые даты уникальны
            return (task == obj.task)
                && (begin == obj.begin);
        }

        public override int GetHashCode()
        {
            //в данном случае имеем в виду, что все сгенерированные сеансы уникальны по 
            //совокупности набора полей

            byte[] task_asciiBytes = Encoding.ASCII.GetBytes(task.ToString());
            byte[] begin_asciiBytes = Encoding.ASCII.GetBytes(begin.ToString());

            int total = 0;

            foreach (var byte_s in task_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in begin_asciiBytes)
            {
                total += (int)byte_s;
            }


            return total;


        }

    }

    public class Place : ISeance
    {

        public Place()
        {
            Marks = new List<Mark>();
        }


        public int seanceId { get; set; }
        public Task task {get; set;}

        public Talk talk {get; set;}

        //код страны
        //(int)MCC.Russia; //Россия
        public int mcc  {get; set;}

        //код оператора
        //(int)MNC.MTS; //МТС
        public int mnc  {get; set;}
        //код локальной зоны       
        public int lac {get; set;}
        //идентификатор соты 
        public int cl { get; set; }

        public IList<Mark> Marks { get; set; }

        public AutoTask autotask { get; set; }

        


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            Place t = obj as Place;
            if ((System.Object)t == null)
            {
                return false;
            }


            // учитывая, что все генерируемые даты уникальны
            return (task == t.task)
                && (mnc == t.mnc) && (lac == t.lac) && (cl == t.cl);
        }


        public bool Equals(Place obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // учитывая, что все генерируемые даты уникальны
            return (task == obj.task)
                && (mnc == obj.mnc) && (lac == obj.lac) && (cl == obj.cl);
        }

        public override int GetHashCode()
        {
            //в данном случае имеем в виду, что все сгенерированные сеансы уникальны по 
            //совокупности набора полей


            byte[] task_asciiBytes = Encoding.ASCII.GetBytes(task.ToString());
            byte[] mnc_asciiBytes = Encoding.ASCII.GetBytes(mnc.ToString());
            byte[] lac_asciiBytes = Encoding.ASCII.GetBytes(lac.ToString());
            byte[] cl_asciiBytes = Encoding.ASCII.GetBytes(cl.ToString());

            int total = 0;

            foreach (var byte_s in task_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in mnc_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in lac_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in cl_asciiBytes)
            {
                total += (int)byte_s;
            }


            return total;


        }


    }


    public class Report
    {

        public Report()
        {
            Talks = new List<Talk>();
            Sms = new List<Sms>();
            Video = new List<Video>();
            Places = new List<Place>();

        }
       

        public Task task { get; set; }

        public bool IsPrinted { get; set; }
        
        public bool IsReprinted { get; set; }
        
        public int PageCount { get; set; }

        public DateTime? PrintTime { get; set; }

        public DateTime ReportBegin { get; set; }

        public DateTime ReportEnd { get; set; }

        public DateTime ReportDate { get; set; }

        public string ReportNumber { get; set; }
       
        public int SeanceCount { get; set; }

        public int? UserIdPrinted { get; set; }

        public ReportType reportType { get; set; }

        public DateTime? PresentedMark {get; set;}

        public IList<Talk> Talks { get; set; }
        public IList<Sms> Sms { get; set; }
        public IList<Video> Video { get; set; }
        public IList<Place> Places { get; set; }
    }


  


    public class Mark
    {
        public string Title {get; set;}
        public string Descr {get; set;}
    }


    

    public class Steno
    {
        //для РИМа необязательно проверять
        public Texts8.Texts8State State { get; set; }
        public string text {get; set;}

        //проверять надо, 
        //public User user { get; set; }

    }



   


}
