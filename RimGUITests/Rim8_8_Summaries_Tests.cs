﻿using System.Threading;
using NUnit.Framework;
using NUnit.Core;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using RimGUI.Tests.Framework.Models;
using System.Text.RegularExpressions;
using System;
using log4net;




namespace RimGUITests
{
    [TestFixture]
    [Description(@"Перед запуском тестов РИМ необходимо настроить,
                выбрав необходимые для поиска колонки, т.к. иначе
                тесты с гридами могут валиться по причине отсутствия в видимой области грида колонок")]
    public class Rim88SummariesTests : BaseTest
    {

        
        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            StartTestCase();

        }

        [SetUp]
        public void Initialise()
        {
            DataBasesCleanUp();
            GeneratedDataCleanUp();
        }


        [TearDown]
        public void CleanUp()
        {
            Stop();
        }

        #endregion

        
        [Test]
        [Description("Тест строится на основе функции GetSummaryTabTasksInfo, которая ожидает изменения количества отображаемых тасков")]
        public void FilterTasks_ByName()
        {
            //БД
            int count_tasks = 10;
            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);

            //Application
            Start();
            

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            //case 1: full tasks list
            var tasks = App.MainWindow.SummaryControl.GetSummaryTabTasksInfo(0);
            var expected_tasks = DbDataModel.Tasks;
            AssertEqual_TaskInfoShortList_To_DBStructureTaskList(expected_tasks, tasks);

            //case 2: search by first task name
            App.MainWindow.SummaryControl.SetSearchText(String.Format("{0}-{1}-1-{2}", meropr, vid_object, first_task_date.Year));
            tasks = App.MainWindow.SummaryControl.GetSummaryTabTasksInfo(tasks.Count);
            expected_tasks = DbDataModel.Tasks.Where(task => task.Meropr == meropr && task.VidObject == vid_object
                && task.Task_number == "1" && task.Task_year == first_task_date.Year.ToString()).ToList();
            AssertEqual_TaskInfoShortList_To_DBStructureTaskList(expected_tasks, tasks);

            //case 3: search by substring
            App.MainWindow.SummaryControl.ClearSearchText();
            App.MainWindow.SummaryControl.SetSearchText(String.Format("{0}-{1}-1", meropr, vid_object));
            tasks = App.MainWindow.SummaryControl.GetSummaryTabTasksInfo(tasks.Count);
            expected_tasks = DbDataModel.Tasks.Where(task => task.Meropr == meropr && task.VidObject == vid_object
                && task.Task_number[0] == '1').ToList();
            AssertEqual_TaskInfoShortList_To_DBStructureTaskList(expected_tasks, tasks);

            //case 4: clear search
            App.MainWindow.SummaryControl.ClearSearchText();
            tasks = App.MainWindow.SummaryControl.GetSummaryTabTasksInfo(tasks.Count);
            expected_tasks = DbDataModel.Tasks;
            AssertEqual_TaskInfoShortList_To_DBStructureTaskList(expected_tasks, tasks);

            Assert.Pass();
        }


        //App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing)
        [Test]
        [Description("")]
        [Ignore("TODO test for assigned tasks")]
        public void FilterTasksByTaskType()
        {

        }


        [Test]
        [Description("проверка отображения данных сеансов БД на GUI")]
        public void CheckSummaryPreviewSeances_All()
        {
            //БД
            int count_tasks = 10;
            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            //метки очищены в подготовке, их нет
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.PressOkBtn();

            //т.к. таски генерятся рэндомом, то выбирая значения 
            //2,5,8 будем все равно получать различные значения

            int[] ckecked_tasks_numbers = new int[]{
                2, 5, 8
            };

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            foreach (var c_task_number in ckecked_tasks_numbers)
            {
                App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, c_task_number, first_task_date.Year));
                var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

                var selected_task_in_db =  DbDataModel.Tasks.Where(task => task.Meropr == meropr &&
                    task.VidObject == vid_object && task.Task_number == c_task_number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();

                AssertEqual_SummaryPreviews(_prev_infos, 
                    selected_task_in_db.Talks,
                    new List<Talk>(),
                    selected_task_in_db.Sms,
                    new List<Sms>(),
                    selected_task_in_db.Places,
                    new List<Place>(),
                    selected_task_in_db.Video,    
                    new List<Video>()
                    );
            }

            Assert.Pass();


        }



        //разбиваем тесты по типам сеансов 
        [Test]
        [Description("проверка фильтрации данных сеансов c типом - разговор")]
        public void CheckSummaryPreviewSeances_FilterBy_SeanceType_Talk()
        {
            //БД
            int count_tasks = 10;
            
            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //case 1: Talks
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.UnCheckSms();
            App.SettingsWindow.SummariesTabControl.UnCheckVideo();
            App.SettingsWindow.SummariesTabControl.UnCheckPlace();
            //метки очищены в подготовке, их нет
            App.SettingsWindow.PressOkBtn();



            //делаем прогон нескольких заданий по одному виду сеанса 
            int[] ckecked_tasks_numbers = new int[]{
                2, 5, 8
            };

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            foreach (var c_task_number in ckecked_tasks_numbers)
            {
                App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, c_task_number, first_task_date.Year));
                var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

                var selected_task_in_db = DbDataModel.Tasks.Where(task => task.Meropr == meropr &&
                    task.VidObject == vid_object && task.Task_number == c_task_number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();

                //передаем пустые списки, т.к. должны быть только разговоры
                AssertEqual_SummaryPreviews(_prev_infos,
                    selected_task_in_db.Talks, new List<Talk>(), 
                    new List<Sms>(), selected_task_in_db.Sms,
                    new List<Place>(),  selected_task_in_db.Places,
                    new List<Video>(), selected_task_in_db.Video);    
            }


            Assert.Pass();



        }



        [Test]
        [Description("проверка фильтрации данных сеансов c типом - смс")]
        public void CheckSummaryPreviewSeances_FilterBy_SeanceType_Sms()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //case 1: Talks
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.UnCheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.UnCheckVideo();
            App.SettingsWindow.SummariesTabControl.UnCheckPlace();
            //метки очищены в подготовке, их нет
            App.SettingsWindow.PressOkBtn();



            //делаем прогон нескольких заданий по одному виду сеанса 
            int[] ckecked_tasks_numbers = new int[]{
                2, 5, 8
            };

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            foreach (var c_task_number in ckecked_tasks_numbers)
            {
                App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, c_task_number, first_task_date.Year));
                var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

                var selected_task_in_db = DbDataModel.Tasks.Where(task => task.Meropr == meropr &&
                    task.VidObject == vid_object && task.Task_number == c_task_number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();

                
                AssertEqual_SummaryPreviews(_prev_infos,
                    new List<Talk>(), selected_task_in_db.Talks,
                    selected_task_in_db.Sms, new List<Sms>(),
                    new List<Place>(), selected_task_in_db.Places, 
                    new List<Video>(), selected_task_in_db.Video);
            }


            Assert.Pass();



        }




        [Test]
        public void CheckSummaryPreviewSeances_FilterBy_SeanceType_Places()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //case 1: Talks
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.UnCheckTalk();
            App.SettingsWindow.SummariesTabControl.UnCheckSms();
            App.SettingsWindow.SummariesTabControl.UnCheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            //метки очищены в подготовке, их нет
            App.SettingsWindow.PressOkBtn();



            //делаем прогон нескольких заданий по одному виду сеанса 
            int[] ckecked_tasks_numbers = new int[]{
                2, 5, 8
            };

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            foreach (var c_task_number in ckecked_tasks_numbers)
            {
                App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, c_task_number, first_task_date.Year));
                var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

                var selected_task_in_db = DbDataModel.Tasks.Where(task => task.Meropr == meropr &&
                    task.VidObject == vid_object && task.Task_number == c_task_number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();


                AssertEqual_SummaryPreviews(_prev_infos,
                    new List<Talk>(), selected_task_in_db.Talks,
                    new List<Sms>(), selected_task_in_db.Sms,
                    selected_task_in_db.Places, new List<Place>(), 
                    new List<Video>(), selected_task_in_db.Video);
            }


            Assert.Pass();



        }





        [Test]
        public void CheckSummaryPreviewSeances_FilterBy_SeanceType_Video()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //case 1: Talks
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.UnCheckTalk();
            App.SettingsWindow.SummariesTabControl.UnCheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.UnCheckPlace();
            //метки очищены в подготовке, их нет
            App.SettingsWindow.PressOkBtn();



            //делаем прогон нескольких заданий по одному виду сеанса 
            int[] ckecked_tasks_numbers = new int[]{
                2, 5, 8
            };

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            foreach (var c_task_number in ckecked_tasks_numbers)
            {
                App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, c_task_number, first_task_date.Year));
                var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

                var selected_task_in_db = DbDataModel.Tasks.Where(task => task.Meropr == meropr &&
                    task.VidObject == vid_object && task.Task_number == c_task_number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();


                AssertEqual_SummaryPreviews(_prev_infos,
                    new List<Talk>(), selected_task_in_db.Talks,
                    new List<Sms>(), selected_task_in_db.Sms,
                    new List<Place>(), selected_task_in_db.Places, 
                    selected_task_in_db.Video, new List<Video>());
            }


            Assert.Pass();



        }



        [Test]
        public void CheckSummaryPreviewSeances_FilterBy_Marks()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();
            GenerateMarksForSeances();

            //Application
            Start();

            //номер задания
            var task_number = _random.Next(1, count_tasks+1);
            var selected_task_in_db = DbDataModel.Tasks.Where(task => task.Meropr == meropr &&
                task.VidObject == vid_object && task.Task_number == task_number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();
           
            //метки
            var mark_1 = DbDataModel.Marks[0].Title;
            var mark_2 = DbDataModel.Marks[_random.Next(1, DbDataModel.Marks.Count)].Title;
            var marks_list = new List<string>()
            {
                mark_1, mark_2
            };

            //case 1. все метки не отмечены, поэтому условие отбора - "включая все сеансы"
           
            //TODO
            //2.В конце каждого теста с метками необходимо снимать все отметки (лучше всего писать в [UserManagerBase].[dbo].[um_Settings])
            //просто очистка записи 7D783AC2-5DAE-4E6E-AB6E-3FF68A1DAA10 не поможет
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            //метки не отмечены
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();
             //"включая все сеансы"
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks, new List<Talk>(),
                selected_task_in_db.Sms, new List<Sms>(),
                selected_task_in_db.Places, new List<Place>(), 
                selected_task_in_db.Video, new List<Video>());




            //case 2. Отмечаем одну из меток, => условие отбора "включая только сеансы, помеченные меткой"
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_1);
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();
            
            //"включая только сеансы, помеченные меткой"
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks.Where(talk => talk.Marks.Any(mark => mark.Title == mark_1)).ToList(),
                selected_task_in_db.Talks.Where(talk => !talk.Marks.Any(mark => mark.Title == mark_1)).ToList(),
                selected_task_in_db.Sms.Where(sms => sms.Marks.Any(mark => mark.Title == mark_1)).ToList(),
                selected_task_in_db.Sms.Where(sms => !sms.Marks.Any(mark => mark.Title == mark_1)).ToList(),
                selected_task_in_db.Places.Where(place => place.Marks.Any(mark => mark.Title == mark_1)).ToList(),
                selected_task_in_db.Places.Where(place => !place.Marks.Any(mark => mark.Title == mark_1)).ToList(),
                selected_task_in_db.Video.Where(video => video.Marks.Any(mark => mark.Title == mark_1)).ToList(),
                selected_task_in_db.Video.Where(video => !video.Marks.Any(mark => mark.Title == mark_1)).ToList()
                );



            //case 3. Отмечаем вторую из выбранных меток, => условие отбора "включая только сеансы, помеченные меткой"
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            //снимаем первую метку 
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_1);
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_1);
            //ставим вторую метку
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_2);
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            //"включая только сеансы, помеченные меткой"
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks.Where(talk => talk.Marks.Any(mark => mark.Title == mark_2)).ToList(),
                selected_task_in_db.Talks.Where(talk => !talk.Marks.Any(mark => mark.Title == mark_2)).ToList(),
                selected_task_in_db.Sms.Where(sms => sms.Marks.Any(mark => mark.Title == mark_2)).ToList(),
                selected_task_in_db.Sms.Where(sms => !sms.Marks.Any(mark => mark.Title == mark_2)).ToList(),
                selected_task_in_db.Places.Where(place => place.Marks.Any(mark => mark.Title == mark_2)).ToList(),
                selected_task_in_db.Places.Where(place => !place.Marks.Any(mark => mark.Title == mark_2)).ToList(),
                selected_task_in_db.Video.Where(video => video.Marks.Any(mark => mark.Title == mark_2)).ToList(),
                selected_task_in_db.Video.Where(video => !video.Marks.Any(mark => mark.Title == mark_2)).ToList()
                );



            //case 4. Отмечаем обе метки, => условие отбора "включая все сеансы, помеченные метками"
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            //ставим первую метку 
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_1);
            //вторая метка уже установлена
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            //"включая все сеансы, помеченные метками"
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks.Where(talk => talk.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList(),
                selected_task_in_db.Talks.Except(selected_task_in_db.Talks.Where(talk => talk.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList()).ToList(),
                selected_task_in_db.Sms.Where(sms => sms.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList(),
                selected_task_in_db.Sms.Except(selected_task_in_db.Sms.Where(sms => sms.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList()).ToList(),
                selected_task_in_db.Places.Where(place => place.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList(),
                selected_task_in_db.Places.Except(selected_task_in_db.Places.Where(place => place.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList()).ToList(),
                selected_task_in_db.Video.Where(video => video.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList(),
                selected_task_in_db.Video.Except(selected_task_in_db.Video.Where(video => video.Marks.Any(mark => marks_list.Contains(mark.Title))).ToList()).ToList()
                );




            //убираем метки
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_1);
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_1);
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_2);
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_2);
            App.SettingsWindow.PressOkBtn();
            



            //case 5. Повторно отмечаем одну из меток, => условие отбора "исключая сеансы, помеченные меткой"
            //TODO версия РИМа 1.8.0.449,  тут тест падает(на закладке архивы - работает)
            //summary settings
            //App.MainWindow.ClickSettingsButton();
            //App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            //App.SettingsWindow.SummariesTabControl.CheckTalk();
            //App.SettingsWindow.SummariesTabControl.CheckSms();
            //App.SettingsWindow.SummariesTabControl.CheckVideo();
            //App.SettingsWindow.SummariesTabControl.CheckPlace();
            ////
            //App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_1);
            ////снимаем метку со второй
            //App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_2);
            //App.SettingsWindow.SummariesTabControl.SetMarkCheckBox(mark_2);
            //App.SettingsWindow.PressOkBtn();

            //App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
            //    meropr, vid_object, task_number, first_task_date.Year));
            //_prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            ////"исключая сеансы, помеченные меткой"
            //AssertEqual_SummaryPreviews(_prev_infos,
            //    selected_task_in_db.Talks.Where(talk => !talk.Marks.Any(mark => mark.Title == mark_1)).ToList(),
            //    selected_task_in_db.Talks.Where(talk => talk.Marks.Any(mark => mark.Title == mark_1)).ToList(),
            //    selected_task_in_db.Sms.Where(sms => !sms.Marks.Any(mark => mark.Title == mark_1)).ToList(),
            //    selected_task_in_db.Sms.Where(sms => sms.Marks.Any(mark => mark.Title == mark_1)).ToList(),
            //    selected_task_in_db.Places.Where(place => !place.Marks.Any(mark => mark.Title == mark_1)).ToList(),
            //    selected_task_in_db.Places.Where(place => place.Marks.Any(mark => mark.Title == mark_1)).ToList(),
            //    selected_task_in_db.Video.Where(video => !video.Marks.Any(mark => mark.Title == mark_1)).ToList(),
            //    selected_task_in_db.Video.Where(video => video.Marks.Any(mark => mark.Title == mark_1)).ToList()
            //    );
            


            //Assert.Pass();



        }



        [Test]
        [Description("Фильтрация по периоду (включая все сенасы) ")]
        public void CheckSummaryPreviewSeances_FilterBy_Period_Include_All_Seances()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);


            //case 1.  "включая все сеансы"
            //summary settings

            //workaround
            //App.MainWindow.Click1TabControl();
            //App.MainWindow.Click2TabControl();
            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetPeriod(selected_task_in_db.GetSeancesDates().Min(), selected_task_in_db.GetSeancesDates().Max());
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();


            //"включая все сеансы"
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks, new List<Talk>(),
                selected_task_in_db.Sms, new List<Sms>(),
                selected_task_in_db.Places, new List<Place>(),
                selected_task_in_db.Video, new List<Video>());

        }



        [Test]
        [Description("Фильтрация по периоду (не включая первый по времени сеанс) ")]
        public void CheckSummaryPreviewSeances_FilterBy_Period__Not_Include_First_Seance()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);


             
            var period_begin = selected_task_in_db.GetSeancesDates().Min().AddSeconds(1);
            var period_end = selected_task_in_db.GetSeancesDates().Max();

            //workaround
            //App.MainWindow.Click1TabControl();
            //App.MainWindow.Click2TabControl();

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetPeriod(period_begin, period_end);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            //TODO add Extension methods to this and another tests
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks.Where(talk => talk.begin >= period_begin && talk.begin <= period_end).ToList(),
                selected_task_in_db.Talks.Except(selected_task_in_db.Talks.Where(talk => talk.begin >= period_begin && talk.begin <= period_end).ToList()).ToList(),
                selected_task_in_db.Sms.Where(sms => sms.recvTime >= period_begin && sms.recvTime <= period_end).ToList(),
                selected_task_in_db.Sms.Except(selected_task_in_db.Sms.Where(sms => sms.recvTime >= period_begin && sms.recvTime <= period_end).ToList()).ToList(),
                selected_task_in_db.Places.Where(place => place.talk.begin >= period_begin && place.talk.begin <= period_end).ToList(),
                selected_task_in_db.Places.Except(selected_task_in_db.Places.Where(place => place.talk.begin >= period_begin && place.talk.begin <= period_end).ToList()).ToList(),
                selected_task_in_db.Video.Where(video => video.begin >= period_begin && video.begin <= period_end).ToList(),
                selected_task_in_db.Video.Except(selected_task_in_db.Video.Where(video => video.begin >= period_begin && video.begin <= period_end).ToList()).ToList()
                );

        }



        [Test]
        [Description("Фильтрация по периоду (только последний по времени сеанс) ")]
        public void CheckSummaryPreviewSeances_FilterBy_Period_Last_Seance()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);


            var period_begin = selected_task_in_db.GetSeancesDates().Max();
            var period_end = selected_task_in_db.GetSeancesDates().Max();

            //workaround
            //App.MainWindow.Click1TabControl();
            //App.MainWindow.Click2TabControl();

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetPeriod(period_begin, period_end);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            //TODO add Extension methods to this and another tests
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks.Where(talk => talk.begin >= period_begin && talk.begin <= period_end).ToList(),
                selected_task_in_db.Talks.Except(selected_task_in_db.Talks.Where(talk => talk.begin >= period_begin && talk.begin <= period_end).ToList()).ToList(),
                selected_task_in_db.Sms.Where(sms => sms.recvTime >= period_begin && sms.recvTime <= period_end).ToList(),
                selected_task_in_db.Sms.Except(selected_task_in_db.Sms.Where(sms => sms.recvTime >= period_begin && sms.recvTime <= period_end).ToList()).ToList(),
                selected_task_in_db.Places.Where(place => place.talk.begin >= period_begin && place.talk.begin <= period_end).ToList(),
                selected_task_in_db.Places.Except(selected_task_in_db.Places.Where(place => place.talk.begin >= period_begin && place.talk.begin <= period_end).ToList()).ToList(),
                selected_task_in_db.Video.Where(video => video.begin >= period_begin && video.begin <= period_end).ToList(),
                selected_task_in_db.Video.Except(selected_task_in_db.Video.Where(video => video.begin >= period_begin && video.begin <= period_end).ToList()).ToList()
                );

        }



        [Test]
        [Description("Фильтрация по периоду (ни один из сеансов) ")]
        public void CheckSummaryPreviewSeances_FilterBy_Period_Include_No_Seances()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);


            var period_begin = selected_task_in_db.GetSeancesDates().Max().AddSeconds(1);
            var period_end = selected_task_in_db.GetSeancesDates().Max().AddSeconds(3600);

            //workaround
            //App.MainWindow.Click1TabControl();
            //App.MainWindow.Click2TabControl();

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetPeriod(period_begin, period_end);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            //TODO add Extension methods to this and another tests
            AssertEqual_SummaryPreviews(_prev_infos,
                selected_task_in_db.Talks.Where(talk => talk.begin >= period_begin && talk.begin <= period_end).ToList(),
                selected_task_in_db.Talks.Except(selected_task_in_db.Talks.Where(talk => talk.begin >= period_begin && talk.begin <= period_end).ToList()).ToList(),
                selected_task_in_db.Sms.Where(sms => sms.recvTime >= period_begin && sms.recvTime <= period_end).ToList(),
                selected_task_in_db.Sms.Except(selected_task_in_db.Sms.Where(sms => sms.recvTime >= period_begin && sms.recvTime <= period_end).ToList()).ToList(),
                selected_task_in_db.Places.Where(place => place.talk.begin >= period_begin && place.talk.begin <= period_end).ToList(),
                selected_task_in_db.Places.Except(selected_task_in_db.Places.Where(place => place.talk.begin >= period_begin && place.talk.begin <= period_end).ToList()).ToList(),
                selected_task_in_db.Video.Where(video => video.begin >= period_begin && video.begin <= period_end).ToList(),
                selected_task_in_db.Video.Except(selected_task_in_db.Video.Where(video => video.begin >= period_begin && video.begin <= period_end).ToList()).ToList()
                );



        }


        [Test]
        [Description("")]
        [Ignore("Нестабильная работа теста на ToggleSummaryFilter, разбит на 2 в классе Rim88SummariesTests_FilterBy_Flag_Include_Earlier_Seances ")]
        public void CheckSummaryPreviewSeances_FilterBy_Flag_Include_Earlier_Seances()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            //3 сводки по данному заданию
            GenerateSummariesForTask_WithRandomSeances(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString(), 3);


            //Application
            Start();

            
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);


            //case 1. не включая ранние сеансы
            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            AssertEqual_SummaryPreviews(_prev_infos,
            selected_task_in_db.Talks.Except(selected_task_in_db.GetTalksIncludedInSummaries()).ToList(), selected_task_in_db.GetTalksIncludedInSummaries(),
            selected_task_in_db.Sms.Except(selected_task_in_db.GetSmsIncludedInSummaries()).ToList(), selected_task_in_db.GetSmsIncludedInSummaries(),
            selected_task_in_db.Places.Except(selected_task_in_db.GetPlacesIncludedInSummaries()).ToList(), selected_task_in_db.GetPlacesIncludedInSummaries(),
            selected_task_in_db.Video.Except(selected_task_in_db.GetVideoIncludedInSummaries()).ToList(),  selected_task_in_db.GetVideoIncludedInSummaries());


            //case 2.  "включая все сеансы"
            //summary settings
            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetPeriod(selected_task_in_db.GetSeancesDates().Min(), selected_task_in_db.GetSeancesDates().Max() );
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            AssertEqual_SummaryPreviews(_prev_infos,
                    selected_task_in_db.Talks,
                    new List<Talk>(),
                    selected_task_in_db.Sms,
                    new List<Sms>(),
                    selected_task_in_db.Places,
                    new List<Place>(),
                    selected_task_in_db.Video,
                    new List<Video>()
                    );

        }


        
        [Test]
        [Ignore("При устойчивой работе предыдущего теста - можно попробовать использовать API для сохранения .xps на диске")]
        public void CheckCreateSummaries_Save_In_XPS_And_Check_Content()
        {
            //БД
            int count_tasks = 10;

            var first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            var meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            var vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            //App.MainWindow.ClickSettingsButton();
            //App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            //App.SettingsWindow.SummariesTabControl.CheckTalk();
            //App.SettingsWindow.SummariesTabControl.CheckSms();
            //App.SettingsWindow.SummariesTabControl.CheckVideo();
            //App.SettingsWindow.SummariesTabControl.CheckPlace();
            //App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));

            App.MainWindow.SummaryControl.EnterSummaryPreviewCommand();
            App.PrintPreviewWindow.ClickPrint();
            App.SummaryPrintWindow.SaveInXPSPrintFormat(@"C:\Users\kornienko\Desktop\sample.xps");

        }



        [Test]
        [Description("В случае нестабильной работы теста уменьшить количество генерируемых сеансов, для подгрузки их на одну страницу")]
        public void PreviewSummary_PrePrintContent()
        {
            //TODO вынести генерацию данных в общую подготовку?

            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            //Application
            Start();

            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            //default type
            App.SettingsWindow.SummariesTabControl.SelectSummaryTemplate("Шаблон сводки (по типам сеансов) 8.6");
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));

            App.MainWindow.SummaryControl.EnterSummaryPreviewCommand();
            var content = App.PrintPreviewWindow.GetVisiblePreviewContent();

            string pattern = @"[\n\r\t\s]+";
            Regex regex = new Regex(pattern);

            content = regex.Replace(content, "");
            var expected_task_info = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            
            //номер сводки - 1
            //TODO имя пользователя
            Assert_Summary_Content(expected_task_info, content, 1, selected_task_in_db.GetSeancesDates().Min(), selected_task_in_db.GetSeancesDates().Max(), "Admin");


            Assert.Pass();
        }




        [Test]
        [Description("проверка информации по свойствам заданий ")]
        public void CheckTaskProperties()
        {

            //БД
            int count_tasks = 10;
            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);

            //Application
            Start();


            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            
            //case 1: 
            var _rnd = new Random();
            //var number = _rnd.Next(1, count_tasks + 1);
            var number = 1;

            App.MainWindow.SummaryControl.OpenTaskPropertiesByDoubleClick(String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, number, first_task_date.Year), true);
            var expected_task_info = DbDataModel.Tasks.Where(task => task.Meropr == meropr && task.VidObject == vid_object
                && task.Task_number == number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();
            var task_info = App.TaskInfoCardWindow.GetFullTaskInfo();
            AssertEqual_TaskInfoFull_To_DBTask(expected_task_info, task_info);
            App.TaskInfoCardWindow.PressCloseBtn();


            //case 2: 
            _rnd = new Random();
            number = _rnd.Next(1, count_tasks + 1);

            App.MainWindow.SummaryControl.OpenTaskPropertiesByDoubleClick(String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, number, first_task_date.Year), true);
            expected_task_info = DbDataModel.Tasks.Where(task => task.Meropr == meropr && task.VidObject == vid_object
                && task.Task_number == number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();
            task_info = App.TaskInfoCardWindow.GetFullTaskInfo();
            AssertEqual_TaskInfoFull_To_DBTask(expected_task_info, task_info);
            App.TaskInfoCardWindow.PressCloseBtn();


            //case 3: 
            _rnd = new Random();
            number = _rnd.Next(1, count_tasks + 1);

            App.MainWindow.SummaryControl.OpenTaskPropertiesByDoubleClick(String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, number, first_task_date.Year), true);
            expected_task_info = DbDataModel.Tasks.Where(task => task.Meropr == meropr && task.VidObject == vid_object
                && task.Task_number == number.ToString() && task.Task_year == first_task_date.Year.ToString()).First();
            task_info = App.TaskInfoCardWindow.GetFullTaskInfo();
            AssertEqual_TaskInfoFull_To_DBTask(expected_task_info, task_info);
            App.TaskInfoCardWindow.PressCloseBtn();
        }




        [Test]
        [Description("Проверка формирования превью сводок c несколькими вариантами настроек для разных шифров ОТМ")]
        public void CheckSummaryPreviewSeances_For_Different_Otm_Shifr()
        {
            //БД
            //чтобы помещались в видимую область
            int count_tasks = 5;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            
            //для формирования 2 раных шифров заданий
            var meropr_list =  GetTwoRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string meropr1 = meropr_list[0];
            string meropr2 = meropr_list[1];

            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr1, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateOnlyTasks_LinearDistribution(meropr2, vid_object, 6, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

           
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            //создаем настройку для первого шифра, - тип сеанса - смс
            App.SettingsWindow.SummariesTabControl.ClickAddNewShifrButton();
            App.AddNewTaskShifrWindow.EnterTaskCipher(meropr1);
            App.AddNewTaskShifrWindow.ClickOk();
            ////workaround
            //App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);
            //App.SettingsWindow.ClickTabControl(SettingsWindowTabs.Summaries);
            
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.UnCheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.UnCheckVideo();
            App.SettingsWindow.SummariesTabControl.UnCheckPlace();
            //для всех остальных ОТМ (т.е. для meropr2) - тип сеанса - разговоры
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.UnCheckSms();
            App.SettingsWindow.SummariesTabControl.UnCheckVideo();
            App.SettingsWindow.SummariesTabControl.UnCheckPlace();
            
            App.SettingsWindow.PressOkBtn();



            //проверяем шифр с недефолтными настройками
            int[] ckecked_tasks_numbers = new int[]{
                2, 5
            };

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            foreach (var c_task_number in ckecked_tasks_numbers)
            {
                App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr1, vid_object, c_task_number, first_task_date.Year));
                var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

                var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr1, vid_object, c_task_number.ToString(), first_task_date.Year.ToString());

                //смс
                AssertEqual_SummaryPreviews(_prev_infos,
                    new List<Talk>(), selected_task_in_db.Talks,
                    selected_task_in_db.Sms, new List<Sms>(),
                    new List<Place>(), selected_task_in_db.Places,
                    new List<Video>(), selected_task_in_db.Video);
            }


            //проверяем шифр с дефолтными настройками
            ckecked_tasks_numbers = new int[]{
                7, 9
            };


            foreach (var c_task_number in ckecked_tasks_numbers)
            {
                App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr2, vid_object, c_task_number, first_task_date.Year));
                var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

                var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr2, vid_object, c_task_number.ToString(), first_task_date.Year.ToString());

                //разговоры
                AssertEqual_SummaryPreviews(_prev_infos,
                    selected_task_in_db.Talks, new List<Talk>(),
                    new List<Sms>(), selected_task_in_db.Sms,
                    new List<Place>(), selected_task_in_db.Places,
                    new List<Video>(), selected_task_in_db.Video);
            }


            Assert.Pass();



        }






    }



    [TestFixture]
    public class Rim88SummariesTests_FilterBy_Flag_Include_Earlier_Seances : BaseTest
    {


        public string meropr { get; set; }
        public string vid_object { get; set; }
        public int task_number { get; set; }
        public DateTime first_task_date { get; set; }

        public Task selected_task_in_db { get; set; }


        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            StartTestCase();

        }

        [SetUp]
        public void Initialise()
        {
            DataBasesCleanUp();
            GeneratedDataCleanUp();

            //БД
            int count_tasks = 10;

            first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //номер задания
            task_number = _random.Next(1, count_tasks + 1);
            selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            //3 сводки по данному заданию
            GenerateSummariesForTask_WithRandomSeances(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString(), 3);


            //Application
            Start();


            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);



        }

        [TearDown]
        public void CleanUp()
        {
            Stop();
        }


        [Test]
        public void Summaries_Preview_Exclude_Earlier_Seances()
        {
            //case 1. не включая ранние сеансы
            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            AssertEqual_SummaryPreviews(_prev_infos,
            selected_task_in_db.Talks.Except(selected_task_in_db.GetTalksIncludedInSummaries()).ToList(), selected_task_in_db.GetTalksIncludedInSummaries(),
            selected_task_in_db.Sms.Except(selected_task_in_db.GetSmsIncludedInSummaries()).ToList(), selected_task_in_db.GetSmsIncludedInSummaries(),
            selected_task_in_db.Places.Except(selected_task_in_db.GetPlacesIncludedInSummaries()).ToList(), selected_task_in_db.GetPlacesIncludedInSummaries(),
            selected_task_in_db.Video.Except(selected_task_in_db.GetVideoIncludedInSummaries()).ToList(), selected_task_in_db.GetVideoIncludedInSummaries());


        }


        [Test]
        public void Summaries_Preview_Include_Earlier_Seances()
        {

            //case 2.  "включая все сеансы"
            //summary settings
            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            //App.MainWindow.Click1TabControl();
            //App.MainWindow.Click2TabControl();
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetPeriod(selected_task_in_db.GetSeancesDates().Min(), selected_task_in_db.GetSeancesDates().Max());
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();

            AssertEqual_SummaryPreviews(_prev_infos,
                    selected_task_in_db.Talks,
                    new List<Talk>(),
                    selected_task_in_db.Sms,
                    new List<Sms>(),
                    selected_task_in_db.Places,
                    new List<Place>(),
                    selected_task_in_db.Video,
                    new List<Video>()
                    );

        }




        #endregion




    }


    [TestFixture]
    public class Rim88SummariesTests_With_Summary_Creation : BaseTest
    {

        public string meropr { get; set; }
        public string vid_object { get; set; }
        public int task_number { get; set; }
        public DateTime first_task_date { get; set; }


        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            StartTestCase();

        }

        [SetUp]
        public void Initialise()
        {
            DataBasesCleanUp();
            GeneratedDataCleanUp();

            //БД
            int count_tasks = 10;

            first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            //номер задания
            task_number = _random.Next(1, count_tasks + 1);
            //var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());

            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.DeleteNonDefaultTaskShifrs();
            App.SettingsWindow.SummariesTabControl.SelectDefaultShifrName();
            App.SettingsWindow.SummariesTabControl.UnCheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);


            //Create summary (with default number 1)

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.EnterPrintCommand();

            App.SummaryPrintWindow.SelectXPSPrintFormatAndCloseSaveWindow();

            Stop();

        }


        [TearDown]
        public void CleanUp()
        {
            Stop();
        }

        #endregion

        [Test]
        [Description("")]
        public void CheckCreateSummary()
        {

            //Application
            StartApp();
            App.MainWindow.ExpandWindow();
            //сформировали сводку, переходим на страницу итоговых материалов
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            //период должен быть дефолтный, т.е. за текущую неделю и включать текущую дату
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            //1 запись  о сводке
            //при развернутом окне должны войти основные колонки для проверки
            //DateAndtime, Type, OTM, number
            AssertEqual_JournalDataGridString(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                new List<JournalDataGridString>()
                {
                    new JournalDataGridString(){
                        OTM = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, task_number.ToString(), first_task_date.Year),
                        Number = "1",
                        DateAndtime = DateTime.Now.ToString()
                    }
                }
            );


        }



        [Test]
        [Description("в подготовке создана сводка с номером 1, попробуем создать сводку по тому же заданию с тем же номером, со сброшенным флагом в настройках - заменять номер сводки ")]
        public void CheckCreateSummaries_Flag_Replace_Summary_Number_Off()
        {

            //Application
            StartApp();

            //settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.GeneralTabControl.UnCheckReplaceCurrentSummaryNumber();
            App.SettingsWindow.PressOkBtn();


            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetSummaryNumber("1");
            App.MainWindow.SummaryControl.ToggleSummaryFilter();

            //1.8.0.1062 - косяк с ошибкой не воспроизводится, кажется починили

            ////тут косяк в данной версии РИМа, поэтому пока так
            //Thread.Sleep(1000);
            //App.ErrorMessageWindow.ClickOk();
            //White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(White.Core.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
            Thread.Sleep(1000);

            App.MainWindow.SummaryControl.EnterPrintCommand();
            App.SummaryPrintWindow.SelectXPSPrintFormat();
            //таймаут, чтобы окно открылось наверняка
            Thread.Sleep(5000);
            var message =  App.SummaryCreationDialogWindow.GetMessage();

            Assert.IsTrue(message.Contains("Невозможно создать сводку с номером 1, так как этот номер сводки уже занят."));


        }



        [Test]
        [Description("те же условия теста, но с флажком - заменять номер сводки ")]
        public void CheckCreateSummaries_Flag_Replace_Summary_Number_On()
        {

            //Application
            StartApp();

            App.MainWindow.ExpandWindow();

            //settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.GeneralTabControl.CheckReplaceCurrentSummaryNumber();
            App.SettingsWindow.PressOkBtn();


            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            App.MainWindow.SummaryControl.SelectTaskFromList(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year));
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetSummaryNumber("1");
            App.MainWindow.SummaryControl.ToggleSummaryFilter();


            //1.8.0.1062 - косяк с ошибкой не воспроизводится, кажется починили

            ////тут косяк в данной версии РИМа, поэтому пока так
            //Thread.Sleep(1000);
            //App.ErrorMessageWindow.ClickOk();
            //White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(White.Core.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
            Thread.Sleep(1000);
            
            App.MainWindow.SummaryControl.EnterPrintCommand();
            App.SummaryPrintWindow.SelectXPSPrintFormatAndCloseSaveWindow();
            App.ErrorMessageWindow.ClickOk();
            

            //сформировали сводку, переходим на страницу итоговых материалов
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            //период должен быть дефолтный, т.е. за текущую неделю и включать текущую дату
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            //1 запись  о сводке
            //при развернутом окне должны войти основные колонки для проверки
            //DateAndtime, Type, OTM, number
            
            AssertEqual_JournalDataGridString(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                new List<JournalDataGridString>()
                {
                    new JournalDataGridString(){
                        OTM = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, task_number.ToString(), first_task_date.Year),
                        Number = "1",
                        DateAndtime = DateTime.Now.ToString()
                    },
                    new JournalDataGridString(){
                        OTM = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, task_number.ToString(), first_task_date.Year),
                        Number = "2",
                        DateAndtime = DateTime.Now.ToString()
                    }
                }
            );


        }



    }




    [TestFixture]
    public class Rim88Journal : BaseTest
    {


        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            StartTestCase();

        }

        [SetUp]
        public void Initialise()
        {
            DataBasesCleanUp();
            GeneratedDataCleanUp();
        }


        [TearDown]
        public void CleanUp()
        {
            Stop();
        }

        #endregion
    

        [Test]
        [Description("Фильтрация по типу отображаемых материалов")]
        public void Filter_Journal_By_Records_Type()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            ////номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            //только сводки
            GenerateSummariesForTask_WithRandomSeances(selected_task_in_db.Meropr, selected_task_in_db.VidObject, selected_task_in_db.Task_number, selected_task_in_db.Task_year, 10,
                    false, DateTime.Now);

            //Application
            Start();

            App.MainWindow.ExpandWindow();
            

            //все итоговые материалы
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            //App.MainWindow.JournalControl.SelectExecutor("Все исполнители");
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates(selected_task_in_db).Min(), DbDataModel.GetReportsDates(selected_task_in_db).Max());
            //workaround - иначе не определяются(видны) подгружаемые элменты в гриде 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                selected_task_in_db.Reports.ToList() );


            //сводки
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates(selected_task_in_db).Min(), DbDataModel.GetReportsDates(selected_task_in_db).Max());
            //workaround - иначе не определяются(видны) подгружаемые элменты в гриде 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                selected_task_in_db.Reports.ToList());


            //архивы
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.InformationArchives);
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates(selected_task_in_db).Min(), DbDataModel.GetReportsDates(selected_task_in_db).Max());
            //workaround - иначе не определяются(видны) подгружаемые элменты в гриде 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                new List<Report>() { });


            Assert.Pass();


        }



        [Test]
        [Description("Фильтрация по периоду отображаемых материалов")]
        public void Filter_Journal_By_Records_Period()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            ////номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            //10 сводок с периодичностью в 1 сутки
            int reports_count = 10;
            GenerateSummariesForTask_WithRandomSeances(selected_task_in_db.Meropr, selected_task_in_db.VidObject, selected_task_in_db.Task_number, selected_task_in_db.Task_year, reports_count,
                    false, DateTime.Now);

            //Application
            Start();

            App.MainWindow.ExpandWindow();

            //case 1. Период фильтрации - включая верхнюю/нижнюю границы
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates(selected_task_in_db).Min(), DbDataModel.GetReportsDates(selected_task_in_db).Max());
            //workaround - иначе не определяются(видны) подгружаемые элменты в гриде 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            //все сводки
            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                selected_task_in_db.Reports.ToList());



            //case 2. Период фильтрации -  самой ранней сводки < дата начала периода фильтрации < самой поздней сводки
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            var filter_period_begin = DbDataModel.GetReportsDates(selected_task_in_db).Min().AddDays(_random.Next(1, reports_count - 1));
            var filter_period_end = DbDataModel.GetReportsDates(selected_task_in_db).Max();
            App.MainWindow.JournalControl.SetDatesInterval(filter_period_begin, filter_period_end);
            //workaround - иначе не определяются(видны) подгружаемые элменты в гриде 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            
            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                selected_task_in_db.Reports.Where(report => 
                    report.ReportDate >= filter_period_begin.Date &&
                    report.ReportDate <= filter_period_end.Date.AddDays(1).AddSeconds(-1)).ToList());



            //case 3. Период фильтрации -  самой ранней сводки < дата начала периода фильтрации
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates(selected_task_in_db).Max().AddDays(1), DbDataModel.GetReportsDates(selected_task_in_db).Max().AddDays(10));
            //workaround - иначе не определяются(видны) подгружаемые элменты в гриде 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            //0 сводок
            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                new List<Report>() { });



            Assert.Pass();


        }


        [Test]
        [Description("Фильтрация по исполнителю")]
        [Ignore("TODO add users")]
        public void Filter_Journal_By_Executor()
        {

        }


        [Test]
        [Description("Поиск по шифру ОТМ, номеру ОТМ ")]
        public void Search_By_OTM()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //2 сводки по каждому заданию c датой создания, начиная с текущей даты
            foreach (var task in this.DbDataModel.Tasks)
            {
                GenerateSummariesForTask_WithRandomSeances(task.Meropr, task.VidObject, task.Task_number, task.Task_year, 2,
                    false, DateTime.Now);
            }
            
            

            //Application
            Start();

            App.MainWindow.ExpandWindow();

            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.Otm);
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates().Min(), DbDataModel.GetReportsDates().Max());
            

            //case 1. filter by meropr, vid_object, task_number
            var task_number = _random.Next(1, count_tasks + 1);
            var task_name = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, task_number, first_task_date.Year);
            var search_text = String.Join("-", task_name.Split('-').Take(3).ToList());  
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.Where(task => task.TaskShifr.Contains(search_text)).SelectMany(task => task.Reports).ToList());
        


            //case 2. filter by meropr
            task_number = _random.Next(1, count_tasks + 1);
            task_name = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, task_number, first_task_date.Year);
            search_text = String.Join("-", task_name.Split('-').Take(1).ToList()) + "-";  
            App.MainWindow.JournalControl.SetSearchText("");
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.Where(task => task.TaskShifr.Contains(search_text)).SelectMany(task => task.Reports).ToList());



            //case 3. filter by  meropr, vid_object
            task_number = _random.Next(1, count_tasks + 1);
            task_name = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, task_number, first_task_date.Year);
            search_text = String.Join("-", task_name.Split('-').Take(2).ToList());
            App.MainWindow.JournalControl.SetSearchText("");
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.Where(task => task.TaskShifr.Contains(search_text)).SelectMany(task => task.Reports).ToList());
        

            //case 4. filter by empty string
            search_text = "";  
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.SelectMany(task => task.Reports).ToList());


            //case 5. 
            //т.к. по каждому ОТМ по 2 сводок, то возможные номера сводок - 1-2
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.SerialNumber);
            search_text = "1";
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();
            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.SelectMany(task => task.Reports).Where(report => report.ReportNumber.Contains(search_text)).ToList());


            //case 6. 
            //т.к. по каждому ОТМ по 2 сводки, то возможные номера сводок - 1-2
            search_text = "2";
            App.MainWindow.JournalControl.SetSearchText("");
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                 DbDataModel.Tasks.SelectMany(task => task.Reports).Where(report => report.ReportNumber.Contains(search_text)).ToList());
        

        }



        [Test]
        [Description("Поиск по инициатору, подразделению")]
        public void Search_By_Initiator()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //2 сводки по каждому заданию c датой создания, начиная с текущей даты
            foreach (var task in this.DbDataModel.Tasks)
            {
                GenerateSummariesForTask_WithRandomSeances(task.Meropr, task.VidObject, task.Task_number, task.Task_year, 2,
                    false, DateTime.Now);
            }



            //Application
            Start();

            App.MainWindow.ExpandWindow();

            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.Initiator);
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates().Min(), DbDataModel.GetReportsDates().Max());
           

            //case 1. filter by фамилия
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            var search_text = String.Join(" ", selected_task_in_db.Init_Famini.Split(' ').Take(1).ToList());
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.Where(task => task.Init_Famini.Contains(search_text)).SelectMany(task => task.Reports).ToList());


            //case 2. filter by фамилия имя
            task_number = _random.Next(1, count_tasks + 1);
            selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            search_text = String.Join(" ", selected_task_in_db.Init_Famini.Split(' ').Take(2).ToList());
            App.MainWindow.JournalControl.SetSearchText("");
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.Where(task => task.Init_Famini.Contains(search_text)).SelectMany(task => task.Reports).ToList());


            //case 3. filter by фамилия имя отчество
            task_number = _random.Next(1, count_tasks + 1);
            selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            search_text = String.Join(" ", selected_task_in_db.Init_Famini.Split(' ').Take(3).ToList());
            App.MainWindow.JournalControl.SetSearchText("");
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.Where(task => task.Init_Famini.Contains(search_text)).SelectMany(task => task.Reports).ToList());


            //case 4. filter by подразделение
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.InitiatorDepartment);
            task_number = _random.Next(1, count_tasks + 1);
            selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            search_text = selected_task_in_db.Init_podrazd;
            App.MainWindow.JournalControl.SetSearchText("");
            App.MainWindow.JournalControl.SetSearchText(search_text);
            //workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.Where(task => task.Init_podrazd.Contains(search_text)).SelectMany(task => task.Reports).ToList());


        
        }



        [Test]
        [Description("Удаление записи о сводке")]
        public void DeleteSummaryRecord()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //1 сводка по каждому заданию c датой создания, начиная с текущей даты
            foreach (var task in this.DbDataModel.Tasks)
            {
                GenerateSummariesForTask_WithRandomSeances(task.Meropr, task.VidObject, task.Task_number, task.Task_year, 1,
                    false, DateTime.Now);
            }

            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
            


            //Application
            Start();

            App.MainWindow.ExpandWindow();

            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates().Min(), DbDataModel.GetReportsDates().Max());
            ////workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.SelectMany(task => task.Reports).ToList());


            //т.к. 1 сводка на задание
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString(selected_task_in_db.TaskShifr, "1");
            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.DeleteSummary);
            App.YesNoDialogWindow.ClickOk();

            var report_db = selected_task_in_db.Reports.Where(report => report.ReportNumber == "1").First();
            report_db.reportType = ReportType.DeletedReport;

            ////workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.SelectMany(task => task.Reports).ToList());
        }



        

        [Test]
        [Description("Установить отметку о предоставлении. Разбили тест на 2 т.к. ContextMenuClickCommand стабильно работает только так ")]
        public void SetMarkPresented()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //1 сводка по каждому заданию c датой создания, начиная с текущей даты
            foreach (var task in this.DbDataModel.Tasks)
            {
                GenerateSummariesForTask_WithRandomSeances(task.Meropr, task.VidObject, task.Task_number, task.Task_year, 1,
                    false, DateTime.Now);
            }

            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            //Application
            Start();

            App.MainWindow.ExpandWindow();

            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates().Min(), DbDataModel.GetReportsDates().Max());
            ////workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();


            var provided_date = DateTime.Now.AddDays(5);

          
            //т.к. 1 сводка на каждое сгенеренное  задание
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString(selected_task_in_db.TaskShifr, "1");
            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.SetMarkProvided);
            App.SummaryProvidedWindow.EnterDate(provided_date);
            App.SummaryProvidedWindow.ClickOk();

            ////workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            var report_db = selected_task_in_db.Reports.Where(report => report.ReportNumber == "1").First();
            report_db.PresentedMark = provided_date;

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.SelectMany(task => task.Reports).ToList());



        }



        [Test]
        [Description("Удалить отметку о предоставлении")]
        public void UnSetMarkPresented()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //1 сводка по каждому заданию c датой создания, начиная с текущей даты, +
            //для каждой сводки указываем дату ее предоставления
            var presented_date = DateTime.Now.AddDays(5);
            foreach (var task in this.DbDataModel.Tasks)
            {
                GenerateSummariesForTask_WithRandomSeances(task.Meropr, task.VidObject, task.Task_number, task.Task_year, 1,
                    false, DateTime.Now, 24 * 3600, presented_date);
            }

            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            //Application
            Start();

            App.MainWindow.ExpandWindow();

            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates().Min(), DbDataModel.GetReportsDates().Max());
            ////workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();


            //Case 2. Удалить отметку о предоставлении
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString(selected_task_in_db.TaskShifr, "1");
            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.UnSetMarkProvided);
            App.YesNoDialogWindow.ClickOk();

            ////workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();

            var report_db = selected_task_in_db.Reports.Where(report => report.ReportNumber == "1").First();
            report_db.PresentedMark = null;

            AssertEqual_JournalDataGridString_To_DBModel_Report(
                App.MainWindow.JournalControl.GetJournalGridCells(),
                DbDataModel.Tasks.SelectMany(task => task.Reports).ToList());

        }




        [Test]
        [Description("Перепечатать сводку")]
        public void ReprintSummary()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //1 сводка по каждому заданию c датой создания, начиная с текущей даты
            foreach (var task in this.DbDataModel.Tasks)
            {
                GenerateSummariesForTask_WithRandomSeances(task.Meropr, task.VidObject, task.Task_number, task.Task_year, 1,
                    false, DateTime.Now);
            }

            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            //Application
            Start();

            App.MainWindow.ExpandWindow();

            App.MainWindow.JournalControl.SelectExecutor_White("Все исполнители");
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetDatesInterval(DbDataModel.GetReportsDates().Min(), DbDataModel.GetReportsDates().Max());
            ////workaround 
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();


            App.MainWindow.JournalControl.OpenContextMenuOnJournalString(selected_task_in_db.TaskShifr, "1");
            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.Reprint);
            App.MainWindow.Click1TabControl();
            App.MainWindow.Click2TabControl();
            var actual = App.MainWindow.SummaryControl.FindSummaryReprintText();

            Assert.IsTrue(actual);

        }









    
    }



    [TestFixture]
    [Description("Максимальное количество записей сеансов, входящих в видимую часть грида - около 40")]
    public class Rim88Archives : BaseTest
    {


        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            StartTestCase();

        }

        [SetUp]
        public void Initialise()
        {
            DataBasesCleanUp();
            GeneratedDataCleanUp();
        }


        [TearDown]
        public void CleanUp()
        {
            Stop();
        }

        #endregion


        [Test]
        [Description("Проверка отображения сеансов в БД на GUI")]
        public void Check_View_Archive_Seances()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            int[] task_numbers = new int[]{
                4, 5, 9
            };


            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            int checked_task_number = 0;

            foreach (var t_number in task_numbers )
            {
                var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, t_number.ToString(), first_task_date.Year.ToString());


                App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, checked_task_number, first_task_date.Year), false);
                App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, t_number, first_task_date.Year), true);
                checked_task_number = t_number;


                var s_dates =  selected_task_in_db.GetSeancesDates();
                //период
                App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(s_dates.Min(), s_dates.Max());

                //все виды сеансов
                App.MainWindow.InformationalArchiveControl.TogglePresetButton();
                App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
                App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
                App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
                App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
                //дефолтное значение
                App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
                App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
                App.MainWindow.InformationalArchiveControl.TogglePresetButton();


                var  actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

                AssertEqual_ArchiveDataGridStrings(actual_cells,
                    selected_task_in_db.Talks, new List<Talk>(),
                    selected_task_in_db.Sms, new List<Sms>(),
                    selected_task_in_db.Places, new List<Place>(),
                    selected_task_in_db.Video, new List<Video>()
                    );


            }


            
        }



        [Test]
        [Description("Проверка фильтрации сеансов по периоду")]
        public void Filter_Archive_Seances_By_Period()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();


            //доп. настройки фильтрации

            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            //дефолтное значение
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();


            //case 1. Включая все сеансы
            var begin_date = s_dates.Min().Date;
            var end_date = s_dates.Max().Date;
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);

            var actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                selected_task_in_db.Talks, new List<Talk>(),
                selected_task_in_db.Sms, new List<Sms>(),
                selected_task_in_db.Places, new List<Place>(),
                selected_task_in_db.Video, new List<Video>()
                );




            //case 2. Не всключая верхнюю границу
            begin_date = s_dates.Min().AddDays(1).Date;
            end_date = s_dates.Max().Date;
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);

            actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();


            //для разговоров фильтрация по дате окончания, для остальных типов - по дате начала

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                selected_task_in_db.Talks.Where(seance => seance.end.Date >= begin_date && seance.end.Date <= end_date).ToList(),
                selected_task_in_db.Talks.Except(selected_task_in_db.Talks.Where(seance => seance.end.Date >= begin_date && seance.end.Date <= end_date).ToList()).ToList(),
                selected_task_in_db.Sms.Where(seance => seance.recvTime.Date >= begin_date && seance.recvTime.Date <= end_date).ToList(),
                selected_task_in_db.Sms.Except(selected_task_in_db.Sms.Where(seance => seance.recvTime.Date >= begin_date && seance.recvTime.Date <= end_date).ToList()).ToList(),
                selected_task_in_db.Places.Where(seance => seance.talk.begin.Date >= begin_date && seance.talk.begin.Date <= end_date).ToList(),
                selected_task_in_db.Places.Except(selected_task_in_db.Places.Where(seance => seance.talk.begin.Date >= begin_date && seance.talk.begin.Date <= end_date).ToList()).ToList(),
                selected_task_in_db.Video.Where(seance => seance.begin.Date >= begin_date && seance.begin.Date <= end_date).ToList(),
                selected_task_in_db.Video.Except(selected_task_in_db.Video.Where(seance => seance.begin.Date >= begin_date && seance.begin.Date <= end_date).ToList()).ToList()
                
                );



            //case 3. Ни один из сеансов
            begin_date = s_dates.Max().AddDays(1).Date;
            end_date = s_dates.Max().AddDays(10).Date;
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);

            actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                selected_task_in_db.Talks.Where(seance => seance.end.Date >= begin_date && seance.end.Date <= end_date).ToList(),
                selected_task_in_db.Talks.Except(selected_task_in_db.Talks.Where(seance => seance.end.Date >= begin_date && seance.end.Date <= end_date).ToList()).ToList(),
                selected_task_in_db.Sms.Where(seance => seance.recvTime.Date >= begin_date && seance.recvTime.Date <= end_date).ToList(),
                selected_task_in_db.Sms.Except(selected_task_in_db.Sms.Where(seance => seance.recvTime.Date >= begin_date && seance.recvTime.Date <= end_date).ToList()).ToList(),
                selected_task_in_db.Places.Where(seance => seance.talk.begin.Date >= begin_date && seance.talk.begin.Date <= end_date).ToList(),
                selected_task_in_db.Places.Except(selected_task_in_db.Places.Where(seance => seance.talk.begin.Date >= begin_date && seance.talk.begin.Date <= end_date).ToList()).ToList(),
                selected_task_in_db.Video.Where(seance => seance.begin.Date >= begin_date && seance.begin.Date <= end_date).ToList(),
                selected_task_in_db.Video.Except(selected_task_in_db.Video.Where(seance => seance.begin.Date >= begin_date && seance.begin.Date <= end_date).ToList()).ToList()

                );



        }




        [Test]
        [Description("Проверка фильтрации сеансов по типу сеанса")]
        public void Filter_Archive_Seances_By_Seance_Type()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom();
            GenerateStenoForTalksAndVideos();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();
            var begin_date = s_dates.Min();
            var end_date = s_dates.Max();
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);

           
            //case 1. все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();


            var actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                selected_task_in_db.Talks, new List<Talk>(),
                selected_task_in_db.Sms, new List<Sms>(),
                selected_task_in_db.Places, new List<Place>(),
                selected_task_in_db.Video, new List<Video>()
                );



            //case 2. разговоры
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();


            actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                selected_task_in_db.Talks, new List<Talk>(),
                new List<Sms>(), selected_task_in_db.Sms,
                new List<Place>(), selected_task_in_db.Places, 
                new List<Video>(), selected_task_in_db.Video
                );


            //case 2. смс
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();


            actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                new List<Talk>(), selected_task_in_db.Talks,
                selected_task_in_db.Sms, new List<Sms>(),
                new List<Place>(), selected_task_in_db.Places,
                new List<Video>(), selected_task_in_db.Video
                );


            //case 3. places
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();


            actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                new List<Talk>(), selected_task_in_db.Talks,
                new List<Sms>(), selected_task_in_db.Sms,
                selected_task_in_db.Places, new List<Place>(),
                new List<Video>(), selected_task_in_db.Video
                );


            //case 4. video
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();


            actual_cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();

            AssertEqual_ArchiveDataGridStrings(actual_cells,
                new List<Talk>(), selected_task_in_db.Talks,
                new List<Sms>(), selected_task_in_db.Sms,
                new List<Place>(), selected_task_in_db.Places,
                selected_task_in_db.Video, new List<Video>()
                );


        }





        [Test]
        [Category("Archivation")]
        [Description("Проверка экспорта сеансов с/без настройки очищать папку ")]
        public void CheckExportSeances_With_ClearFolder_Setting()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 1, 1, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //очистить папку архмвов
            ClearArchiveSeancesFolder();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();
            var begin_date = s_dates.Min();
            var end_date = s_dates.Max();
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);


            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            App.ExportSettingsWindow.UnCheckCreateSubfolders();
            App.ExportSettingsWindow.UnCheckCreateSeancesDescriptions();
            //сразу с данной настройкой, чтобы не обрабатывать клик в диалоговм окне
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            //App.ExportSettingsWindow.SetSoundFilesFormat(SoundFormat.WAV);
            App.ExportSettingsWindow.UnCheckRenameMediaFiles();
            App.ExportSettingsWindow.ClickOkButton();


            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //case 1. Export 
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            //обработчик ожидания формирования архива по индикатору выполнения, работает стабильно
            //Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "");


            //case 2.Повторный экспорт, проверяем что не создается новая папка
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "");
            Assert_Check_Otm_Directory_Not_Exists(selected_task_in_db.TaskShifr, "(2)");


            //case 3.Повторный экспорт, проверяем что создается новая папка

            //при настройке  - не очищать папку
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.UnCheckClearFolderBeforeCreatingArchive();
            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "");
            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "(2)");


            Assert.Pass();


            
        }




        [Test]
        [Category("Archivation")]
        [Description("Экспорт сеансов по нескольким заданиям ")]
        public void CheckExportSeances_For_Some_Tasks()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 1, 1, 8, 6, true);
            GenerateStenoForTalksAndVideos();


            //выбираем номера заданий
            List<int> task_numbers = RimGUI.Tests.Framework.Utils.RandomListEntries<int>(
                new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, _random);



            //очистить папку архмвов
            ClearArchiveSeancesFolder();

            //Application
            Start();

            App.MainWindow.ExpandWindow();

            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

            foreach (var task_number in task_numbers)
            {
                App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);
            }
            
            //максимально возможный интервал
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(new DateTime(1900, 1, 1), new DateTime(2100, 1, 1));


            //настройки архивации
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            App.ExportSettingsWindow.UnCheckCreateSubfolders();
            App.ExportSettingsWindow.UnCheckCreateSeancesDescriptions();
            //сразу с данной настройкой, чтобы не обрабатывать клик в диалоговм окне
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            App.ExportSettingsWindow.UnCheckRenameMediaFiles();
            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //архивация
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            

            foreach (var task_number in task_numbers)
            {
                var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());
                Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "");
            }


            Assert.Pass();



        }




        [Test]
        [Category("Archivation")]
        [Description("Экспорта сеансов в подпапки в формате ГГГГ-ММ-ДД ")]
        public void CheckExportSeances_With_Create_Subfolders_Setting()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 1, 1, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //очистить папку архмвов
            ClearArchiveSeancesFolder();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();
            var begin_date = s_dates.Min();
            var end_date = s_dates.Max();
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);

            //настройки архивации
            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            //создавать подпапки
            App.ExportSettingsWindow.CheckCreateSubfolders();
            App.ExportSettingsWindow.UnCheckCreateSeancesDescriptions();
            //сразу с данной настройкой, чтобы не обрабатывать клик в диалоговм окне
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            App.ExportSettingsWindow.UnCheckRenameMediaFiles();
            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //экспорт
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_With_Dates_Subfolders_And_Seances_Exists(selected_task_in_db, "");



            Assert.Pass();



        }




        [Test]
        [Category("Archivation")]
        [Description("Экспорт c преобразованием формата звука  ")]
        public void CheckExportSeances_With_Convert_Sound_files_Setting()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 1, 1, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //очистить папку архмвов
            ClearArchiveSeancesFolder();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();
            var begin_date = s_dates.Min();
            var end_date = s_dates.Max();
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);


            //case 1. Преобразовывать звуковые файлы к .WAV формату

            //настройки архивации
            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            //создавать подпапки
            App.ExportSettingsWindow.UnCheckCreateSubfolders();
            App.ExportSettingsWindow.UnCheckCreateSeancesDescriptions();
            //сразу с данной настройкой, чтобы не обрабатывать клик в диалоговм окне
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            App.ExportSettingsWindow.CheckConvertSoundFiles();
            App.ExportSettingsWindow.SetSoundFilesFormat(SoundFormat.WAV);
            App.ExportSettingsWindow.UnCheckRenameMediaFiles();
            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //экспорт
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "", "wav");



            //case 2. Преобразовывать звуковые файлы к .WSD формату

            //настройки архивации
            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            App.ExportSettingsWindow.SetSoundFilesFormat(SoundFormat.WSD);
            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //экспорт
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "", "wsd");




            Assert.Pass();



        }




        [Test]
        [Category("Archivation")]
        [Description("Экспорт c переименованием медиафайлов по шаблону ")]
        public void CheckExportSeances_With_Rename_Files_By_Template()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 1, 1, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //очистить папку архмвов
            ClearArchiveSeancesFolder();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();
            var begin_date = s_dates.Min();
            var end_date = s_dates.Max();
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);


            //case 1. Шаблон переименования 

            var rename_template = "#id #date #time #num1 #num2";

            //настройки архивации
            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            //создавать подпапки
            App.ExportSettingsWindow.UnCheckCreateSubfolders();
            App.ExportSettingsWindow.UnCheckCreateSeancesDescriptions();
            //сразу с данной настройкой, чтобы не обрабатывать клик в диалоговм окне
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            App.ExportSettingsWindow.CheckRenameMediaFiles();
            App.ExportSettingsWindow.ClickConfigureMediaNames();

            //окно настроек имен файлов
            App.FileNameTemplateWindow.EnterTemplate(rename_template);
            App.FileNameTemplateWindow.ClickOk();

            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //экспорт
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "", "wsd", "avi", rename_template);




            //case 2. Шаблон переименования 

            rename_template = "#date#time some words  #num1#num2 #id";

            //настройки архивации
            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.ClickConfigureMediaNames();

            //окно настроек имен файлов
            App.FileNameTemplateWindow.EnterTemplate(rename_template);
            App.FileNameTemplateWindow.ClickOk();

            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();


            //экспорт
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "", "wsd", "avi", rename_template);





            Assert.Pass();



        }




        [Test]
        [Category("Archivation")]
        [Description("Экспорт c формированием описания сеансов ")]
        public void CheckExportSeances_With_Seance_Description()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 1, 1, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //очистить папку архмвов
            ClearArchiveSeancesFolder();

            //Application
            Start();

            App.MainWindow.ExpandWindow();


            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();
            var begin_date = s_dates.Min();
            var end_date = s_dates.Max();
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);


           
            //настройки архивации
            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            App.ExportSettingsWindow.UnCheckCreateSubfolders();
            //описания сеансов
            App.ExportSettingsWindow.CheckCreateSeancesDescriptions();
            //сразу с данной настройкой, чтобы не обрабатывать клик в диалоговм окне
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            App.ExportSettingsWindow.UnCheckRenameMediaFiles();
            App.ExportSettingsWindow.ClickConfigureMediaNames();
            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //экспорт
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();

            //с описанием
            Assert_Check_Otm_Directory_And_Seances_Exists(selected_task_in_db, "", "wsd", "avi", "", true);



            Assert.Pass();



        }


        [Test]
        [Category("Archivation")]
        [Description("Экспорт c формированием Excel - документа")]
        public void CheckExportSeances_With_Seances_Registry_In_Excel()
        {
            //БД
            int count_tasks = 10;

            DateTime first_task_date = new DateTime(2016, 1, 1);
            int task_interval = 2;
            short task_srok = 30;
            //generate certain shifr parts
            string meropr = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            string vid_object = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);

            GenerateOnlyTasks_LinearDistribution(meropr, vid_object, 1, count_tasks,
            first_task_date, task_interval, task_srok);

            
            GenerateSeancesForTasks_ByRandom(12, 10, 8, 8, 6, 1, 1, 8, 6, true);
            GenerateStenoForTalksAndVideos();

            //очистить папку архмвов
            ClearArchiveSeancesFolder();

            //Application
            Start();
            

            App.MainWindow.ExpandWindow();

          
            //номер задания
            var task_number = _random.Next(1, count_tasks + 1);
            var selected_task_in_db = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number.ToString(), first_task_date.Year.ToString());



            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox(String.Format("{0}-{1}-{2}-{3}",
                meropr, vid_object, task_number, first_task_date.Year), true);

            var s_dates = selected_task_in_db.GetSeancesDates();
            var begin_date = s_dates.Min();
            var end_date = s_dates.Max();
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(begin_date, end_date);



            //настройки архивации
            //все виды сеансов
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            //настройки архивации
            //используем дефолтные настройки
            //
            App.ExportSettingsWindow.SelectArchive(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.ExportSettingsWindow.SetArchiveFilePathText(Rim_Export_Seances_FIle_Path[Get_ENV()]);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            App.ExportSettingsWindow.UnCheckCreateSubfolders();
            //описания сеансов
            App.ExportSettingsWindow.UnCheckCreateSeancesDescriptions();
            //создавать описание сеансов
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            App.ExportSettingsWindow.ClickConfigureExcelFormat();
            //выбрать какие-то значения и получить список
            App.ExportToExcelSettingsWindow.SelectAnyChBoxes();
            var checked_column_names = App.ExportToExcelSettingsWindow.GetCheckedColumns();
            App.ExportToExcelSettingsWindow.ClickOk();

            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            App.ExportSettingsWindow.UnCheckRenameMediaFiles();
            App.ExportSettingsWindow.ClickOkButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting(RimApp.Tests.Framework.Elements.ExportSettingsWindow.DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            //экспорт
            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            App.MainWindow.InformationalArchiveControl.WaitSeancesExport();


           
            Assert_Check_Excel_Registry_File_Content(selected_task_in_db.Talks, selected_task_in_db,
                checked_column_names);



            Assert.Pass();



        }



    
    
    }
    


}

