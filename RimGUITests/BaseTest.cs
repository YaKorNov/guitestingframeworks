﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Globalization;
using ArtOfTest.WebAii.Core;
using RimApp.Tests.Framework.Elements;
using White.Core;
//TODO merge to common namespace CommonGenerator!!
using CommonGenerator;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;
using DataGenerator.Moj.MojBuilders;
using DataGenerator.Moj;
using CommonGenerator.Moj.Tables;
using CommonGenerator.Moj;
using CommonGenerator.Mtb;
using CommonGenerator;
using DataGenerator.Mtb.MtbBuilders;
using DataGenerator.Mtb;
using DataGenerator.Mtb.Tables;
using BLToolkit.Data.DataProvider;
using RimGUI.Tests.Framework.Models;
using NUnit.Framework;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]



namespace RimGUITests
{

    public class BaseTest
    {
        protected RimApp.Tests.Framework.Elements.App App { get; set; }

        protected readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        

        private MtbSettings _mtbSettings;
        private MtbDataBuilder _mtbDataBuilder;
        private MojSettings _mojSettings;
        private MojDataBuilder _mojDataBuilder;
        private string _talkFileName;
        private string _videoFileName;
        protected Random _random = new Random();

        protected DbDataModel DbDataModel { get; set; }


        #region MainSettings

        protected string Get_ENV()
        {
            if (Environment.MachineName == "gui-test")
            {
                return "DEV";
            }
            else if (Environment.MachineName == "gui-test")
            {
                return "TEST";
            }

            return "TEST";
        }

        //TODO перенести все настройки путей и данных для генерации в конфиг
        protected static Dictionary<string, string> Rim_Application_Path
        {
            get { 
                
                    return new Dictionary<string, string>()
                    {
                        {"DEV", @"C:\Mag\Rim\"},
                        {"TEST", @"C:\Mag\Rim\"}
                    };

            }
        }


        protected static Dictionary<string, string> Rim_Export_Seances_FIle_Path
        {
            get
            {

                return new Dictionary<string, string>()
                    {
                        {"DEV", @"C:\Информационные архивы МАГ"},
                        {"TEST", @"C:\Информационные архивы МАГ"}
                    };

            }
        }


        protected static Dictionary<string, Dictionary<string, string>> Rim_Seances_Templates_File_Path
        {
            get
            {

                return new Dictionary<string, Dictionary<string, string>>()
                    {
                        {"DEV",  new Dictionary<string, string>(){
                             {"talk_file" ,  @"C:\SampleFiles\sample.wsd"},
                             {"video_file",  @"C:\SampleFiles\sample.wsd"}
                        }},
                        {"TEST", new Dictionary<string, string>(){
                             {"talk_file" ,  @"C:\SampleFiles\sample.wsd"},
                             {"video_file",  @"C:\SampleFiles\sample.wsd"}
                        }}
                    };

            }
        }
            

        protected static Dictionary<string, Dictionary<string,object>> MAG_DB_Settings
        {
            get
            {

                return new Dictionary<string, Dictionary<string, object>>()
                    {
                        {"DEV", new Dictionary<string, object>(){
                            {"MTB", new MtbSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagTalksBase",
                                    Login = "sa",
                                    Password = "m1m2m30-="
                                }},
                            {"MOJ", new MojSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagObjectsJournal",
                                    Login = "sa",
                                    Password = "m1m2m30-="
             
                                }}
                        }},
                        {"TEST", new Dictionary<string, object>(){
                            {"MTB", new MtbSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagTalksBase",
                                    Login = "sa",
                                    Password = "m1m2m30-="
                                }},
                            {"MOJ", new MojSettings()
                                {
                                    Address = @"gui-test",
                                    Database = "MagObjectsJournal",
                                    Login = "sa",
                                    Password = "m1m2m30-="

                                }}
                        }}
                    };

                }
        }


        #endregion 


        #region DataGeneration

        protected void DataBasesCleanUp()
        {
            _mtbDataBuilder.FullCleanUpMtb();
            _mojDataBuilder.FullCleanUpMoj();

            log.Info("Test step. Успешно откатили состояние баз данных к первоначальному состоянию");
        }

        protected void GeneratedDataCleanUp()
        {
           
            DbDataModel = new DbDataModel();
            DbDataModel.Tasks = new List<Task>();
            DbDataModel.Marks = new List<Mark>();
            //TODO clear another data Marks, Steno, etc

        }

        //TODO Вынести в конфиг
        private IList<string> GetStringParametrByName(TaskGeneratorStringParameterType param_type)
        {
            List<string> string_list = new List<string>();

            switch (param_type)
            {
                case TaskGeneratorStringParameterType.Initiator:
                    string_list = new List<string> { "МВД", "ФСБ" };
                    break;
                case TaskGeneratorStringParameterType.Otdel:
                    string_list = new List<string> { "МВД", "ФСБ" };
                    break;
                case TaskGeneratorStringParameterType.Podrazd:
                    string_list = new List<string> { "Отдел Н", "Отдел К" };
                    break;
                case TaskGeneratorStringParameterType.InitFamIni:
                    string_list = new List<string> { "Петров Иван Анатольевич", "Иванов Дмитрий Сергеевич" };
                    break;
                case TaskGeneratorStringParameterType.Phone:
                    string_list = new List<string> { "89130645634", "89130745634" };
                    break;
                case TaskGeneratorStringParameterType.Meropr:
                    string_list = new List<string> { "ПТП", "ОТП" };
                    break;
                case TaskGeneratorStringParameterType.VidObject:
                    string_list = new List<string> { "30", "40", "50" };
                    break;
                case TaskGeneratorStringParameterType.Orient:
                    string_list = new List<string> { "рост средний", "волосы рыжие" };
                    break;
                case TaskGeneratorStringParameterType.SmsText:
                    string_list = new List<string> { "привет, все готово", "когда начинаем?" };
                    break;
                case TaskGeneratorStringParameterType.StenoText:
                    string_list = new List<string> { "ты меня слышишь?", "я полковник на белом коне" };
                    break;
            }

            return string_list;
        }


        public string GetRandomStringParametrValueByName(TaskGeneratorStringParameterType param_type)
        {
            var string_list = GetStringParametrByName(param_type);
            return string_list[_random.Next(0, string_list.Count)];
        }


        //извлечь 2 значения 
        public List<string> GetTwoRandomStringParametrValueByName(TaskGeneratorStringParameterType param_type)
        {
            var string_list = GetStringParametrByName(param_type);
            var rand_index = _random.Next(0, string_list.Count);

            List<string> res_list = new List<string>();
            res_list.Add(string_list[rand_index]);


            var next_element = string_list.ElementAtOrDefault(rand_index + 1);

            if (next_element != null)
            {
                res_list.Add(next_element);
                return res_list;
            }


            var prev_element = string_list.ElementAtOrDefault(rand_index - 1);

            if (prev_element != null)
            {
                res_list.Add(prev_element);
                return res_list;
            }



            if (res_list.Count < 2)
                throw new Exception(String.Format("Не удалось получить 2 различных значения для параметра {0}, проверьте конфиг", param_type.ToString()));

            return res_list;
           
        }


        public int GetRandomIntegerParametrValueByName(TaskGeneratorIntegerParameterType param_type)
        {
            //Random rnd = new Random();

            switch (param_type)
            {
                case TaskGeneratorIntegerParameterType.TaskSrok:
                    return _random.Next(1, 121);
            }

            return 0;

        }


        protected TaskTarget GetRandomTaskTarget()
        {
           var tt = Enum.GetValues(typeof(TaskTarget));
           //Random random = new Random();
           return (TaskTarget)tt.GetValue(_random.Next(tt.Length));
        }

    


        //генреировать таски с линейным распределением
        protected void GenerateOnlyTasks_LinearDistribution(string meropr_p=null, string vid_object_p=null, int first_task_number=1,  int count_tasks = 10,
            DateTime? first_task_date_p=null, int task_interval=2, short task_srok=30)
        {
            
            var meropr = meropr_p ?? GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Meropr);
            var vid_object = vid_object_p ?? GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.VidObject);
            //var count_tasks = count_tasks_p;
            var first_task_date =  first_task_date_p.HasValue ? first_task_date_p.Value : new DateTime(2016, 1, 1);
            var task_year = first_task_date.Year.ToString();
            //var task_interval = task_interval_p; //суток
            //var task_srok = task_srok_p;

            
            for (int i = 1; i <= count_tasks; i++)
            {
                

                var taskBegin = first_task_date.AddDays((i - 1) * task_interval);
                
                string task_number = (i-1 + first_task_number).ToString();

                var init_Famini = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.InitFamIni);
                var orient = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Orient);
                string goal = Utils.GetEnumDescription(GetRandomTaskTarget());

                var task  =  new Task();
                task.Otdel = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Otdel);
                task.Init_organization = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Initiator);
                task.Init_podrazd = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Podrazd);
                task.Init_Famini = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.InitFamIni);
                task.Init_phoneA = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.Init_phoneB = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.Meropr = meropr;
                task.VidObject = vid_object;
                task.Task_number = task_number;
                task.Task_year = task_year;
                task.BeginDate = taskBegin;
                task.TaskSrok = task_srok;
                task.PhoneControl = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.IMSI = null;
                task.IMEI = null;
                task.Orient = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Orient);
                task.Goal = GetRandomTaskTarget();      
                        

                DbDataModel.Tasks.Add(task);
               
            }


            log.Info(String.Format("Test step. В ОЗУ сгенерировали {0} заданий с маской шифра {1}, дата начала первого задания - {2}, интервал - {3} суток",
                count_tasks, String.Format("{0}-{1}-number-{2}", meropr, vid_object, task_year), first_task_date, task_interval  ));

        }


        //генерировать по таскам сеансы (распределение случайным образом)
        protected void GenerateSeancesForTasks_ByRandom(
                int seance_max_duration_in_hours = 12, int talk_per_task_max_count = 10, int talk_per_task_min_count = 8,
                int sms_per_task_max_count = 8, int sms_per_task_min_count = 6,  
                int place_per_task_max_count = 1,   int place_per_task_min_count = 1,
                int video_per_task_max_count = 8,   int video_per_task_min_count = 6,
                bool generate_audio_video_files = false )
         {

           
             foreach (Task task in  DbDataModel.Tasks)
             {
                 //var _rand = new Random();

                 var task_interval_in_hours = (task.BeginDate.AddDays(task.TaskSrok) - task.BeginDate).TotalHours;

                 //даты сеансов линейно по возрастанию в порядке, как в превью сводки
                 //разговоры->смс->место->видео
                 //для возможности смерджить в дальнейшем список сеансов в общий
                 var seances_interval_in_hours = (int)(task_interval_in_hours /
                     (place_per_task_max_count + sms_per_task_max_count + place_per_task_max_count + video_per_task_max_count) / 2);

                 DateTime seance_begin_date = task.BeginDate;

                 //Talks
                 for (int i=1; i<=talk_per_task_max_count; i++ )
                 {

                    if (_random.Next(0, 20) % 2 == 1 && i>talk_per_task_min_count)
                        continue;

                     //var begin_date = task.BeginDate.AddHours(_rand.Next(1, (int)task_interval_in_hours - seance_max_duration_in_hours));

                    seance_begin_date = seance_begin_date.AddHours(seances_interval_in_hours);
                    var end_date = seance_begin_date.AddHours(_random.Next(1, seance_max_duration_in_hours));


                     var talk = new Talk()
                     {
                         task = task,
                         begin = seance_begin_date,
                         end = end_date,
                         dir = _random.Next(0, 100) % 2 == 0 ? TalkPhoneDirection.Incoming : TalkPhoneDirection.Outgoing,
                         InterlocutorPhone = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone),
                         //т.к. тест-кейсы в РИМе не предусматривают проигрывание файлов,
                         //для ускорения генерации генерим статические разговоры
                         createAudioFile = generate_audio_video_files
                     };

                     task.Talks.Add(talk);

                 }

                 //Sms
                 for (int i = 1; i <= sms_per_task_max_count; i++)
                 {
                     if (_random.Next(0, 20) % 2 == 1 && i > sms_per_task_min_count)
                         continue;

                     //var begin_date = task.BeginDate.AddHours(_rand.Next(1, (int)task_interval_in_hours - seance_max_duration_in_hours));
                     seance_begin_date = seance_begin_date.AddHours(seances_interval_in_hours);
                     var end_date = seance_begin_date.AddHours(_random.Next(1, seance_max_duration_in_hours));


                     var sms = new Sms()
                     {
                         task = task,
                         sendTime = seance_begin_date,
                         recvTime = end_date,
                         Direction = _random.Next(0, 100) % 2 == 0 ? PhoneDirection.Incoming : PhoneDirection.Outgoing,
                         InterlocutorPhone = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone),
                         text = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.SmsText),
                         archived = false
                     };

                      task.Sms.Add(sms);

                 }


                 if (task.Talks.Count > 0)
                 {

                     //Place
                     for (int i = 1; i <= place_per_task_max_count; i++)
                     {
                         if (_random.Next(0, 20) % 2 == 1 && i > place_per_task_min_count)
                             continue;


                         var place = new Place()
                         {
                             task = task,
                             talk = task.Talks[0],
                             //код страны
                             mcc = (int)MCC.Russia,
                             mnc = (int)MNC.MTS,
                             lac = _random.Next(10, 26),
                             cl = _random.Next(15, 26)
                         };

                         task.Places.Add(place);

                     }
                 

                 }



                 //Video
                 for (int i = 1; i <= video_per_task_max_count; i++)
                 {
                     if (_random.Next(0, 20) % 2 == 1 && i > video_per_task_min_count)
                         continue;


                     //var begin_date = task.BeginDate.AddHours(_rand.Next(1, (int)task_interval_in_hours - seance_max_duration_in_hours));

                     seance_begin_date = seance_begin_date.AddHours(seances_interval_in_hours);
                     var end_date = seance_begin_date.AddHours(_random.Next(1, seance_max_duration_in_hours));

                     var video = new Video()
                     {
                         task = task,
                         begin = seance_begin_date,
                         end = end_date,
                         proccessed = false,
                         createtVideoFile = generate_audio_video_files
                     };

                     task.Video.Add(video);

                 }

                 

             }


             log.Info(String.Format("Test step. В ОЗУ сгенерировали сеансы для заданий: {0} разговоров, {1} смс, {2} видео, {3} местоположений,",
                 DbDataModel.Tasks.SelectMany(task => task.Talks).Sum(talk=>1),
                 DbDataModel.Tasks.SelectMany(task => task.Sms).Sum(sms => 1),
                 DbDataModel.Tasks.SelectMany(task => task.Video).Sum(video=>1),
                 DbDataModel.Tasks.SelectMany(task => task.Places).Sum(place=>1)
            ));


         }



        protected void  GenerateStenoForTalksAndVideos(bool for_talks =true, bool for_videos =true)
        {
            foreach (Task task in DbDataModel.Tasks)
            {

                if (for_talks == true)
                {
                    foreach (Talk talk in task.Talks)
                    {
                        //var _rand = new Random();

                        talk.Steno = new Steno()
                        {
                            text = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.StenoText),
                            State = Texts8.Texts8State.Published
                        };
                    }
                }


                if (for_videos == true)
                {
                    foreach (Video vid in task.Video)
                    {
                        //var _rand = new Random();

                        vid.Steno = new Steno()
                        {
                            text = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.StenoText),
                            State = Texts8.Texts8State.Published
                        };
                    }
                }
                


            }


            log.Info("Test step. В ОЗУ сгенерировали стенограммы для разговоров " + (for_videos == true ? "и видео" : ""));


        }



        protected void GenerateMarksForSeances()
        {
            //hradcoded marks list
            DbDataModel.Marks = new List<Mark>()
            {
                new Mark()
                {
                    Title = "Важный",
                    Descr = "Важный"
                },
                new Mark()
                {
                    Title = "Аналитику",
                    Descr = "Аналитику"
                },
                new Mark()
                {
                    Title = "В сводку",
                    Descr = "В сводку"
                }
            };


            foreach (Task task in DbDataModel.Tasks)
            {

                //var _rand_flag = new Random();
                foreach (Talk talk in task.Talks)
                {

                    var marks_list = RimGUI.Tests.Framework.Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        talk.Marks.Add(mark);
                    }


                }


                foreach (var sms_info in task.Sms)
                {

                    var marks_list = RimGUI.Tests.Framework.Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        sms_info.Marks.Add(mark);
                    }

                }


                foreach (var place in task.Places)
                {
                    var marks_list = RimGUI.Tests.Framework.Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        place.Marks.Add(mark);
                    }


                }


                foreach (var video_info in task.Video)
                {
                    var marks_list = RimGUI.Tests.Framework.Utils.RandomListEntries(DbDataModel.Marks, _random);

                    foreach (var mark in marks_list)
                    {
                        video_info.Marks.Add(mark);
                    }



                }

            }


            log.Info(String.Format("Test step. В ОЗУ сгенерировали метки для сеансов: для разговоров {0}, для смс - {1}, для видео - {2}, для местоположений -{3} ",
                 DbDataModel.Tasks.SelectMany(task => task.Talks).SelectMany(seance => seance.Marks).Sum(mark => 1),
                 DbDataModel.Tasks.SelectMany(task => task.Sms).SelectMany(seance => seance.Marks).Sum(mark => 1),
                 DbDataModel.Tasks.SelectMany(task => task.Video).SelectMany(seance => seance.Marks).Sum(mark => 1),
                 DbDataModel.Tasks.SelectMany(task => task.Places).SelectMany(seance => seance.Marks).Sum(mark => 1)
            ));


        }


        //генерируем включая ранее вкюченные сеансы
        public void GenerateSummariesForTask_WithRandomSeances(string meropr, string vid_object, string task_number, string task_year,
            int summaries_count, bool set_default_report_date_now = true, DateTime? first_report_date=null, int interval_in_seconds = 24*3600, DateTime? presented_date = null)
        {

            Task task = DbDataModel.GetTaskByCipherParts(meropr, vid_object, task_number, task_year);

            for (int i=1; i<=summaries_count; i++)
            {
                var report = new Report();
                report.task = task;
                report.reportType = ReportType.Report;
                report.IsPrinted = true;

                if (set_default_report_date_now)
                {
                    report.ReportDate = DateTime.Now;
                    report.PrintTime = DateTime.Now;
                }
                else
                {
                    var rep_date = first_report_date.Value.AddSeconds(interval_in_seconds * (i - 1));
                    report.ReportDate = rep_date;
                    report.PrintTime = rep_date;
                }

                report.ReportNumber = i.ToString();
                //TODO generete custom users
                report.UserIdPrinted = 1;
                var rep_dates = new List<DateTime>() { };

                foreach (Talk talk in task.Talks)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Talks.Add(talk);
                    rep_dates.Add(talk.begin);
                }

                foreach (Sms sms in task.Sms)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Sms.Add(sms);
                    rep_dates.Add(sms.sendTime);
                }

                foreach (Video vid in task.Video)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Video.Add(vid);
                    rep_dates.Add(vid.begin);
                }

                foreach (Place place in task.Places)
                {
                    if (_random.Next(0, 20) % 2 == 1)
                        continue;
                    report.Places.Add(place);
                }

                report.SeanceCount = report.Talks.Count + report.Sms.Count +
                    report.Video.Count + report.Places.Count;
                report.ReportBegin = rep_dates.Min();
                report.ReportEnd = rep_dates.Max();

                report.PresentedMark = presented_date;

                task.Reports.Add(report);
            }


            log.Info(String.Format("Test step. В ОЗУ сгенерировали {0} сводок  для задания {1},  ",
                task.Reports.Count,
                task.TaskShifr
            ));


        }



        protected void ProjectDBStructToDatabase()
        {

            Dictionary<string, MarkDescription> mark_d = new Dictionary<string, MarkDescription>();
            foreach (var mark in DbDataModel.Marks)
            {
                mark_d.Add(mark.Title ,  _mtbDataBuilder.GetMarkDescription(mark.Title, mark.Descr, true));
                
            }



            byte[] talk_file = new byte[]{};
            byte[] video_file = new byte[]{};
            var create_files = false;

            //генерить ли сеансы со звуковыми и видеофайлами 
            if (DbDataModel.Tasks.SelectMany(task => task.Talks).Any(talk => talk.createAudioFile == true)
                || DbDataModel.Tasks.SelectMany(task => task.Video).Any(video => video.createtVideoFile == true))
            {
                create_files = true;
                //чтение файла
                talk_file = FileHelper.LoadFile(_talkFileName);
                video_file = FileHelper.LoadFile(_videoFileName);
            }

            


            foreach (var task in DbDataModel.Tasks)
            {

                //создаем задание c видом постановки на контроль по номеру телефона
                //статус - исполняемое, для отображения задания в РИМе, например.
                //тип контроля - МЕСТО | СМС | ТЕЛЕФОН | ВИДЕО | ДВО (без ФАКСА)
                //НЕ секретное

                var t_moj = _mojDataBuilder.CreateTaskMoj(
                task.Otdel, task.Init_organization, task.Init_podrazd, task.Init_Famini, 
                task.Init_phoneA, task.Init_phoneB, task.Meropr, task.VidObject, task.Task_number, task.Task_year,
                task.BeginDate, null, task.PhoneControl, null, null, Categories.Executed, SecurityLevel.Normal,
                "", "ЖИТЕЛЬСТВА", task.TaskSrok, "АСП", "232", 
                new List<TaskTypeControl>() { TaskTypeControl.DVO, TaskTypeControl.Place, TaskTypeControl.Sms, TaskTypeControl.Talk, TaskTypeControl.Video });

                ////ориентировку
                _mojDataBuilder.AddOrientation(t_moj, task.Orient);
                ////цель проведения - проверить причастность
                _mojDataBuilder.AddTaskTarget(t_moj, Utils.GetEnumDescription(task.Goal));


                var t_mtb = _mtbDataBuilder.SetTaskMtb(task.BeginDate, task.TaskSrok, task.Meropr, task.VidObject, task.Task_number, task.Task_year, Categories.Executed,
                task.Init_Famini, task.Orient, Utils.GetEnumDescription(task.Goal), DataTypes.Unsigned, 
                new List<TaskTypes>() { TaskTypes.DVO, TaskTypes.Place, TaskTypes.Sms, TaskTypes.Talk, TaskTypes.Video }, 
                SecurityLevel.Normal, _mojDataBuilder.GetTaskGuidById(t_moj.task_id),
                10, 20, 30);

                //назначить задание
                var ta = _mtbDataBuilder.CreateTaskAuto(String.Format("1_{0}", task.Task_number), SourceTypes.MagFlowBroker, 14, 0, task.BeginDate);

                var tg = _mtbDataBuilder.CreateTaskGrant(ta, t_mtb, task.BeginDate.AddHours(-4), null);
                

                ///////////////////////сеансы////////////////////////////////////
                //чтение файлов
                //var talk_file = FileHelper.LoadFile(_talkFileName);
                //var video_file = FileHelper.LoadFile(_videoFileName);
                TalkMtb first_talk = null;

                foreach (var talk in task.Talks)
                {

                    var _talk = create_files == true ?
                        _mtbDataBuilder.CreateTalkMtb(ta, talk.begin, talk.end, Arc_flags.FileWrited, talk.dir, 1, talk_file)
                        : _mtbDataBuilder.CreateStaticTalkMtb(ta, talk.begin, talk.end, talk.dir, 1);

                    
                    _mtbDataBuilder.CreateTalkPhones(_talk, task.PhoneControl, talk.InterlocutorPhone);
                    if (task.Talks.IndexOf(talk) == 0)
                    {
                        first_talk = _talk;
                    }

                    if (talk.Steno != null)
                    {
                        _mtbDataBuilder.CreateTexts8(t_mtb, _talk, talk.Steno.State, talk.Steno.text);

                    }

                    foreach (var mark in talk.Marks)
                    {
                        _mtbDataBuilder.CreateTalkMark(_talk, t_mtb, mark_d[mark.Descr]);
                    }

                    talk.seanceId = _talk.TalkId;

                }

                foreach (var sms in task.Sms)
                {
                    var _sms = _mtbDataBuilder.CreateSmsMtb(ta, task.PhoneControl, sms.InterlocutorPhone, sms.sendTime, sms.recvTime,
                        sms.Direction, sms.archived, sms.text);


                    foreach (var mark in sms.Marks)
                    {
                        _mtbDataBuilder.CreateSmsMark(_sms, t_mtb, mark_d[mark.Descr]);
                    }

                    sms.seanceId = _sms.SmsId;
                }


                if (task.Talks.Count > 0)
                {
                    foreach (var place in task.Places)
                    {
                        var _place = _mtbDataBuilder.CreatePlace(ta, first_talk, place.mcc, place.mnc, place.lac, place.cl);

                        foreach (var mark in place.Marks)
                        {
                            _mtbDataBuilder.CreatePlaceMark(_place, mark_d[mark.Descr], t_mtb);
                        }

                        place.seanceId = _place.PlaceId;

                    }

                   
                }
                

                foreach (var video in task.Video)
                {

                    var _video = create_files == true ?
                        _mtbDataBuilder.CreateVideo(ta, video.begin, video.end, false, 0, video_file, true)
                       : _mtbDataBuilder.CreateVideo(ta, video.begin, video.end, false, 0, new byte[] { }, false);

                  
                    if (video.Steno != null)
                    {
                        _mtbDataBuilder.CreateTexts8(t_mtb, _video, video.Steno.State, video.Steno.text);

                    }


                    foreach (var mark in video.Marks)
                    {
                        _mtbDataBuilder.CreateVideoMark(_video, mark_d[mark.Descr], t_mtb);
                    }

                    video.seanceId = _video.VideoId;

                }
                


                foreach (var report in task.Reports)
                {
                     var report_mtb = _mtbDataBuilder.CreateReport(t_mtb, report.ReportNumber, report.ReportDate, report.PrintTime, report.PresentedMark, false);

                    foreach (Talk talk in report.Talks)
                    {
                        _mtbDataBuilder.SetReportSeances(talk.seanceId, report_mtb.ReportId, Texts8.TypeSeance.Talk);
                    }

                    foreach (Sms sms in report.Sms)
                    {
                        _mtbDataBuilder.SetReportSeances(sms.seanceId, report_mtb.ReportId, Texts8.TypeSeance.SMS);
                    }

                    foreach (Video vid in report.Video)
                    {
                        _mtbDataBuilder.SetReportSeances(vid.seanceId, report_mtb.ReportId, Texts8.TypeSeance.Video);
                    }

                    foreach (Place place in report.Places)
                    {
                        _mtbDataBuilder.SetReportSeances(place.seanceId, report_mtb.ReportId, Texts8.TypeSeance.Place);
                    }

                    
                }

                

            }


            log.Info("Test step. Успешно записали задания,сеансы, связанную информацию в БД ");

        }


        #endregion




        //TODO добавить генерацию пользователей c различными уровнями доступа
        protected void Authorize()
        {
            var login = "Admin";
            var pass = "mag";

            App.AuthWindow.EnterUserName(login);
            App.AuthWindow.EnterUserPass(pass);
            App.AuthWindow.ClickOk();

            log.Info(String.Format("Test step. Авторизовались с логином  {0} паролем {1}", login, pass));
        }

        protected void DataGeneratorInit()
        {
            _mtbSettings = (MtbSettings)MAG_DB_Settings[Get_ENV()]["MTB"];
            _mojSettings = (MojSettings)MAG_DB_Settings[Get_ENV()]["MOJ"];

            _mtbDataBuilder = new MtbDataBuilder(new MtbDbManager(new SqlDataProvider(), _mtbSettings), MagVersion.ver_8_8);
            _mojDataBuilder = new MojDataBuilder(new MojDbManager(new SqlDataProvider(), _mojSettings));

            _talkFileName = Rim_Seances_Templates_File_Path[Get_ENV()]["talk_file"];
            _videoFileName = Rim_Seances_Templates_File_Path[Get_ENV()]["video_file"];
        }

        //
        protected void StartTestCase()
        {
            //log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(@"RimGUITests.dll.config"));
            DataGeneratorInit();
        }

        protected void StartApp()
        {
            if (App == null)
            {
                Application appWhite = Application.Launch(Rim_Application_Path[Get_ENV()] + "Rim.App.exe");
                Manager manager = new Manager(false);
                manager.Start();
                App = new RimApp.Tests.Framework.Elements.App(manager.ConnectToApplication(appWhite.Process), appWhite, log);

                log.Info("Test step. Запуск приложения");

                //launch auth_window
                Authorize();

            }
        }


        //
        protected void Start()
        {
            ProjectDBStructToDatabase();

            StartApp();

            
        }

        //
        protected void Stop()
        {
            if (App != null && App.ApplicationWhite != null)
            {
                App.ApplicationWhite.Kill();
            }
            App = null;

            log.Info("Test step. Приложение закрыто");
        }




        #region AssertExtensions

        public void AssertEqual_TaskInfoShortList_To_DBStructureTaskList(IList<Task> exp_tasks, IList<TaskInfoShort> actual_tasks)
        {
            //TaskInfoShort data example

            //Init "Петров Иван Анатольевич, Отдел Н"
            //Name "ПТП-30-10-2016"
            //SeancesCount "0"
            //Shifr "ПТП-30-10-2016"
            //SrokInfo "Сводок нет"

            List<string> error_list = new List<string>(); 

            if (exp_tasks.Count != actual_tasks.Count)
                error_list.Add(String.Format("Не совпадает количество записей по заданиям: expected - {0}, actual - {1}"
                    , exp_tasks.Count, actual_tasks.Count));
               

            foreach (Task exp_task in exp_tasks)
            {
                try
                {
                    TaskInfoShort act_task = actual_tasks.Where(task => task.Name == String.Format("{0}-{1}-{2}-{3}",
                    exp_task.Meropr, exp_task.VidObject, exp_task.Task_number, exp_task.Task_year)).First();

                    //Init "Петров Иван Анатольевич, Отдел Н"
                    if (String.Format("{0}, {1}", exp_task.Init_Famini, exp_task.Init_podrazd).ToLower() != act_task.Init.ToLower())
                        error_list.Add(String.Format("Для таска {0}-{1}-{2}-{3} не совпадает инициатор: expected - {4}, actual - {5}",
                       exp_task.Meropr, exp_task.VidObject, exp_task.Task_number, exp_task.Task_year, String.Format("{0}, {1}", exp_task.Init_Famini, exp_task.Init_podrazd), act_task.Init));

                    //SeancesCount "12"
                    if (exp_task.SeancesCount != act_task.SeansesCount)
                        error_list.Add(String.Format("Для таска {0}-{1}-{2}-{3} не совпадает количество включаемых сеансов в сводку: expected - {4}, actual - {5}",
                       exp_task.Meropr, exp_task.VidObject, exp_task.Task_number, exp_task.Task_year, exp_task.SeancesCount, act_task.SeansesCount));

                    //SrokInfo "Сводок нет" "255 сут. назад"
                    if (exp_task.SrokInfo != act_task.SrokInfo)
                        error_list.Add(String.Format("Для таска {0}-{1}-{2}-{3} не совпадает количество дней, прошедших с последней по нему печати сводки: expected - {4}, actual - {5}",
                       exp_task.Meropr, exp_task.VidObject, exp_task.Task_number, exp_task.Task_year, exp_task.SrokInfo, act_task.SrokInfo));
                    
                }
                catch 
                {
                    error_list.Add(String.Format("В списке отображаемых заданий не найден таск {0}-{1}-{2}-{3}",
                       exp_task.Meropr, exp_task.VidObject, exp_task.Task_number, exp_task.Task_year));
 
                }
                
                
            }

            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
            }


            log.Info(String.Format("Test step. Информация по заданиям, отображаемым в панели заданий страницы сводок соответствует ожидаемым значениям"));
            
        }



        //consider thar earlier in setup was generated unique tasks dates by ascending 
        //(except places, beacause its dates are equal talks dates)
        public void AssertEqual_SummaryPreviews(IList<SummaryPreviewInfo> actual_seanse_info,
            IList<Talk> talks, IList<Talk> unfiltered_talks,
            IList<Sms> sms, IList<Sms> unfiltered_sms,
            IList<Place> places, IList<Place> unfiltered_places,
            IList<Video> videos, IList<Video> unfiltered_video)
        {
            

            List<string> error_list = new List<string>();

            if ((talks.Count + sms.Count + places.Count + videos.Count) != actual_seanse_info.Count)
                error_list.Add(String.Format("Не совпадает количество записей по сеансам: expected - {0}, actual - {1}"
                    , (talks.Count + sms.Count + places.Count + videos.Count), actual_seanse_info.Count));


            //talks
            //{ "SeanceDateTime", "01.01.2016 03:00:00 — 1:00:00"},
            //            { "TalkSMSInterlocutorInfo", "(7)(983) 087-89-24, Входящий"},
            //            { "TalkSMSVideoStenoOrPlaceInfo", "Текст стенограммы"}
            foreach (var talk in talks)
            {
                //генерим даты уникальными в рамках каждого ОТМ 
                //поиск сеансов (разговор, смс, видео) по дате. поиск местопложений - по lac, lc.


                //Changelist
                //v. 1.8.0.449.
                //      (формат даты в 12-часовом формате)
                //v. 1.8.0.852. 
                //      (формат даты в 24-часовом формате)
                //TODO посмотреть, где настраивается формат.

                //var act_talk = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(talk.begin.ToString("dd.MM.yyyy hh:mm:ss") + " — ")).FirstOrDefault();
                var act_talk = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(talk.begin.ToString("dd.MM.yyyy HH:mm:ss") + " — ")).FirstOrDefault();


                if (act_talk != null)
                {
                    
                    //длительность разговора "01.01.2016 03:00:00 — 1:00:00"
                    var duration = string.Format("{0:h\\:mm\\:ss}", (talk.end - talk.begin));
                    Regex regex = new Regex(@"—\s" + duration);
                    if (!regex.Match(act_talk.SeanceDateTime).Success)
                        error_list.Add(String.Format("Для разговора не совпадает длительность: actual - {0}, expected - {1}"
                                        , act_talk.SeanceDateTime, duration));

                    //TalkSMSInterlocutorInfo "(7)(983) 087-89-24, Входящий"
                    //TODO здесь опять применены настройки форматирования телефонных номеров
                    var act_talk_info = act_talk.TalkSMSInterlocutorInfo.Replace("(7)","8");
                    string replace_pattern = @"([\(\)-]|\s)";
                    regex = new Regex(replace_pattern);
                    act_talk_info = regex.Replace(act_talk_info, "");

                    var exp_talk_info = String.Format("{0},{1}",talk.InterlocutorPhone, talk.dir == TalkPhoneDirection.Incoming ? "Входящий" : "Исходящий");
                    
                    if (act_talk_info != exp_talk_info)
                    {
                        error_list.Add(String.Format("Для разговора {0} не совпадают сведения о собеседнике: actual - {1}, expected - {2}"
                                        , act_talk.SeanceDateTime, act_talk_info, exp_talk_info));
                    }


                    //TalkSMSVideoStenoOrPlaceInfo
                    //стенограмма
                    //TODO добавлять условие для случая, когда ставим чекбокс
                    //"стенограммы текущего пользователя", т.е. фильтрацию по юзеру
                    if (talk.Steno == null && act_talk.TalkSMSVideoStenoOrPlaceInfo != "Не задан текст стенограммы")
                    {
                        error_list.Add(String.Format("Для разговора {0} не совпадают сведения о стенограмме: actual - {1}, expected - {2}"
                                        , act_talk.SeanceDateTime, act_talk.TalkSMSVideoStenoOrPlaceInfo, "Не задан текст стенограммы"));
                    }

                    if (talk.Steno != null && talk.Steno.State == Texts8.Texts8State.Darft && act_talk.TalkSMSVideoStenoOrPlaceInfo != "Не задан текст стенограммы")
                    {
                        error_list.Add(String.Format("Для разговора {0} не совпадают сведения о стенограмме: actual - {1}, expected - {2}"
                                        , act_talk.SeanceDateTime, act_talk.TalkSMSVideoStenoOrPlaceInfo, "Не задан текст стенограммы"));
                    }

                    if (talk.Steno != null && talk.Steno.State == Texts8.Texts8State.Published && act_talk.TalkSMSVideoStenoOrPlaceInfo != talk.Steno.text)
                    {
                        error_list.Add(String.Format("Для разговора {0} не совпадают сведения о стенограмме: actual - {1}, expected - {2}"
                                        , act_talk.SeanceDateTime, act_talk.TalkSMSVideoStenoOrPlaceInfo, talk.Steno.text));
                    }

                    

                }
                else
                {
                    error_list.Add(String.Format("Не отображается разговор с датой начала: {0}"
                                        ,talk.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                }


            }



            foreach (var talk in unfiltered_talks)
            {
                //var act_talk = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(talk.begin.ToString("dd.MM.yyyy hh:mm:ss") + " — ")).FirstOrDefault();
                var act_talk = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(talk.begin.ToString("dd.MM.yyyy HH:mm:ss") + " — ")).FirstOrDefault();


                if (act_talk != null)
                {
                    error_list.Add(String.Format("В списке отображен разговор {0}, что не удовлетворяет условиям фильтрации"
                                        , talk.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                   
                }
            }



            //sms
            //{ "SeanceDateTime", "01.01.2016 03:00:00"},
            //            { "TalkSMSInterlocutorInfo", "(7)(983) 087-89-24, Входящий"},
            //            { "TalkSMSVideoStenoOrPlaceInfo", "Текст смс сообщения"}
            foreach (var sms_info in sms)
            {


                //var act_sms = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(sms_info.sendTime.ToString("dd.MM.yyyy hh:mm:ss"))).FirstOrDefault();
                var act_sms = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(sms_info.sendTime.ToString("dd.MM.yyyy HH:mm:ss"))).FirstOrDefault();
                
                
                if (act_sms == null)
                {
                    error_list.Add(String.Format("Не отображается sms с датой начала: {0}"
                    , sms_info.sendTime.ToString("dd.MM.yyyy hh:mm:ss")));

                }
                else
                {

                    //TalkSMSInterlocutorInfo "(7)(983) 087-89-24, Входящий"
                    //TODO здесь опять применены настройки форматирования телефонных номеров
                    var act_sms_info = act_sms.TalkSMSInterlocutorInfo.Replace("(7)", "8");
                    string replace_pattern = @"([\(\)-]|\s)";
                    Regex regex = new Regex(replace_pattern);
                    act_sms_info = regex.Replace(act_sms_info, "");

                    var exp_sms_info = String.Format("{0},{1}", sms_info.InterlocutorPhone, sms_info.Direction == PhoneDirection.Incoming ? "Входящий" : "Исходящий");

                    if (act_sms_info != exp_sms_info)
                    {
                        error_list.Add(String.Format("Для смс {0} не совпадают сведения о собеседнике: actual - {1}, expected - {2}"
                                        , act_sms.SeanceDateTime, act_sms_info, exp_sms_info));
                    }


                    //"TalkSMSVideoStenoOrPlaceInfo", "Текст смс сообщения"
                    if (act_sms.TalkSMSVideoStenoOrPlaceInfo != sms_info.text)
                    {
                        error_list.Add(String.Format("Для смс {0} не совпадают тексты: actual - {1}, expected - {2}"
                                        , act_sms.SeanceDateTime, act_sms.TalkSMSVideoStenoOrPlaceInfo, sms_info.text));
                    }

                }


            }


            foreach (var sms_info in unfiltered_sms)
            {
                //var act_sms = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(sms_info.sendTime.ToString("dd.MM.yyyy hh:mm:ss"))).FirstOrDefault();
                var act_sms = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(sms_info.sendTime.ToString("dd.MM.yyyy HH:mm:ss"))).FirstOrDefault();
                
                if (act_sms != null)
                {
                    error_list.Add(String.Format("В списке отображен смс {0}, что не удовлетворяет условиям фильтрации"
                                        , sms_info.sendTime.ToString("dd.MM.yyyy hh:mm:ss")));

                }
            }



            //place ( в количестве 1 штука, поэтому достаточно искать по совокупности полей mnc,lac,cl  )
            //{ "SeanceDateTime", "01.01.2016 03:00:00"},
            //            { "TalkSMSInterlocutorInfo", ""},
            //            { "TalkSMSVideoStenoOrPlaceInfo", "Сеть: 1\r\nЗона: 15\r\nСота: 24"}
            foreach (var place in places)
            {
                Regex regex = new Regex(".*" + place.mnc + ".*\r\n.*" + place.lac + ".*\r\n.*" + place.cl);

                var act_place = actual_seanse_info.Where(seanse => regex.Match(seanse.TalkSMSVideoStenoOrPlaceInfo).Success).FirstOrDefault();
                if (act_place == null)
                {
                    error_list.Add(String.Format("Не отображается place с реквизитами: {0} {1} {2}"
                    , place.mnc, place.lac, place.cl));

                }

            }


            foreach (var place in unfiltered_places)
            {
                Regex regex = new Regex(".*" + place.mnc + ".*\r\n.*" + place.lac + ".*\r\n.*" + place.cl);

                var act_place = actual_seanse_info.Where(seanse => regex.Match(seanse.TalkSMSVideoStenoOrPlaceInfo).Success).FirstOrDefault();
                if (act_place != null)
                {
                    error_list.Add(String.Format("В списке отображен place с реквизитами: {0} {1} {2}, что не удовлетворяет условиям фильтрации"
                                         , place.mnc, place.lac, place.cl));

                }

            }



            //video
            //{ "SeanceDateTime", "17.01.2016 04:00:00 — 2:00:00"},
            //            { "TalkSMSInterlocutorInfo", ""},
            //            { "TalkSMSVideoStenoOrPlaceInfo", "Текст стенограммы"}
            foreach (var video_info in videos)
            {
                
                //var act_video = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(video_info.begin.ToString("dd.MM.yyyy hh:mm:ss") + " — ")).FirstOrDefault();
                var act_video = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(video_info.begin.ToString("dd.MM.yyyy HH:mm:ss") + " — ")).FirstOrDefault();
                
                
                if (act_video == null)
                {
                    error_list.Add(String.Format("Не отображается video с датой начала: {0}"
                    , video_info.begin.ToString("dd.MM.yyyy hh:mm:ss")));

                }
                else
                {
                    //длительность видео "01.01.2016 03:00:00 — 1:00:00"
                    var duration = string.Format("{0:h\\:mm\\:ss}", (video_info.end - video_info.begin));
                    Regex regex = new Regex(@"—\s" + duration);
                    if (!regex.Match(act_video.SeanceDateTime).Success)
                        error_list.Add(String.Format("Для видео не совпадает длительность: actual - {0}, expected - {1}"
                                        , act_video.SeanceDateTime, duration));


                    //TalkSMSVideoStenoOrPlaceInfo
                    //стенограмма
                    if (video_info.Steno == null && act_video.TalkSMSVideoStenoOrPlaceInfo != "Не задан текст стенограммы")
                    {
                        error_list.Add(String.Format("Для видео {0} не совпадают сведения о стенограмме: actual - {1}, expected - {2}"
                                        , act_video.SeanceDateTime, act_video.TalkSMSVideoStenoOrPlaceInfo, "Не задан текст стенограммы"));
                    }

                    if (video_info.Steno != null && video_info.Steno.State == Texts8.Texts8State.Darft && act_video.TalkSMSVideoStenoOrPlaceInfo != "Не задан текст стенограммы")
                    {
                        error_list.Add(String.Format("Для видео {0} не совпадают сведения о стенограмме: actual - {1}, expected - {2}"
                                        , act_video.SeanceDateTime, act_video.TalkSMSVideoStenoOrPlaceInfo, "Не задан текст стенограммы"));
                    }

                    if (video_info.Steno != null && video_info.Steno.State == Texts8.Texts8State.Published && act_video.TalkSMSVideoStenoOrPlaceInfo != video_info.Steno.text)
                    {
                        error_list.Add(String.Format("Для видео {0} не совпадают сведения о стенограмме: actual - {1}, expected - {2}"
                                        , act_video.SeanceDateTime, act_video.TalkSMSVideoStenoOrPlaceInfo, video_info.Steno.text));
                    }
                }


            }



            foreach (var video_info in unfiltered_video)
            {

                //var act_video = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(video_info.begin.ToString("dd.MM.yyyy hh:mm:ss") + " — ")).FirstOrDefault();
                var act_video = actual_seanse_info.Where(seanse => seanse.SeanceDateTime.Contains(video_info.begin.ToString("dd.MM.yyyy HH:mm:ss") + " — ")).FirstOrDefault();
                
                
                if (act_video != null)
                {
                    error_list.Add(String.Format("В списке отображено видео {0}, что не удовлетворяет условиям фильтрации"
                                        , video_info.begin ));

                }
            }


            

            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
            }



            log.Info(String.Format("Test step. Информация по сеансам, отображаемым в панели предпросмотра сводок страницы сводок соответствует ожидаемым значениям"));
            

        }





        public void AssertEqual_TaskInfoFull_To_DBTask(Task exp_task_info, TaskInfo act_task_info)
        {

            List<string> error_list = new List<string>();

            var exp_task_shifr = String.Format("{0}-{1}-{2}-{3}", exp_task_info.Meropr, exp_task_info.VidObject, exp_task_info.Task_number, exp_task_info.Task_year);

            if (exp_task_shifr != act_task_info.Shifr )
                error_list.Add(String.Format("Не совпадает шифр задания: expected - {0}, actual - {1}",
                      exp_task_shifr, act_task_info.Shifr));

            if (exp_task_info.PhoneControl != act_task_info.ControlPoint )
                error_list.Add(String.Format("Не совпадает контролируемый номер: expected - {0}, actual - {1}",
                      exp_task_info.PhoneControl, act_task_info.ControlPoint));

            if (exp_task_info.Init_Famini != act_task_info.Init )
                error_list.Add(String.Format("Не совпадает инициатор: expected - {0}, actual - {1}",
                      exp_task_info.Init_Famini, act_task_info.Init));


            if (Utils.GetEnumDescription(exp_task_info.Goal) != act_task_info.Goal )
                error_list.Add(String.Format("Не совпадает цель: expected - {0}, actual - {1}",
                      Utils.GetEnumDescription(exp_task_info.Goal), act_task_info.Goal));


            if (exp_task_info.Orient != act_task_info.Orientir )
                error_list.Add(String.Format("Не совпадает ориентировка: expected - {0}, actual - {1}",
                      exp_task_info.Init_Famini, act_task_info.Init));


            CultureInfo ruRU = new CultureInfo("ru-RU");

            var exp_srok_info = String.Format("c {0} по {1} года, сроком {2} суток",
                exp_task_info.BeginDate.Month == exp_task_info.BeginDate.AddDays(exp_task_info.TaskSrok-1).Month
                ?  exp_task_info.BeginDate.Day.ToString()
                : exp_task_info.BeginDate.ToString("d MMMM", ruRU),
                exp_task_info.BeginDate.AddDays(exp_task_info.TaskSrok-1).ToString("d MMMM yyyy", ruRU),
                //exp_task_info.BeginDate.Year,
                exp_task_info.TaskSrok  
                );
            //{ "SrokInfo", "c 1 января по 29 февраля 2016 года, сроком 60 суток Срок исполнения истек"},

            if (!Regex.Replace(act_task_info.SrokInfo, @"\n+", "").Contains(exp_srok_info))
                error_list.Add(String.Format("Не совпадает срок проведения: expected - {0}, actual - {1}",
                      exp_srok_info, act_task_info.SrokInfo));


            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
            }


            log.Info(String.Format("Test step. Информация по заданию {0} отображаемая на карточке задания, соответствует ожидаемым значениям", act_task_info.Shifr));

        }




        public void AssertEqual_JournalDataGridString(List<JournalDataGridString> actual_strings, List<JournalDataGridString> expected_strings)
        {

            List<string> error_list = new List<string>();

            if (actual_strings.Count != expected_strings.Count)
                error_list.Add(String.Format("Не совпадает количество записей в журнале итоговых материалов: expected - {0}, actual - {1}"
                    , expected_strings.Count, actual_strings.Count));


            foreach (JournalDataGridString exp_string in expected_strings)
            {
                try
                {
                    JournalDataGridString act_string = actual_strings.Where(j_string => j_string.OTM == exp_string.OTM &&
                        j_string.Number == exp_string.Number).First();

                    if (DateTime.Parse(exp_string.DateAndtime).Date != DateTime.Parse(act_string.DateAndtime).Date)
                        error_list.Add(String.Format("В списке отображаемых сводок для сводки по заданию {0} с номером {1} неверно указана дата формирования {2}",
                        exp_string.OTM, exp_string.Number, exp_string.DateAndtime));

                }
                catch
                {
                    error_list.Add(String.Format("В списке отображаемых сводок не найдена сводка по заданию {0} с номером {1}",
                       exp_string.OTM, exp_string.Number));

                }


            }

            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
            }


            log.Info(String.Format("Test step. Информация в таблице журнала итоговых материалов соответствует ожидаемым значениям"));

        }



        public void AssertEqual_JournalDataGridString_To_DBModel_Report(List<JournalDataGridString> actual_strings, List<Report> expected_strings,
            IList<string> compared_columns_list = null)
        {

            List<string> error_list = new List<string>();

            if (actual_strings.Count != expected_strings.Count)
                error_list.Add(String.Format("Не совпадает количество записей в журнале итоговых материалов: expected - {0}, actual - {1}"
                    , expected_strings.Count, actual_strings.Count));

            //т.к. в видимую область может входить только заданный набор колонок
            if (compared_columns_list == null)
                compared_columns_list = new List<string>()
                {
                    "Type", "OTM", "DateAndtime", "Number", "Take_date"
                };


            //поиск соответствия по набору полей - "Type", "OTM", "DateAndtime"
            //поэтому дата каждой сводки/архива дата создания должна быть уникальна в рамках ОТМ

            foreach (Report exp_string in expected_strings)
            {
                try
                {
                    JournalDataGridString act_string = actual_strings.Where(j_string => j_string.OTM == exp_string.task.TaskShifr &&
                        j_string.Type == Utils.GetEnumDescription(exp_string.reportType) && j_string.DateAndtime == exp_string.ReportDate.ToString("dd.MM.yyyy HH:mm:ss")).First();

                    if (compared_columns_list.Contains("Number") && act_string.Number != exp_string.ReportNumber)
                        error_list.Add(String.Format("Для {0} по заданию {1} с датой формирования {2} номер:  expected -  {3}, actual  - {4}",
                        Utils.GetEnumDescription(exp_string.reportType), exp_string.task.TaskShifr, exp_string.ReportDate.ToString("dd.MM.yyyy HH:mm:ss"), exp_string.ReportNumber, act_string.Number));

                    var exp_presented_date = exp_string.PresentedMark.HasValue ? exp_string.PresentedMark.Value.ToString("dd.MM.yyyy") : "";
                    if (compared_columns_list.Contains("Take_date") && act_string.Take_date != exp_presented_date)
                        error_list.Add(String.Format("Для {0} по заданию {1} с датой формирования {2} дата предоставления:  expected -  {3}, actual  - {4}",
                        Utils.GetEnumDescription(exp_string.reportType), exp_string.task.TaskShifr, exp_string.ReportDate.ToString("dd.MM.yyyy HH:mm:ss"), exp_presented_date, act_string.Take_date));

                    //
                    //var exp_period = exp_string.ReportBegin.ToString("dd.MM.yyyy H:mm") + "-" + exp_string.ReportEnd.ToString("dd.MM.yyyy H:mm");
                    //if (compared_columns_list.Contains("Period") && act_string.Period != exp_period)
                    //    error_list.Add(String.Format("Для {0} по заданию {1} с датой формирования {2} период:  expected -  {3}, actual  - {4}",
                    //    Utils.GetEnumDescription(exp_string.reportType), exp_string.task.TaskShifr, exp_string.ReportDate.ToString("dd.MM.yyyy HH:mm:ss"), exp_period, act_string.Period));


                }
                catch
                {
                    error_list.Add(String.Format("В списке отображаемых итоговых материалов не найдена {0} по заданию {1} дата создания - {2}",
                       Utils.GetEnumDescription(exp_string.reportType), exp_string.task.TaskShifr, exp_string.ReportDate.ToString("dd.MM.yyyy HH:mm:ss")));

                }


            }

            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
            }


            log.Info(String.Format("Test step. Информация в таблице журнала итоговых материалов соответствует ожидаемым значениям"));

        }




        public void AssertEqual_ArchiveDataGridStrings(List<ArchiveSeanceInfo> actual_strings,
            IList<Talk> talks, IList<Talk> unfiltered_talks,
            IList<Sms> sms, IList<Sms> unfiltered_sms,
            IList<Place> places, IList<Place> unfiltered_places,
            IList<Video> videos, IList<Video> unfiltered_video)
        {

            List<string> error_list = new List<string>();

            if ((talks.Count + sms.Count + places.Count + videos.Count) != actual_strings.Count)
                error_list.Add(String.Format("Не совпадает количество записей по сеансам: expected - {0}, actual - {1}"
                    , (talks.Count + sms.Count + places.Count + videos.Count), actual_strings.Count));


            //производим поиск по набору полей ОТМ, тип сеанса, дата

            //далее сравнение идет по полям длительность, объект контроля, собеседник, направление
            //если поле не присутствует в гриде, то оно пропускается при сравнении

            //talks
            foreach (var talk in talks)
            {
                
                //TODO формат отображаемой даты в 24-часовом формате, в отличие от сводок
                var act_talk = actual_strings.Where(seanse => seanse.Date == talk.begin.ToString("dd.MM.yyyy HH:mm:ss") &&  seanse.Type == "Talk" 
                    && seanse.Shifr == talk.task.TaskShifr ).FirstOrDefault();

                if (act_talk != null)
                {

                    if (act_talk.Duration != null)
                    {
                        var duration = string.Format("{0:hh\\:mm\\:ss}", (talk.end - talk.begin));
                        if (act_talk.Duration != duration)
                            error_list.Add(String.Format("Для разговора не совпадает длительность: actual - {0}, expected - {1}"
                                   , act_talk.Duration, duration));
                    }

                    if (act_talk.ControlObject != null)
                    {
                        if (act_talk.ControlObject != talk.task.PhoneControl )
                            error_list.Add(String.Format("Для разговора не совпадает номер контроля: actual - {0}, expected - {1}"
                                   , act_talk.ControlObject, talk.task.PhoneControl));

                    }

                    if (act_talk.Interlocutor != null)
                    {
                        if (act_talk.Interlocutor != talk.InterlocutorPhone)
                            error_list.Add(String.Format("Для разговора не совпадает номер собесдника: actual - {0}, expected - {1}"
                                   , act_talk.Interlocutor, talk.InterlocutorPhone));

                    }

                    if (act_talk.Direction != null)
                    {
                        if (act_talk.Direction != (talk.dir == TalkPhoneDirection.Incoming ? "In" : "Out" ) )
                            error_list.Add(String.Format("Для разговора не совпадает направление: actual - {0}, expected - {1}"
                                   , act_talk.Direction, talk.dir));

                    }


                }
                else
                {
                    error_list.Add(String.Format("Для задания {0} Не отображается разговор с датой начала: {1}"
                                        ,talk.task.TaskShifr,  talk.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                }


                
            }



            foreach (var talk in unfiltered_talks)
            {
                var act_talk = actual_strings.Where(seanse => seanse.Date == talk.begin.ToString("dd.MM.yyyy HH:mm:ss") && seanse.Type == "Talk"
                    && seanse.Shifr == talk.task.TaskShifr).FirstOrDefault();
                if (act_talk != null)
                {
                    error_list.Add(String.Format("Для задания {0} в списке отображен разговор {1}, что не удовлетворяет условиям фильтрации"
                                        ,talk.task.TaskShifr , talk.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                }
            }




            //sms
            foreach (var sms_info in sms)
            {

                //TODO формат отображаемой даты в 24-часовом формате, в отличие от сводок
                var act_seance = actual_strings.Where(seanse => seanse.Date == sms_info.recvTime.ToString("dd.MM.yyyy HH:mm:ss") && seanse.Type == "Sms"
                    && seanse.Shifr == sms_info.task.TaskShifr).FirstOrDefault();

                if (act_seance != null)
                {


                    if (act_seance.ControlObject != null)
                    {
                        if (act_seance.ControlObject != sms_info.task.PhoneControl)
                            error_list.Add(String.Format("Для смс не совпадает номер контроля: actual - {0}, expected - {1}"
                                   , act_seance.ControlObject, sms_info.task.PhoneControl));

                    }

                    if (act_seance.Interlocutor != null)
                    {
                        if (act_seance.Interlocutor != sms_info.InterlocutorPhone)
                            error_list.Add(String.Format("Для смс не совпадает номер собесдника: actual - {0}, expected - {1}"
                                   , act_seance.Interlocutor, sms_info.InterlocutorPhone));

                    }

                    if (act_seance.Direction != null)
                    {
                        if (act_seance.Direction != (sms_info.Direction == PhoneDirection.Incoming ? "In" : "Out"))
                            error_list.Add(String.Format("Для смс не совпадает направление: actual - {0}, expected - {1}"
                                   , act_seance.Direction, sms_info.Direction));

                    }
                }
                else
                {
                    error_list.Add(String.Format("Для задания {0} Не отображается смс с датой начала: {1}"
                                        , sms_info.task.TaskShifr, sms_info.recvTime.ToString("dd.MM.yyyy hh:mm:ss")));
                }


            }



            foreach (var sms_info in unfiltered_sms)
            {
                var act_seance = actual_strings.Where(seanse => seanse.Date == sms_info.sendTime.ToString("dd.MM.yyyy HH:mm:ss") && seanse.Type == "Sms"
                   && seanse.Shifr == sms_info.task.TaskShifr).FirstOrDefault();

                if (act_seance != null)
                {
                    error_list.Add(String.Format("Для задания {0} в списке отображен смс {1}, что не удовлетворяет условиям фильтрации"
                                        , sms_info.task.TaskShifr, sms_info.sendTime.ToString("dd.MM.yyyy hh:mm:ss")));
                }
            }




            //places
            foreach (var place in places)
            {

                //TODO формат отображаемой даты в 24-часовом формате, в отличие от сводок
                var act_seance = actual_strings.Where(seanse => seanse.Date == place.talk.begin.ToString("dd.MM.yyyy HH:mm:ss") && seanse.Type == "Place"
                    && seanse.Shifr == place.task.TaskShifr).FirstOrDefault();

                if (act_seance != null)
                {
                    if (act_seance.ControlObject != null)
                    {
                        if (act_seance.ControlObject != place.task.PhoneControl)
                            error_list.Add(String.Format("Для местоположения не совпадает номер контроля: actual - {0}, expected - {1}"
                                   , act_seance.ControlObject, place.task.PhoneControl));

                    }

                    if (act_seance.Interlocutor != null)
                    {
                        if (act_seance.Interlocutor != place.talk.InterlocutorPhone)
                            error_list.Add(String.Format("Для местоположения не совпадает номер собесдника: actual - {0}, expected - {1}"
                                   , act_seance.Interlocutor, place.talk.InterlocutorPhone));

                    }

                   
                }
                else
                {
                    error_list.Add(String.Format("Для задания {0} Не отображается местоположение с датой: {1}"
                                        , place.task.TaskShifr, place.talk.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                }


            }



            foreach (var place in unfiltered_places)
            {
                var act_seance = actual_strings.Where(seanse => seanse.Date == place.talk.begin.ToString("dd.MM.yyyy HH:mm:ss") && seanse.Type == "Place"
                    && seanse.Shifr == place.task.TaskShifr).FirstOrDefault();

                if (act_seance != null)
                {
                    error_list.Add(String.Format("Для задания {0} в списке отображено местоположение {1}, что не удовлетворяет условиям фильтрации"
                                        , place.task.TaskShifr, place.talk.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                }
            }





            //videos
            foreach (var video in videos)
            {

                //TODO формат отображаемой даты в 24-часовом формате, в отличие от сводок
                var act_seance = actual_strings.Where(seanse => seanse.Date == video.begin.ToString("dd.MM.yyyy HH:mm:ss") && seanse.Type == "Video"
                    && seanse.Shifr == video.task.TaskShifr).FirstOrDefault();

                if (act_seance != null)
                {

                }
                else
                {
                    error_list.Add(String.Format("Для задания {0} Не отображается Видео с датой: {1}"
                                        , video.task.TaskShifr, video.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                }


            }



            foreach (var video in unfiltered_video)
            {
                var act_seance = actual_strings.Where(seanse => seanse.Date == video.begin.ToString("dd.MM.yyyy HH:mm:ss") && seanse.Type == "Video"
                    && seanse.Shifr == video.task.TaskShifr).FirstOrDefault();


                if (act_seance != null)
                {
                    error_list.Add(String.Format("Для задания {0} в списке отображено видео {1}, что не удовлетворяет условиям фильтрации"
                                        , video.task.TaskShifr, video.begin.ToString("dd.MM.yyyy hh:mm:ss")));
                }
            }








            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
            }


            log.Info(String.Format("Test step. Информация в таблице сеансов страницы архивов соответствует ожидаемым значениям"));


        }





         public void Assert_Summary_Content(Task expected_task_info, string content, int summary_number, DateTime begin_date, DateTime end_date, string creator_name)
         {

             //формат полученного текстового блока (без \s)

             //ВЕДОМСТВО УПРАВЛЕНИЕ адрес
             //(для Иванов Дмитрий Сергеевич )
             //Сводка мероприятия ОТП № 1 от 26.10.2016 за объектом 89130745634 по заданию ОТП - 30 - 4 - 2016 за период с 1/7/2016 по 1/27/2016 
             //Рег. № ________ « ___ » _____________ 2016 г. На 4 лист. 
             //Разговоры: 
             //07.01.2016 20:00:00 (7)(913) 064 - 56 - 34 Входящий Admin я полковник на белом коне 
             //08.01.2016 16:00:00 (7)(913) 074 - 56 - 34 Входящий Admin ты меня слышишь? 
             //09.01.2016 12:00:00 (7)(913) 074 - 56 - 34 Исходящий Admin я полковник на белом коне 
             //10.01.2016 08:00:00 (7)(913) 074 - 56 - 34 Входящий Admin я полковник на белом коне 
             //13.01.2016 16:00:00 (7)(913) 064 - 56 - 34 Входящий 
             //Печатать Печатать Печатать Печатать 0 ошибок 100% 2
             //С водка № , Рег.№ ________ от 26.10.2016 г. за объектом
             //Admin я полковник на белом коне
             //14.01.2016 12:00:00 (7)(913) 074 - 56 - 34 Входящий Admin я полковник на белом коне 
             //СМС: 
             //16.01.2016 04:00:00 (7)(913) 064 - 56 - 34 Входящий привет, все готово 
             //17.01.2016 00:00:00 (7)(913) 064 - 56 - 34 Входящий привет, все готово 
             //Местоположения: 
             //07.01.2016 20:00:00 1 : 11 : 25 :: 
             //Видеозаписи: 
             //22.01.2016 00:00:00 – 07:00:00 Admin ты меня слышишь? 
             //22.01.2016 20:00:00 – 04:00:00 Admin я полковник на белом коне 
             //Печатать Печатать Печатать Печатать 0 ошибок 100% 3 
             //С водка № , Рег.№ ________ от 26.10.2016 г. за объектом 
             //Admin ты меня слышишь?
             //25.01.2016 08:00:00 – 11:00:00 Admin я полковник на белом коне 
             //УПРАВЛЕНИЕ ___________________________________________________________ 
             //Печатать Печатать Печатать Печатать 0 ошибок 100% 4
             //С водка № , Рег.№ ________ от 26.10.2016 г. за объектом 
             //26.10.2016 Файл не сохранен 
             //Печатать Печатать Печатать Печатать 0 ошибок 100% 4 
             //С водка № , Рег.№ ________ от 26.10.2016 г. за объектом 26.10.2016

             for (int i=1; i<=10 ; i++)
             {
                 content = content.Replace(
                     String.Format("{0}Сводка№,Рег.№________от{1}г.заобъектом", i.ToString(), DateTime.Now.ToString("dd.MM.yyyy") ),
                     ""
                     );
             }

             content = content.Replace(" ", "");

             //TODO здесь опять применены настройки форматирования телефонных номеров, 
             //как и в превью сводки, поэтому пока
             //ограничимся разговорами
             List<string> error_list = new List<string>();

             if (!content.Contains(expected_task_info.Init_Famini.Replace(" ","")))
                 error_list.Add(String.Format("Инициатор: expected - {0}" , expected_task_info.Init_Famini));

             var summary_header = String.Format("Сводка мероприятия {0} № {1} от {2} за объектом {3} по заданию {4} за период с {5} по {6} ",
                 expected_task_info.Meropr, summary_number, DateTime.Now.ToString("dd.MM.yyyy"), expected_task_info.PhoneControl, String.Format("{0}-{1}-{2}-{3}",
                expected_task_info.Meropr, expected_task_info.VidObject, expected_task_info.Task_number, expected_task_info.Task_year), begin_date.ToString(@"M\/d\/yyyy"), end_date.ToString(@"M\/d\/yyyy"));

             if (!content.Contains(summary_header.Replace(" ", "")))
                 error_list.Add(String.Format("Хедер сводки: expected - {0}", summary_header));


             //Разговоры: 
             foreach (var talk in expected_task_info.Talks)
             {
                
                 //07.01.2016 20:00:00 (7)(913) 064 - 56 - 34 Входящий Admin я полковник на белом коне 
                 var talk_string = String.Format("{0} {1} {2} {3} {4}",
                     talk.begin.ToString("dd.MM.yyyy HH:mm:ss"),
                     RimGUI.Tests.Framework.Utils.GetPhoneNumberFormatted(talk.InterlocutorPhone),
                     talk.dir == TalkPhoneDirection.Incoming ? "Входящий" : "Исходящий",
                     creator_name,
                     talk.Steno.text).Replace(" ","");

                 if (!content.Contains(talk_string))
                     error_list.Add(String.Format("Не найден разговор: expected - {0}", talk_string));
             }

            //TODO СМС, местоположения, итд 
         

             if (error_list.Count > 0)
             {
                 Assert.Fail(String.Join("; ", error_list.ToArray()));
             }

             log.Info(String.Format("Test step. Информация в окне предварительного просмотра сводки соответствует ожидаемым значениям"));


         }




         public void Assert_Check_Otm_Directory_And_Seances_Exists(Task archived_task, string folder_name_ending, string talk_ext="wsd", string video_ext="avi", string template = "", bool description = false )
        {
            var archive_seances_path = Rim_Export_Seances_FIle_Path[Get_ENV()];
            if (!Directory.Exists(String.Format(@"{0}\{1}{2}", archive_seances_path, archived_task.TaskShifr, folder_name_ending)))
            {
                Assert.Fail("Не найдена директория архива для задания {0} - {1}", archived_task.TaskShifr,
                    String.Format(@"{0}\{1}{2}", archive_seances_path, archived_task.TaskShifr, folder_name_ending));
            }

            


            foreach (var talk in archived_task.Talks)
            {

                string file_name = "";

                if (template == "")
                {
                    file_name = talk.seanceId.ToString();
                }
                else
                {

                    //здесь похоже путаница с наименованием, написано на форме, что #num2 - это объект контроля

                    file_name = template.Replace("#date", talk.begin.ToString("yyyy-MM-dd")).Replace("#num2", talk.InterlocutorPhone).Replace(
                       "#num1", talk.task.PhoneControl).Replace("#id", talk.seanceId.ToString()).Replace("#time", string.Format("{0:hh\\.mm\\.ss}", talk.begin.TimeOfDay));

                }

                var full_path = String.Format(@"{0}\{1}{2}\01\{3}.{4}", archive_seances_path, archived_task.TaskShifr, folder_name_ending, file_name, talk_ext);

                if (!File.Exists(full_path))
                {
                    
                     Assert.Fail("Не найден файл разговора с id {0} для задания {1}", file_name, archived_task.TaskShifr);
                }
                 
                //если есть описание файла разговора
                if (description)
                {
                    var desc_file_name = full_path.Replace(talk_ext, "txt");

                    if (!File.Exists(desc_file_name))
                    {
                        Assert.Fail("Не найден файл описания для разговора с id {0} для задания {1}", desc_file_name, archived_task.TaskShifr);
                    }


                    string line;
                    string full_content = "";
                    using (StreamReader reader = File.OpenText(desc_file_name))
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            full_content += line;
                        }
                    }


                    //Источник - МАГ
                    //Регистрационный номер - ОТП-30-6-2016
                    //Дата записи - 11.01.2016
                    //Время начала записи - 20:00:00
                    //Продолжительность записи - 06:00:00
                    //Направление - Входящий
                    //Объект контроля - 89130745634
                    //Собеседник - 89130645634

                    var checked_strings = new List<string>()
                    {
                        string.Format("Дата записи - {0}", talk.begin.ToString("dd.MM.yyyy")),
                        string.Format("Время начала записи - {0}", string.Format("{0:hh\\:mm\\:ss}", talk.begin.TimeOfDay)),
                        string.Format("Продолжительность записи - {0}", string.Format("{0:hh\\:mm\\:ss}", talk.begin - talk.end)),
                        string.Format("Направление - {0}", talk.dir == TalkPhoneDirection.Incoming ? "Входящий" : "Исходящий"),
                        string.Format("Объект контроля - {0}", talk.task.PhoneControl),
                        string.Format("Собеседник - {0}", talk.InterlocutorPhone),
                    };

                    foreach (var ch_string in checked_strings)
                    {
                        if (!full_content.Contains(ch_string))
                            Assert.Fail("В файле описания для разговора с id {0} для задания {1} не найден текст {2}", desc_file_name, archived_task.TaskShifr, ch_string);
                    }
                    


                }

                
            }


            foreach (var video in archived_task.Video)
            {

                string file_name = "";

                if (template == "")
                {
                    file_name = "vid_"+video.seanceId.ToString();
                }
                else
                {

                    file_name = template.Replace("#date", video.begin.ToString("yyyy-MM-dd")).Replace("#id",
                        video.seanceId.ToString()).Replace("#time", string.Format("{0:hh\\.mm\\.ss}", video.begin.TimeOfDay)).Replace("#num1",
                        "").Replace("#num2", "");

                }

                var full_file_name = String.Format(@"{0}\{1}{2}\01\{3}.{4}",
                    archive_seances_path, archived_task.TaskShifr, folder_name_ending, file_name, video_ext);

                if (!File.Exists(full_file_name))
                {

                    Assert.Fail("Не найден файл видео с id {0} для задания {1}", file_name, archived_task.TaskShifr);
                }



                if (description)
                {
                    var desc_file_name = full_file_name.Replace(video_ext, "txt");

                    if (!File.Exists(desc_file_name))
                    {
                        Assert.Fail("Не найден файл описания для видео с id {0} для задания {1}", desc_file_name, archived_task.TaskShifr);
                    }



                    string line;
                    string full_content = "";
                    using (StreamReader reader = File.OpenText(desc_file_name))
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            full_content += line;
                        }
                    }


                    //Источник - МАГ
                    //Регистрационный номер - ОТП-30-6-2016
                    //Дата записи - 26.01.2016
                    //Время начала записи - 00:00:00
                    //Продолжительность записи - 10:00:00

                    var checked_strings = new List<string>()
                    {
                        string.Format("Дата записи - {0}", video.begin.ToString("dd.MM.yyyy")),
                        string.Format("Время начала записи - {0}", string.Format("{0:hh\\:mm\\:ss}", video.begin.TimeOfDay)),
                        string.Format("Продолжительность записи - {0}", string.Format("{0:hh\\:mm\\:ss}", video.begin - video.end))
                    };

                    foreach (var ch_string in checked_strings)
                    {
                        if (!full_content.Contains(ch_string))
                            Assert.Fail("В файле описания для видео с id {0} для задания {1} не найден текст {2}", desc_file_name, archived_task.TaskShifr, ch_string);
                    }


                }


                 
            }

            log.Info(String.Format("Test step. Директория с архивом для задания {0}, файлы сеансов и их описаний существуют, их наименование, размер, содержимое соотвествуют заданным параметрам ", 
                archived_task.TaskShifr));




        }




         public void Assert_Check_Otm_Directory_With_Dates_Subfolders_And_Seances_Exists(Task archived_task, string folder_name_ending, string talk_ext = "wsd", string video_ext = "avi")
        {
            var archive_seances_path = Rim_Export_Seances_FIle_Path[Get_ENV()];
            if (!Directory.Exists(String.Format(@"{0}\{1}{2}", archive_seances_path, archived_task.TaskShifr, folder_name_ending)))
            {
                Assert.Fail("Не найдена директория архива для задания {0} - {1}", archived_task.TaskShifr,
                    String.Format(@"{0}\{1}{2}", archive_seances_path, archived_task.TaskShifr, folder_name_ending));
            }



            //подпапки
            List<DateTime> talk_video_begin_dates = new List<DateTime>();
            talk_video_begin_dates.AddRange(archived_task.Talks.Select(talk => talk.begin.Date).ToList());
            talk_video_begin_dates.AddRange(archived_task.Video.Select(video => video.begin.Date).ToList());

            foreach (var seance_date in  talk_video_begin_dates.Distinct())
            {

                if (!Directory.Exists(String.Format(@"{0}\{1}{2}\01\{3}", archive_seances_path, archived_task.TaskShifr, folder_name_ending, seance_date.ToString("yyyy-MM-dd"))))
                {
                    Assert.Fail("Не найдена директория для задания {0} для даты {1}", archived_task.TaskShifr,
                        seance_date.ToString("yyyy-MM-dd"));
                }


                foreach (var talk in archived_task.Talks.Where(talk => talk.begin.Date == seance_date))
                {

                    if (!File.Exists(String.Format(@"{0}\{1}{2}\01\{3}\{4}.{5}", archive_seances_path, archived_task.TaskShifr, folder_name_ending, seance_date.ToString("yyyy-MM-dd"), talk.seanceId, talk_ext)))
                    {
                        Assert.Fail("Не найден файл разговора с id {0} для задания {1}", talk.seanceId, archived_task.TaskShifr);
                    }

                }


                foreach (var video in archived_task.Video.Where(video => video.begin.Date == seance_date))
                {

                    if (!File.Exists(String.Format(@"{0}\{1}{2}\01\{3}\vid_{4}.{5}",
                        archive_seances_path, archived_task.TaskShifr, folder_name_ending, seance_date.ToString("yyyy-MM-dd"), video.seanceId, video_ext)))
                    {
                        Assert.Fail("Не найден файл видео с id {0} для задания {1}", video.seanceId, archived_task.TaskShifr);
                    }

                }

            }


            log.Info(String.Format("Test step. Директория с архивом для задания {0} и суб-директории существуют и соотвествуют заданным параметрам ", 
                archived_task));


        }




        public void Assert_Check_Otm_Directory_Not_Exists(string directory_name, string folder_name_ending)
        {
            var archive_seances_path = Rim_Export_Seances_FIle_Path[Get_ENV()];
            if (Directory.Exists(String.Format(@"{0}\{1}{2}", archive_seances_path, directory_name, folder_name_ending)))
            {
                Assert.Fail("Директория существует {0}", String.Format(@"{0}\{1}{2}", archive_seances_path, directory_name, folder_name_ending));
            }

            
        }



        public void Assert_Check_Excel_Registry_File_Content(IList<Talk> checked_talks, Task checked_task, IList<ExcelArchiveFields> checked_column_names)
        {
            //file_name
            string[] formats = new string[] { "xlsx", "xlsm" };
            var xlsx_file_name = @"FilesList";
            var archive_seances_path = Rim_Export_Seances_FIle_Path[Get_ENV()];

            bool found = false;
            string full_path = "";

            foreach (var format in formats)
            {
                full_path = String.Format(@"{0}\{1}\01\{2}.{3}", archive_seances_path, checked_task.TaskShifr, xlsx_file_name, format);

                if (File.Exists(full_path))
                {
                    found = true;
                    break;
                }

            }


            if (!found)
            {
                Assert.Fail("Не найден Excel - файл описания сеансов для задания {0} ", checked_task.TaskShifr);
            }

            

          
            DataTable dt = new DataTable();

            foreach (var col_name in checked_column_names)
            {
                dt.Columns.Add(col_name.ToString(), typeof(string));
            }

            Dictionary<int, ExcelArchiveFields> col_indexes_mapping = new Dictionary<int, ExcelArchiveFields>();           
            //open and read  .xlsx file
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(full_path, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;

                //ищем листы в файле с именами  Разговоры, Видео, SMS, Местоположения
                //Разговоры считываем и проверяем содержимое, проверяем существование остальных листов
                string[] sheet_names  = new string[]{"Разговоры", "Видео", "SMS", "Местоположения"};

                WorksheetPart worksheetPart = null;

                foreach (var sheet_name in sheet_names)
                {
                    Sheet theSheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheet_name).FirstOrDefault();

                    if (theSheet == null)
                    {
                        Assert.Fail("Не найдена страница в реестре сеансов с именем " + sheet_name);
                    }

                    if (sheet_name == "Разговоры")
                        worksheetPart = (WorksheetPart)(workbookPart.GetPartById(theSheet.Id));


                }


                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                
                var sheet_rows = sheetData.Elements<Row>().ToList();

                foreach (Row r in sheet_rows)
                {
                    //хедер
                    if (sheet_rows.IndexOf(r) == 0)
                    {
                        var cells = r.Elements<Cell>().ToList();
                        foreach (Cell c in cells)
                        {

                            string value = GetCellValue(c, workbookPart);
                            col_indexes_mapping.Add(cells.IndexOf(c), (ExcelArchiveFields)RimGUI.Tests.Framework.Utils.FindEnumByDescription(value, typeof(ExcelArchiveFields)));

                        }
                    }
                    else
                    {
                        //остальные строки - тело экселевского документа
                        var cells = r.Elements<Cell>().ToList();
                        var new_row = dt.NewRow();
                        
                        foreach (Cell c in cells)
                        {
                            var col_name = col_indexes_mapping[cells.IndexOf(c)].ToString();
                            //GetCellValue(c, workbookPart);
                            new_row[col_name] = GetCellValue(c, workbookPart);

                        }


                        dt.Rows.Add(new_row);
                    }

                   
                }
               
            }


            //заполнили таблицу данными о разговорах, сравниваем значения

             List<string> error_list = new List<string>();

            foreach (Talk talk in checked_talks)
            {
                //поиск сеанса по id
                DataRow[] rows = dt.Select(ExcelArchiveFields.id.ToString() + " = " + talk.seanceId);

                if (rows.Length > 0)
                {
                    
                    var talk_row = rows[0];

                    //сравниваем значения по выбранным колонкам
                    foreach (var checked_column in checked_column_names)
                    {
                        string value_in_excel = (string)talk_row[checked_column.ToString()];

                        //сравниваем значения некоторых колонок

                        switch (checked_column)
                        {
                           
                            case (ExcelArchiveFields.Shifr):
                                {
                                    if (value_in_excel != talk.task.TaskShifr)
                                        error_list.Add(String.Format("Для разговора с id {0} не совпадает шифр, в реестре - {1}, в РИМе - {2] ", talk.seanceId, value_in_excel, talk.task.TaskShifr ));
                                    break;
                                }
                            case (ExcelArchiveFields.BeginDate):
                                {
                                    if (value_in_excel != talk.begin.ToString("dd.MM.yyyy H:mm:ss"))
                                        error_list.Add(String.Format("Для разговора с id {0} не совпадает дата начала, в реестре - {1}, в РИМе - {2} ", talk.seanceId, value_in_excel, talk.begin.ToString("dd.MM.yyyy H:mm:ss")));
                                    break;
                                }
                                //собеседник
                            case (ExcelArchiveFields.Phone1):
                                {
                                    if (value_in_excel != talk.InterlocutorPhone)
                                        error_list.Add(String.Format("Для разговора с id {0} не совпадает номер собеседника, в реестре - {1}, в РИМе - {2} ", talk.seanceId, value_in_excel, talk.InterlocutorPhone));
                                    break;
                                }
                            //объект контроля
                            case (ExcelArchiveFields.Phone2):
                                {
                                    if (value_in_excel != talk.task.PhoneControl)
                                        error_list.Add(String.Format("Для разговора с id {0} не совпадает номер объекта контроля, в реестре - {1}, в РИМе - {2} ", talk.seanceId, value_in_excel, talk.task.PhoneControl));
                                    break;
                                }
                        }
                    }

                   
                }
                else
                {
                    error_list.Add("В реестре сеансов не найден разговор с id " + talk.seanceId);
                }
                   

            }

            if (error_list.Count > 0)
            {
                Assert.Fail(String.Join("; ", error_list.ToArray()));
            }


            log.Info(String.Format("Test step. Файл описания сеансов в формате Excel для задания {0} существует и соотвествуют заданным параметрам ",
                checked_task.TaskShifr));

        }



        private string GetCellValue(Cell cell, WorkbookPart wb_part)
        {
            string value = cell.InnerText;

            if (value == "")
                return value;

            if (cell.DataType != null)
            {
                switch (cell.DataType.Value)
                {
                    case CellValues.SharedString:

                        // For shared strings, look up the value in the
                        // shared strings table.
                        var stringTable =
                            wb_part.GetPartsOfType<SharedStringTablePart>()
                            .FirstOrDefault();

                        // If the shared string table is missing, something 
                        // is wrong. Return the index that is in
                        // the cell. Otherwise, look up the correct text in 
                        // the table.
                        if (stringTable != null)
                        {
                            value =
                                stringTable.SharedStringTable
                                .ElementAt(int.Parse(value)).InnerText;
                        }
                        break;

                    case CellValues.Boolean:
                        switch (value)
                        {
                            case "0":
                                value = "FALSE";
                                break;
                            default:
                                value = "TRUE";
                                break;
                        }
                        break;
                }
            }
            else
            //Date format keeps as null
            {
                try
                {
                    value = value.Replace(".", ",");
                    value = DateTime.FromOADate(double.Parse(value)).ToString();
                }
                catch
                {
                    return value;
                }

                
            }

            return value;
        }



        #endregion



        #region Helper methods


        public void ClearArchiveSeancesFolder()
        {

            var archive_seances_path = Rim_Export_Seances_FIle_Path[Get_ENV()];
            if (!Directory.Exists(archive_seances_path))
            {
                Directory.CreateDirectory(archive_seances_path);
            }
            else
            {
                try
                {
                    ClearDirectory(Rim_Export_Seances_FIle_Path[Get_ENV()]);
                }
                catch(Exception ex)
                {
                    throw new Exception(String.Format("Неудачная попытка очистить директорию для хранения архивов сеансов {0}. {1}",
                    archive_seances_path, ex.Message));
                }
                
            }


            log.Info(String.Format("Очистили папку архивов сеансов "));

        }

         public void ClearDirectory(string folder)
         {
             // Delete all files and sub-folders?
             // Yep... Let's do this
             var subfolders = Directory.GetDirectories(folder);
             foreach (var s in subfolders)
             {
                 ClearDirectory(s);
             }


             // Get all files of the folder
             var files = Directory.GetFiles(folder);
             foreach (var f in files)
             {
                 // Get the attributes of the file
                 var attr = File.GetAttributes(f);

                 // Is this file marked as 'read-only'?
                 if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                 {
                     // Yes... Remove the 'read-only' attribute, then
                     File.SetAttributes(f, attr ^ FileAttributes.ReadOnly);
                 }

                 // Delete the file
                 File.Delete(f);
             }

             // When we get here, all the files of the folder were
             // already deleted, so we just delete the empty folder
             Directory.Delete(folder);


         }

        #endregion


    }
}
