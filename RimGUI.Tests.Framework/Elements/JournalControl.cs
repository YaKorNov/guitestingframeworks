﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System;
using RimGUI.Tests.Framework.Models;
using RimGUI.Tests.Framework;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems.Finders;

//using TestStack.White.UIItems.Finders;
//using TestStack.White.UIItems.WindowItems;
using System.Windows.Automation;
using RimGUI.Tests.Framework.Models;


namespace RimApp.Tests.Framework.Elements
{
    public class JournalControl : XamlElementContainer
    {

        private White.Core.UIItems.WindowItems.Window _whiteWindow;
        private MainWindow _telerik_wnd;

        #region Element paths

        #endregion

        public JournalControl(VisualFind find, MainWindow telerik_wnd, White.Core.UIItems.WindowItems.Window white_wnd)
            : base(find) 
        {
            _whiteWindow = white_wnd;
            _telerik_wnd = telerik_wnd;
        }


        #region Elements


        private IList<TextBlock> TextBlocks { get { return Find.AllByType<TextBlock>(); } }

        private TextBlock AnchorWordForFilterPanel { get { return TextBlocks.Where(control => control.Text == "за").First(); } }

        private ComboBox JournalVariantCombo { get { return AnchorWordForFilterPanel.AnySibling<ComboBox>(); } }

        private DatePicker BeginTimeDatePicker { get { return Find.ByName("beginTimeDatePicker").As<DatePicker>(); } }

        private DatePicker EndTimeDatePicker { get { return Find.ByName("endTimeDatePicker").As<DatePicker>(); } }


        private FrameworkElement SearchComboBox { get { return Find.ByExpression(new XamlFindExpression("XamlTag=SearchComboBox") ); } }

        private Button LoupeButton { get { return SearchComboBox.Find.ByName<Button>("LoupeButton"); } }


        private Button ClearSearchButton { get { return LoupeButton.AnySibling<Button>(); } }


        private ComboBox SearchFilterVariantCBox { get { return SearchComboBox.AnySibling<ComboBox>(); } }


        private ListBox ExecutorList { get { return SearchComboBox.AnySibling<ListBox>(); } }


        private IList<FrameworkElement> ExecutorsElements { get { return ExecutorList.Find.AllByExpression(new XamlFindExpression("XamlTag=ListBoxItem")); } } 


       
        #endregion


        #region Actions
        

        public void SetJournalVariant(JournalSummaryVariants variant)
        {
            //TODO в Utils
            JournalVariantCombo.Text = Enum.GetName(typeof(JournalSummaryVariants), variant);
            //Thread.Sleep(500);

        }


        public void SetDatesInterval(DateTime? begin_date =null, DateTime? end_date=null)
        {

            BeginTimeDatePicker.Text = begin_date == null ? BeginTimeDatePicker.Text : begin_date.Value.ToString("dd.MM.yyyy");
            BeginTimeDatePicker.User.Click();
            EndTimeDatePicker.Text = end_date == null ? EndTimeDatePicker.Text : end_date.Value.ToString("dd.MM.yyyy");
            EndTimeDatePicker.User.Click();
        }



        public void SetSearchText(string text)
        {
            if (text == "")
            {
                ClearSearchButton.Wait.ForVisible();
                ClearSearchButton.User.Click();

            }
            else
                SearchComboBox.User.TypeText(text, 1);

            //Thread.Sleep(500);

        }


        public void SetSearchFilterVariant(SearchFilterVariant variant)
        {
            SearchFilterVariantCBox.Text = Enum.GetName(typeof(SearchFilterVariant), variant);
            //Thread.Sleep(500);

        }


        public List<string> GetExecutrosList()
        {
            var _lst = new List<string>();

            foreach (var el in ExecutorsElements)
            {
                el.Wait.For(item => item.Children.Count > 0);
                _lst.Add(el.Find.ByType<TextBlock>().Text);
            }

            return _lst;

        }

        //unstable, replace by  white??
        public void SelectExecutor(string executor_name)
        {
            //Thread.Sleep(1000);
            foreach (var el in ExecutorsElements)
            {
                el.Wait.Timeout = 5000;
                el.Wait.For(item => item.Find.AllByType<TextBlock>().Count > 0 );
                

                if (el.Find.ByType<TextBlock>().Text == executor_name)
                {
                    el.User.Click();
                }

            }
        }


        public void SelectExecutor_White(string executor_name)
        {
            var executor_items = _whiteWindow.AutomationElement.FindAll(TreeScope.Children, new PropertyCondition(AutomationElement.NameProperty, "Rim.App.ViewModels.JournalMaterialExecutorViewModel"));

            foreach (AutomationElement executor_item in executor_items)
            {
                var text_block =  executor_item.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Text));

                if (text_block.Current.Name == executor_name)
                    White.Core.InputDevices.Mouse.instance.Click(text_block.GetClickablePoint());
            }

        }


        //TODO выделить код для поиска элементов грида в общий метод
        public List<string> GetJournalGridColumnHeadersWhite()
        {

            var _lst = new List<string>();

            //
            // ClassName:	"ItemsControlBase"
            //ControlType:	"ControlType.Pane"
            //AutomationId:	"GridControl"
            White.Core.UIItems.IUIItem grid_header = _whiteWindow.Get(SearchCriteria.ByAutomationId("GridControl").AndControlType(ControlType.Pane));

            var columns = grid_header.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.HeaderItem));

            foreach (AutomationElement _col in columns)
            {
                AutomationElement text_ctrl = _col.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Text));

                try
                {
                    _lst.Add(text_ctrl.Current.Name);
                }
                catch
                {

                }

            }

            return _lst;
        }



        public List<JournalDataGridString> GetJournalGridCells()
        {
            _whiteWindow.ReInitialize(White.Core.Factory.InitializeOption.NoCache);
            //максимально возможный таймаут
            //Thread.Sleep(3000);
            List<string> column_headers = GetJournalGridColumnHeadersWhite();

            var _lst = new List<JournalDataGridString>();

            White.Core.UIItems.IUIItem dp = _whiteWindow.Get(SearchCriteria.ByAutomationId("dataPresenter"));

            var rows = dp.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.DataItem));

            foreach (AutomationElement row in rows)
            {
                AutomationElementCollection cells = row.FindAll(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Custom));

                int index = 0;
                var _s_info = new JournalDataGridString();
                foreach (AutomationElement cell in cells)
                {
                    //map columns names and fields values
                    //try на случай того если все колонки не входят в видимую область
                    try
                    {
                        string grid_column_name = column_headers[index];
                        string cell_value = ((ValuePattern)cell.GetCurrentPattern(ValuePattern.Pattern)).Current.Value;
                        Utils.SetFieldValueByDesc(_s_info, grid_column_name, cell_value);
                    }
                    catch
                    {

                    }


                    index++;
                }

                _lst.Add(_s_info);

            }
            return _lst;


        }


        public void OpenContextMenuOnJournalString(string task_shifr, string summary_number)
        {
            var cells = GetJournalGridCells();
            var founded_cell = cells.Where(cell => cell.Type == "Сводка" && cell.OTM == task_shifr && cell.Number == summary_number).FirstOrDefault();

            if (founded_cell !=null)
            {
                //позиционируемся на строке по ее индексу в коллекции
                int founded_row_index = cells.IndexOf(founded_cell);

                White.Core.UIItems.IUIItem dp = _whiteWindow.Get(SearchCriteria.ByAutomationId("dataPresenter"));

                var rows = dp.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.DataItem));

                ((SelectionItemPattern)rows[founded_row_index].GetCurrentPattern(SelectionItemPattern.Pattern)).Select();

                White.Core.InputDevices.Mouse.instance.RightClick(rows[founded_row_index].GetClickablePoint());

            }
                

        }

    
        //один тест - один вызов, т.к. иначе не видит элементов контекстного меню
        public void ContextMenuClickCommand(SummaryJournalContextMenuCommand command)
        {
            //Thread.Sleep(1000);

            White.Core.UIItems.IUIItem[] menu_items = _whiteWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.Menu));

            foreach (var item in menu_items)
            {
                if (item.AutomationElement.Current.ClassName == "GridPopupMenu")
                {
                    //      Identification
                    //ClassName:	"BarButtonItemLinkControl"
                    //ControlType:	"ControlType.Button"
                    //Culture:	"(null)"
                    //AutomationId:	"BarButtonItemLink_rePrintSummaryBarItem"
                    //LocalizedControlType:	"кнопка"
                    //Name:	"Перепечатать"
                    AutomationElement command_button = item.AutomationElement.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.NameProperty, Utils.GetEnumDescription(command)));
                    command_button.SetFocus();
                    White.Core.InputDevices.Mouse.instance.Click(command_button.GetClickablePoint());
                    //White.Core.InputDevices.Mouse.instance.Location = new System.Windows.Point(White.Core.InputDevices.Mouse.instance.Location.X + 10,
                    //    White.Core.InputDevices.Mouse.instance.Location.Y - 10);
                    //вызов InvokePattern raisit bug
                    //((InvokePattern)command_button.GetCurrentPattern(InvokePattern.Pattern)).Invoke();
                    
                    //Увеличил таймаут (не успевало открыться окно с выбром даты)
                    Thread.Sleep(2000);

                    if (command == RimGUI.Tests.Framework.Models.SummaryJournalContextMenuCommand.Reprint)
                    {
                        //время на переключение табов
                        Thread.Sleep(1000);
                        _telerik_wnd._journal_tab_active = false;
                        _telerik_wnd._summary_tab_active = true;
                    }
                        
                }
            }

        }


       

        #endregion


    }
}





