﻿using ArtOfTest.WebAii.Wpf;
using White.Core;
using White.Core.UIItems.WindowItems;
using log4net;


namespace RimApp.Tests.Framework.Elements
{
    //////////////////////////////////RIM Tested Versions///////////////////////////////////////////////////////

    //1.8.0.449
    //1.8.0.852
    //1.8.0.965
    //1.8.0.1062
    //1.8.186.1081
    //1.8.200.1103

    //////////////////////////////////RIM Tested Versions///////////////////////////////////////////////////////

    public class App
    {

        private MainWindow _mainWnd;
        private AuthWindow _authWnd;
        private SettingsWindow _settingsWnd;
        private PrintPreviewWindow _printPreviewWnd;
        private ILog _log;

        public WpfApplication ApplicationWebAii { get; private set; }
        public Application ApplicationWhite { get; private set; }

        public App(WpfApplication webAiiApp, Application whiteApp, ILog log)
        {
            ApplicationWebAii = webAiiApp;
            ApplicationWhite = whiteApp;
            _log = log;
        }

        #region Windows

        #region WPF Windows
        public MainWindow MainWindow { get { return _mainWnd = _mainWnd ?? new MainWindow(ApplicationWebAii.WaitForWindow(MainWindow.WINDOW_NAME, 15000).Find, GetWindowByName(MainWindow.WINDOW_NAME), _log); } }

        public AuthWindow AuthWindow { get { return _authWnd = _authWnd ?? new AuthWindow(ApplicationWebAii.WaitForWindow(AuthWindow.WINDOW_NAME, 15000).Find, GetWindowByName(AuthWindow.WINDOW_NAME)); } }

        public TaskInfoCardWindow TaskInfoCardWindow { get { return new TaskInfoCardWindow(ApplicationWebAii.WaitForWindow(TaskInfoCardWindow.WINDOW_NAME, 10000).Find); } }

        public PrintPreviewWindow PrintPreviewWindow { get { return _printPreviewWnd = _printPreviewWnd ?? new PrintPreviewWindow(ApplicationWebAii.WaitForWindow(PrintPreviewWindow.WINDOW_NAME, 10000).Find, GetWindowByName(PrintPreviewWindow.WINDOW_NAME)); } }

        public InputBoxWindow InputBoxWindow { get { return new InputBoxWindow(ApplicationWebAii.WaitForWindow(InputBoxWindow.WINDOW_NAME, 10000).Find); } }

        public InputBoxWindow ArchiveInputBoxWindow { get { return new InputBoxWindow(ApplicationWebAii.WaitForWindow(InputBoxWindow.ARCHIVE_WINDOW_NAME, 10000).Find); } }

        public SummaryProvidedWindow SummaryProvidedWindow { get { return new SummaryProvidedWindow(ApplicationWebAii.WaitForWindow(SummaryProvidedWindow.WINDOW_NAME, 10000).Find); } }

        public SettingsWindow SettingsWindow 
        { 
            get 
            {
                return _settingsWnd = _settingsWnd ?? new SettingsWindow(ApplicationWebAii.WaitForWindow(SettingsWindow.WINDOW_NAME, 10000).Find, GetWindowByName(SettingsWindow.WINDOW_NAME), this); 
            }
            set
            {
                _settingsWnd = value;
            }
        }

        public AddNewTaskShifrWindow AddNewTaskShifrWindow { get { return new AddNewTaskShifrWindow(ApplicationWebAii.WaitForWindow(AddNewTaskShifrWindow.WINDOW_NAME, 10000).Find); } }


        public ExportSettingsWindow ExportSettingsWindow { get { return new ExportSettingsWindow(ApplicationWebAii.WaitForWindow(ExportSettingsWindow.WINDOW_NAME, 10000).Find, GetWindowByName(ExportSettingsWindow.WINDOW_NAME), _log); } }


        public ExportToExcelSettingsWindow ExportToExcelSettingsWindow { get { return new ExportToExcelSettingsWindow(ApplicationWebAii.WaitForWindow(ExportToExcelSettingsWindow.WINDOW_NAME, 10000).Find, _log); } }



        public EditingTemplateWindow AddTemplateWindow
        {
            get
            {
                return new EditingTemplateWindow(ApplicationWebAii.WaitForWindow(EditingTemplateWindow.ADD_WINDOW_NAME, 10000).Find);
            }
        }

        public EditingTemplateWindow EditingTemplateWindow
        {
            get
            {
                return new EditingTemplateWindow(ApplicationWebAii.WaitForWindow(EditingTemplateWindow.EDIT_WINDOW_NAME, 10000).Find);
            }
        }


        public FileNameTemplateWindow FileNameTemplateWindow
        {
            get
            {
                return new FileNameTemplateWindow(ApplicationWebAii.WaitForWindow(FileNameTemplateWindow.WINDOW_NAME, 10000).Find);
            }
        }


        #endregion


        #region Win32 Windows
        public AccompanyingDocumentPrintPreviewWindow AccompanyingDocumentPrintPreviewWindow { get { return new AccompanyingDocumentPrintPreviewWindow(GetWindowByName("Просмотр")); } }

        public AccompanyingDocumentPrintWindow AccompanyingDocumentPrintWindow { get { return new AccompanyingDocumentPrintWindow(GetWindowByName("Печать")); } }

        public SummaryPrintWindow SummaryPrintWindow { get { return new SummaryPrintWindow(GetWindowByName("Печать")); } }

        public ErrorMessageWindow ErrorMessageWindow { get { return new ErrorMessageWindow(GetWindowByAutomationId("ErrorMessageWindowWithDetails")); } }


        public SummaryCreationDialogWindow SummaryCreationDialogWindow { get { return new SummaryCreationDialogWindow(GetWindowByName("Создание сводки")); } }

        public YesNoDialogWindow YesNoDialogWindow 
        { 
            get 
            {
                var wnd = GetWindowByName("Редактор итоговых материалов");
                wnd = wnd ??  GetWindowByName("Удалить шаблон документа");
                return new YesNoDialogWindow(wnd); 
            } 
        }
        #endregion


        #endregion

        #region Private
        public Window GetWindowByName(string windowName)
        {
            // Workaround as method app.GetWindow("Open") is not working
            foreach (Window window in ApplicationWhite.GetWindows())
            {
                if (windowName.Equals(window.Name))
                {
                    return window;
                }
            }
            return null;
        }

        public Window GetWindowByAutomationId(string id)
        {
            foreach (Window window in ApplicationWhite.GetWindows())
            {
                if (window.AutomationElement.Current.AutomationId == id)
                {
                    return window;
                }
            }
            return null;
        }


        public void GetWindow()
        {

        }


        #endregion
    }
}
