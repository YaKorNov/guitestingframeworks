﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using RimGUI.Tests.Framework.Models;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using log4net;



namespace RimApp.Tests.Framework.Elements
{
    public class MainWindow : XamlElementContainer
    {
        private White.Core.UIItems.WindowItems.Window _whiteWindow;
        private ILog _log;

        //определяют активность табов в VisualTree 
        public bool _journal_tab_active = false;
        public bool _summary_tab_active = false;
        public bool _archive_tab_active = false;

        #region Element paths
        //TODO заголовок окна меняется в зависимости от юзера
        public static string WINDOW_NAME = "Редактор итоговых материалов - Admin";
        private static string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]/TabControl[0]/Grid[0]";
        private static string tabControlPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]/TabControl[0]";
        private static string settingsButtonPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]/StackPanel[0]";
        
        //private static string summaryControlPath = mainPath + "/Border[0]/ContentPresenter[0]/SummaryControl[0]/Border[0]/ContentPresenter[0]/Grid[0]";

        #region Tabs
        private static string TabPanel = mainPath + "/Border[1]/TabPanel[0]";
        private static string journalTab = "Name=journalTab";
        private static string summaryTab = "Name=summaryTab";
        private static string informationalArchiveTab = "Name=informationalArchiveTab";


        #endregion

        #region Controls
        private static string journalControl = "XamlTag=JournalControl";
        private static string summaryControl = "XamlTag=SummaryControl";
        private static string informationalArchiveControl = "XamlTag=InformationalArchiveControl";

        #endregion



        #endregion

        public MainWindow(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd, ILog log) : base(find) 
        {
            _whiteWindow = white_wnd;
            _log = log;
        }


        #region Elements
        private TabPanel Tab_Panel { get { return Get<TabPanel>(TabPanel); } }

        private TabItem First_Tab { get { return Get<TabItem>(TabPanel, "|", journalTab); } }

        private TabItem Second_Tab { get { return Get<TabItem>(TabPanel, "|", summaryTab); } }

        private TabItem Third_Tab { get { return Get<TabItem>(TabPanel, "|", informationalArchiveTab); } }



        public JournalControl JournalControl
        {
            get
            {
                if (!_journal_tab_active)
                {
                    Click1TabControl();
                    _journal_tab_active = true;
                    _summary_tab_active = false;
                    _archive_tab_active = false;
                }

                return new JournalControl(Get(mainPath, "|", journalControl).Find, this, _whiteWindow);
            }

        }


        public SummaryControl SummaryControl
        { 
            get 
            {
                if (!_summary_tab_active)
                {
                    Click2TabControl();
                    _summary_tab_active = true;
                    _journal_tab_active = false;
                    _archive_tab_active = false;
                }
                
                return new SummaryControl(Get(mainPath, "|", summaryControl).Find, _whiteWindow); 
            } 
        
        }


        public InformationalArchiveControl InformationalArchiveControl
        {
            get
            {
                if (!_archive_tab_active)
                {
                    Click3TabControl();
                    _archive_tab_active = true;
                    _summary_tab_active = false;
                    _journal_tab_active = false;
                }

                return new InformationalArchiveControl(Get(mainPath, "|", informationalArchiveControl).Find, _whiteWindow, _log);
            }

        }


        private FrameworkElement ButtonsPanel {
            get { return Get(new XamlFindExpression(settingsButtonPath)); } 
            //get { return TabControl.GetNextSibling(); }
        }

        private FrameworkElement TabControl { get { return Get(new XamlFindExpression(tabControlPath)); } }


        private IList<Button> ButtonsOnPanel { get { return ButtonsPanel.Find.AllByType<Button>(); } }




        #endregion

        #region Actions
        public void Click1TabControl()
        {
            First_Tab.User.Click();
            Thread.Sleep(1000);
            _log.Info("Test step. Клик по табу ЖИМ ");
        }

        public void Click2TabControl()
        {
            Second_Tab.User.Click();
            Thread.Sleep(1000);
            _log.Info("Test step. Клик по табу сводок ");
        }

        public void Click3TabControl()
        {
            Third_Tab.User.Click();
            Thread.Sleep(1000);
            _log.Info("Test step. Клик по табу архивов ");
        }

        //предполагается, что диалоговое окно открывается с активной кнопкой "Ок"
        //public void ClickOkDialogWindow()
        //{
        //    _whiteWindow.Keyboard.PressSpecialKey(White.Core.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
        //    Thread.Sleep(1000);        
        //}


        public void ExpandWindow()
        {
            AutomationElement command_button = _whiteWindow.AutomationElement.FindFirst(TreeScope.Descendants, new PropertyCondition(AutomationElement.AutomationIdProperty, "Maximize"));
            White.Core.InputDevices.Mouse.instance.Click(command_button.GetClickablePoint());
            _log.Info("Test step. Окно развернуто");
        }

        public void ClickSettingsButton()
        {
            //unstable open settings window
            Thread.Sleep(2000);
            ButtonsOnPanel[2].User.Click();
            _log.Info("Test step. Клик по кнопке настроек ");
        }

        public void ClickAboutButton()
        {
            ButtonsOnPanel[1].User.Click();
            
        }


        public void Focus()
        {
            _whiteWindow.Focus();
        }




        #endregion
    }
}
