﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System;
using RimGUI.Tests.Framework.Models;
using RimGUI.Tests.Framework;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;



namespace RimApp.Tests.Framework.Elements
{
    public class SummaryControl : XamlElementContainer
    {
        //стоит заменить на таб, вместо целого окна?
        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        //располагаем контролы сверху вниз в том порядке, в котром они расположены в XAML-дереве
        #region Element paths

        #region TasksControl


        private static string[] tasksControl = new string[] {"XamlTag=TasksControl",  "|", "XamlTag=Grid"};
        private static string[] search_text_box = new string[] { "|", "XamlTag=SearchComboBox" };
        //TODO почему - то по такому птуи найти не получается, возможно потому что ComboBox кастомный
        private static string[] task_status_path = new string[]
            {
                "|", "XamlTag=ComboBox"
            };
        
        private static string taskControl = "XamlTag=TaskControl";

        private static string CheckBox = "XamlTag=CheckBox";

        #endregion


        #region Summary preview and filter

        private static string[] summaryFilterControl = new string[] { "XamlTag=SummaryFilterControl" };

        private static string[] specialSummaryModelPanelControl = new string[] {"XamlTag=SpecialSummaryModelPanelControl" };

        private static string[] toggleButton = new string[] { "|", "XamlTag=ToggleButton" };

        private static string[] popupControlPath = new string[] { "|", "Name=summaryFilterPopup" };

        private static string[] summaryFilterControlCheckbox = new string[] { "|", "XamlTag=Checkbox" };

        private static string[] seancesListBox = new string[] { "Name=seancesListBox" };
        
        
        #endregion


        private static string[] comboboxes_path = new string[]
            {
                "XamlTag=ComboBox"
            };

        #endregion

        public SummaryControl(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd) : base(find) 
        {
            _whiteWindow = white_wnd;
        }


        #region Elements

        private FrameworkElement TasksControl { get { return Get(tasksControl);  }}

        private FrameworkElement SearchTextBox { get { return Get(Utils.ConcatArrays<string>(tasksControl, search_text_box)); } }


        private Grid TaskTypeGrid { get { return SearchTextBox.AnySibling<Grid>(); } }

        private TextBlock TaskTypeTextBlock { get { return TaskTypeGrid.Find.AllByType<TextBlock>()[0]; } }

        private Button LoupeButton { get { return SearchTextBox.Find.ByName<Button>("LoupeButton"); } }


        private Button ClearSearchButton { get { return LoupeButton.AnySibling<Button>(); } }



        private FrameworkElement TaskStatus { get { return Get(Utils.ConcatArrays<string>(tasksControl, comboboxes_path)); } }


        private IList<ComboBox> ComboBoxes
        {
            get
            {
                return Find.AllByExpression(new XamlFindExpression(comboboxes_path)).Select(x => x.As<ComboBox>()).ToList();

            }
        }


        private FrameworkElement ComboBoxButtonControl
        {
            get
            {
                return Find.ByName("comboBoxButtonControl");
            }
        }

        private Button SummaryPreviewButton
        {
            get
            {
                return ComboBoxButtonControl.AnySibling<Button>();
            }
        }


        private ComboBox PrintComboBox
        {
            get
            {
                //return Find.AllByExpression(new XamlFindExpression(comboboxes_path)).Select(x => x.As<ComboBox>()).ToList();
                //return ComboBoxes.Select(x => x.Name == "printComboBox").FirstOrDefault();
                //Thread.Sleep()
                //Find.WaitOnElementsTimeout = 20000;
                return Find.ByName<ComboBox>("printComboBox");
            }
        }

        private FrameworkElement PrintComboBoxName
        {
            get
            {
                //return Find.AllByExpression(new XamlFindExpression(comboboxes_path)).Select(x => x.As<ComboBox>()).ToList();
                //return ComboBoxes.Select(x => x.Name == "printComboBox").FirstOrDefault();
                return PrintComboBox.Find.ByExpression(new XamlFindExpression("XamlTag=Grid", "|", "XamlTag=Grid", "|", "XamlTag=TextBlock"));
            }
        }

        private ToggleButton PrintComboBoxToggle
        {
            get
            {
                //return Find.AllByExpression(new XamlFindExpression(comboboxes_path)).Select(x => x.As<ComboBox>()).ToList();
                //return ComboBoxes.Select(x => x.Name == "printComboBox").FirstOrDefault();
                return PrintComboBox.Find.ByExpression(new XamlFindExpression("XamlTag=Grid", "|", "XamlTag=ToggleButton")).As<ToggleButton>();
            }
        }

        private Button PrintComboBoxButton
        {
            get
            {
                //return Find.AllByExpression(new XamlFindExpression(comboboxes_path)).Select(x => x.As<ComboBox>()).ToList();
                //return ComboBoxes.Select(x => x.Name == "printComboBox").FirstOrDefault();
                return PrintComboBox.AnySibling<Button>();
            }
        }

        


        private IList<FrameworkElement> TasksItems
        {
            get
            {
                //TODO 
                //1.достаточен ли таймаут для загрузки элементов?
                //2.Добавить кэширование, т.к. список может быть огромным?
                //Thread.Sleep(1000);
                return Find.AllByExpression(new XamlFindExpression(taskControl));

            }
        }


        private FrameworkElement SummaryFilter
        {
            get
            {
                return Find.ByExpression(new XamlFindExpression(summaryFilterControl));
            }
        }



        private FrameworkElement SpecialSummaryModelPanelControl
        {
            get
            {
                //Find.Strategy = FindStrategy.WhenNotVisibleReturnElementProxy;
                //return Find.ByExpression(new XamlFindExpression("XamlPath=Border[0]/ContentPresenter[0]/Grid[0]/Border[0]/Grid[0]/Grid[0]/SpecialSummaryModelPanelControl[0]"));
                
                return Find.ByExpression(new XamlFindExpression("XamlTag=SpecialSummaryModelPanelControl"));
            
            }
        }


        private FrameworkElement SummaryFilterPopup
        {
            get
            {
                return Find.ByExpression(new XamlFindExpression(Utils.ConcatArrays<string>(summaryFilterControl, popupControlPath)));
            }
        }


        private FrameworkElement SummaryFilterToggleButton
        {
            get
            {
                return Find.ByExpression(new XamlFindExpression(Utils.ConcatArrays<string>(summaryFilterControl, toggleButton)));
            }
        }



        private CheckBox SummaryFilterControlCheckbox
        {
            get
            {
                return Find.ByExpression(new XamlFindExpression(Utils.ConcatArrays<string>(summaryFilterControl, popupControlPath, summaryFilterControlCheckbox))).As<CheckBox>();
            }
        }



        //Changelist
        //v. 1.8.0.449.
        //v. 1.8.0.852.
        //      1.Убрали AutomationId с ListBox
        //      попытка сделать более универсальный поиск
        //      либо надо требовать возврата  AutomationId  
        private White.Core.UIItems.ListBoxItems.ListBox SummaryPreviewList
        {
            get
            {
                //return _whiteWindow.Get<White.Core.UIItems.ListBoxItems.ListBox>(SearchCriteria.ByControlType(ControlType.List));

                var list = _whiteWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.List))[1];
                return (White.Core.UIItems.ListBoxItems.ListBox)list;
            }
        }


        private IList<White.Core.UIItems.ListBoxItems.ListItem> SeancesInfoList
        {
            get
            {
                
                //             ClassName:	"ListBoxItem"
                //ControlType:	"ControlType.ListItem"
                //Culture:	"(null)"
                //AutomationId:	""
                //LocalizedControlType:	"элемент списка"
                //Name:	"Rim.App.ViewModels.StenogramViewModel"
                //return SummaryPreviewList.Items;

                return SummaryPreviewList.Items;

               // List<White.Core.UIItems.UIItem> items = new List<White.Core.UIItems.UIItem>();

               //foreach (var control in _whiteWindow.GetMultiple(SearchCriteria.All))
               //{
               //    if (control.AutomationElement.Current.Name == "Rim.App.ViewModels.StenogramViewModel")
               //    {
               //        items.Add((White.Core.UIItems.UIItem)control);
               //    }
               //}

               //return items;

             }
        }


        private FrameworkElement BusyIndicator { get { return Find.ByExpression(new XamlFindExpression("XamlTag=BusyIndicator")); } }


        #endregion


        #region Actions
        

        public void SetSearchText(string text)
        {
            if (text == "" && SearchTextBox.Text != "")
            {
                ClearSearchButton.Wait.ForVisible();
                ClearSearchButton.User.Click();

            }
            else
                SearchTextBox.User.TypeText(text, 1);

            Thread.Sleep(500);

        }


        public void ClearSearchText()
        {
            ClearSearchButton.Wait.ForVisible();
            ClearSearchButton.User.Click();
            Thread.Sleep(500);
        }



        public void SetTaskStatusType(TaskStatus status)
        {

            //Changelist
            //v. 1.8.0.449. В качестве контрола был дефолтный комбобокс
            //v. 1.8.0.852. 
            //      В качестве контрола стал текстовый блок 

            //ComboBoxes[1].Text =  Enum.GetName(typeof(TaskStatus), status);
            TaskTypeTextBlock.Text = Utils.GetEnumDescription(status);
            Thread.Sleep(500);
        }

        public List<TaskInfoShort> GetSummaryTabTasksInfo(int? previous_tasks_count=null, int waitTimeout = 20000)
        {

            //Changelist
            //v. 1.8.0.449. 
            //v. 1.8.0.852. 
            //     Убрали AutomationId для  некоторых текстовых блоков

            
            Thread.Sleep(1000);

            if (previous_tasks_count.HasValue)
            {
                TasksControl.Wait.Timeout = waitTimeout;
                TasksControl.Wait.For(control => TasksItems.Count != previous_tasks_count.Value);
            }
                
            var st_info = new List<TaskInfoShort>() { };
            foreach (var _t_item in TasksItems)
            {
                
                //st_info.Add(
                //    new TaskInfoShort()
                //    {       
                //        Shifr =  _t_item.Find.ByName("cipherTextBlock").As<TextBlock>().Text,
                //        Name =  _t_item.Find.ByName("aliasTextBlock").As<TextBlock>().Text,
                //        Init = _t_item.Find.ByName("InitiatorInfoTextBlock").As<TextBlock>().Text,
                //        SeansesCount = _t_item.Find.ByName("selectedSeancesCountTextBlock").As<TextBlock>().Text,
                //        SrokInfo =  _t_item.Find.ByName("lastSummaryDateTextBlock").As<TextBlock>().Text
                    
                //    });

               var text_blocks = _t_item.Find.AllByType<TextBlock>();

               st_info.Add(
                   new TaskInfoShort()
                   {
                       Name = text_blocks[0].Text,
                       Shifr = text_blocks[1].Text,
                       SrokInfo = text_blocks[2].Text, 
                       Init = text_blocks[3].Text,
                       SeansesCount = text_blocks[4].Text

                   });

                //st_info.Add(string.Join(",", _t_item.Find.AllByType("TextBlock").Select(x => x.As<TextBlock>().Text).ToList()));
            }

            return st_info;
        }


        public void SetTaskCheckBox(string task_name, bool set_flag)
        {

            foreach (var _t_item in TasksItems)
            {
                if (_t_item.Find.ByName("aliasTextBlock").As<TextBlock>().Text == task_name)
                {
                   var _ch_box = _t_item.Find.ByExpression(new XamlFindExpression(CheckBox)).As<CheckBox>();
                   if (set_flag == true && !_ch_box.IsChecked.Value)
                       _ch_box.Toggle();
                   else if (set_flag == false && _ch_box.IsChecked.Value)
                       _ch_box.Toggle();
                }
            }

        }


        public void SelectTaskFromList(string task_name)
        {

            foreach (var _t_item in TasksItems)
            {
                if (_t_item.Find.ByName("aliasTextBlock").As<TextBlock>().Text == task_name)
                {
                    _t_item.User.Click();
                    //индикатор загрузки сводки
                    //большой таймаут, предполагаем, что индикатор загрузки должен появиться
                    Thread.Sleep(2000);
                    //BusyIndicator.Wait.ForVisible();
                    //данные загрузятся, и он исчезнет
                    BusyIndicator.Wait.ForVisibleNot();
                }
            }

        }


        public void OpenTaskPropertiesByDoubleClick(string task_name, bool isdoubleClick)
        {
            //TODO обработчик на контекстное меню
            foreach (var _t_item in TasksItems)
            {
                if (_t_item.Find.ByName("aliasTextBlock").As<TextBlock>().Text == task_name)
                {
                    _t_item.User.Click(isdoubleClick == true ? 
                        ArtOfTest.WebAii.Core.MouseClickType.LeftDoubleClick
                        : ArtOfTest.WebAii.Core.MouseClickType.RightClick);
                   
                }
            }

        }



        public void ToggleSummaryFilter()
        {
            //Thread.Sleep(2000);
            //SummaryFilterToggleButton.SetFocus();
            ////50 мсек - оптимальное время удержания клавиши, чтобы не ивент не зарайзился еще раз
            //SummaryFilterToggleButton.KeyPress(System.Windows.Forms.Keys.Enter, 50, 500, 1);

            var toggle_button = _whiteWindow.Get(SearchCriteria.ByAutomationId("summaryFilterButton")).AutomationElement;
            ((TogglePattern)toggle_button.GetCurrentPattern(TogglePattern.Pattern)).Toggle();
            Thread.Sleep(1000);

        }

        //telerik не отображает элмент в VisualTree
        //public void SetIncludeEarlierSeances(bool is_include)
        //{
        //    SummaryFilterPopup.User.Click();
        //    if (is_include)
        //        SummaryFilterControlCheckbox.Check(true);
        //    else
        //        SummaryFilterControlCheckbox.Check(false);
        //}


        public void SetPrintVariant()
        {
            PrintComboBox.OpenDropDown(true);
        }

        public void EnterPrintCommand()
        {
            PrintComboBoxButton.Wait.For(x => ((Button)x).IsEnabled);
            PrintComboBoxButton.User.Click();
            //Unstable behaviour, add timeout  
            Thread.Sleep(2000);
        }

        public void PrintToggleCommand()
        {
            PrintComboBoxToggle.Wait.For(x => ((ToggleButton)x).IsEnabled);
            PrintComboBoxToggle.Toggle();
        }

        //TODO добиться возможности выбора варианта команды печати
        public void selectPrintCommand()
        {
            //PrintComboBoxName.Wait.For(x => ((TextBlock)x).IsEnabled);
            var name = PrintComboBoxName.Text;

        }
        

        public void EnterSummaryPreviewCommand()
        {
            SummaryPreviewButton.Wait.For(x => ((Button)x).IsEnabled);
            //бага в РИМе если не дать загрузиться сводке
            #region bug
//            System.NullReferenceException: Ссылка на объект не указывает на экземпляр объекта.
//   в Rim.App.ViewModels.SummaryContainerViewModel.CreateSummaryInitialData(ITaskExtInfo task, IUserObject user, UserSignature userFields, Int32 documentNumber, Nullable`1 filterStart, Nullable`1 filterEnd)
//   в Rim.App.ViewModels.SummaryContainerViewModel.<>c__DisplayClass7d.<RefreshSummary>b__73()
//   в System.Threading.Tasks.Task`1.InnerInvoke()
//   в System.Threading.Tasks.Task.Execute()
//--- Конец трассировка стека из предыдущего расположения, где возникло исключение ---
//   в System.Runtime.CompilerServices.TaskAwaiter.ThrowForNonSuccess(Task task)
//   в System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task)
//   в Rim.App.ViewModels.SummaryContainerViewModel.<RefreshSummary>d__7f.MoveNext()
//--- Конец трассировка стека из предыдущего расположения, где возникло исключение ---
//   в System.Runtime.CompilerServices.TaskAwaiter.ThrowForNonSuccess(Task task)
//   в System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task)
//   в Rim.App.ViewModels.SummaryContainerViewModel.<<RefreshSummaryWithInvoke>b__5c>d__60.MoveNext()
//--- Конец трассировка стека из предыдущего расположения, где возникло исключение ---
//   в System.Runtime.CompilerServices.AsyncMethodBuilderCore.<ThrowAsync>b__4(Object state)
//   в System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)
//   в MS.Internal.Threading.ExceptionFilterHelper.TryCatchWhen(Object source, Delegate method, Object args, Int32 numArgs, Delegate catchHandler)
            #endregion
            Thread.Sleep(5000);
            SummaryPreviewButton.User.Click();
        }
        

        public string GetSummaryReprintText_Telerik()
        {
            List<string> sum_text = new List<string>();
            SpecialSummaryModelPanelControl.Wait.ForExists();
            foreach (var t_block in SpecialSummaryModelPanelControl.Find.AllByType<TextBlock>())
            {
                sum_text.Add(t_block.Text);
            }
            return string.Join(" ", sum_text);


        }

        
        public bool FindSummaryReprintText()
        {

            var text_elems = _whiteWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.Text));

            foreach (var elem in text_elems)
            {
                if (elem.AutomationElement.Current.Name == "Повторная печать сводки №")
                {
                    return true;
                }
                
            }

            return false;

        }



        //все, что не смог найти telerik, используем White 

        #region SummaryFilter with White

        public void SetIncludeEarlierSeances(bool is_include)
        {

            var ch_boxes = _whiteWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.CheckBox)).Select(x => (White.Core.UIItems.CheckBox)x);

            foreach (var _ch_box in ch_boxes)
            {
                if (_ch_box.NameMatches("Включать сеансы, попадавшие в сводки"))
                {
                    if (is_include && !_ch_box.Checked)
                        //работает только этот метод -Toggle
                        _ch_box.Toggle();
                    if (!is_include && _ch_box.Checked)
                        _ch_box.Toggle();

                    return;
                }

            }

        }

        public void SetSummaryNumber(string number)
        {
            _whiteWindow.Get<White.Core.UIItems.TextBox>(SearchCriteria.ByAutomationId("summaryNumber")).Text = number;
            //s_numver.SetFocus();

        }

        /// <summary>
        /// период дат начала сеансов, включаемых в сводку
        /// </summary>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        public void SetPeriod(System.DateTime BeginDate, System.DateTime EndDate)
        {
            var dates_controls = _whiteWindow.GetMultiple(SearchCriteria.ByAutomationId("PART_Editor")).Select(x => (White.Core.UIItems.TextBox)x).ToList();
            //только так, иначе контрол теряет фокус, и посик идет черт занет где
            dates_controls[0].Click();
            dates_controls[0].Text = BeginDate.ToString();
            dates_controls[1].Click();
            dates_controls[1].Text = EndDate.ToString();
            //dates_controls[1].Text = EndDate.ToString();


        }

        //Changelist
        //v. 1.8.0.449.
        //v. 1.8.0.852.
        //      1.добавлено текстовое поле "Повторная печать", индексы съехали. Временное решение - поменять индексы,
        //      Более универсальным решением бы было спарсить весь текстовый блок целиком, и потом 
        //      производить поиск и сравнение его содержимого через RegExpы.  
        private List<SummaryPreviewInfo>  GetSummaryPrewiewOneChunk()
        {
            var _sp_infos = new List<SummaryPreviewInfo>() { };

            foreach (var _s_item in SeancesInfoList)
            {
                var _sp_info = new SummaryPreviewInfo();

                var _desc_elem = _s_item.AutomationElement.FindAll(TreeScope.Descendants, Condition.TrueCondition);
                var _text_content = new List<string>();

                foreach (AutomationElement _el in _desc_elem)
                {

                    if (_el.Current.ControlType == ControlType.Text)
                    {
                        var _text = _el.Current.Name;
                        _text_content.Add(_text);
                    }

                }

                _sp_info.SeanceDateTime = _text_content[0];
                _sp_info.TalkSMSInterlocutorInfo = _text_content[2];
                _sp_info.TalkSMSVideoStenoOrPlaceInfo = _text_content[3];

                _sp_infos.Add(_sp_info);

            }

            return _sp_infos;
        }


        public List<SummaryPreviewInfo> GetSummaryPrewiewInfos()
        {
            //TODO timeout??
            Thread.Sleep(1000);
            
            //скроллим и считываем данные
            var all_chunks = new List<SummaryPreviewInfo>();

            var prev_chunk = new List<SummaryPreviewInfo>();
            var next_chunk = GetSummaryPrewiewOneChunk();

            //сначала скроллим вверх до упора
            while (next_chunk.OrderBy(chunk => chunk.SeanceDateTime).Except(prev_chunk.OrderBy(chunk => chunk.SeanceDateTime)).ToList().Count > 0)
            {
                prev_chunk = next_chunk;
                try
                {
                    ScrollSummaryPreviewSeancesUp();
                }
                catch
                {
                    //если загружено мало сеансов может отсутствовать скролл
                }
                Thread.Sleep(300);
                next_chunk = GetSummaryPrewiewOneChunk();
            }


            
            //а теперь вниз и считываем данные
            prev_chunk = new List<SummaryPreviewInfo>();
            all_chunks.AddRange(next_chunk);

            while (next_chunk.OrderBy(chunk=>chunk.SeanceDateTime).Except(prev_chunk.OrderBy(chunk=>chunk.SeanceDateTime)).ToList().Count > 0)
            {
                prev_chunk = next_chunk;
                try
                {
                    ScrollSummaryPreviewSeancesDown();
                }
                catch
                {
                    //если загружено мало сеансов может отсутствовать скролл
                }
                Thread.Sleep(300);
                next_chunk = GetSummaryPrewiewOneChunk();
                all_chunks = all_chunks.Union(next_chunk).ToList();
            }

            return all_chunks;
            
        }



        public void ScrollSummaryPreviewSeancesDown()
        {

            //var preview = _whiteWindow.Get(SearchCriteria.ByAutomationId("seancesListBox")).AutomationElement;


            ((ScrollPattern)SummaryPreviewList.AutomationElement.GetCurrentPattern(ScrollPattern.Pattern)).ScrollVertical(ScrollAmount.LargeIncrement);

        }


        public void ScrollSummaryPreviewSeancesUp()
        {

            //var preview = _whiteWindow.Get(SearchCriteria.ByAutomationId("seancesListBox")).AutomationElement;
            ((ScrollPattern)SummaryPreviewList.AutomationElement.GetCurrentPattern(ScrollPattern.Pattern)).ScrollVertical(ScrollAmount.LargeDecrement);
            
        }


        //для теста включения/исключения сеансво из сводки достаточно
        //нескольких элментов, находить какой-то элемент по строке поиска - проблематично
        public void SetPrewiewInfoCheckBox(int _index_in_list)
        {
            //TODO timeout??
            Thread.Sleep(1000);

            //var _sp_infos = new List<SummaryPreviewInfo>() { };

            if (SeancesInfoList.Count - 1 < _index_in_list)
                return;

            var _desc_elem = SeancesInfoList[_index_in_list].AutomationElement.FindAll(TreeScope.Descendants, Condition.TrueCondition);

            foreach (AutomationElement _el in _desc_elem)
            {

                if (_el.Current.ControlType == ControlType.CheckBox)
                {
                    var pattern = _el.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
                    pattern.Toggle();
                }

            }

            //aeOpenDialogEdit = aeOpenDialog.FindFirst(TreeScope.Descendants, new PropertyCondition(AutomationElement.ClassNameProperty, "Edit"));

            //Set the value of the file name/path to the string variable "loadSettingsString"
            //vpOpenDialogEdit = (ValuePattern)aeOpenDialogEdit.GetCurrentPattern(ValuePattern.Pattern); 
            //vpOpenDialogEdit.SetValue(loadSettingsString);

        }


        #endregion


        #endregion
    }
}





