﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;


namespace RimApp.Tests.Framework.Elements
{
    public class InputBoxWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Сохранение набора настроек архивов";
        public static string ARCHIVE_WINDOW_NAME = "Создание набора настроек экспорта файлов";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/border[0]/Grid[0]";
        public InputBoxWindow(VisualFind find) : base(find) { }

        #region Elements


        private TextBox editTextBox { get { return Find.ByName<TextBox>("editTextBox"); } }

        private Button okButton { get { return Find.ByName<Button>("okButton"); } }

        
      
        #endregion

        #region Actions

        public void EnterArchiveSettingsName(string name)
        {
            editTextBox.SetText(false, name, 1, 1, false);
        }

        public void ClickOk()
        {
            okButton.User.Click();
        }

        #endregion
    }
}





