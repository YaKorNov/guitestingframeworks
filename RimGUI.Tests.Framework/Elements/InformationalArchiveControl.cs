﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using RimGUI.Tests.Framework.Models;
using RimGUI.Tests.Framework;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using RimGUI.Tests.Framework.Models;
using White.Core.WindowsAPI;
using log4net;



namespace RimApp.Tests.Framework.Elements
{
    public class InformationalArchiveControl : XamlElementContainer
    {
        private ILog _log;
        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        //располагаем контролы сверху вниз в том порядке, в котром они расположены в XAML-дереве
        #region Element paths

        private static string[] archiveSettingsControl = new string[] { "Name=archiveSettingsControl" };

        private static string[] createPresetButton = new string[] { "|", "Name=createPresetButton" };

        private static string[] presetControl = new string[] {"Name=presetControl" };

        private static string[] tasksControl = new string[] { "XamlTag=TasksControl" };

        private static string[] archiveSeancesControl = new string[] { "XamlTag=ArchiveSeancesControl" };

        private static string[] searchTextBox = new string[] { "|", "XamlTag=SearchComboBox" };


        private static string[] comboboxes_path = new string[]
            {
                "XamlTag=ComboBox"
            };


        private static string taskControl = "XamlTag=TaskControl";

        private static string[] marksThreeStateCheckboxes = new string[] { "|", "XamlTag=ListBox", "|", "XamlTag=ListBoxItem" };
        
     

        #endregion

        public InformationalArchiveControl(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd, ILog log)
            : base(find) 
        {
            _whiteWindow = white_wnd;
            _log = log;
        }


        #region Elements

        #region ArchiveSettingsControl

        private FrameworkElement ArchiveSettingsControl { get { return Get(archiveSettingsControl); } }

        private Button CreatePresetButton { get { return Get(Utils.ConcatArrays<string>(archiveSettingsControl, createPresetButton)).As<Button>(); } }


        private FrameworkElement presetsStackPanel { get { return Find.ByName("presetsStackPanel"); } } 


        private FrameworkElement PresetControl { get { return Get(presetControl); } }


        private FrameworkElement SeanceTypeTextBlock { get { return PresetControl.Find.ByTextContent("Тип сеансов");  } }


        private WrapPanel SeanceTypePanel { get { return SeanceTypeTextBlock.AnySibling<WrapPanel>(); } }

        private IList<CheckBox> SeanceTypeCheckboxes { get { return SeanceTypePanel.Find.AllByType<CheckBox>(); } }


        private FrameworkElement MarksTextBlock { get { return PresetControl.Find.ByTextContent("Метки"); } }

        private FrameworkElement FileExportTextBlock { get { return PresetControl.Find.ByTextContent("Настройки экспорта файлов"); } }

        

        //private ListBox MarksThreeStateListBox { get { return MarksTextBlock.AnySibling<ListBox>(); } }

        private CheckBox OnlyNotIncludedSeances { get { return MarksTextBlock.AnySibling<CheckBox>(); } }


        private ComboBox FileExportComboBox { get { return FileExportTextBlock.AnySibling<ComboBox>(); }  }


        private Button FileExportSettingsButton { get { return FileExportTextBlock.AnySibling<Button>(); } }


        private IList<FrameworkElement> MarksThreeStateCheckBoxes { get { return Find.AllByExpression(new XamlFindExpression("XamlTag = ListBoxItem")); } }


        private CheckBox AdditiveDocumentCheckbox
        {
            get
            {
                //TODO плохо, но т.к. wait не успевает, пока так
                Thread.Sleep(1000);
                foreach (var _ch_box in PresetControl.Find.AllByExpression(new XamlFindExpression("XamlTag=CheckBox")).Select(el => el.As<CheckBox>()))
                {
                    //TODO don't see TextBlock, then try wait for
                    _ch_box.Wait.For(control => control.Find.AllByType<TextBlock>().Count>0);
                    var text_block = _ch_box.Find.ByType<TextBlock>();

                    if (text_block != null && text_block.Text == "Сопроводительный документ")
                    {
                        return _ch_box;
                        //return;
                    }


                }

                return null;
            }
        }


        private ComboBox AdditiveDocumentTemplateComboBox
        {
           get { return AdditiveDocumentCheckbox.AnySibling<ComboBox>(); }
        }


        private Button CreateButton
        {
            get { return PresetControl.Find.ByName<Button>("createOrSavePresetButton"); }
        }

        private Button ApplyButton
        {
            get { return CreateButton.NextSibling<Button>(); }
        }



        private IList<DatePickerTextBox> DatePickers { get { return Find.AllByType<DatePickerTextBox>(); } }


        #endregion


        #region tasksControl

        //TODO отрефакторить код для контролов, выделить общий код для вкладок "Сводки" и "Информационые архивы"
        //касается панели заданий

       
        private FrameworkElement TasksControl { get { return Get(tasksControl); } }

        private FrameworkElement SearchTextBox { get { return Get(Utils.ConcatArrays<string>(tasksControl, searchTextBox)); } }

        private FrameworkElement TaskStatusGrid { get { return SearchTextBox.AnySibling<Grid>(); } }


        private TextBlock TaskStatus { get { return TaskStatusGrid.Find.AllByType<TextBlock>()[0]; } }

        // private ComboBox TaskStatus { get { return TaskStatusGrid.Find.ByType<ComboBox>(); } }


        private Button ReloadStatistic { get { return TaskStatus.AnySibling<Button>(); } }


        private Button LoupeButton { get { return SearchTextBox.Find.ByName<Button>("LoupeButton"); } }


        private Button ClearSearchButton { get { return LoupeButton.AnySibling<Button>(); } }


        private IList<FrameworkElement> TasksItems
        {
            get
            {
                //TODO 
                //1.достаточен ли таймаут для загрузки элементов?
                //2.Добавить кэширование, т.к. список может быть огромным?
                //Thread.Sleep(1000);
                return Find.AllByExpression(new XamlFindExpression(taskControl));

            }
        }


        #endregion


        #region archiveSeancesControl

        private FrameworkElement ArchiveSeancesControl { get { return Get(archiveSeancesControl); } }

        //private IList<FrameworkElement> GridHeaders { get { ArchiveSeancesControl.Wait.For(control => control.Children.Count > 0); return ArchiveSeancesControl.Find.AllByExpression(new XamlFindExpression("XamlTag=DataGridColumnHeader")); } }

        private FrameworkElement HeadersPanel { get { return ArchiveSeancesControl.Find.ByName("headersPanel"); } }

        private FrameworkElement HeaderItemsControl { get { return ArchiveSeancesControl.Find.ByName("headerItemsControl"); } }


        private Grid RowPresenterGrid { get { return ArchiveSeancesControl.Find.ByName<Grid>("rowPresenterGrid"); } }

        private IList<FrameworkElement> CellItemsControls { get { return RowPresenterGrid. Find.AllByExpression(new XamlFindExpression("XamlTag=cellItemsControl")); } }

        private IList<FrameworkElement> PART_DataRowContents { get { return RowPresenterGrid.Find.AllByExpression(new XamlFindExpression("Name=PART_DataRowContent")); } }

        private IList<FrameworkElement> PART_DataRows { get { return RowPresenterGrid.Find.AllByExpression(new XamlFindExpression("Name=PART_DataRow")); } }

        private IList<FrameworkElement> GridRows { get { return RowPresenterGrid.Find.AllByExpression(new XamlFindExpression("XamlTag=GridRow")); } }



        private FrameworkElement ComboxButtonControl2 { get { return Find.ByExpression(new XamlFindExpression("XamlTag=ComboBoxButtonControl2")); } }

        private Button PrintButton { get { return ComboxButtonControl2.Find.ByType<Button>(); } }


        private Button ExportButton { get { return ComboxButtonControl2.AnySibling<Button>(); } }


        private FrameworkElement BusyIndicator { get { return ExportButton.Find.ByName("UserControl"); } }
        


        #endregion




        #endregion


        #region Actions


        #region ArchiveSettingsControl

        public void TogglePresetButton()
        {
            //TODO в Utils
             CreatePresetButton.User.Click();
            Thread.Sleep(500);
            _log.Info("Test step. Клик по кнопке пресетов");
        }



        public void SetArchiveFilterSeanceType(ArchiveSeancesFilterType filter_type)
        {
            SeanceTypeCheckboxes[(int)filter_type].Toggle();
            _log.Info(String.Format("Test step. Отметили чекбокс типа сеанса - {0}", Utils.GetEnumDescription(filter_type)));
        }

        public void CheckArchiveFilterSeanceType(ArchiveSeancesFilterType filter_type)
        {
            if (!SeanceTypeCheckboxes[(int)filter_type].IsChecked.Value)
                SeanceTypeCheckboxes[(int)filter_type].Toggle();
            _log.Info(String.Format("Test step. Отметили чекбокс типа сеанса - {0}", Utils.GetEnumDescription(filter_type)));
        }

        public void UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType filter_type)
        {
            if (SeanceTypeCheckboxes[(int)filter_type].IsChecked.Value)
                SeanceTypeCheckboxes[(int)filter_type].Toggle();
            _log.Info(String.Format("Test step. Сняли отметку с чекбокса типа сеанса - {0}", Utils.GetEnumDescription(filter_type)));
        }


        //данные методы работают такжде как toggle,  что не позволяет 
        //отследить текущее состояние чекбокса
        //public void CheckArchiveFilterSeanceType(ArchiveSeancesFilterType filter_type)
        //{
        //    SeanceTypeCheckboxes[(int)filter_type].Check(true);
        //}

        //public void UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType filter_type)
        //{
        //    SeanceTypeCheckboxes[(int)filter_type].UnCheck(true);
        //}



        private List<AutomationElement> MarkCheckboxesListItems
        {
            get
            {

                List<AutomationElement> list_el = new List<AutomationElement>();
                var presetControl =  _whiteWindow.Get(SearchCriteria.ByAutomationId("presetControl")).AutomationElement;
                //presetControl.SetFocus();
                var _desc_elem = presetControl.FindAll(TreeScope.Descendants, Condition.TrueCondition);
                
                foreach (AutomationElement _el in _desc_elem)
                {

                    if (_el.Current.ControlType == ControlType.ListItem && _el.Current.Name == "Rim.App.ViewModels.MarkViewModel")
                    {
                        list_el.Add(_el);
                    }

                }

                return list_el;
            }
        }


        public void ToggleMarkCheckBoxByName(string name)
        {
            

            foreach (var _item in MarkCheckboxesListItems)
            {
                var elems = _item.FindAll(TreeScope.Descendants, Condition.TrueCondition);

                string mark_name = "";
                foreach (AutomationElement el in elems)
                {
                    if (el.Current.ClassName == "TextBlock")
                        mark_name = el.Current.Name;
                }


                if (mark_name == name)
                {
                    System.Windows.Point p = _item.GetClickablePoint();
                    White.Core.InputDevices.Mouse.instance.Location = p;
                    White.Core.InputDevices.Mouse.instance.Click();
                }

                _log.Info(String.Format("Test step. Кликнули по чекбоксу метки - {0}", name ));

                //var mark_name = "";
                //aeDialogs = aeBot.FindAll(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, "#32770"));
                //foreach (AutomationElement _el in elements)
                //{

                //    if (_el.Current.ControlType == ControlType.Text)
                //    {
                //       if (_el.Current.Name != name)
                //           continue;
                //    }


                //    System.Windows.Point p = _el.GetClickablePoint();
                //    White.Core.InputDevices.Mouse.instance.Location = p;
                //    White.Core.InputDevices.Mouse.instance.Click();

                  

                //}

            }
           
        }


       
        public void CheckNotIncludedSeancesCheckbox()
        {
            if (!OnlyNotIncludedSeances.IsChecked.Value)
                OnlyNotIncludedSeances.Toggle();
            _log.Info(String.Format("Test step. Отметили чекбокс - включая ранне включенные сеансы "));
        }

        public void UnCheckNotIncludedSeancesCheckbox()
        {
            if (OnlyNotIncludedSeances.IsChecked.Value)
                OnlyNotIncludedSeances.Toggle();
            _log.Info(String.Format("Test step. Сняли отметку с чекбокса -  включая ранне включенные сеансы "));
        }

        public void SelectFileExportSetting(string setting_variant)
        {
            ComboBox  _c_box =  FileExportComboBox;

            string prev_step_text  = "";
           
            _c_box.SetFocus();
            _c_box.User.KeyPress(System.Windows.Forms.Keys.Down, 50);

           
            //листаем вверх до упора
            while (true)
            {
                if (prev_step_text == _c_box.Text && _c_box.Text != "")
                    break;
                prev_step_text = _c_box.Text;
                if (setting_variant == _c_box.Text)
                    return;
                _c_box.User.KeyPress(System.Windows.Forms.Keys.Up, 50);
            }

        
            prev_step_text = "";
            //листаем вниз до упора
            while (true)
            {
                if (prev_step_text == _c_box.Text && _c_box.Text != "")
                    break;
                prev_step_text = _c_box.Text;
                if (setting_variant == _c_box.Text)
                    return;
                _c_box.User.KeyPress(System.Windows.Forms.Keys.Down, 50);
            }

            _log.Info(String.Format("Test step. Выбрали вариант экспорта {0}", setting_variant));

        }


        public void ClickEditFileExportButton()
        {
            FileExportSettingsButton.User.Click();
            _log.Info(String.Format("Test step. Клик по кнопке - редактирование настроек экспорта файлов "));
        }


        
        public void CheckAdditiveDocumentCheckbox()
        {
            var _ch_box = AdditiveDocumentCheckbox;
            if (!_ch_box.IsChecked.Value)
                _ch_box.Toggle();
            _log.Info(String.Format("Test step. Отметили чекбокс - сопроводительный документ "));
        }

        public void UnCheckAdditiveDocumentCheckbox()
        {
            var _ch_box = AdditiveDocumentCheckbox;
            if (_ch_box.IsChecked.Value)
                _ch_box.Toggle();
            _log.Info(String.Format("Test step. Сняли отметку с чекбокса - сопроводительный документ "));
        }
            


        public void SelectAdditiveDocumentTemplateComboBox(string setting_variant)
        {
            ComboBox  _c_box =   AdditiveDocumentTemplateComboBox;

            string prev_step_text  = "";
           
            _c_box.SetFocus();
            _c_box.User.KeyPress(System.Windows.Forms.Keys.Down, 50);

           
            //листаем вверх до упора
            while (true)
            {
                if (prev_step_text == _c_box.Text && _c_box.Text != "")
                    break;
                prev_step_text = _c_box.Text;
                if (setting_variant == _c_box.Text)
                    return;
                _c_box.User.KeyPress(System.Windows.Forms.Keys.Up, 50);
            }

        
            prev_step_text = "";
            //листаем вниз до упора
            while (true)
            {
                if (prev_step_text == _c_box.Text && _c_box.Text != "")
                    break;
                prev_step_text = _c_box.Text;
                if (setting_variant == _c_box.Text)
                    return;
                _c_box.User.KeyPress(System.Windows.Forms.Keys.Down, 50);
            }


            _log.Info(String.Format("Test step. Выбрали шаблон сопр. документа {0} ", setting_variant));
        }



        public void ClickCreateArchiveFilterButton()
        {
            CreateButton.User.Click();
            Thread.Sleep(500);

            _log.Info(String.Format("Test step. Кликнули по кнопке - создать архив "));
        }

        public void ClickApplyArchiveFilterButton()
        {
            ApplyButton.User.Click();
            _log.Info(String.Format("Test step. Кликнули по кнопке - применить "));
        }


        public void ClickArchivePreset(string name)
        {
            //надо подождать загрузки
           presetsStackPanel.Wait.For(panel => panel.Children.Count > 0);
           var listBoxitems = presetsStackPanel.Find.AllByExpression(new XamlFindExpression("XamlTag=ListBoxItem"));
           foreach (var _item in listBoxitems)
           {
               var _tb = _item.Find.ByType<TextBlock>();
               if (_tb != null && _tb.Text == name)
               {
                   _item.User.Click();
                   return;
               }
                   
              
           }

           _log.Info(String.Format("Test step. Кликнули пресет {0}", name));
        }



        public void SetArchivesFilterInterval(DateTime? begin_date =null, DateTime? end_date=null)
        {

            DatePickers[0].Text = begin_date == null ? DatePickers[0].Text : begin_date.Value.ToString("dd.MM.yyyy") ;
            DatePickers[0].User.Click();
            DatePickers[1].Text = end_date == null ? DatePickers[1].Text : end_date.Value.ToString("dd.MM.yyyy");
            DatePickers[1].User.Click();

            _log.Info(String.Format("Test step. Установили интервал фильтрации  c {0} gj {1}", DatePickers[0].Text, DatePickers[1].Text));
        }




        //public FrameworkElement GetMarkCheckBoxByName(string mark_name)
        //{
        //    PresetControl.Wait.For(control => control.Find.AllByExpression(new XamlFindExpression("XamlTag=ThreeStateCheckbox")).Count > 0);
        //    foreach (var _thr_st_chbox in MarksThreeStateCheckBoxes)
        //    {
        //        TextBlock _chb_name = _thr_st_chbox.Find.ByType<TextBlock>();
        //        if (_chb_name != null && _chb_name.Text == mark_name)
        //            return _thr_st_chbox;
        //    }
        //    return null;

        //}

        //public void ToggleMarkCheckBoxByName(string mark_name)
        //{
        //    var _ch_box = GetMarkCheckBoxByName(mark_name);
        //    if (_ch_box != null)
        //    {
        //        var status = (_ch_box as CheckBox).IsThreeState;
        //        var status2 = (_ch_box as CheckBox).IsChecked; 
        //        _ch_box.User.Click();
                
        //    }


        //}
        #endregion

        
        #region TasksControl 


        public void SearchTaskBy(string task_name)
        {
            if (task_name == "")
            {
                ClearSearchButton.Wait.ForVisible();
                ClearSearchButton.User.Click();
                
            }
            else
                SearchTextBox.User.TypeText(task_name, 1);
            
            Thread.Sleep(500);

            _log.Info(String.Format("Test step. Поиск таска по  строке {0}", task_name));
        }


        public void SetTaskStatusType(TaskStatus status)
        {
            
            //Changelist
            //v. 1.8.0.449. Контрол - дефолтный комбобокс
            //v. 1.8.0.852. 
            //      Контрол - текстовый блок 

            //ComboBoxes[1].Text =  Enum.GetName(typeof(TaskStatus), status);
            TaskStatus.Text = Utils.GetEnumDescription(status);
            Thread.Sleep(500);

            _log.Info(String.Format("Test step. Установили статус заданий {0}", Utils.GetEnumDescription(status) ));
        }

        public void ClickReloadStatistic()
        {
            ReloadStatistic.User.Click();
            Thread.Sleep(500);

            _log.Info(String.Format("Test step. Перезагрузка статистики " ));
        }


        public List<TaskInfoShort> GetTasksInfo()
        {
            //TODO timeout??
            
            var st_info = new List<TaskInfoShort>() { };
            foreach (var _t_item in TasksItems)
            {
                //TODO маппинг TaskInfoShort на textBlock Name?? 
                st_info.Add(
                    new TaskInfoShort()
                    {
                        Shifr = _t_item.Find.ByName("cipherTextBlock").As<TextBlock>().Text,
                        Name = _t_item.Find.ByName("aliasTextBlock").As<TextBlock>().Text,
                        Init = _t_item.Find.ByName("InitiatorInfoTextBlock").As<TextBlock>().Text,
                        SeansesCount = _t_item.Find.ByName("selectedSeancesCountTextBlock").As<TextBlock>().Text,
                        SrokInfo = _t_item.Find.ByName("lastSummaryDateTextBlock").As<TextBlock>().Text

                    });


                //st_info.Add(string.Join(",", _t_item.Find.AllByType("TextBlock").Select(x => x.As<TextBlock>().Text).ToList()));
            }

            Thread.Sleep(500);
            _log.Info(String.Format("Test step. Считали данные по отображаемым заданиям " ));

            return st_info;


        }


        public void SetTaskCheckBox(string task_name, bool set_flag)
        {

            foreach (var _t_item in TasksItems)
            {
                if (_t_item.Find.ByName("aliasTextBlock").As<TextBlock>().Text == task_name)
                {
                    var _ch_box = _t_item.Find.ByExpression(new XamlFindExpression("XamlTag=CheckBox")).As<CheckBox>();
                    if (set_flag == true && !_ch_box.IsChecked.Value)
                        _ch_box.Toggle();
                    else if (set_flag == false && _ch_box.IsChecked.Value)
                        _ch_box.Toggle();
                }
            }

            _log.Info(String.Format("Test step. Кликнули чекбокс для задания {0} ", task_name));
        }


        public void OpenTaskContextMenu(string task_name)
        {

            foreach (var _t_item in TasksItems)
            {
                if (_t_item.Find.ByName("aliasTextBlock").As<TextBlock>().Text == task_name)
                {
                    _t_item.User.Click(ArtOfTest.WebAii.Core.MouseClickType.RightClick);
                }
            }

            _log.Info(String.Format("Test step. Открыли контекстное меню для задания {0} ", task_name));

        }

        #endregion


        #region archiveSeancesControl

        //telerik - в VisualTree если грид не входит целиком, не видит
        //TextBlock
        public List<string> GetSeancesGridColumnHeaders()
        {

            var _lst = new List<string>();
            foreach (var _gh in HeadersPanel.Children)
            {
                try
                {
                    _gh.Find.Strategy = FindStrategy.WhenNotVisibleThrowException;
                    _lst.Add(_gh.Find.ByType<TextBlock>().Text);
                }
                catch
                {

                }
                

            }
            return _lst;
        }

        //то же через White,  и то же самое, скролл не помогает
        public List<string> GetSeancesGridColumnHeadersWhite()
        {

            //scroll to right side

            //White.Core.UIItems.IUIItem grid_control = _whiteWindow.Get(SearchCriteria.ByAutomationId("gridControl").AndControlType(ControlType.DataGrid));
            //((ScrollPattern)grid_control.AutomationElement.GetCurrentPattern(ScrollPattern.Pattern)).ScrollHorizontal(ScrollAmount.LargeIncrement);
            //((ScrollPattern)grid_control.AutomationElement.GetCurrentPattern(ScrollPattern.Pattern)).ScrollHorizontal(ScrollAmount.LargeDecrement);


            //Thread.Sleep(1000);

            var _lst = new List<string>();

            White.Core.UIItems.IUIItem grid_header = _whiteWindow.Get(SearchCriteria.ByAutomationId("gridControl").AndControlType(ControlType.Pane));

            var columns = grid_header.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.HeaderItem));

            foreach (AutomationElement _col in columns)
            {
                AutomationElement text_ctrl = _col.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Text));

                try
                {
                    _lst.Add(text_ctrl.Current.Name);
                }
                catch
                {

                }
                
            }

            return _lst;
        }



        public List<ArchiveSeanceInfo> GetSeancesGridCells()
        {

            Thread.Sleep(3000);

            List<string> column_headers = GetSeancesGridColumnHeadersWhite();

            var _lst = new List<ArchiveSeanceInfo>();

            White.Core.UIItems.IUIItem dp = _whiteWindow.Get(SearchCriteria.ByAutomationId("dataPresenter"));

            var rows = dp.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.DataItem));

            foreach (AutomationElement row in rows)
            {
                AutomationElementCollection cells = row.FindAll(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Custom));

                int index = 0;
                var _s_info = new ArchiveSeanceInfo();
                foreach (AutomationElement cell in cells)
                {
                    //map columns names and fields values
                    //try на случай того если все колонки не входят в видимую область
                    try
                    {
                        string grid_column_name = column_headers[index];
                        string cell_value = ((ValuePattern)cell.GetCurrentPattern(ValuePattern.Pattern)).Current.Value;
                        Utils.SetFieldValueByDesc(_s_info, grid_column_name, cell_value);
                    }
                    catch
                    {

                    }
                   

                    index++;
                }

                _lst.Add(_s_info);

            }
            return _lst;

            _log.Info(String.Format("Test step. Считали данные отображаемые в таблице сеансов "));

        }


        public void ClickExportButton()
        {
            ExportButton.Wait.For(control => ((Button)control).IsEnabled);
            ExportButton.User.Click();

            _log.Info(String.Format("Test step. Кликнули кнопку экспортировать "));
        }

        public void ClickPrintButton()
        {
            PrintButton.Wait.For(control => ((Button)control).IsEnabled);
            PrintButton.User.Click();
            //opened WinFormw Window
            Thread.Sleep(3000);

            _log.Info(String.Format("Test step. Кликнули кнопку печать сопроводительного документа "));
        }


        public void WaitSeancesExport()
        {
            BusyIndicator.Wait.Timeout = 30000;
            BusyIndicator.Wait.ForVisibleNot();
            Thread.Sleep(1000);
        }

        
        

        //грид с телериком не взлетает
        //public List<string> GetSeancesGridCells()
        //{
        //    var _lst = new List<string>();

        //    //var _ct1 = CellItemsControls;
        //    //var _ct2 = PART_DataRowContents;
        //    //var _ct3 = PART_DataRows;
        //    var _ct4 = GridRows;

        //    var stri = RowPresenterGrid.TextBlockContent;
            

        //    foreach (var _gh in GridRows)
        //    {
        //        if (_gh.Find.ByType<TextBlock>()!=null)
        //            _lst.Add(_gh.Find.ByType<TextBlock>().Text);

        //    }
        //    return _lst;
        //}


        #endregion

        #endregion
    }
}












