﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System;



namespace RimApp.Tests.Framework.Elements
{
    public class SummaryProvidedWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Предоставление сводки";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Border[0]/Grid[0]";
        public SummaryProvidedWindow(VisualFind find) : base(find) { }

        #region Elements


        private DatePicker EditDateBox { get { return Find.ByName("editDateBox").As<DatePicker>(); } }


        private Button OkBtn { get { return Find.ByName<Button>("okButton"); } }

     
        #endregion

        #region Actions

        public void EnterDate(DateTime date)
        {
            EditDateBox.Text = date.ToString("dd.MM.yyyy");
        }


        public void ClickOk()
        {
            OkBtn.User.Click();
            Thread.Sleep(1000);
        }

        #endregion
    }
}
