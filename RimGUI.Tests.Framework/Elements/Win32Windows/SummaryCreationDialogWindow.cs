﻿using System.Windows.Automation;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using White.Core.WindowsAPI;
using System;



namespace RimApp.Tests.Framework.Elements
{
    public class SummaryCreationDialogWindow
    {
        private Window win;
        public SummaryCreationDialogWindow(Window window)
        {
            win = window;
        }


        #region Elements

        IUIItem text_message_area { get { return win.Get(SearchCriteria.ByControlType(ControlType.Text)); } }

        Button Ok_Button { get { return win.Get<Button>(SearchCriteria.ByText("ОК")); } }

        Button Cancel_Button { get { return win.Get<Button>(SearchCriteria.ByText("Отмена")); } }

        #endregion



        public void ClickOk()
        {
            Ok_Button.Click();
        }

        public void ClickCancel()
        {
            Cancel_Button.Click();
        }


        public string GetMessage()
        {
            return text_message_area.AutomationElement.Current.Name;
        }

    }
}




