﻿using System.Windows.Automation;
//using TestStack.White.UIItems;
//using TestStack.White.UIItems.Finders;
//using TestStack.White.UIItems.WindowItems;
//using TestStack.White.WindowsAPI;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using White.Core.WindowsAPI;
using System;
using System.Threading;



namespace RimApp.Tests.Framework.Elements
{
    public class YesNoDialogWindow
    {
        private Window win;
        public YesNoDialogWindow(Window window)
        {
            win = window;
        }

        #region Elements
        
        #endregion

        #region Actions
        public void ClickOk()
        {
            win.Focus();
            win.Keyboard.PressSpecialKey(White.Core.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
            Thread.Sleep(1000); 

        }

        
        #endregion
    }
}




