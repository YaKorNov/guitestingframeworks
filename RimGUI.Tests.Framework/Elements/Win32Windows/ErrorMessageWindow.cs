﻿using System.Windows.Automation;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using White.Core.WindowsAPI;
using System;
using System.Threading;



namespace RimApp.Tests.Framework.Elements
{
    public class ErrorMessageWindow
    {
        private Window win;
        public ErrorMessageWindow(Window window)
        {
            win = window;
        }

        #region Elements
        
        #endregion

        #region Actions
        public void ClickOk()
        {
            win.Focus();
            win.Keyboard.PressSpecialKey(White.Core.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
            Thread.Sleep(1000); 

        }

        
        #endregion
    }
}








