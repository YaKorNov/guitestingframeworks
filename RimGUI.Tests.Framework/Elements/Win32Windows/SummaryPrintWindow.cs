﻿using System.Windows.Automation;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using White.Core.WindowsAPI;
using System;



namespace RimApp.Tests.Framework.Elements
{
    public class SummaryPrintWindow
    {

        private Window win;
        public SummaryPrintWindow(Window window)
        {
            win = window;
        }

        #region Elements

        IUIItem SelectPrinterList { get { return win.Get(SearchCriteria.ByControlType(ControlType.List)); } }

        
        #endregion

        #region Actions


        public void SelectXPSPrintFormat()
        {
            win.Focus();
            SelectPrinterList.Focus();
            win.Keyboard.Enter("Microsoft XPS Document Writer");
            win.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
        }


        public void SelectXPSPrintFormatAndCloseSaveWindow()
        {
            win.Focus();
            SelectPrinterList.Focus();
            win.Keyboard.Enter("Microsoft XPS Document Writer");
            win.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            //wait for open Win32 print window 
            System.Threading.Thread.Sleep(5000);
            //закрыть окно печати (процесс splwow64.exe)
            System.Diagnostics.Process.GetProcessesByName("splwow64")[0].Kill();
            System.Threading.Thread.Sleep(1000);
            


            ////ждем открытия окна сохранения документа в формате .xps
            ////при этом сводка генерится и добавляется в БД
            ////поэтому очень долго
            //System.Threading.Thread.Sleep(10000);

            ////окно периодически открывается без фокуса
            //White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.ESCAPE);
            //System.Threading.Thread.Sleep(1000);
            //White.Core.InputDevices.Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.ALT);
            //White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.TAB);
            //White.Core.InputDevices.Keyboard.Instance.LeaveAllKeys();
            //System.Threading.Thread.Sleep(1000);
            //White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.ESCAPE);
            ////может быть потерян фокус окна!!!
        }


        public void SaveInXPSPrintFormat(string file_path)
        {
            win.Focus();
            SelectPrinterList.Focus();
            win.Keyboard.Enter("Microsoft XPS Document Writer");
            win.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
            //ждем открытия окна сохранения документа в формате .xps
            //при этом сводка генерится и добавляется в БД
            //поэтому очень долго
            System.Threading.Thread.Sleep(10000);

            //окно периодически открывается без фокуса
            //поэтому

            White.Core.InputDevices.Keyboard.Instance.Enter(file_path);
            White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            //wait for doc save
            System.Threading.Thread.Sleep(3000);
            White.Core.InputDevices.Keyboard.Instance.HoldKey(KeyboardInput.SpecialKeys.ALT);
            White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.TAB);
            White.Core.InputDevices.Keyboard.Instance.LeaveAllKeys();

            White.Core.InputDevices.Keyboard.Instance.Enter(file_path);
            White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            //может быть потерян фокус окна!!!

            //White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.ESCAPE);
        }


        
        #endregion

    }
}


