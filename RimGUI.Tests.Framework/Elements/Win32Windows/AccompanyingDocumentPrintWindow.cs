﻿using System.Windows.Automation;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using White.Core.WindowsAPI;
using System;


namespace RimApp.Tests.Framework.Elements
{
    public class AccompanyingDocumentPrintWindow
    {
        private Window win;
        public AccompanyingDocumentPrintWindow(Window window)
        {
            win = window;
        }

        #region Elements

        Panel Chb_Print_To_File { get { return win.Get<Panel>(SearchCriteria.ByAutomationId("chbPrintToFile")); } }

        Panel BrowseFile { get { return win.Get<Panel>(SearchCriteria.ByAutomationId("chbPrintToFile")); } }

        Panel PrinterComboBox { get { return win.Get<Panel>(SearchCriteria.ByAutomationId("icbInstalledPrinters")); } }




        IUIItem BrowseEdit { get { return win.Get<Panel>(SearchCriteria.ByAutomationId("5375810")); } }
        
        #endregion

        #region Actions

        public void SelectXPSPrintFormat()
        {
            win.Focus();
            PrinterComboBox.Focus();
            win.Keyboard.Enter("Microsoft XPS Document Writer");
            win.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
            System.Threading.Thread.Sleep(2000);

            //TODO тут подумать как все-таки заставить (или на ккком стенеде)
            //чтобы печатный варинт документа можно было все-таки сохранить
            White.Core.InputDevices.Keyboard.Instance.Enter("some_xps_file.xps");
            White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
    
            System.Threading.Thread.Sleep(2000);
        }


        //не получается найти edit c путем до файла так
        public void Print()
        {
            win.Focus();
            Chb_Print_To_File.Click();
            System.Threading.Thread.Sleep(3000);
            //BrowseFile.Enter("первонах");
            //BrowseFile.Focus();
            //win.Keyboard.Enter("второнах");
            //new PropertyCondition(AutomationElement.ControlTypeProperty, "#32770")
            //BrowseFile.AutomationElement.FindAll(TreeScope.Children, new PropertyCondition(  AutomationElement.ControlTypeProperty, ControlType.Edit));
            
            AutomationElement edit_box = null;
            for (int i=1; i<10; i++  )
            {
                System.Threading.Thread.Sleep(1000);
                edit_box = BrowseFile.AutomationElement.FindFirst(TreeScope.Children, Condition.TrueCondition);
                if (edit_box != null)
                    break;
            }
            var elems = BrowseFile.AutomationElement.FindAll(TreeScope.Children, Condition.TrueCondition);

            //edit_box.SetFocus();
            //win.Keyboard.Enter("второнах");

            BrowseEdit.Enter("sdafasd");

            //BrowseEdit.Click();
            //BrowseEdit.Enter("dsfasdfdf");

        }

        
        #endregion

    }
}
