﻿using System.Windows.Automation;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using White.Core.WindowsAPI;
using System;


namespace RimApp.Tests.Framework.Elements
{
    public class AccompanyingDocumentPrintPreviewWindow
    {
        private Window win;
        public AccompanyingDocumentPrintPreviewWindow(Window window)
        {
            win = window;
        }

        #region Elements
        
        #endregion

        #region Actions
        public void Print()
        {
            win.Focus();
            win.Keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
            win.Keyboard.Enter("P");
            win.Keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
            System.Threading.Thread.Sleep(3000);

        }

        
        #endregion

    }
}


