﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using RimGUI.Tests.Framework.Models;



namespace RimApp.Tests.Framework.Elements
{
    public class TaskInfoCardWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Свойства ОТМ";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]";
        public TaskInfoCardWindow(VisualFind find) : base(find) { }

        #region Elements


        private TextBox Shifr { get { return Get<TextBox>(mainPath + "/Grid[0]/TextBox[0]"); } }

        private TextBox Alias { get { return Get<TextBox>(mainPath + "/Grid[0]/TextBox[1]"); } }


        private ScrollViewer ScrV {get {return Get<ScrollViewer>(mainPath, "|",  "XamlTag=ScrollViewer"); }}

      
        private Button CloseBtn { get { return Get<Button>(mainPath + "/Grid[1]/StackPanel[0]/Button[1]"); } }

     
        #endregion

        #region Actions

        public TaskInfo GetFullTaskInfo()
        {
            var _ti = new TaskInfo()
            {
                Shifr = Shifr.Text,
                Name  = Alias.Text
            };

            var _t_info_list = Find.AllByType<TextBox>();

            //так наверное будет максимально гибко в данном случае?
            _ti.Shifr = _t_info_list[0].Text;
            _ti.Name = _t_info_list[1].Text;
            //где-то затесался еще один чекбокс
            _ti.SrokInfo = _t_info_list[3].Text;
            _ti.ControlPoint = _t_info_list[4].Text;
            _ti.Init = _t_info_list[5].Text;
            _ti.Goal = _t_info_list[6].Text;
            _ti.Orientir = _t_info_list[7].Text;


            return _ti;


        }

        public void  PressCloseBtn()
        {
            CloseBtn.User.Click();
        }

        

        

        #endregion

    }
}




