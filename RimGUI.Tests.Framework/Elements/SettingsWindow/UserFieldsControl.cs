﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;



namespace RimApp.Tests.Framework.Elements
{
    public class UserFieldsControl : XamlElementContainer
    {
        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        public UserFieldsControl(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd)
            : base(find) 
        {
            _whiteWindow = white_wnd;
        }


        #region Elements

        //private TextBlock SummariesNumberingText { get { return Find.ByTextContent("Нумерация сводок").As<TextBlock>(); } }

        //UserFields.Field1
        private TextBlock Field1 { get { return Find.ByTextContent("UserFields.Field1").As<TextBlock>(); } }

        private TextBox Field1TextBox { get { return Field1.AnySibling<TextBox>(); } }

        private TextBlock Field2 { get { return Find.ByTextContent("UserFields.Field2").As<TextBlock>(); } }

        private TextBox Field2TextBox { get { return Field2.AnySibling<TextBox>(); } }

        private TextBlock Field3 { get { return Find.ByTextContent("UserFields.Field3").As<TextBlock>(); } }

        private TextBox Field3TextBox { get { return Field3.AnySibling<TextBox>(); } }


        #endregion

        #region Actions

        public void SetField1Text(string text)
        {
            Field1TextBox.Text = text;
        }

        public void SetField2Text(string text)
        {
            Field2TextBox.Text = text;
        }

        public void SetField3Text(string text)
        {
            Field3TextBox.Text = text;
        }

       

        

        #endregion
    }
}







