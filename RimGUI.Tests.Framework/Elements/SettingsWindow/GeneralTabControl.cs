﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;


namespace RimApp.Tests.Framework.Elements
{
    public class GeneralTabControl : XamlElementContainer
    {
        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        public GeneralTabControl(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd)
            : base(find) 
        {
            _whiteWindow = white_wnd;
        }


        #region Elements

        private TextBlock SummariesNumberingText { get { return Find.ByTextContent("Нумерация сводок").As<TextBlock>(); } }


        private CheckBox SummariesNumberingCheckbox { get { return SummariesNumberingText.AnySibling<CheckBox>(); } }

      
     
        #endregion

        #region Actions

        public void CheckReplaceCurrentSummaryNumber()
        {
            if (!SummariesNumberingCheckbox.IsChecked.Value)
            {
                SummariesNumberingCheckbox.Check(true);
            }
        }

        public void UnCheckReplaceCurrentSummaryNumber()
        {
            if (SummariesNumberingCheckbox.IsChecked.Value)
            {
                SummariesNumberingCheckbox.Check(true);
            }
        }

        

        #endregion
    }
}





