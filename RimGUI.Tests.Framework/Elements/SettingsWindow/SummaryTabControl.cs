﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;



namespace RimApp.Tests.Framework.Elements
{
    public class SummaryTabControl : XamlElementContainer
    {

        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        private string DEFAULT_SHIFR_NAME = "Другие ОТМ";

        public SummaryTabControl(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd)
            : base(find) 
        {
            _whiteWindow = white_wnd;
        }


        #region Elements

        private TextBlock OTMShifrText { get { return Find.ByTextContent("Шифр ОТМ").As<TextBlock>(); } }


        private Grid TasksGrid { get { return OTMShifrText.AnySibling<Grid>(); } }

        private FrameworkElement ScrollViewer { get { return OTMShifrText.AnySibling<ScrollViewer>(); } }


        private ListBox TasksListBox { get { return TasksGrid.Find.ByType<ListBox>(); } }


        private IList<ListBoxItem> TasksListBoxItems { get { return TasksListBox.Find.AllByType<ListBoxItem>(); } }


        private IList<Button> TasksGridActionButtons { get { return TasksGrid.Find.AllByType<Button>(); } }


        private TextBlock SeanceTypetextBlock { get { return ScrollViewer.Find.ByTextContent("Тип сеансов").As<TextBlock>(); } }


        private WrapPanel SeanceTypeWrapPanel { get { return SeanceTypetextBlock.NextSibling<WrapPanel>(); } }


        private IList<CheckBox> SeanceTypeCheckboxes { get { return SeanceTypeWrapPanel.Find.AllByType<CheckBox>(); } }


        private TextBlock StenotextBlock { get { return ScrollViewer.Find.ByTextContent("Стенограммы").As<TextBlock>(); } }


        private CheckBox StenoCheckBox { get { return StenotextBlock.NextSibling<CheckBox>(); } }



        private IList<FrameworkElement> MarksCheckboxes { get { return ScrollViewer.Find.AllByExpression(new XamlFindExpression("XamlTag=ThreeStateCheckbox")); } }


        private TextBlock SummaryTemplateBlock { get { return ScrollViewer.Find.ByTextContent("Шаблон").As<TextBlock>(); } }


        private ComboBox SummaryTemplateComboBox { get { return SummaryTemplateBlock.NextSibling<ComboBox>(); } }

     
        #endregion

        #region Actions

        #region Left Panel
        public void ClickAddNewShifrButton()
        {
            //
            TasksGridActionButtons[0].User.Click();
        }


        public void ClickDeleteShifrButton()
        {
            if (TasksGridActionButtons[1].IsEnabled)
            {
                TasksGridActionButtons[1].User.Click();
                Thread.Sleep(500);
            }    
            
                
        }


        public List<string> GetShifrList()
        {
            Thread.Sleep(500);
            TasksGrid.SetFocus();

            var _lst = new List<string>();

            foreach (var task_item in TasksListBoxItems)
            {
                _lst.Add(task_item.Find.ByType<TextBlock>().Text);
            }
            return _lst;

        }


        public void SelectShifr(string shifr)
        {
            foreach (var task_item in TasksListBoxItems)
            {
                if (task_item.Find.ByType<TextBlock>().Text == shifr)
                {
                    task_item.User.Click();
                    return;
                }
            }
        }

        #endregion


        #region right panel

        public void CheckSteno()
        {
            //крестик
            if (StenoCheckBox.IsChecked == null)
            {
                StenoCheckBox.Check(true);
                StenoCheckBox.Check(true);
                return;
            }


            if (!StenoCheckBox.IsChecked.Value)
            {
                StenoCheckBox.Check(true);
            }

                
        }

        public void UnCheckSteno()
        {
            //крестик
            if (StenoCheckBox.IsChecked == null)
            {
                StenoCheckBox.Check(true);
                return;
            }


            if (StenoCheckBox.IsChecked.Value)
            {
                StenoCheckBox.Check(true);
                StenoCheckBox.Check(true);
            }
        }


        public void CheckTalk()
        {
            if (!SeanceTypeCheckboxes[0].IsChecked.Value)
                SeanceTypeCheckboxes[0].User.Click();
        }


        public void UnCheckTalk()
        {
            if (SeanceTypeCheckboxes[0].IsChecked.Value)
                SeanceTypeCheckboxes[0].User.Click();
        }

        public void CheckSms()
        {
            if (!SeanceTypeCheckboxes[1].IsChecked.Value)
                SeanceTypeCheckboxes[1].User.Click();
        }


        public void UnCheckSms()
        {
            if (SeanceTypeCheckboxes[1].IsChecked.Value)
                SeanceTypeCheckboxes[1].User.Click();
        }

        
        public void CheckVideo()
        {
            if (!SeanceTypeCheckboxes[2].IsChecked.Value)
                SeanceTypeCheckboxes[2].User.Click();
        }


        public void UnCheckVideo()
        {
            if (SeanceTypeCheckboxes[2].IsChecked.Value)
                SeanceTypeCheckboxes[2].User.Click();
        }


        public void CheckPlace()
        {
            if (!SeanceTypeCheckboxes[3].IsChecked.Value)
                SeanceTypeCheckboxes[3].User.Click();
        }


        public void UnCheckPlace()
        {
            if (SeanceTypeCheckboxes[3].IsChecked.Value)
                SeanceTypeCheckboxes[3].User.Click();
        }



        public void SetMarkCheckBox(string mark_name)
        {
            foreach (var ch_box in MarksCheckboxes)
            {
                if (ch_box.Text == mark_name)
                {
                    //var three_state = ch_box.As<CheckBox>();
                    //if (!three_state.IsChecked.Value)
                    ch_box.User.Click();
                }
                    
            }
            
        }


        public void SelectSummaryTemplate(string template_name)
        {
            //SummaryTemplateComboBox.OpenDropDown(true);
            //SummaryTemplateComboBox.Find.ByType<TextBlock>().Text = name;
            Thread.Sleep(500);

            string prev_step_text  = "";

            SummaryTemplateComboBox.SetFocus();
            SummaryTemplateComboBox.User.KeyPress(System.Windows.Forms.Keys.Down, 50);

           
            //листаем вверх до упора
            while (true)
            {
                var current_cbox_text = SummaryTemplateComboBox.Find.ByType<TextBlock>().Text;
                if (prev_step_text == current_cbox_text && current_cbox_text != "")
                    break;
                prev_step_text = current_cbox_text;
                if (template_name == current_cbox_text)
                    return;
                SummaryTemplateComboBox.User.KeyPress(System.Windows.Forms.Keys.Up, 50);
            }

        
            prev_step_text = "";
            //листаем вниз до упора
            while (true)
            {
                var current_cbox_text = SummaryTemplateComboBox.Find.ByType<TextBlock>().Text;
                if (prev_step_text == current_cbox_text && current_cbox_text != "")
                    break;
                prev_step_text = current_cbox_text;
                if (template_name == current_cbox_text)
                    return;
                SummaryTemplateComboBox.User.KeyPress(System.Windows.Forms.Keys.Down, 50);
            }


      
        }



        //подготовка - удалить все недефолтные настройки 
        public void DeleteNonDefaultTaskShifrs()
        {
            foreach  ( var shifr_name in  GetShifrList())
            {
                if (shifr_name != DEFAULT_SHIFR_NAME)
                {
                    SelectShifr(shifr_name);
                    ClickDeleteShifrButton();

                }
            }


        }


        public void SelectDefaultShifrName()
        {
            SelectShifr(DEFAULT_SHIFR_NAME);
        }



        #endregion

        #endregion

    }
}






