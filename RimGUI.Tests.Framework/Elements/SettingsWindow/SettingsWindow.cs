﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System;
using RimGUI.Tests.Framework;
using RimGUI.Tests.Framework.Models;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;



namespace RimApp.Tests.Framework.Elements
{
    public class SettingsWindow : XamlElementContainer
    {
        private White.Core.UIItems.WindowItems.Window _whiteWindow;
        private App _app;

        public static string WINDOW_NAME = "Настройки";
        private static string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]";

        //определяют активность табов в VisualTree 
        public bool _general_tab_active = false;
        public bool _summary_tab_active = false;
        public bool _template_tab_active = false;
        public bool _user_fields_tab_active = false;


        #region Element paths
        
        #region Tabs
        private static string TabPanel = mainPath + "/TabControl[0]/Grid[0]/Border[1]/TabPanel[0]";
        private static string TabItem = TabPanel + "/TabItem";
        private static string ContentPresenter = mainPath + "/TabControl[0]/Grid[0]/Border[0]/ContentPresenter[0]/Grid[0]";
        //private static string generalTab = TabPanel + "/TabItem[0]";
        //private static string summaryTab = TabPanel + "/TabItem[1]";
        //private static string templateTab = TabPanel + "/TabItem[2]";
        //private static string userFieldsTab = TabPanel + "/TabItem[3]";
        #endregion

        #region Controls
        //private static string journalControl = "XamlTag=JournalControl";
        //private static string summaryControl = "XamlTag=SummaryControl";
        //private static string informationalArchiveControl = "XamlTag=InformationalArchiveControl";
        #endregion

        #endregion

        public SettingsWindow(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd, App app) : base(find)
        {
            _app = app;
            _whiteWindow = white_wnd;
        }

        
        #region Elements

        private FrameworkElement mainPathControl
        {
            get{
                return Get(mainPath);
            }
             
        }

        private FrameworkElement Tab_Panel 
        { 
            get 
            {
                //mainPathControl.Wait.Timeout = 7000;
                //mainPathControl.Wait.For(control => control.Find.ByExpression(new XamlFindExpression(TabPanel)) != null);
                //Thread.Sleep(1000);
                return  Get<TabPanel>(TabPanel); 
                
            }
        }

        private IList<TabItem> Tab_Items { get { return Tab_Panel.Find.AllByType<TabItem>(); } }

        private Button OkBtn { get { return Get<Button>(mainPath + "/StackPanel[0]/Button[0]"); } }


        public GeneralTabControl GeneralTabControl
        {
            get
            {
                if (!_general_tab_active)
                {
                    ClickTabControl(SettingsWindowTabs.General);
                }

                return new GeneralTabControl(Get(ContentPresenter).Find, _whiteWindow);
            }

        }


        public SummaryTabControl SummariesTabControl
        {
            get
            {
                if (!_summary_tab_active)
                {
                    ClickTabControl(SettingsWindowTabs.Summaries);
                }

                return new SummaryTabControl(Get(ContentPresenter).Find, _whiteWindow);
            }

        }


        public DocumentsTemplatesTabControl DocumentsTemplatesTabControl
        {
            get
            {
                if (!_template_tab_active)
                {
                    ClickTabControl(SettingsWindowTabs.DocumentTemplates);
                }

                return new DocumentsTemplatesTabControl(Get(ContentPresenter).Find, _whiteWindow);
            }

        }


        public UserFieldsControl UserFieldsControl
        {
            get
            {
                if (!_user_fields_tab_active)
                {
                    ClickTabControl(SettingsWindowTabs.UserFields);
                }

                return new UserFieldsControl(Get(ContentPresenter).Find, _whiteWindow);
            }

        }


     
        #endregion

        #region Actions

       
        public void  PressOkBtn()
        {
            OkBtn.User.Click();
            //clear window instance
            //для возможности повторного открытия окна настроек в одной сессии теста
            //в противном случае ловим ошибку при инициализации поиска в Tab_Panel
            _app.SettingsWindow = null;
           
            Thread.Sleep(500);
        }


        private TabItem GetTabItemByName(string tab_name)
        {
            foreach (var _t_item in Tab_Items)
            {
                if (_t_item.Find.ByType<TextBlock>().Text == tab_name)
                {
                    return _t_item;
                }
            }

            return null;
        }


        public void ClickTabControl(SettingsWindowTabs tab_name)
        {

            var _t_item =  GetTabItemByName(Utils.GetEnumDescription(tab_name));
            if (_t_item != null)
            {

                _general_tab_active = false;
                _summary_tab_active = false;
                _template_tab_active = false;
                _user_fields_tab_active = false;

                switch (tab_name)
                {
                    case SettingsWindowTabs.General:
                        _general_tab_active = true;
                        break;
                    case SettingsWindowTabs.Summaries:
                        _summary_tab_active = true;
                        break;
                    case SettingsWindowTabs.DocumentTemplates:
                        _template_tab_active = true;
                        break;
                    case SettingsWindowTabs.UserFields:
                        _user_fields_tab_active = true;
                        break;
                }

                _t_item.User.Click();
                //TODO нужны ли такие таймауты? для White - наверное да, т.к. должно успеть загрузиться VisualTree
                Thread.Sleep(1000);
            }

        }

       

        #endregion



    }
}







