﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;


namespace RimApp.Tests.Framework.Elements
{
    public class AddNewTaskShifrWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Добавление маски шифра ОТМ";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/border[0]/Grid[0]";
        public AddNewTaskShifrWindow(VisualFind find) : base(find) { }

        #region Elements


        private TextBox cipherTextBox { get { return Find.ByName<TextBox>("cipherTextBox"); } }

        private IList<Button> Buttons { get { return Find.AllByType<Button>(); } }

        
      
        #endregion

        #region Actions

        public void EnterTaskCipher(string cipher)
        {
            cipherTextBox.SetText(false, cipher, 1, 1, false);
        }

        public void ClickOk()
        {
            Buttons[0].User.Click();
        }

        #endregion
    }
}





