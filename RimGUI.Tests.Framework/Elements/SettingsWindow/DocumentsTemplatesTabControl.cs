﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using RimGUI.Tests.Framework.Models;
using RimGUI.Tests.Framework;
using White.Core.WindowsAPI;



namespace RimApp.Tests.Framework.Elements
{
    public class DocumentsTemplatesTabControl : XamlElementContainer
    {
        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        public DocumentsTemplatesTabControl(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd)
            : base(find) 
        {
            _whiteWindow = white_wnd;
        }


        #region Elements


        DataGrid TemplatesGrid { get { return Find.ByType<DataGrid>(); } }

        StackPanel CRUDButtonsPanel { get { return TemplatesGrid.AnySibling<StackPanel>(); } }

        IList<ArtOfTest.WebAii.Controls.Xaml.Wpf.Button> CRUDButtons { get { return CRUDButtonsPanel.Find.AllByType<ArtOfTest.WebAii.Controls.Xaml.Wpf.Button>(); } }


        //white elements

        IUIItem DocumentTemplatesTab { get { return _whiteWindow.Get(SearchCriteria.ByControlType(ControlType.TabItem).AndNativeProperty(AutomationElement.NameProperty, "Шаблоны документов")); } }

      
        #endregion

        #region Actions


        public List<string> GetGridColumnHeaders()
        {

            var _lst = new List<string>();


            var columns = DocumentTemplatesTab.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.HeaderItem));

            foreach (AutomationElement _col in columns)
            {
                AutomationElement text_ctrl = _col.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Text));

                try
                {
                    _lst.Add(text_ctrl.Current.Name);
                }
                catch
                {

                }

            }

            return _lst;
        }



        public List<DocumentTemplatesInfo> GetGridCells()
        {

            List<string> column_headers = GetGridColumnHeaders();

            var _lst = new List<DocumentTemplatesInfo>();


            var rows = DocumentTemplatesTab.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.DataItem));

            foreach (AutomationElement row in rows)
            {
                AutomationElementCollection cells = row.FindAll(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Custom));

                int index = 0;
                var _s_info = new DocumentTemplatesInfo();
                foreach (AutomationElement cell in cells)
                {
                    //map columns names and fields values
                    //try на случай того если все колонки не входят в видимую область
                    try
                    {
                        string grid_column_name = column_headers[index];
                        string cell_value = ((ValuePattern)cell.GetCurrentPattern(ValuePattern.Pattern)).Current.Value;
                        Utils.SetFieldValueByDesc(_s_info, grid_column_name, cell_value);
                    }
                    catch
                    {

                    }


                    index++;
                }

                _lst.Add(_s_info);

            }
            return _lst;


        }



        public void SelectDocumentTemplate(string name, DocumentTemplateType type)
        {
            var cells = GetGridCells();
            var founded_cell = cells.Where(cell => cell.Type == Utils.GetEnumDescription(type) && cell.Name == name).FirstOrDefault();
           
            if (founded_cell != null)
            {
                //позиционируемся на строке по ее индексу в коллекции
                int founded_row_index = cells.IndexOf(founded_cell);

                var rows = DocumentTemplatesTab.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.DataItem));

                //rows[founded_row_index].SetFocus();

                ((SelectionItemPattern)rows[founded_row_index].GetCurrentPattern(SelectionItemPattern.Pattern)).Select();
                Thread.Sleep(500);
            }
        }



        public void CallCommand(TemplateCommands command_name)
        {

            foreach (var btn in CRUDButtons)
            {
                if (btn.Find.ByType<TextBlock>().Text == Utils.GetEnumDescription(command_name))
                    btn.User.Click();
            }

        }
        

        public void SaveTemplate(string path)
        {
            Thread.Sleep(1000);
            //White.Core.InputDevices.Keyboard.Instance.
            White.Core.InputDevices.Keyboard.Instance.Enter(path);
            White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
            Thread.Sleep(1000);
        }






        

        #endregion
    }
}





