﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System;
using System.Threading;
using System.Collections.Generic;
using White.Core.WindowsAPI;
using RimGUI.Tests.Framework.Models;


namespace RimApp.Tests.Framework.Elements
{
    public class EditingTemplateWindow : XamlElementContainer
    {
        public static string ADD_WINDOW_NAME = "Создание шаблона документов";
        public static string EDIT_WINDOW_NAME = "Свойства шаблона документов";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/border[0]/Grid[0]";
        public EditingTemplateWindow(VisualFind find) : base(find) { }

        #region Elements


        private TextBox fileNameTextBox { get { return Find.ByName<TextBox>("fileNameTextBox"); } }

        private TextBox nameTextBox { get { return Find.ByName<TextBox>("nameTextBox"); } }

        private Button OkButton { get { return Find.ByName<Button>("okButton"); } }

        private Button BrowseButton { get { return fileNameTextBox.AnySibling<Button>(); } }

        private ComboBox TemplateTypeComboBox { get { return fileNameTextBox.AnySibling<ComboBox>(); } }
      
        #endregion

        #region Actions

        public void EnterFileName(string fileName)
        {
            //fileNameTextBox.SetText(false, fileName, 1, 1, true);
            //fileNameTextBox.User.Click();

            BrowseButton.User.Click();
            Thread.Sleep(1000);
            //win 32  saving window
            White.Core.InputDevices.Keyboard.Instance.Enter(fileName);
            White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
            Thread.Sleep(1000);

        }

        public void EnterTemplateName(string name)
        {
            nameTextBox.SetText(false, name, 1, 1, true);
            nameTextBox.User.Click();
        }


        public void ClickTemplateName()
        {
            nameTextBox.User.Click();
        }


        public void ClickBrowseButon()
        {
            BrowseButton.User.Click();
            Thread.Sleep(1000);
            //win 32  saving window
            White.Core.InputDevices.Keyboard.Instance.PressSpecialKey(KeyboardInput.SpecialKeys.ESCAPE);
        }

        public void ClicOkButon()
        {
            OkButton.User.Click();
            Thread.Sleep(500);
        }


        public void SetTemplateType(DocumentTemplateType template_type)
        {
            TemplateTypeComboBox.Text = Enum.GetName(typeof(DocumentTemplateType), template_type);
            TemplateTypeComboBox.User.Click();
        }


        #endregion

    }
}






