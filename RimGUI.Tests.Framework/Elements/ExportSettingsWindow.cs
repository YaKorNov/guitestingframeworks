﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;
using RimGUI.Tests.Framework.Models;
using RimGUI.Tests.Framework;
using log4net;
using System;


namespace RimApp.Tests.Framework.Elements
{
    public class ExportSettingsWindow : XamlElementContainer
    {

        public static string WINDOW_NAME = "Настройки экспорта файлов";
        public static string DEFAULT_ARCHIVE_EXPORT_SETTINGS_NAM = "Стандартный архив";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Border[0]/Grid[0]";
        private ILog _log;

        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        public ExportSettingsWindow(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd, ILog log)
            : base(find) 
        {
            _whiteWindow = white_wnd;
            _whiteWindow.Focus();
            _log = log;
        }


        #region Elements

        /// <summary>
        /// left panel (list)
        /// </summary>
        private ListBox ArchivesList { get { return Get<ListBox>(mainPath + "/ListBox[0]"); } }

        private IList<ListBoxItem> ArchivesListBoxItems { get { return ArchivesList.Find.AllByType<ListBoxItem>(); } }

        private StackPanel ArchivesListButtonsPanel { get { return Get<StackPanel>(mainPath + "/StackPanel[0]"); } }

        private Button CreateButton { get { return Get<Button>(mainPath + "/StackPanel[0]/Button[0]"); } }

        private Button DeleteButton { get { return Get<Button>(mainPath + "/StackPanel[0]/Button[1]"); } }


        /// <summary>
        /// right panel (controls)
        /// </summary>
        private TextBlock ArchivesFolderText { get { return Find.ByTextContent("Папка архива").As<TextBlock>(); } }

        private TextBox FilePathTextBox { get { return Get<TextBox>(mainPath + "/Grid[0]/Grid[0]/Border[0]/TextBox[0]"); } }

        private CheckBox ClearFolderBeforeCreatingArchive { get { return ArchivesFolderText.NextSibling<CheckBox>(); } }


        private CheckBox SplitOnChunks { get { return Get<CheckBox>(mainPath + "/Grid[0]/Grid[0]/StackPanel[0]/CheckBox[0]"); } }

        private TextBox SplitOnChunksTextBox { get { return Get<TextBox>(mainPath + "/Grid[0]/Grid[0]/StackPanel[0]/TextBox[0]"); } }


        private CheckBox CreateSubfolders { get { return ClearFolderBeforeCreatingArchive.NextSibling<CheckBox>(); } }


        private CheckBox CreateSeancesDescriptions { get { return CreateSubfolders.NextSibling<CheckBox>(); } }


        private CheckBox CreateSeancesReestrInExcelFormat { get { return CreateSeancesDescriptions.NextSibling<CheckBox>(); } }

        private Button ConfigureExcelFormat { get { return CreateSeancesReestrInExcelFormat.NextSibling<Button>(); } }

        private CheckBox ConvertSoundFiles { get { return CreateSeancesReestrInExcelFormat.NextSibling<CheckBox>(); } }

        private ComboBox ConvertSoundFilesCombo { get { return ConvertSoundFiles.NextSibling<ComboBox>(); } }

        private CheckBox RenameMediaFiles { get { return ConvertSoundFilesCombo.NextSibling<CheckBox>(); } }

        private Button ConfigureMediaFilesNames { get { return RenameMediaFiles.NextSibling<Button>(); } }


        /// <summary>
        /// command buttons
        /// </summary>
        private StackPanel OkCancelButtonsPanel { get { return Get<StackPanel>(mainPath + "/StackPanel[1]"); } }

        private Button OkButton { get { return Get<Button>(mainPath + "/StackPanel[1]/Button[0]"); } }

     
        #endregion

        #region Actions

        /// left panel (list)

        public List<string> GetArchivesList()
        {
            Thread.Sleep(500);
            
            var _lst = new List<string>();

            foreach (var archive_item in ArchivesListBoxItems)
            {
                _lst.Add(archive_item.Find.ByType<TextBlock>().Text);
            }
            return _lst;

        }


        public void SelectArchive(string name)
        {
            foreach (var archive_item in ArchivesListBoxItems)
            {
                if (archive_item.Find.ByType<TextBlock>().Text == name)
                {
                    archive_item.User.Click();
                }
            }

            _log.Info(String.Format("Test step. Выделили архив {0}", name));
        }


        public void ClickAddButton()
        {
            CreateButton.User.Click();
            _log.Info(String.Format("Test step. Кликнули кнопку добавить"));
        }


        public void ClickDeleteButton()
        {
            DeleteButton.User.Click();
            Thread.Sleep(500);
            _log.Info(String.Format("Test step. Кликнули кнопку удалить"));
        }

        /// right panel (list)

        public void SetArchiveFilePathText(string text)
        {
            FilePathTextBox.Text = text;
            _log.Info(String.Format("Test step. Прописали путь экспорта архива {0}", text));

        }


        public void CheckClearFolderBeforeCreatingArchive()
        {
            if (!ClearFolderBeforeCreatingArchive.IsChecked.Value)
                ClearFolderBeforeCreatingArchive.Check(true);
            _log.Info(String.Format("Test step. Отметили очищать папку перед созданием архива"));
        }

        public void UnCheckClearFolderBeforeCreatingArchive()
        {
            if (ClearFolderBeforeCreatingArchive.IsChecked.Value)
                ClearFolderBeforeCreatingArchive.Check(true);
            _log.Info(String.Format("Test step. Сняли отметку очищать папку перед созданием архива"));
        }


        public void CheckSplitOnChunks()
        {
            if (!SplitOnChunks.IsChecked.Value)
                SplitOnChunks.Check(true);
            _log.Info(String.Format("Test step. Отметили разбивать на части"));
        }

        public void UnCheckSplitOnChunks()
        {
            if (SplitOnChunks.IsChecked.Value)
                SplitOnChunks.Check(true);
        }


        public void SetSplitOnChunksSize(string size)
        {
            SplitOnChunksTextBox.Text = size;
        }


        public void CheckCreateSubfolders()
        {
            if (!CreateSubfolders.IsChecked.Value)
                CreateSubfolders.Check(true);
            _log.Info(String.Format("Test step. Отметили создавать подпапки"));
        }

        public void UnCheckCreateSubfolders()
        {
            if (CreateSubfolders.IsChecked.Value)
                CreateSubfolders.Check(true);
            _log.Info(String.Format("Test step. Сняли отметку создавать подпапки"));
        }

        public void CheckCreateSeancesDescriptions()
        {
            if (!CreateSeancesDescriptions.IsChecked.Value)
                CreateSeancesDescriptions.Check(true);
            _log.Info(String.Format("Test step. Отметили создавать описание сеансов"));
        }

        public void UnCheckCreateSeancesDescriptions()
        {
            if (CreateSeancesDescriptions.IsChecked.Value)
                CreateSeancesDescriptions.Check(true);
            _log.Info(String.Format("Test step.  Сняли отметку создавать описание сеансов"));
        }

        //TODO окно "Колонки реестра сеансов" для экспорта в Excel
        public void CheckCreateSeancesReestrInExcelFormat()
        {
            if (!CreateSeancesReestrInExcelFormat.IsChecked.Value)
                CreateSeancesReestrInExcelFormat.Check(true);
            _log.Info(String.Format("Test step. Отметили создавать реестр сеансов в форматеп Excel"));
        }

        public void UnCheckCreateSeancesReestrInExcelFormat()
        {
            if (CreateSeancesReestrInExcelFormat.IsChecked.Value)
                CreateSeancesReestrInExcelFormat.Check(true);
            _log.Info(String.Format("Test step.  Сняли отметку создавать реестр сеансов в форматеп Excel"));
        }


        public void CheckConvertSoundFiles()
        {
            if (!ConvertSoundFiles.IsChecked.Value)
                ConvertSoundFiles.Check(true);
            _log.Info(String.Format("Test step. Отметили преобразовывать медиа-файлы"));
        }

        public void UnCheckConvertSoundFiles()
        {
            if (ConvertSoundFiles.IsChecked.Value)
                ConvertSoundFiles.Check(true);
            _log.Info(String.Format("Test step.  Сняли отметку преобразовывать медиа-файлы"));
        }


        public void SetSoundFilesFormat(SoundFormat format)
        {
            ConvertSoundFilesCombo.Text =  Utils.GetEnumDescription(format);
            _log.Info(String.Format("Test step. Выбрали формат звуковых файлов {0}", Utils.GetEnumDescription(format)));
        }


        //по дефолту форамат #date#time #num1
        //TODO при наличии свободного времени доделать окно  
        public void CheckRenameMediaFiles()
        {
            if (!RenameMediaFiles.IsChecked.Value)
                RenameMediaFiles.Check(true);

            _log.Info(String.Format("Test step. Поставили отметку переименовывать медиа-файлы"));
        }

        public void UnCheckRenameMediaFiles()
        {
            if (RenameMediaFiles.IsChecked.Value)
                RenameMediaFiles.Check(true);
            _log.Info(String.Format("Test step. Сняли отметку переименовывать медиа-файлы"));
        }


        public void ClickConfigureMediaNames()
        {
            ConfigureMediaFilesNames.User.Click();

            _log.Info(String.Format("Test step. Кликнули настроить медиа-файлы"));

        }

        public void ClickConfigureExcelFormat()
        {
            ConfigureExcelFormat.User.Click();

            _log.Info(String.Format("Test step. Кликнули настроить формат Excel"));
        }



        //Commands
        public void ClickOkButton()
        {
            OkButton.User.Click();
            Thread.Sleep(1000);

            _log.Info(String.Format("Test step. Кликнули Ok"));
        }




        #endregion


    }
}




