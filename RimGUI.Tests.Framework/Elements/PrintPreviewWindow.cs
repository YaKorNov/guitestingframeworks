﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.WindowItems;
//using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using System.Windows.Automation;
using System.Collections.Generic;


namespace RimApp.Tests.Framework.Elements
{
    public class PrintPreviewWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Предварительный просмотр сводки";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]";
        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        public PrintPreviewWindow(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd) : base(find) 
        { 
            _whiteWindow = white_wnd;
        }

        #region Elements


        private ScrollBar VerticalScroolBar { get { return Find.ByName<ScrollBar>("VerticalScrollBar"); } }


        private FrameworkElement PageContentRoot { get { return Find.ByName("PageContentRoot"); } }
       // private Button OkBtn { get { return Get<Button>(mainPath + "/Grid[1]/StackPanel[0]/Button[0]"); } }


        private FrameworkElement printPreview { 
            get 
            {
                Find.RefreshRoot();
                return Find.ByExpression(new XamlFindExpression(mainPath + "/Border[0]/PrintPreviewControl[0]")); 
            } 
        }

        private IList<TextBlock> PreviewTexts { get { return printPreview.Find.AllByType<TextBlock>(); } }


        private ComboBox printComboBox { get { return Find.ByName("printComboBox").As<ComboBox>(); } }

        private Button printButton { get { return printComboBox.NextSibling<Button>();  } }


        //white
        private White.Core.UIItems.IUIItem printPreviewWhite { get { return _whiteWindow.Get(SearchCriteria.ByAutomationId("_printPreview")); } }

        private AutomationElementCollection textContentWhite { get {return printPreviewWhite.AutomationElement.FindAll(System.Windows.Automation.TreeScope.Children, Condition.TrueCondition); } }


        #endregion

        #region Actions

        public string GetVisiblePreviewContent_OneChunk()
        {
            Thread.Sleep(1000);
            List<string> text = new List<string>();

            foreach (AutomationElement el in textContentWhite)
            {
                text.Add(el.Current.Name);
            }

            return string.Join("\n", text);
           
        }


        public string GetVisiblePreviewContent_OneChunk_()
        {
            Find.RefreshRoot();
            //Thread.Sleep(1500);
            return string.Join("\n", PreviewTexts.Select(tx => tx.Text).ToList());
            
        }

        public string GetVisiblePreviewContent()
        {
            //Thread.S

            //скроллим и считываем данные
            var all_chunks = new List<string>();

            var prev_chunk = "";
            var next_chunk = GetVisiblePreviewContent_OneChunk();

            //сначала скроллим вверх до упора
            while (prev_chunk != next_chunk)
            {
                prev_chunk = next_chunk;
                try
                {
                    ScrollUp();
                }
                catch
                {
                    //если остутсвует скролл
                }
                Thread.Sleep(300);
                next_chunk = GetVisiblePreviewContent_OneChunk();
            }



            //а теперь вниз и считываем данные
            prev_chunk = "";
            all_chunks.Add(next_chunk);

            while (prev_chunk != next_chunk)
            {
                prev_chunk = next_chunk;
                try
                {
                    ScrollDown();
                }
                catch
                {
                    //если остутсвует скролл
                }
                next_chunk = GetVisiblePreviewContent_OneChunk();
                all_chunks.Add(next_chunk);
            }

            //будет возвращен текстовые блоки, хвост которого может являться началом
            //следующего
            return  string.Join(" ",all_chunks);

        }


        public void ScrollDown()
        {
            VerticalScroolBar.SetFocus();
            //значение 3000 установлено опытным путем для получения следующей страницы
            //1.Неразвернутого состояния окна
            //2.разрешения экрана 1280 на 1024
            VerticalScroolBar.User.TurnMouseWheel(3000, ArtOfTest.WebAii.Core.MouseWheelTurnDirection.Backward, false);
        }

        public void ScrollUp()
        {
            VerticalScroolBar.SetFocus();
            VerticalScroolBar.User.TurnMouseWheel(3000, ArtOfTest.WebAii.Core.MouseWheelTurnDirection.Forward, false);
        }


        public void ClickPrint()
        {
            printButton.Wait.For(button => ((Button)button).IsEnabled);
            printButton.User.Click();
        }

        #endregion
    }
}






