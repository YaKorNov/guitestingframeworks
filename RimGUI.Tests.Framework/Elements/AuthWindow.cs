﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;


namespace RimApp.Tests.Framework.Elements
{
    public class AuthWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Вход в систему";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Grid[0]";

        private White.Core.UIItems.WindowItems.Window _whiteWindow;

        public AuthWindow(VisualFind find, White.Core.UIItems.WindowItems.Window white_wnd)
            : base(find) 
        {
            _whiteWindow = white_wnd;
            _whiteWindow.Focus();
        }


        #region Elements


        private TextBox User { get { return Get<TextBox>(mainPath + "/Grid[0]/Grid[0]/TextBox[0]"); } }

        private PasswordBox Password { get { return Get<PasswordBox>(mainPath + "/Grid[0]/Grid[0]/PasswordBox[0]"); } }


        private Button OkBtn { get { return Get<Button>(mainPath + "/Grid[1]/StackPanel[0]/Button[0]"); } }

     
        #endregion

        #region Actions

        public void EnterUserName(string name)
        {
            User.SetText(false, name, 1 ,1, false);
        }

        public void EnterUserPass(string pass)
        {
            Password.Password = pass;
        }

        public void ClickOk()
        {
            OkBtn.User.Click();
        }

        #endregion
    }
}
