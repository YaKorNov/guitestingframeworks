﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;
using System;
using RimGUI.Tests.Framework.Models;
using System.Collections.Generic;
using RimGUI.Tests.Framework;
using log4net;


namespace RimApp.Tests.Framework.Elements
{
    public class ExportToExcelSettingsWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Колонки реестра сеансов";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/border[0]/Grid[0]";
        private ILog _log;

        public ExportToExcelSettingsWindow(VisualFind find, ILog log) : base(find) 
        {
            _log = log;    
        }

        #region Elements


        //private TextBox editTextBox { get { return Find.ByName<TextBox>("editTextBox"); } }

        private FrameworkElement columnListBox { get { return  Find.ByName("columnsListBox"); } }

        private Button okButton { get { return Find.ByName<Button>("ButtonOk"); } }

        #endregion

        #region Actions

        public IList<ExcelArchiveFields> GetCheckedColumns()
        {
            IList<ExcelArchiveFields> excel_columns = new List<ExcelArchiveFields>();
            IList<CheckBox> ch_boxes = columnListBox.Find.AllByType<CheckBox>();

            foreach (var c_box in ch_boxes)
            {
                if (c_box.IsChecked == true)
                {
                    var c_box_text = c_box.AnySibling<TextBlock>().Text;
                    excel_columns.Add(
                    (ExcelArchiveFields)Utils.FindEnumByDescription(c_box_text, typeof(ExcelArchiveFields)));
                }
            }

            _log.Info(String.Format("Test step. Считали выбранные колонки "));

            return excel_columns;
        }


        public void SelectAnyChBoxes()
        {
            var _random = new Random();
            IList<CheckBox> ch_boxes = columnListBox.Find.AllByType<CheckBox>();

            //какие-то помечаем
            foreach (var c_box in ch_boxes)
            {
                if (_random.Next(0, 100) % 2 == 0 && c_box.IsChecked == false )
                {
                    c_box.Check(true);
                }
            }

            //с каких-то снимаем отметки
            foreach (var c_box in ch_boxes)
            {
                if (_random.Next(0, 100) % 2 == 0 && c_box.IsChecked == true)
                {
                    c_box.Check(true);
                }

                //отметить чекбокс id - всегда отмечен
                var c_box_text = c_box.AnySibling<TextBlock>().Text;
                if ((c_box_text == "Идентификатор сеанса" || c_box_text == "Время начала" || c_box_text == "Телефон 1" || c_box_text == "Телефон 2") && c_box.IsChecked == false)
                {
                    c_box.Check(true);
                }
            }

            _log.Info(String.Format("Test step. Выделили некоторые колонки "));

        }


        public void ClickOk()
        {
            okButton.User.Click();
        }

        #endregion
    }
}





