﻿using ArtOfTest.WebAii.Controls.Xaml.Wpf;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.TestTemplates;
using System.Threading;


namespace RimApp.Tests.Framework.Elements
{
    public class FileNameTemplateWindow : XamlElementContainer
    {
        public static string WINDOW_NAME = "Шаблон переименования файлов";
        private string mainPath = "XamlPath=/Border[0]/AdornerDecorator[0]/ContentPresenter[0]/Border[0]/Grid[0]";
        public FileNameTemplateWindow(VisualFind find) : base(find) { }

        #region Elements


        private TextBox editTextBox { get { return Find.ByName<TextBox>("renamingTemplatetextBox"); } }

        private Button okButton { get { return Find.AllByType<Button>()[0]; } }

        
      
        #endregion

        #region Actions

        public void EnterTemplate(string name)
        {
            editTextBox.SetText(false, name, 1, 1, true);
        }

        public void ClickOk()
        {
            okButton.User.Click();
        }

        #endregion


    }
}





