﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.Text.RegularExpressions;


namespace RimGUI.Tests.Framework
{
    public class Utils
    {
        public static T[] ConcatArrays<T>(params T[][] list)
        {
            var result = new T[list.Sum(a => a.Length)];
            int offset = 0;
            for (int x = 0; x < list.Length; x++)
            {
                list[x].CopyTo(result, offset);
                offset += list[x].Length;
            }
            return result;
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static Enum FindEnumByDescription(string desc, Type enum_type)
        {
           
            foreach (var value in Enum.GetValues(enum_type) )
            {
                if (Utils.GetEnumDescription((Enum)value) == desc)
                {
                    return (Enum)value;
                }
            }


            return null;

        }


        public static void SetFieldValueByDesc(object instance, string field_desc, object value)
        {
            PropertyInfo[] p_infos = instance.GetType().GetProperties();


            foreach (PropertyInfo _pi in p_infos)
            {
                DescriptionAttribute[] attributes =
                (DescriptionAttribute[])_pi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

                if (attributes != null &&
                    attributes.Length > 0 && attributes[0].Description == field_desc)
                    _pi.SetValue(instance, value);
            }
        }


        public static Func<T, X> PreTimeoutWrapper<T, X>(Func<T, X> _func, int miliseconds)
        {
            System.Threading.Thread.Sleep(miliseconds);
            return _func;
        }

        public static Func<T> PreTimeoutWrapper<T>(Func<T> _func, int miliseconds)
        {
            System.Threading.Thread.Sleep(miliseconds);
            return _func;
        }



        public static Action<T> PreTimeoutWrapper<T>(Action<T> _func, int miliseconds)
        {
            System.Threading.Thread.Sleep(miliseconds);
            return _func;
        }



        public static List<T> RandomListEntries<T>(IList<T> listItems, Random _rand)
        {
            //var _rand_list_ex = new Random();
            int exclude_count = _rand.Next(0, listItems.Count);



            var listItemsCopy = new List<T>(listItems);

            var include_count = listItemsCopy.Count - exclude_count;

            if (listItemsCopy.Count != include_count)
            {
                var shuffled_list = new List<T>();
                //var random = new Random();

                while (listItemsCopy.Count > 0)
                {  // Get the next item at random. 
                    var randomIndex = _rand.Next(0, listItemsCopy.Count);
                    var randomItem = listItemsCopy[randomIndex];
                    // Remove the item from the list and push it to the top of the deck. 
                    listItemsCopy.RemoveAt(randomIndex);
                    shuffled_list.Add(randomItem);

                }


                listItemsCopy = shuffled_list.Take(include_count).ToList();
            }

            return listItemsCopy;


            //Dictionary<string, int> _dict = new Dictionary<string, int>();
            //_dict.Get
        }

        
        public static string GetPhoneNumberFormatted(string number)
        {
            //input
            //"89009365229", 
            //   "8912 936 52 29",

            string pattern = @"^(?<country>(\+7|8)[\s-]?)?(?<city>\(\d{3}\)|\d{3})[\s-]?(?<first_group>\d{3})[\s-]?(?<second_group>\d{2})[\s-]?(?<third_group>\d{2})";
            string replacement = "(7)(${city})${first_group}-${second_group}-${third_group}";
            Regex regex = new Regex(pattern);

            // output (7)(913)064-56-34
            return Regex.Replace(number, pattern, replacement); 
        }



       
        
        public static string StringFormat(string format, IDictionary<string, object> values)
        {
            var matches = Regex.Matches(format, @"#(.+?)\s");
            List<string> words = (from Match matche in matches select matche.Groups[1].Value).ToList();

            return words.Aggregate(
                format,
                (current, key) =>
                    {
                        int colonIndex = key.IndexOf(':');
                        return current.Replace(
                            "#" + key,
                            colonIndex > 0
                                ? string.Format("{0:" + key.Substring(colonIndex + 1) + "}", values[key.Substring(0, colonIndex)])
                                : values[key].ToString());
                    });
        }



    }



    public static class TimeoutExtension
    {

        public static Action<T> AddPreActionTimeout<T>(this Action<T> _func, int miliseconds)
        {
            System.Threading.Thread.Sleep(miliseconds);
            return _func;
        }

        

    }

}
