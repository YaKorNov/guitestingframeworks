﻿using System;
using System.IO;
using System.Collections.Generic;
using ArtOfTest.WebAii.Core;
using RimApp.Tests.Framework.Elements;
using White.Core;
//TODO merge to common namespace CommonGenerator!!
using CommonGenerator;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;
using DataGenerator.Moj.MojBuilders;
using DataGenerator.Moj;
using CommonGenerator.Moj.Tables;
using CommonGenerator.Moj;
using CommonGenerator.Mtb;
using CommonGenerator;
using DataGenerator.Mtb.MtbBuilders;
using DataGenerator.Mtb;
using BLToolkit.Data.DataProvider;





namespace RimGUI.Tests.Framework.Tests
{
    public class BaseTest
    {
        protected RimApp.Tests.Framework.Elements.App App { get; set; }

        private  MtbSettings _mtbSettings;
        private  MtbDataBuilder _mtbDataBuilder;
        private  MojSettings _mojSettings;
        private  MojDataBuilder _mojDataBuilder;
        private  string _talkFileName;
        private  string _videoFileName;

        //убрать хардкодинг
        protected static string RimPath
        {
            get { return @"D:\MAG\Rim\"; }
        }

        protected void  Authorize()
        {
            App.AuthWindow.EnterUserName("Admin");
            App.AuthWindow.EnterUserPass("mag");
            App.AuthWindow.ClickOk();
        }

        protected void DataGeneratorInit()
        {
            _mtbSettings = new MtbSettings()
            {
                Address = @"MATVEENKO7",
                Database = "MagTalksBase",
                Login = "Admin",
                Password = "mag"
                //Address = "ORAKULTESTGUI",
                //Database = "MagTalksBase",
                //Login = "sa",
                //Password = "m1m2m3-="
            };
            _mojSettings = new MojSettings()
            {
                Address = @"MATVEENKO7",
                Database = "MagObjectsJournal",
                Login = "Admin",
                Password = "mag"
                //Address = "ORAKULTESTGUI",
                //Database = "MagObjectsJournal",
                //Login = "sa",
                //Password = "m1m2m3-="
            };

            _mtbDataBuilder = new MtbDataBuilder(new MtbDbManager(new SqlDataProvider(), _mtbSettings), MagVersion.ver_8_6);
            _mojDataBuilder = new MojDataBuilder(new MojDbManager(new SqlDataProvider(), _mojSettings));

            //TODO убрать хардкодинг
            _talkFileName = @"D:\SampleFiles\sample.wsd";
            _videoFileName = @"D:\SampleFiles\sample.wsd";
        }


        protected void GenerateOneTaskWithSomeSeances()
        {
            _mtbDataBuilder.FullCleanUpMtb();
            _mojDataBuilder.FullCleanUpMoj();

            //organ_id ([dt_Initiator]) И otd_id([dt_Tasks]) явл. FK на sl_DivisionGuid, по идее они должны быть равны
            var initiator = "МВД";
            var otdel = "МВД";
            var podrazd = "отдел К";
            var init_Famini = "Петров Иван Анатольевич";
            //var init_Famini = "Семенов Семен Анатольевич";
            string meropr = "ПТП";
            string vid_object = "40";
            string task_number = "15000092";
            string task_year = "2022";
            DateTime taskBegin = new DateTime(2016, 1, 1);
            DateTime taskGrantAutoStart = taskBegin.AddHours(-4);
            short? task_srok = 60;
            //исходим из предположения, что номер объекта контроля - это тот же номер, что и контролируемый объект в учете
            string control_object_phone = "89143787936";
            List<string> interceptor_phones = new List<string>() { "79830878924", "79830878926" };
            string orient_string = "рост средний";
            SecurityLevel sec_level = SecurityLevel.Normal;
            string goal = Utils.GetEnumDescription(TaskTarget.CheckImplication);



            /////// MOJ //////////////

            //создаем задание c видом постановки на контроль по номеру телефона
            //статус - исполняемое, для отображения задания в РИМе, например.
            //задание продолжительностью 60 дней
            //тип контроля - МЕСТО | СМС | ТЕЛЕФОН | ВИДЕО | ДВО (без ФАКСА)
            TaskMoj _new_t_moj = _mojDataBuilder.CreateTaskMoj(otdel, initiator, podrazd, init_Famini, "89130645634", "89130745634", meropr, vid_object, task_number, task_year,
            taskBegin, null, control_object_phone, null, null, Categories.Executed, sec_level,
            "", "ЖИТЕЛЬСТВА", task_srok, "АСП", "232", new List<TaskTypeControl>() { TaskTypeControl.DVO, TaskTypeControl.Place, TaskTypeControl.Sms, TaskTypeControl.Talk, TaskTypeControl.Video });

            //создаем и подключаем различные реквизиты задания
            List<BaseTaskObject> _bt_objects = new List<BaseTaskObject>();

            //физическое лицо
            _bt_objects.Add(_mojDataBuilder.AddPhiz("Никифоров", "Андрей", "Евгеньевич", DateTime.Now, Sex.Male, ExpertORD.MVD_Member));
            //место работы
            _bt_objects.Add(_mojDataBuilder.AddJur("Рога и копыта"));
            //адрес проживания
            _bt_objects.Add(_mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Phiz_Addr));
            //место работы
            _bt_objects.Add(_mojDataBuilder.AddAddr("Новосибирская область", "Новсиб. рпайон", "Новосибирск", "Инженерная", "12", "", "3", TaskObjectType.Yur_Addr));


            foreach (var bto in _bt_objects)
            {
                _mojDataBuilder.LinkPhizWithAdditionalInfo2Task(bto, _new_t_moj);
            }

            //ориентировку
            var _orient = _mojDataBuilder.AddOrientation(_new_t_moj, orient_string);

            //цель проведения - проверить причастность
            var _t_target = _mojDataBuilder.AddTaskTarget(_new_t_moj, Utils.GetEnumDescription(TaskTarget.CheckImplication));



            ////// MTB ///////////////////////

            var _new_t_mtb = _mtbDataBuilder.SetTaskMtb(taskBegin, (int)task_srok, meropr, vid_object, task_number, task_year, Categories.Executed,
               init_Famini, orient_string, goal, DataTypes.Unsigned, new List<TaskTypes>() { TaskTypes.DVO, TaskTypes.Place, TaskTypes.Sms, TaskTypes.Talk, TaskTypes.Video }, sec_level, _mojDataBuilder.GetTaskGuidById(_new_t_moj.task_id),
               10, 20, 30);

            var _new_ta_1 = _mtbDataBuilder.CreateTaskAuto("1_128", SourceTypes.MagFlowBroker, 14, 0, taskBegin);
            var tg1 = _mtbDataBuilder.CreateTaskGrant(_new_ta_1, _new_t_mtb, taskGrantAutoStart, null);

            var _new_ta_2 = _mtbDataBuilder.CreateTaskAuto("1_136", SourceTypes.MagFlowBroker, 14, 0, taskBegin);
            var tg2 = _mtbDataBuilder.CreateTaskGrant(_new_ta_2, _new_t_mtb, taskGrantAutoStart, null);


            //чтение файла
            var talk_file = FileHelper.LoadFile(_talkFileName);
            var video_file = FileHelper.LoadFile(_videoFileName);

            //разг.
            //для попадания в период, добавляем несколько часов
            //TODO для разговоров файлы пишутся, но в ЖС не воспроизводятся(в поле звуковой файл нет пути до файла, пустое поле)
            var _talk = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, taskBegin.AddHours(3), taskBegin.AddHours(4), Arc_flags.FileWrited, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_1 = _mtbDataBuilder.CreateTalkPhones(_talk, control_object_phone, interceptor_phones[0]);

            var _talk2 = _mtbDataBuilder.CreateTalkMtb(_new_ta_1, taskBegin.AddHours(3), taskBegin.AddHours(5), Arc_flags.FileWrited, DataGenerator.Mtb.Tables.TalkPhoneDirection.Incoming, 1, talk_file);
            List<DataGenerator.Mtb.Tables.TalkPhone> _talk_phones_2 = _mtbDataBuilder.CreateTalkPhones(_talk2, control_object_phone, interceptor_phones[1]);

            //дво
            //2 сеанса ДВО на один разговор
            var dvo_seance1 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, _talk, CommonGenerator.Moj.Enums.DVOCodes.AllCF);
            var dvo_seance2 = _mtbDataBuilder.CreateDVOMtb(_new_ta_1, _talk, CommonGenerator.Moj.Enums.DVOCodes.CONF);

            //проверили
            //смс
            var sms = _mtbDataBuilder.CreateSmsMtb(_new_ta_1, control_object_phone, interceptor_phones[0], taskBegin.AddHours(6), taskBegin.AddHours(7), DataGenerator.Mtb.Tables.PhoneDirection.Incoming, false, "Это текст смс сообщения");

            //место
            var _place1 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk);
            var _place2 = _mtbDataBuilder.CreatePlace(_new_ta_2, _talk2);

            //видео
            var video = _mtbDataBuilder.CreateVideo(_new_ta_2, taskBegin.AddHours(6), taskBegin.AddHours(7), false, 1, video_file, true);


            //стенограммы
            var steno1 = _mtbDataBuilder.CreateTexts8(_new_t_mtb, _talk, DataGenerator.Mtb.Tables.Texts8.Texts8State.Published, "Текст стенограммы");
            var steno2 = _mtbDataBuilder.CreateTexts8(_new_t_mtb, _talk2, DataGenerator.Mtb.Tables.Texts8.Texts8State.Darft, "Текст стенограммы2");

            //метки
            var _marck_descr1 = _mtbDataBuilder.GetMarkDescription("Важный", "Важный", true);
            var _marck_descr2 = _mtbDataBuilder.GetMarkDescription("Аналитику", "Аналитику", true);

            _mtbDataBuilder.CreateTalkMark(_talk, _new_t_mtb, _marck_descr1);
            _mtbDataBuilder.CreateTalkMark(_talk, _new_t_mtb, _marck_descr2);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr1, _new_t_mtb);
            _mtbDataBuilder.CreateVideoMark(video, _marck_descr2, _new_t_mtb);

            //сводки (обязательно указываем дату печати сводки, для ее включения в список журнала сводок)
            var report = _mtbDataBuilder.CreateReport(_new_t_mtb, "1", taskBegin.AddDays(3), taskBegin.AddDays(4), DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report, ReportSeancesAlgoritm.RandomIncludeEarlierSeances);

            var report2 = _mtbDataBuilder.CreateReport(_new_t_mtb, "2", taskBegin.AddDays(4), taskBegin.AddDays(5), DateTime.Now, false);
            _mtbDataBuilder.SetReportSeancesForReport(report2, ReportSeancesAlgoritm.RandomExcludeEarlierSeances);
            
        }



        protected void StartTestCase()
        {
            DataGeneratorInit();
            GenerateOneTaskWithSomeSeances();
        }

        protected void Start()
        {
            if (App == null)
            {
                //генерация данных пока перенесена в StartTestCase
              
                Application appWhite = Application.Launch(RimPath + "Rim.App.exe");
                Manager manager = new Manager(false);
                manager.Start();
                //App = new RimApp.Tests.Framework.Elements.App(manager.ConnectToApplication(appWhite.Process), appWhite);


                Authorize();
                //launch auth_window
            }
        }

        protected void Stop()
        {
            if (App != null && App.ApplicationWhite != null)
            {
                App.ApplicationWhite.Kill();
            }
            App = null;
        }
    }
}

