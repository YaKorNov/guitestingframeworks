﻿using System.Threading;
//using White.Core;
using NUnit.Framework;
using NUnit.Core;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using RimGUI.Tests.Framework.Models;
using System.Text.RegularExpressions;
using System;


namespace RimGUI.Tests.Framework.Tests
{
    [TestFixture]
    public class RimTests : BaseTest
    {

        #region setup/teardown

        [OneTimeSetUp]
        public void InitialiseFixture()
        {
            StartTestCase();
        }

        [SetUp]
        public void Initialise()
        {
            Start();
        }

       
        [TearDown]
        public void CleanUp()
        {
            Stop();
        }

        #endregion


        [Test]
        public void TabControlClickTest()
        {
            App.MainWindow.Click1TabControl();
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click3TabControl();
            App.MainWindow.Click1TabControl();

        }



        [Test]
        [Ignore("Check SetSearchText with empty string")]
        public void SummaryControlSetSearchText()
        {
            App.MainWindow.SummaryControl.SetSearchText("search text");
            Thread.Sleep(2000);
            App.MainWindow.SummaryControl.SetSearchText("");
            Thread.Sleep(2000);
        }

        [Test]
        [Description("Failed because must setup right data ")]
        [Ignore("must setup right data ")]
        public void SummaryControlGetSummaryTabTasksInfo()
        {
            
            var info = App.MainWindow.SummaryControl.GetSummaryTabTasksInfo();
            Thread.Sleep(1000);
            //ПТП-40-15000092-2022,ПТП-40-15000092-2022,273 сут. назад,Петров Иван Анатольевич, отдел К,0
            
            AssertEx.PropertyValuesAreEquals(info[0], new Dictionary<string, object>()
            {
                { "Shifr", "ПТП-40-15000092-2022"},
                { "Name", "ПТП-40-15000092-2022"},
                { "Init", "Петров Иван Анатольевич, отдел К"},
                { "SeansesCount", "0"},
                //{ "SrokInfo", "274 сут. назад"},

            });
        }


        [Test]
        public void SummaryControlSetTaskStatusType()
        {
            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.AssignedTasks);
            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.AssignedTasks);
            App.MainWindow.SummaryControl.SetTaskStatusType(TaskStatus.TasksInProcessing);

        }


        [Test]
        public void SummaryControlSetTaskCheckBox()
        {
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", false);
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", false);
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            Thread.Sleep(1000);
        }


        [Test]
        [Description("Unable to find the window properties OTM")]
        public void SummaryControlGetFullTaskInfo()
        {
            App.MainWindow.SummaryControl.OpenTaskPropertiesByDoubleClick("ПТП-40-15000092-2022", true);
            var info =  App.TaskInfoCardWindow.GetFullTaskInfo();

            AssertEx.PropertyValuesAreEquals(info, new Dictionary<string, object>()
            {
                { "Shifr", "ПТП-40-15000092-2022"},
                { "Name", "ПТП-40-15000092-2022"},
                //{ "SrokInfo", "c 1 января по 29 февраля 2016 года, сроком 60 суток Срок исполнения истек"},
                { "ControlPoint", "89143787936"},
                { "Init", "Петров Иван Анатольевич"},
                { "Goal", "ПРОВЕРИТЬ ПРИЧАСТНОСТЬ"},
                { "Orientir" , "рост средний"}

            });
            App.TaskInfoCardWindow.PressCloseBtn();

            App.MainWindow.SummaryControl.OpenTaskPropertiesByDoubleClick("ПТП-40-15000092-2022", true);
            info = App.TaskInfoCardWindow.GetFullTaskInfo();

            AssertEx.PropertyValuesAreEquals(info, new Dictionary<string, object>()
            {
                { "Shifr", "ПТП-40-15000092-2022"},
                { "Name", "ПТП-40-15000092-2022"},
                //{ "SrokInfo", "c 1 января по 29 февраля 2016 года, сроком 60 суток Срок исполнения истек"},
                { "ControlPoint", "89143787936"},
                { "Init", "Петров Иван Анатольевич"},
                { "Goal", "ПРОВЕРИТЬ ПРИЧАСТНОСТЬ"},
                { "Orientir" , "рост средний"}

            });
            App.TaskInfoCardWindow.PressCloseBtn();

        }



        [Test]
        public void SummaryControlFilterButtonTest()
        {
            //контрол фильтрации работает корректно, 
            Thread.Sleep(7000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(1000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(2000);

        }


        [Test]
        public void SummaryControlFilterButtonAnotherTest()
        {
            Thread.Sleep(7000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(5000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(2000);
            

        }

        [Test]
        public void SummaryControlFilterButtonAnotherTest2()
        {
            Thread.Sleep(7000);
            //var s_contr = App.MainWindow.SummaryControl;
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            Thread.Sleep(2000);
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            //Thread.Sleep(2000);
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(false);
            //App.MainWindow.SummaryControl.SetIncludeEarlierSeances(false);
            //Thread.Sleep(2000);
            App.MainWindow.SummaryControl.SetPeriod(System.DateTime.Now, System.DateTime.Now);
            //App.MainWindow.SummaryControl.SetPeriod(System.DateTime.Now, System.DateTime.Now);
            //Thread.Sleep(2000);
            App.MainWindow.SummaryControl.SetSummaryNumber("12");
            App.MainWindow.SummaryControl.SetSummaryNumber("5");
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            //App.MainWindow.SummaryControl.SetSummaryNumber("12");
            //App.MainWindow.SummaryControl.SetSummaryNumber("5");
            //App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            Thread.Sleep(5000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            //must see loaded seances
            Thread.Sleep(3000);


        }


        [Test]
        [Description("Can be Failed cause of inproperly summary settings")]
        [Ignore("Необходима корректная подготовка данных")]
        public void SummaryControlCheckSummaryPreview()
        {
            Thread.Sleep(7000);
            //summary settings
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.CheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            Thread.Sleep(1000);
            App.SettingsWindow.PressOkBtn();

            //var s_contr = App.MainWindow.SummaryControl;
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            //App.MainWindow.SummaryControl.SetPeriod(System.DateTime.Now, System.DateTime.Now);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            //must see loaded seances
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            Thread.Sleep(3000);
            var _prev_infos = App.MainWindow.SummaryControl.GetSummaryPrewiewInfos();
            
            
            var _expected_seances_data = new List<SummaryPreviewInfo>()
            {
                new SummaryPreviewInfo()
                {
                    SeanceDateTime = "sdafds", 
                    TalkSMSInterlocutorInfo = "sfsdf",
                    TalkSMSVideoStenoOrPlaceInfo = "sdfsdfd"

                }
            };
            
            //TODO Assert lists

            foreach (var exp_info in _expected_seances_data)
            {
                //Assert.AreEqual(exp_info, _prev_infos[_expected_seances_data.IndexOf(exp_info)]);
                
            
            }

            AssertEx.PropertyValuesAreEquals(_prev_infos[0], new Dictionary<string, object>()
                {
                    { "SeanceDateTime", "01.01.2016 03:00:00 — 1:00:00"},
                    { "TalkSMSInterlocutorInfo", "(7)(983) 087-89-24, Входящий"},
                    { "TalkSMSVideoStenoOrPlaceInfo", "Текст стенограммы"}

                });


        }



        [Test]
        public void SummaryControlSelectSummaryPreview()
        {
            Thread.Sleep(7000);
            //TODO summary settings перенести в подготовку общего test suite
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.CheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.PressOkBtn();

            
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            //App.MainWindow.SummaryControl.SetPeriod(System.DateTime.Now, System.DateTime.Now);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            //must see loaded seances
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            Thread.Sleep(3000);
            App.MainWindow.SummaryControl.SetPrewiewInfoCheckBox(2);
            App.MainWindow.SummaryControl.SetPrewiewInfoCheckBox(3);
            App.MainWindow.SummaryControl.SetPrewiewInfoCheckBox(4);
            App.MainWindow.SummaryControl.SetPrewiewInfoCheckBox(3);
            Thread.Sleep(3000);


        }


        [Test]
        public void SummaryControlEnterPrintCommand()
        {
            //Thread.Sleep(7000);
            //App.MainWindow.SummaryControl.SetPrintVariant();

            App.MainWindow.SummaryControl.EnterPrintCommand();
            //App.MainWindow.SummaryControl.PrintToggleCommand();
            //App.MainWindow.SummaryControl.selectPrintCommand();
            Thread.Sleep(3000);

        }

        [Test]
        public void SummaryControlPrintToggleCommand()
        {
            App.MainWindow.SummaryControl.PrintToggleCommand();
            //App.MainWindow.SummaryControl.selectPrintCommand();
            Thread.Sleep(3000);

        }

        [Test]
        [Ignore("Падают падют листья.....")]
        public void SummaryControlSelectPrintCommand()
        {
            App.MainWindow.SummaryControl.selectPrintCommand();
            //App.MainWindow.SummaryControl.selectPrintCommand();
            Thread.Sleep(3000);

        }



        #region  PreviewContent
//        ВЕДОМСТВО
//УПРАВЛЕНИЕ
//адрес
//(для
//Петров
//Иван
//Анатольевич
//)
//Сводка
//мероприятия
//ПТП
//№
 
//3
//от
 
//07.10.2016
//за
 
//объектом
//89143787936
//по
 
//заданию
//ПТП
//-
//40
//-
//15000092
//-
//2022
//за период
//с 
//1/1/2016
//по 
//1/1/2016
//Рег.
 
//№
//________
//«
//___
//»
//_____________
//2022
 
//г.
//На
//2
//лист.
//Разговоры:
//01.01.2016
//03:00:00
//(7)(983)
//087
//-
//89
//-
//24
//Входящий
//Admin
//Текст
//стенограммы
//01.01.2016
//03:00:00
//(7)(983)
//087
//-
//89
//-
//26
//Входящий
//Admin
//Не
//задан
//текст
//стенограммы
//СМС:
//01.01.2016
//06:00:00
//(7)(983)
//087
//-
//89
//-
//24
//Входящий
//Это
//текст
//смс
//сообщения
//Местоположения:
//01.01.2016
//03:00:00
//1
//:
//16
//:
//16
//::
//01.01.2016
//03:00:00
//1
//:
//16
//:
//16
//::
//Видеозаписи:
//01.01.2016
//06:00:00
//–
//07:00:00
//Не
//задан
//текст
//стенограммы
//Печатать
//Печатать
//Печатать
//Печатать
//0 ошибок
//100%
        #endregion

        [Test]
        public void PreviewSummary()
        {
            //TODO timeout?
            Thread.Sleep(5000);

            //TODO summary settings перенести в подготовку общего test suite
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.CheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            App.SettingsWindow.PressOkBtn();


            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            //TODO replace for select element?
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            //App.MainWindow.SummaryControl.SetPeriod(System.DateTime.Now, System.DateTime.Now);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            //must see loaded seances
            App.MainWindow.SummaryControl.SelectTaskFromList("ПТП-40-15000092-2022");
            
            App.MainWindow.SummaryControl.EnterSummaryPreviewCommand();
            var content = App.PrintPreviewWindow.GetVisiblePreviewContent();


            //string pattern = @"^(\+7|8)[\s-]?\d{3}[\s-]?\d{3}[\s-]?\d{2}[\s-]?\d{2}$";
            //TODO разобраться с regexpом
            List<string> pattern_list = new List<string>()
            {
                "для", "Петров", "Иван", "Анатольевич", 
                //"Сводка", "мероприятия", "ПТП", "№", "3", "от", "07.10.2016",
                //"за", "объектом", "89143787936", "по", "заданию", "ПТП", "40", "15000092",
                //"2022"
            };
            string pattern = ""+ string.Join("\n", pattern_list);
            

            Regex regex = new Regex(pattern);
           
            Assert.True(regex.Match(content).Success);
        }


        [Test]
        public void PreviewSummaryScroll()
        {
            Thread.Sleep(5000);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            //TODO replace for select element?
            App.MainWindow.SummaryControl.SetIncludeEarlierSeances(true);
            //App.MainWindow.SummaryControl.SetPeriod(System.DateTime.Now, System.DateTime.Now);
            App.MainWindow.SummaryControl.ToggleSummaryFilter();
            //must see loaded seances
            App.MainWindow.SummaryControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);

            App.MainWindow.SummaryControl.EnterSummaryPreviewCommand();
            App.PrintPreviewWindow.ScrollDown();
           
            App.PrintPreviewWindow.ScrollDown();
            
            App.PrintPreviewWindow.ScrollDown();
            
            App.PrintPreviewWindow.ScrollDown();
            
            App.PrintPreviewWindow.ScrollDown();
            Thread.Sleep(1000);
            App.PrintPreviewWindow.ScrollUp();
            App.PrintPreviewWindow.ScrollUp();
            App.PrintPreviewWindow.ScrollUp();
            App.PrintPreviewWindow.ScrollUp();

            Thread.Sleep(2000);
        }


        [Test]
        public void InformationalArchiveControlTogglePresetButtonTest()
        {
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);

            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            App.MainWindow.InformationalArchiveControl.UnCheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.CheckArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            //Thread.Sleep(2000);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Talk);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            //Thread.Sleep(2000);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            //Thread.Sleep(2000);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Place);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            //Thread.Sleep(2000);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);


            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            //Thread.Sleep(2000);
            //App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Video);
            
            


            Thread.Sleep(5000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
        }


        [Test]
        public void InformationalArchiveControlToggleArchiveMarksCheckboxes()
        {
            App.MainWindow.Click2TabControl();
            App.MainWindow.Click1TabControl();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.ToggleMarkCheckBoxByName("Важный");
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.ToggleMarkCheckBoxByName("Аналитику");
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.ToggleMarkCheckBoxByName("Важный");
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.ToggleMarkCheckBoxByName("Аналитику");
            Thread.Sleep(2000);

            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
        }

        [Test]
        public void InformationalArchiveControlCheckNotIncludedSeancesCheckbox()
        {
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.CheckNotIncludedSeancesCheckbox();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.UnCheckNotIncludedSeancesCheckbox();
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.CheckAdditiveDocumentCheckbox();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.CheckAdditiveDocumentCheckbox();
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            Thread.Sleep(2000);
        }


        [Test]
        [Ignore("Must setup right data")]
        public void InformationalArchiveControlFileExportVariant()
        {
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            Thread.Sleep(1000);

            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("вообще нестандартный");
            Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            Thread.Sleep(1000);

            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("соврешенно нестандартный");
            Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            Thread.Sleep(1000);


            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("вообще нестандартный");
            Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            Thread.Sleep(1000);


            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
        }


        [Test]
        [Description("Error Find element exception Exact TextBlock ")]
        [Ignore("Периодически падает, несмотря на добавленные таймауты, можно юзать данное АПИ в других тестах")]
        public void InformationalArchiveControlCheckadditiveCheckBox()
        {
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
           
            App.MainWindow.InformationalArchiveControl.CheckAdditiveDocumentCheckbox();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.UnCheckAdditiveDocumentCheckbox();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.CheckAdditiveDocumentCheckbox();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.SelectAdditiveDocumentTemplateComboBox("старый");
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();

            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.SelectAdditiveDocumentTemplateComboBox("новый");
            Thread.Sleep(2000);
        }


        [Test]
        [Ignore("Must setup right data")]
        public void InformationalArchiveControlCheckadditiveCheckBox2()
        {
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("вообще нестандартный");
            Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            Thread.Sleep(2000);
            App.MainWindow.InformationalArchiveControl.ClickCreateArchiveFilterButton();
            App.InputBoxWindow.EnterArchiveSettingsName("новый вариант архива");
            App.InputBoxWindow.ClickOk();

            Thread.Sleep(1000);
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            App.MainWindow.InformationalArchiveControl.SetArchiveFilterSeanceType(ArchiveSeancesFilterType.Sms);

            //сохранить
            App.MainWindow.InformationalArchiveControl.ClickCreateArchiveFilterButton();
            Thread.Sleep(3000);

            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(1000);
            App.MainWindow.InformationalArchiveControl.ClickArchivePreset("новый вариант архива");
            Thread.Sleep(1000);
            //App.MainWindow.InformationalArchiveControl.ClickArchivePreset("новый вариант архива");
            //Thread.Sleep(1000);

            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            App.MainWindow.InformationalArchiveControl.ClickCreateArchiveFilterButton();
            App.InputBoxWindow.EnterArchiveSettingsName("еще один вариант архива");
            App.InputBoxWindow.ClickOk();

            Thread.Sleep(1000);

            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(1000);
            App.MainWindow.InformationalArchiveControl.ClickArchivePreset("еще один вариант архива");
            Thread.Sleep(1000);



            App.MainWindow.InformationalArchiveControl.ClickArchivePreset("новый вариант архива");
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(3000);
            //должна открыться панель пресетов с активной кнопкой - сохранить
        }


        [Test]
        public void InformationalArchiveControlSetArchivesFilterInterval()
        {
            //App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            //Thread.Sleep(1000);
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(System.DateTime.Now, System.DateTime.Now);
            Thread.Sleep(5000);
            //App.MainWindow.InformationalArchiveControl.GetMarkCheckBoxByName("Важный");

        }

        [Test]
        public void InformationalArchiveControlSearchTaskBy()
        {
            //App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            //Thread.Sleep(1000);
            App.MainWindow.InformationalArchiveControl.SearchTaskBy("балалал");
            Thread.Sleep(1000);
            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.AllTasks);
            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.AssignedTasks);
            App.MainWindow.InformationalArchiveControl.SetTaskStatusType(TaskStatus.TasksInProcessing);
            Thread.Sleep(5000);
            //App.MainWindow.InformationalArchiveControl.GetMarkCheckBoxByName("Важный");

        }


        [Test]
        [Ignore("Не работает обновление статистики")]
        public void InformationalArchiveControlGettasks()
        {
            //App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            //Thread.Sleep(1000);


            App.MainWindow.InformationalArchiveControl.SearchTaskBy("балалал");
            Thread.Sleep(1000);
            App.MainWindow.InformationalArchiveControl.SearchTaskBy("");


            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            Thread.Sleep(500);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox("ПТП-40-15000092-2022", false);
            Thread.Sleep(500);
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            Thread.Sleep(500);
            App.MainWindow.InformationalArchiveControl.OpenTaskContextMenu("ПТП-40-15000092-2022");

            Thread.Sleep(500);

            var info = App.MainWindow.InformationalArchiveControl.GetTasksInfo();
            //Thread.Sleep(1000);
            //ПТП-40-15000092-2022,ПТП-40-15000092-2022,273 сут. назад,Петров Иван Анатольевич, отдел К,0

            AssertEx.PropertyValuesAreEquals(info[0], new Dictionary<string, object>()
            {
                { "Shifr", "ПТП-40-15000092-2022"},
                { "Name", "ПТП-40-15000092-2022"},
                { "Init", "Петров Иван Анатольевич, отдел К"},
                { "SeansesCount", "0"},
                { "SrokInfo", "Сводок нет"},

            });


            App.MainWindow.InformationalArchiveControl.ClickReloadStatistic();

            Thread.Sleep(500);

            info = App.MainWindow.InformationalArchiveControl.GetTasksInfo();
            //Thread.Sleep(1000);
            //ПТП-40-15000092-2022,ПТП-40-15000092-2022,273 сут. назад,Петров Иван Анатольевич, отдел К,0

            AssertEx.PropertyValuesAreEquals(info[0], new Dictionary<string, object>()
            {
                { "Shifr", "ПТП-40-15000092-2022"},
                { "Name", "ПТП-40-15000092-2022"},
                { "Init", "Петров Иван Анатольевич, отдел К"},
                { "SeansesCount", "0"},
                { "SrokInfo", "279 сут. назад"},

            });

        }


        [Test]
        public void InformationalArchiveControlGetSeancesGridColumnHeaders()
        {

            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(new System.DateTime(2016, 1, 1));
            //App.MainWindow.InformationalArchiveControl.SearchTaskBy("");
            var headers = App.MainWindow.InformationalArchiveControl.GetSeancesGridColumnHeadersWhite();

            var cells = App.MainWindow.InformationalArchiveControl.GetSeancesGridCells();


            var _exp_headers = new List<string>(){
                " ",
                //"ОТМ",
                //"Направление",
                "Длительность",
                "ОТМ",
                "Тип",
                "Дата",
                "Объект контроля",
                "Собеседник"
                
            };

            foreach (var _e_header in _exp_headers)
            {
                Assert.AreEqual(_e_header, headers[_exp_headers.IndexOf(_e_header)]);
            }


            AssertEx.PropertyValuesAreEquals(cells[0], new Dictionary<string, object>()
                {
                    { "Type", "Talk"},
                    { "Date", "01.01.2016 03:00:00"},
                    { "ControlObject", "89143787936"},
                    { "Interlocutor", "79830878924"}
                });

            
        
        }



        [Test]
        public void InformationalArchiveControlExport()
        {
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(new System.DateTime(2016, 1, 1));
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(500);


            App.MainWindow.InformationalArchiveControl.ClickExportButton();
            //App.MainWindow.InformationalArchiveControl.ClickPrintButton();
            Thread.Sleep(3000);
        }


        [Test]
        [Ignore("Окно ссохраения грузит процесс")]
        public void InformationalArchiveControlPrintArchiveAccompanyingDocument()
        {
            App.MainWindow.InformationalArchiveControl.SetTaskCheckBox("ПТП-40-15000092-2022", true);
            App.MainWindow.InformationalArchiveControl.SetArchivesFilterInterval(new System.DateTime(2016, 1, 1));
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            App.MainWindow.InformationalArchiveControl.SelectFileExportSetting("Стандартный архив");
            App.MainWindow.InformationalArchiveControl.ClickApplyArchiveFilterButton();
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(500);

            App.MainWindow.InformationalArchiveControl.ClickPrintButton();
            App.AccompanyingDocumentPrintPreviewWindow.Print();
            App.AccompanyingDocumentPrintWindow.SelectXPSPrintFormat();
            Thread.Sleep(3000);
        }


        [Test]
        public void JournalControlSetJournalVariant()
        {
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.InformationArchives);
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            Thread.Sleep(2000);
        }


        [Test]
        public void JournalControlSetDatesInterval()
        {
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2014, 1, 1), new System.DateTime(2016, 1, 1));
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2015, 1, 1) );
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2011, 1, 23), new System.DateTime(2014, 5, 15));
            Thread.Sleep(2000);
        }


        [Test]
        public void JournalControlSetSearchText()
        {
            App.MainWindow.JournalControl.SetSearchText("some text");
            App.MainWindow.JournalControl.SetSearchText("");
            App.MainWindow.JournalControl.SetSearchText("another text");

            Thread.Sleep(2000);
        }


        [Test]
        public void JournalControlSetSearchFilterVariant()
        {
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.Initiator);
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.Otm);
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.SerialNumber);

            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.AllColumns);
            App.MainWindow.JournalControl.SetSearchFilterVariant(SearchFilterVariant.InitiatorDepartment);


            var wrapped = Utils.PreTimeoutWrapper<SearchFilterVariant>(App.MainWindow.JournalControl.SetSearchFilterVariant, 5000);
            wrapped(SearchFilterVariant.Otm);


            Thread.Sleep(2000);
        }


        [Test]
        public void JournalControlGetExecutorsList()
        {
            var actual = App.MainWindow.JournalControl.GetExecutrosList();
            Assert.AreEqual("Все исполнители", actual[0]);

        }

        [Test]
        public void JournalControlSelectExecutorsList()
        {
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            App.MainWindow.JournalControl.SelectExecutor("Все исполнители");
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            App.MainWindow.JournalControl.SelectExecutor("Все исполнители");
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            App.MainWindow.JournalControl.SelectExecutor("Все исполнители");
            System.Threading.Thread.Sleep(2000);
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            System.Threading.Thread.Sleep(3000);
        }


        [Test]
        [Description("Failed because must setup right data ")]
        [Ignore("Failed because must setup right data")]
        public void JournalControlGetJournalGridCells()
        {
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2011, 1, 1), new System.DateTime(2016, 12, 31));
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            var cells = App.MainWindow.JournalControl.GetJournalGridCells();

            AssertEx.PropertyValuesAreEquals(cells[0], new Dictionary<string, object>()
                {
                    { "DateAndtime", "05.01.2016 00:00:00"},
                    { "Period", "01.01.0001 0:00 - 01.01.0001 0:00"},
                    { "Type", "Сводка"},
                    { "Number", "1"},
                    { "OTM", "ПТП-40-15000092-2022"}
                });

            Assert.AreEqual(15, cells.Count);

            

        }


        [Test]
        public void JournalControlOpenContextMenuOnJournalString()
        {
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2011, 1, 1), new System.DateTime(2016, 12, 31));
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "3");
            System.Threading.Thread.Sleep(1000);
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "2");
            System.Threading.Thread.Sleep(1000);
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "4");
            System.Threading.Thread.Sleep(1000);
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "1");
            

            System.Threading.Thread.Sleep(3000);
            
        }


        [Test]
        //[Ignore("При программном перключении табов не получается доступиться до контролов открытой вкладки")]
        public void JournalControlContextMenuClickReprintCommandAndReturnBack()
        {
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2011, 1, 1), new System.DateTime(2016, 12, 31));
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "4");
            
            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.Reprint);
            App.MainWindow.Click1TabControl();
            App.MainWindow.Click2TabControl();
            App.MainWindow.SummaryControl.SetSearchText("");
            var actual = App.MainWindow.SummaryControl.FindSummaryReprintText();
            System.Threading.Thread.Sleep(3000);

            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.Summaries);
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2013, 1, 1), new System.DateTime(2016, 12, 31));
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual("Повторная печать сводки № 4 по ОТМ ПТП-40-15000092-2022", actual);
        }



        [Test]
        [Description("Failed because unsufficient timeout ")]
        public void JournalControlContextMenuClickSetMarkProvided()
        {
            App.MainWindow.ExpandWindow();
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2011, 1, 1), new System.DateTime(2016, 12, 31));
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "2");

            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.SetMarkProvided);
            App.SummaryProvidedWindow.EnterDate(new DateTime(2016 , 10, 1));
            App.SummaryProvidedWindow.ClickOk();

            //Thread.Sleep(3000);
            //Assert
            var cells = App.MainWindow.JournalControl.GetJournalGridCells();

            AssertEx.PropertyValuesAreEquals(cells[1], new Dictionary<string, object>()
                {
                    { "Take_date", "01.10.2016"},
                });

            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "2");
            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.UnSetMarkProvided);
            //Thread.Sleep(2000);
            App.YesNoDialogWindow.ClickOk();

            //Thread.Sleep(3000);
            cells = App.MainWindow.JournalControl.GetJournalGridCells();

            AssertEx.PropertyValuesAreEquals(cells[1], new Dictionary<string, object>()
                {
                    { "Take_date", ""},
                });

            System.Threading.Thread.Sleep(1000);
        }




        [Test]
        [Description("Failed because must setup right data ")]
        public void JournalControlContextMenuClickDeleteSummary()
        {
            App.MainWindow.ExpandWindow();
            App.MainWindow.JournalControl.SetJournalVariant(JournalSummaryVariants.All);
            App.MainWindow.JournalControl.SetDatesInterval(new System.DateTime(2011, 1, 1), new System.DateTime(2016, 12, 31));
            App.MainWindow.JournalControl.SelectExecutor("Администратор");
            App.MainWindow.JournalControl.OpenContextMenuOnJournalString("ПТП-40-15000092-2022", "2");

            var cells = App.MainWindow.JournalControl.GetJournalGridCells();

            AssertEx.PropertyValuesAreEquals(cells[1], new Dictionary<string, object>()
                {
                    { "Type", "Сводка"},
                });

            App.MainWindow.JournalControl.ContextMenuClickCommand(SummaryJournalContextMenuCommand.DeleteSummary);
            App.YesNoDialogWindow.ClickOk();

            //Thread.Sleep(3000);
            cells = App.MainWindow.JournalControl.GetJournalGridCells();

            AssertEx.PropertyValuesAreEquals(cells[1], new Dictionary<string, object>()
                {
                    { "Type", "Удаленная сводка"},
                });

            System.Threading.Thread.Sleep(1000);
        }



        [Test]
        public void OpenSettingsWindow()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.PressOkBtn();
            Thread.Sleep(1000);
        }



        [Test]
        public void SettingsWindowOpenSettingsWindowTabs()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.Summaries);
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.DocumentTemplates);
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.UserFields);
            //App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);

            App.SettingsWindow.PressOkBtn();
            Thread.Sleep(1000);
        }



        [Test]
        public void SettingsWindowCheckReplace()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.GeneralTabControl.CheckReplaceCurrentSummaryNumber();
            Thread.Sleep(1000);
            App.SettingsWindow.GeneralTabControl.UnCheckReplaceCurrentSummaryNumber();
            Thread.Sleep(1000);
            //App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);

            App.SettingsWindow.PressOkBtn();
            Thread.Sleep(1000);
        }


        [Test]
        public void SettingsWindowAddShifr()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.ClickAddNewShifrButton();
            App.AddNewTaskShifrWindow.EnterTaskCipher("ПТП");
            App.AddNewTaskShifrWindow.ClickOk();
            App.SettingsWindow.SummariesTabControl.ClickAddNewShifrButton();
            App.AddNewTaskShifrWindow.EnterTaskCipher("ОТП");
            App.AddNewTaskShifrWindow.ClickOk();
            
            //в данном случае сразу после добавления лист полностью не прогружается, поэтому
            //workaround
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);
            var actual = App.SettingsWindow.SummariesTabControl.GetShifrList();

            Assert.AreEqual("ПТП", actual[1]);
            Assert.AreEqual("ОТП", actual[2]);
            //App.SettingsWindow.PressOkBtn();
            //Thread.Sleep(1000);
        }

        [Test]
        public void SettingsWindowDeleteShifr()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.ClickAddNewShifrButton();
            App.AddNewTaskShifrWindow.EnterTaskCipher("ПТП");
            App.AddNewTaskShifrWindow.ClickOk();
            App.SettingsWindow.SummariesTabControl.ClickAddNewShifrButton();
            App.AddNewTaskShifrWindow.EnterTaskCipher("ОТП");
            App.AddNewTaskShifrWindow.ClickOk();

            //в данном случае сразу после добавления лист полностью не прогружается, поэтому
            //workaround
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);
            App.SettingsWindow.SummariesTabControl.SelectShifr("ОТП");
            App.SettingsWindow.SummariesTabControl.ClickDeleteShifrButton();
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);
            App.SettingsWindow.SummariesTabControl.SelectShifr("ПТП");
            App.SettingsWindow.SummariesTabControl.ClickDeleteShifrButton();

            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.General);
            var actual = App.SettingsWindow.SummariesTabControl.GetShifrList();

            Assert.AreEqual("Другие ОТМ", actual[0]);


        }



        [Test]
        public void SettingsRightPanelActions()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.SummariesTabControl.CheckSteno();
            App.SettingsWindow.SummariesTabControl.CheckTalk();
            App.SettingsWindow.SummariesTabControl.CheckSms();
            App.SettingsWindow.SummariesTabControl.CheckPlace();
            App.SettingsWindow.SummariesTabControl.CheckVideo();
            Thread.Sleep(1000);
            App.SettingsWindow.SummariesTabControl.UnCheckTalk();
            App.SettingsWindow.SummariesTabControl.UnCheckSms();
            App.SettingsWindow.SummariesTabControl.UnCheckPlace();
            App.SettingsWindow.SummariesTabControl.UnCheckVideo();
            Thread.Sleep(1000);
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox("Важный");
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox("Аналитику");
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox("Аналитику");
            App.SettingsWindow.SummariesTabControl.SetMarkCheckBox("Аналитику");
            Thread.Sleep(1000);
            App.SettingsWindow.SummariesTabControl.SelectSummaryTemplate("Шаблон сводки (по типам сеансов) 8.6");
            Thread.Sleep(1000);
            App.SettingsWindow.SummariesTabControl.SelectSummaryTemplate("Шаблон сводки (по времени) 8.6");
            Thread.Sleep(1000);
            App.SettingsWindow.PressOkBtn();

            //повторон откроем окно
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.Summaries);
            App.SettingsWindow.SummariesTabControl.SelectSummaryTemplate("Шаблон сводки (по типам сеансов) 8.6");
            App.SettingsWindow.SummariesTabControl.UnCheckSms();
            App.SettingsWindow.PressOkBtn();

            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.ClickTabControl(SettingsWindowTabs.Summaries);
            Thread.Sleep(2000);
        }



        [Test]
        [Ignore("may use testing API ")]
        public void DocumentsTemplatesTabControl_GetSeancesGridCells()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.DocumentsTemplatesTabControl.SelectDocumentTemplate("Шаблон сводки (по типам сеансов) 8.6", DocumentTemplateType.Summary);
            Thread.Sleep(1000);
            App.SettingsWindow.DocumentsTemplatesTabControl.SelectDocumentTemplate("Шаблон сопроводительного документа 8.6", DocumentTemplateType.CoveringNote);
           
            var actual = App.SettingsWindow.DocumentsTemplatesTabControl.GetGridCells();


            AssertEx.PropertyValuesAreEquals(actual[2], new Dictionary<string, object>()
                {
                    { "Name", "ret"},
                    { "Type", "Сопроводительный документ"},
                });



        }



        [Test]
        [Ignore("Осюда можно брать API для сохранения шаблонов")]
        public void DocumentsTemplatesTabControl_CallCommand()
        {
            App.MainWindow.ClickSettingsButton();

            //сохранить шаблон
            App.SettingsWindow.DocumentsTemplatesTabControl.SelectDocumentTemplate("Шаблон сводки (по типам сеансов) 8.6", DocumentTemplateType.Summary);
            App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.SaveAs);
            App.SettingsWindow.DocumentsTemplatesTabControl.SaveTemplate(@"C:\Users\kornienko\Desktop\Template saved new.docx");


            //добавить шаблон 
            App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.Add);
            App.AddTemplateWindow.EnterFileName(@"C:\Users\kornienko\Desktop\Template saved new.docx");
            App.AddTemplateWindow.SetTemplateType(DocumentTemplateType.CoveringNote);
            App.AddTemplateWindow.EnterTemplateName("old");
            App.AddTemplateWindow.ClickBrowseButon();
            App.AddTemplateWindow.ClicOkButon();
            Thread.Sleep(2000);

            //еще один
            App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.Add);
            App.AddTemplateWindow.EnterFileName(@"C:\Users\kornienko\Desktop\Template saved new.docx");
            App.AddTemplateWindow.SetTemplateType(DocumentTemplateType.CoveringNote);
            App.AddTemplateWindow.EnterTemplateName("new");
            App.AddTemplateWindow.ClickBrowseButon();
            App.AddTemplateWindow.ClicOkButon();
            Thread.Sleep(2000);

            //редактирование
            App.SettingsWindow.DocumentsTemplatesTabControl.SelectDocumentTemplate("new", DocumentTemplateType.CoveringNote);
            Thread.Sleep(1000);
            App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.Edit);
            App.EditingTemplateWindow.EnterTemplateName("new2");
            App.EditingTemplateWindow.ClicOkButon();
            Thread.Sleep(2000);


            App.SettingsWindow.DocumentsTemplatesTabControl.SelectDocumentTemplate("new2", DocumentTemplateType.CoveringNote);
            Thread.Sleep(1000);
            App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.Delete);
            App.YesNoDialogWindow.ClickOk();

            //App.SettingsWindow.PressOkBtn();


            //App.SettingsWindow.DocumentsTemplatesTabControl.SelectDocumentTemplate("old", DocumentTemplateType.CoveringNote);
            //Thread.Sleep(1000);
            //App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.Delete);
            //App.YesNoDialogWindow.ClickOk();
            //Thread.Sleep(1000)
        }


        [Test]
        public void SaveAndAddNewTemplate()
        {
            App.MainWindow.ClickSettingsButton();


            //string file_name = @"C:\Users\kornienko\Desktop\Template saved supernew.docx";
            string file_name = @"C:\Users\kornienko\Template saved supernew.docx";

            //сохранить шаблон
            App.SettingsWindow.DocumentsTemplatesTabControl.SelectDocumentTemplate("Шаблон сводки (по типам сеансов) 8.6", DocumentTemplateType.Summary);
            App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.SaveAs);
            App.SettingsWindow.DocumentsTemplatesTabControl.SaveTemplate(file_name);


            //добавить шаблон 
            App.SettingsWindow.DocumentsTemplatesTabControl.CallCommand(TemplateCommands.Add);
            //App.AddTemplateWindow.ClickBrowseButon();
            App.AddTemplateWindow.SetTemplateType(DocumentTemplateType.CoveringNote);
            App.AddTemplateWindow.EnterTemplateName("supernew");
            //App.AddTemplateWindow.ClickTemplateName();
            App.AddTemplateWindow.EnterFileName(file_name);
            
            //App.AddTemplateWindow.ClickBrowseButon();
            App.AddTemplateWindow.ClicOkButon();
            Thread.Sleep(2000);


            App.SettingsWindow.PressOkBtn();
            Thread.Sleep(2000);
            
            //снести файл
            
            try
            {
                if (System.IO.File.Exists(file_name))
                {
                    System.IO.File.Delete(file_name);
                }
            }
            catch
            {
            }
        }



        //[Test]
        //подготовка к тестам
        //public void DeleteSummariesTest()
        //{

        //}


        [Test]
        public void UserFieldsControl_SetField1Text()
        {
            App.MainWindow.ClickSettingsButton();
            App.SettingsWindow.UserFieldsControl.SetField1Text("sdafdsf");
            App.SettingsWindow.UserFieldsControl.SetField2Text("sdafdsf");
            App.SettingsWindow.UserFieldsControl.SetField3Text("sdafdsf4324324");
            Thread.Sleep(2000);
        }


        [Test]
        [Description("Test for API")]
        public void ArchiveControl_GetArchivesList()
        {
             Thread.Sleep(3000);
             App.MainWindow.InformationalArchiveControl.TogglePresetButton();
             Thread.Sleep(2000);

             App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
             Thread.Sleep(1000);
             App.ExportSettingsWindow.SelectArchive("нестандартный архив");
             Thread.Sleep(1000);
             App.ExportSettingsWindow.SelectArchive("вообще нестандартный");
             
             var arc_list  = App.ExportSettingsWindow.GetArchivesList();

             Assert.AreEqual("Стандартный архив", arc_list[0]);

        }


        [Test]
        [Description("Test for API")]
        public void ArchiveControl_EditArchivesList()
        {
            Thread.Sleep(3000);
            App.MainWindow.InformationalArchiveControl.TogglePresetButton();
            Thread.Sleep(2000);

            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            Thread.Sleep(1000);
            App.ExportSettingsWindow.ClickAddButton();
            App.ArchiveInputBoxWindow.EnterArchiveSettingsName("new_testing_archive");
            App.ArchiveInputBoxWindow.ClickOk();

            App.ExportSettingsWindow.ClickAddButton();
            App.ArchiveInputBoxWindow.EnterArchiveSettingsName("supernew_testing_archive");
            App.ArchiveInputBoxWindow.ClickOk();

            App.ExportSettingsWindow.SelectArchive("new_testing_archive");
            App.ExportSettingsWindow.ClickDeleteButton();
            App.YesNoDialogWindow.ClickOk();
            Thread.Sleep(500);

            App.ExportSettingsWindow.SelectArchive("supernew_testing_archive");
            App.ExportSettingsWindow.ClickDeleteButton();
            App.YesNoDialogWindow.ClickOk();
            Thread.Sleep(500);

            var arc_list = App.ExportSettingsWindow.GetArchivesList();

            Assert.AreEqual("Стандартный архив", arc_list[0]);


            App.ExportSettingsWindow.SelectArchive("Стандартный архив");
            App.ExportSettingsWindow.SetArchiveFilePathText(@"C:\Users\kornienko\InformationalArchives");
            App.ExportSettingsWindow.UnCheckClearFolderBeforeCreatingArchive();
            Thread.Sleep(500);
            App.ExportSettingsWindow.CheckClearFolderBeforeCreatingArchive();
            Thread.Sleep(500);
            App.ExportSettingsWindow.UnCheckSplitOnChunks();
            Thread.Sleep(500);
            App.ExportSettingsWindow.CheckSplitOnChunks();
            Thread.Sleep(500);
            App.ExportSettingsWindow.SetSplitOnChunksSize("600");
            Thread.Sleep(500);
            App.ExportSettingsWindow.UnCheckCreateSubfolders();
            Thread.Sleep(500);
            App.ExportSettingsWindow.CheckCreateSubfolders();
            Thread.Sleep(500);
            App.ExportSettingsWindow.UnCheckCreateSeancesDescriptions();
            Thread.Sleep(500);
            App.ExportSettingsWindow.CheckCreateSeancesDescriptions();
            Thread.Sleep(500);
            App.ExportSettingsWindow.UnCheckCreateSeancesReestrInExcelFormat();
            Thread.Sleep(500);
            App.ExportSettingsWindow.CheckCreateSeancesReestrInExcelFormat();
            Thread.Sleep(500);
            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            Thread.Sleep(500);
            App.ExportSettingsWindow.CheckConvertSoundFiles();
            Thread.Sleep(500);
            App.ExportSettingsWindow.UnCheckRenameMediaFiles();
            Thread.Sleep(500);
            App.ExportSettingsWindow.CheckRenameMediaFiles();
            Thread.Sleep(500);
            App.ExportSettingsWindow.SetSoundFilesFormat(SoundFormat.WAV);
            Thread.Sleep(500);
            App.ExportSettingsWindow.SetSoundFilesFormat(SoundFormat.WSD);

            App.ExportSettingsWindow.ClickOkButton();


            //repeat open window
            App.MainWindow.InformationalArchiveControl.ClickEditFileExportButton();
            App.ExportSettingsWindow.ClickAddButton();
            App.ArchiveInputBoxWindow.EnterArchiveSettingsName("new_testing_archive");
            App.ArchiveInputBoxWindow.ClickOk();
            App.ExportSettingsWindow.SelectArchive("new_testing_archive");
            App.ExportSettingsWindow.UnCheckSplitOnChunks();
            App.ExportSettingsWindow.UnCheckConvertSoundFiles();
            App.ExportSettingsWindow.SetSoundFilesFormat(SoundFormat.WAV);
            App.ExportSettingsWindow.ClickOkButton();


        }






        public static class AssertEx
        {
            public static void PropertyValuesAreEquals(object actual, Dictionary<string, object> expected)
            {
                PropertyInfo[] properties = actual.GetType().GetProperties();

                foreach (var exp_kv in expected)
                {
                    object expectedValue = exp_kv.Value;
                    object actualValue = properties.FirstOrDefault(x => x.Name == exp_kv.Key).GetValue(actual, null);

                    if (!Equals(expectedValue, actualValue))
                        Assert.Fail("Property {0} does not match. Expected: {1} but was: {2}", exp_kv.Key, expectedValue, actualValue);
                }


            }

        }



        



    }



    public class HelperTests
    {

        [Test]
        public void DatesdsaaFormatting()
        {

            var strinfsdf =   Utils.StringFormat("#abc 1 #asdasd 2 ",
                new Dictionary<string, object>()
                {
                    {"abc",  "dsfafdsfsdfsdfs"},
                    {"asdasd",  "1233213"}
                }
                );

        }



        [Test]
        [Ignore("Helper")]
        public void DatesFormatting()
        {
            var _n_dt = new DateTime(2016, 4, 3, 1, 2, 1);
            var formatted = _n_dt.ToString("dd.MM.yyyy H:mm");
            var formatted2 = _n_dt.ToString("dd.MM.yyyy HH:mm:ss");

        }


        [Test]
        [Ignore("Helper")]
        public void Test_Phone_Pattern_3()
        {

            string pattern = @"^((\+7|8)[\s-]?)?(\(\d{3}\)|\d{3})[\s-]?\d{3}[\s-]?\d{2}[\s-]?\d{2}$";
            Regex regex = new Regex(pattern);

            List<string> phone_numbers = new List<string>()
            {
                "+7978 888 67 59", "+7978-888-67-59", "8916-222-69-35", "8916 222 69 35", "8916 222-69-35", "8916 222-69 35",
                "+7916 2226935", "+79162226935", "+79162226935", "+79046168628", "89009365229", "8 9876543212", "8-922-240-45-01",
                "+7 962 692 74 03", "8(962)6948078", "8-(962) 69480-78", "(916)507 05 66"
            };

            // (7)(913) 064 - 56 - 34
            foreach (var ph_n in phone_numbers)
            {
                Assert.True(regex.Match(ph_n).Success);
            }
        }

        [Test]
        [Ignore("Helper")]
        public void Test_Phone_Pattern_2()
        {

            //string pattern = @"\p{Sc}*(?<amount>\s?\d+[.,]?\d*)\p{Sc}*";
            //string replacement = "${amount}";
            //string input = "$16.32 12.19 £16.29 €18.29  €18,29";
            //string result = Regex.Replace(input, pattern, replacement);
            //Console.WriteLine(result);

            //       16.32 12.19 16.29 18.29  18,29

            //string pattern = @"^(?<country>(\+7|8)[\s-]?)?((?<city>\(\d{3}\)|\d{3})[\s-])?\d{3}[\s-]?\d{2}[\s-]?\d{2}$";

            string pattern = @"^(?<country>(\+7|8)[\s-]?)?(?<city>\(\d{3}\)|\d{3})[\s-]?(?<first_group>\d{3})[\s-]?(?<second_group>\d{2})[\s-]?(?<third_group>\d{2})";
            string replacement = "$(7)(${city})${first_group}-${second_group}-${third_group}";
            Regex regex = new Regex(pattern);

            List<string> phone_numbers = new List<string>()
            {
               "89009365229", 
               "8912 936 52 29",
            };

            // (7)(913) 064 - 56 - 34
            foreach (var ph_n in phone_numbers)
            {
                //Assert.True(regex.Match(ph_n).Success);
                var numb = Regex.Replace(ph_n, pattern, replacement);
            }
        }



        [Test]
        [Ignore("Helper test")]
        public void Helpertest()
        {
            //длительность разговора "01.01.2016 03:00:00 — 1:00:00"
            var SeanceDateTime = "01.01.2016 03:00:00 — 4:00:00";
            var talk_end = new DateTime(2016, 1, 1, 14, 0, 0);
            var talk_begin = new DateTime(2016, 1, 1, 10, 0, 0);

            var duration = string.Format("{0:h\\:mm\\:ss}", (talk_end - talk_begin));
            Regex regex = new Regex(@"—\s" + duration);
            var abc = regex.Match(SeanceDateTime).Success;

            //01.01.2016 03:00:00 — 4:00:00
            Assert.True(regex.Match(SeanceDateTime).Success);

            var TalkSMSInterlocutorInfo = "(7)(983) 087-89-24, Входящий";

            var act_talk_info = TalkSMSInterlocutorInfo.Replace("(7)", "8");
            string replace_pattern = @"([\(\)-]|\s)";
            regex = new Regex(replace_pattern);
            act_talk_info = regex.Replace(act_talk_info, "");



            //var exp_talk_info = String.Format("{0},{1}", talk.InterlocutorPhone, talk.dir == TalkPhoneDirection.Incoming ? "Входящий" : "Исходящий");

        }


        [Test]
        public void Helper2()
        {
            var _random = new Random();

            List<string> abc = new List<string>() { "JNV", "sadfdsf" };


            var rand_index = _random.Next(0, abc.Count);

            var d = abc.ElementAtOrDefault(rand_index + 1);
            var e = abc.ElementAtOrDefault(rand_index - 1);
        }

    }

}
