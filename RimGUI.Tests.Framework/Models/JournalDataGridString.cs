﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace RimGUI.Tests.Framework.Models
{
   
    public class JournalDataGridString
    {
        [Description("Дата и время создания")]
        public string DateAndtime { get; set; }
        [Description("Период")]
        public string Period { get; set; }
        [Description("Тип")]
        public string Type { get; set; }
        [Description("Номер сводки")]
        public string Number { get; set; }
        [Description("ОТМ")]
        public string OTM { get; set; }
        [Description("Исполнитель")]
        public string Executor { get; set; }
        [Description("Страниц")]
        public string Pages { get; set; }
        [Description("Инициатор")]
        public string Initiator { get; set; }
        [Description("Подразделение инициатора")]
        public string Ini_podrazd { get; set; }
        [Description("Дата создания")]
        public string Creation_date { get; set; }
        [Description("Время создания")]
        public string Creation_time { get; set; }
        [Description("Дата предоставления")]
        public string Take_date { get; set; }
        [Description("Расчистка")]
        public string Clened { get; set; }

    }
}
