﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;



namespace RimGUI.Tests.Framework.Models
{
    public class DocumentTemplatesInfo
    {
        [Description("Название")]
        public string Name { get; set; }
        [Description("Тип")]
        public string Type { get; set; }
    }
}
