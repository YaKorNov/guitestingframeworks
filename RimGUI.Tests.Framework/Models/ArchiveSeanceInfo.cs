﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;


namespace RimGUI.Tests.Framework.Models
{
    public class ArchiveSeanceInfo
    {
        [Description("Тип")]
        public string Type { get; set; }
        [Description("Дата")]
        public string Date { get; set; }
        [Description("Объект контроля")]
        public string ControlObject { get; set; }
        [Description("Собеседник")]
        public string Interlocutor { get; set; }
        [Description("Метки")]
        public string  Marks { get; set; }
        [Description("Длительность")]
        public string Duration { get; set; }
        [Description("Направление")]
        public string Direction { get; set; }
        [Description("Окончание")]
        public string EndDate { get; set; }
        [Description("ОТМ")]
        public string Shifr { get; set; }
        [Description("Псевдоним ОТМ")]
        public string Alias { get; set; }


       

        //public void SetFieldValueByDesc(object instance, string field_desc, object value)
        //{
        //    FieldInfo[] f_infos = instance.GetType().GetFields();


        //    foreach (FieldInfo _fi in f_infos)
        //    {
        //        DescriptionAttribute[] attributes =
        //        (DescriptionAttribute[])_fi.GetCustomAttributes(
        //        typeof(DescriptionAttribute),
        //        false);

        //        if (attributes != null &&
        //            attributes.Length > 0 && attributes[0].Description == field_desc)
        //                _fi.SetValue(this, value);
        //    }
        //}

    }
}
