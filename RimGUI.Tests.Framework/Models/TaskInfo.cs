﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RimGUI.Tests.Framework.Models
{
    public class TaskInfoShort
    {
        public string Shifr { get; set; }

        public string Name { get; set; } 

        public string Init { get; set; }


        public string SeansesCount { get; set; }
       

        public string SrokInfo { get; set; }
    }


    public class TaskInfo
    {
        public string Shifr { get; set; }

        public string Name { get; set; }

        public string SrokInfo { get; set; }

        public string ControlPoint { get; set; }


        public string Init { get; set; }

        public string Goal { get; set; }


        public string Orientir { get; set; }


        
    }
}
