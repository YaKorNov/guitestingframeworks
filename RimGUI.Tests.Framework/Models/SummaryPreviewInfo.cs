﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RimGUI.Tests.Framework.Models
{
    public class SummaryPreviewInfo : System.Object
    {
        public string SeanceDateTime { get; set; }
        public string TalkSMSInterlocutorInfo { get; set; }
        public string TalkSMSVideoStenoOrPlaceInfo { get; set; }


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            SummaryPreviewInfo p = obj as SummaryPreviewInfo;
            if ((System.Object)p == null)
            {
                return false;
            }


            // Return true if the fields match:
            return (SeanceDateTime == p.SeanceDateTime)
                && (TalkSMSInterlocutorInfo == p.TalkSMSInterlocutorInfo)
                && (TalkSMSVideoStenoOrPlaceInfo == p.TalkSMSVideoStenoOrPlaceInfo);
        }


        public bool Equals(SummaryPreviewInfo obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (SeanceDateTime == obj.SeanceDateTime)
                && (TalkSMSInterlocutorInfo == obj.TalkSMSInterlocutorInfo)
                && (TalkSMSVideoStenoOrPlaceInfo == obj.TalkSMSVideoStenoOrPlaceInfo);
        }

        public override int GetHashCode()
        {
            //в данном случае имеем в виду, что все сгенерированные сеансы уникальны по 
            //совокупности набора полей

            byte[] seance_asciiBytes = Encoding.ASCII.GetBytes(SeanceDateTime);
            byte[] TalkSMSInterlocutorInfo_asciiBytes = Encoding.ASCII.GetBytes(TalkSMSInterlocutorInfo);
            byte[] TalkSMSVideoStenoOrPlaceInfo_asciiBytes = Encoding.ASCII.GetBytes(TalkSMSVideoStenoOrPlaceInfo);

            int total = 0;

            foreach (var byte_s in seance_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in TalkSMSInterlocutorInfo_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in TalkSMSVideoStenoOrPlaceInfo_asciiBytes)
            {
                total += (int)byte_s;
            }

            return total;


        }

    }
}
