﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;



namespace RimGUI.Tests.Framework.Models
{
    public enum TaskStatus
    {
        [Description("Все")]
        AllTasks,
        [Description("Назначенные ОТМ")]
        AssignedTasks,
        [Description("Все ОТМ в обработке")]
        TasksInProcessing,

    }

    
    public enum ArchiveSeancesFilterType
    {
        [Description("Разговор")]
        Talk = 0,
        [Description("Смс")]
        Sms = 1,
        [Description("Место")]
        Place = 2,
        [Description("Видео")]
        Video = 3
    }


    public enum JournalSummaryVariants
    {
        [Description("Журнал итоговых материалов")]
        All,
        [Description("Журнал сводок")]
        Summaries,
        [Description("Журнал информационных архивов")]
        InformationArchives
    }


    public enum SearchFilterVariant
    {
        [Description("Все колонки")]
        AllColumns,
        [Description("ОТМ")]
        Otm,
        [Description("Инициатор")]
        Initiator,
        [Description("Подразделение инициатора")]
        InitiatorDepartment,
        [Description("Номер сводки")]
        SerialNumber
    }


    public enum SummaryJournalContextMenuCommand
    {
        [Description("Перепечатать")]
        Reprint,
        [Description("Удалить запись о сводке")]
        DeleteSummary,
        [Description("Установить отметку о предоставлении")]
        SetMarkProvided,
        [Description("Снять отметку о предоставлении")]
        UnSetMarkProvided,
    }


    public enum SettingsWindowTabs
    {
        [Description("Общие")]
        General,
        [Description("Сводки")]
        Summaries,
        [Description("Шаблоны документов")]
        DocumentTemplates,
        [Description("Пользовательские поля")]
        UserFields
    }

    public enum DocumentTemplateType
    {
        [Description("Сводка")]
        Summary,
        [Description("Сопроводительный документ")]
        CoveringNote
    }



    public enum TemplateCommands
    {
        [Description("Создать")]
        Add,
        [Description("Изменить")]
        Edit,
        [Description("Сохранить как...")]
        SaveAs,
        [Description("Удалить")]
        Delete

    }


    public enum SoundFormat
    {
        [Description("Wav")]
        WAV,
        [Description("Wsd")]
        WSD
    }


    public enum ExcelArchiveFields
    {
        [Description("Идентификатор сеанса")]
        id = 0,
        [Description("Шифр")]
        Shifr = 1,
        [Description("Имя файла")]
        FileName = 2,
        [Description("Размер файла (байт)")]
        FileSize = 3,
        [Description("Время начала")]
        BeginDate = 4,
        [Description("Длительность")]
        Duraton = 5,
        [Description("Телефон 1")]
        Phone1 = 6,
        [Description("Телефон 2")]
        Phone2 = 7,
        [Description("Направление")]
        Directon = 8,
        [Description("Метка")]
        Mark = 9,
        [Description("Комментарий метки")]
        MarkComment = 10,
        [Description("Контрольная сумма")]
        CRC = 11,
    }
  


}
