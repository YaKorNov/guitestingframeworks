﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.TestAttributes;
using ArtOfTest.WebAii.TestTemplates;
using ArtOfTest.WebAii.Win32;
using ArtOfTest.WebAii.Win32.Dialogs;

using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

using ArtOfTest.Common;

using NUnit.Framework;
using Core = NUnit.Core;

using System.Threading;



namespace AnalyticalReport
{
    /// <summary>
    /// Summary description for TelerikNUnitTest1
    /// </summary>
    [TestFixture]
    public class AnalyticalReport : BaseTest
    {

        private string startPage = "http://192.168.9.165/";


        #region [Setup / TearDown]

        /// <summary>
        /// Initialization for each test.
        /// </summary>
        [SetUp]
        public void MyTestInitialize()
        {
            #region WebAii Initialization

            // Initializes WebAii manager to be used by the test case.
            // If a WebAii configuration section exists, settings will be
            // loaded from it. Otherwise, will create a default settings
            // object with system defaults.
            //
            // Note: We are passing in a delegate to the NUnit's TestContext.Out.
            // WriteLine() method. This way any logging
            // done from WebAii (i.e. Manager.Log.WriteLine()) is
            // automatically logged to same output as NUnit.
            //
            // If you do not care about unifying the log, then you can simply
            // initialize the test by calling Initialize() with no parameters;
            // that will cause the log location to be picked up from the config
            // file if it exists or will use the default system settings.
            // You can also use Initialize(LogLocation) to set a specific log
            // location for this test.

            // Pass in 'true' to recycle the browser between test methods

            //Settings settings = GetSettings();

            //// Override the settings you want. For example:
            //settings.Web.DefaultBrowser = BrowserType.Chrome;

            //// Now call Initialize again with your updated settings object
            //Initialize(settings, new TestContextWriteLine(Console.Out.WriteLine));
            Settings settings = GetSettings();

            // Override the settings you want. For example:
            settings.Web.DefaultBrowser = BrowserType.Chrome;
            settings.AnnotateExecution = true;

            // Now call Initialize again with your updated settings object
            Initialize(settings, new TestContextWriteLine(Console.Out.WriteLine));

            

            //Initialize(false, new TestContextWriteLine(Console.Out.WriteLine));

            // If you need to override any other settings coming from the
            // config section or you don't have a config section, you can
            // comment the 'Initialize' line above and instead use the
            // following:

            /*

            // This will get a new Settings object. If a configuration
            // section exists, then settings from that section will be
            // loaded

            Settings settings = GetSettings();

            // Override the settings you want. For example:
            settings.Web.DefaultBrowser = BrowserType.FireFox;

            // Now call Initialize again with your updated settings object
            Initialize(settings, new TestContextWriteLine(Console.Out.WriteLine));

            */

            #endregion

            //
            // Place any additional initialization here
            //
        }

        /// <summary>
        /// Clean up after each test.
        /// </summary>
        [TearDown]
        public void MyTestCleanUp()
        {
            //
            // Place any additional cleanup here
            //

            #region WebAii CleanUp

            // Shuts down WebAii manager and closes all browsers currently running
            this.CleanUp();

            #endregion
        }

        /// <summary>
        /// Called after all tests in this class are executed.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureCleanup()
        {
            // This will shut down all browsers if
            // recycleBrowser is turned on. Else
            // will do nothing.
            ShutDown();
        }

        #endregion


        [Test]
        public void Search_One_Iss_Positive()
        {


            Manager.LaunchNewBrowser();
            Annotator myAnnotator = new Annotator(ActiveBrowser);
            ActiveBrowser.NavigateTo(startPage);


            //var text_box = ActiveBrowser.Find.ByXPath("//input[@type='text']");
            var search_box = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=input", "ng-model=searchString"), 10000, false);

            //var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input", "placeholder=Поиск"));
            //var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input","ng-model=searchString"));


            //var button = ActiveBrowser.Find.ByXPath("(//button[@type='submit'])[2]");
            //var search_iss_btn = ActiveBrowser.Find.ByExpression(new HtmlFindExpression("tagname=button", "ng-click=~searchIss"));

            var search_iss_btn = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=button", "ng-click=~searchIss"), 10000, false);


            //TODO РґРѕР±Р°РІРёС‚СЊ РіРµРЅРµСЂР°С†РёСЋ РґР°РЅРЅС‹С…!!!!
            var iss_list = new List<string>(){
                "79061953013", "79051930000", "79039436951"
            };
            foreach (var iss in iss_list)
            {
                ActiveBrowser.Actions.SetText(search_box, iss);
                var anno_step = String.Format("Ввели поисковую строку {0}", iss);
                Log.WriteLine(LogType.Information, anno_step);
                myAnnotator.Annotate(anno_step);
                
                ActiveBrowser.Actions.Click(search_iss_btn);
                anno_step = String.Format("Кликнули по кнопке поиска {0}", iss);
                Log.WriteLine(LogType.Information, anno_step);
                Log.CaptureBrowser(Manager.ActiveBrowser);
                myAnnotator.Annotate(anno_step);


                var search_res = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult", String.Format("InnerText=~{0}", iss)), 20000, false);

                Assert.IsNotNull(search_res);
            }

            


        }



        [Test]
        public void Search_One_Iss_Negative()
        {


            Manager.LaunchNewBrowser();
            ActiveBrowser.NavigateTo(startPage);


            //var text_box = ActiveBrowser.Find.ByXPath("//input[@type='text']");
            //var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input", "placeholder=РџРѕРёСЃРє"));
            var search_box = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=input", "ng-model=searchString"), 10000, false);
			//var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input","ng-model=searchString"));


            //var button = ActiveBrowser.Find.ByXPath("(//button[@type='submit'])[2]");
            var search_iss_btn = ActiveBrowser.Find.ByExpression(new HtmlFindExpression("tagname=button", "ng-click=~searchIss"));

            //TODO РґРѕР±Р°РІРёС‚СЊ РіРµРЅРµСЂР°С†РёСЋ РґР°РЅРЅС‹С…!!!!
            var iss_list = new List<string>(){
                "79061953014", "79051930020"
            };
            foreach (var iss in iss_list)
            {
                ActiveBrowser.Actions.SetText(search_box, iss);
                var anno_step = String.Format("Ввели поисковую строку {0}", iss);
                Log.WriteLine(LogType.Information, anno_step);
                
                ActiveBrowser.Actions.Click(search_iss_btn);
                anno_step = String.Format("Кликнули по кнопке поиска {0}", iss);
                Log.WriteLine(LogType.Information, anno_step);

                Assert.Throws<ArtOfTest.Common.Exceptions.FindElementException>(() => ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult", String.Format("InnerText=РўРµР»РµС„РѕРЅ: {0}", iss)), 10000, false));
               
            }




        }




        [Test]
        public void Search_Iss_by_Asterisk()
        {

            Manager.LaunchNewBrowser();
            ActiveBrowser.NavigateTo(startPage);


            //var text_box = ActiveBrowser.Find.ByXPath("//input[@type='text']");
            //var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input", "placeholder=РџРѕРёСЃРє"));
            var search_box = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=input", "ng-model=searchString"), 10000, false);
			//var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input","ng-model=searchString"));


            //var button = ActiveBrowser.Find.ByXPath("(//button[@type='submit'])[2]");
            var search_iss_btn = ActiveBrowser.Find.ByExpression(new HtmlFindExpression("tagname=button", "ng-click=~searchIss"));

            ActiveBrowser.Actions.SetText(search_box, "*");
            ActiveBrowser.Actions.Click(search_iss_btn);

            
            var search_res = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult"), 10000, false);

            //РїСЂРѕРІРµСЂРёРј, С‡С‚Рѕ Р·Р°РіСЂСѓР¶Р°РµС‚СЃСЏ РјРЅРѕРіРѕ СЌР»РµРјРµРЅС‚РѕРІ
            Assert.IsTrue(ActiveBrowser.Find.AllByExpression(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult")).Count > 1);


            //TODO РѕРїСЏС‚СЊ-С‚Р°РєРё РЅСѓР¶РЅР° РіРµРЅРµСЂР°С†РёСЏ РґР°РЅРЅС‹С…, 
            //С‚РѕРіРґР° РјРѕР¶РЅРѕ Р±СѓРґРµС‚ РЅР°РїРёСЃР°С‚СЊ С‚Р°Рє, Рє РїСЂРёРјРµСЂСѓ
            //Assert.IsTrue(ActiveBrowser.Find.AllByExpression(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult")).Count = 20);

        }



        [Test]
        public void Search_One_PhizItem_Positive()
        {

            Manager.LaunchNewBrowser();
            ActiveBrowser.NavigateTo(startPage);

            //var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input", "placeholder=РџРѕРёСЃРє"));
            var search_box = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=input", "ng-model=searchString"), 10000, false);
			var search_person_btn = ActiveBrowser.Find.ByExpression(new HtmlFindExpression("tagname=button", "ng-click=~searchPerson"));


            //TODO РґРѕР±Р°РІРёС‚СЊ РіРµРЅРµСЂР°С†РёСЋ РґР°РЅРЅС‹С…!!!!
            var phiz_list = new List<string>(){
                "Баклажановый Алексей", "кружка", "1999"
            };

            foreach (var phiz in phiz_list)
            {
                ActiveBrowser.Actions.SetText(search_box, phiz);
                ActiveBrowser.Actions.Click(search_person_btn);

                //РґР°Р»РµРµ СЂР°Р·РјРµС‚РєР° РїРѕРґРіСЂСѓР¶Р°РµС‚СЃСЏ ajax-РѕРј

                var search_res = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult", String.Format("InnerText=~{0}", phiz)), 10000, false);

                Assert.IsNotNull(search_res);

            }

            

        }



        [Test]
        public void Search_One_PhizItem_Negative()
        {

            Manager.LaunchNewBrowser();
            ActiveBrowser.NavigateTo(startPage);

            //var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input", "placeholder=РџРѕРёСЃРє"));
            var search_box = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=input", "ng-model=searchString"), 10000, false);
			var search_person_btn = ActiveBrowser.Find.ByExpression(new HtmlFindExpression("tagname=button", "ng-click=~searchPerson"));


            //TODO РґРѕР±Р°РІРёС‚СЊ РіРµРЅРµСЂР°С†РёСЋ РґР°РЅРЅС‹С…!!!!
            var phiz_list = new List<string>(){
                "аываыываыфваыфваыаыва", "ываыфваыфваыфваываыва ываываыв s"
            };

            foreach (var phiz in phiz_list)
            {
                ActiveBrowser.Actions.SetText(search_box, phiz);
                ActiveBrowser.Actions.Click(search_person_btn);

                Assert.Throws<ArtOfTest.Common.Exceptions.FindElementException>(() => ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult", String.Format("InnerText=~{0}", phiz)), 10000, false));
               

            }



        }




        
        [Test]
        public void AnalyticalReport_Open()
        {

            Manager.LaunchNewBrowser();
            ActiveBrowser.NavigateTo(startPage);

            //var search_box = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=input", "placeholder=РџРѕРёСЃРє"), 10000, false);
			var search_box = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=input", "ng-model=searchString"), 10000, false);

            //var search_box = ActiveBrowser.Find.ByExpression<HtmlInputText>(new HtmlFindExpression("tagname=input", "placeholder=РџРѕРёСЃРє"));
            var search_iss_btn = ActiveBrowser.Find.ByExpression(new HtmlFindExpression("tagname=button", "ng-click=~searchIss"));

            var iss = "79061953013";


            ActiveBrowser.Actions.SetText(search_box, iss);
            ActiveBrowser.Actions.Click(search_iss_btn);

            var search_iss = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=a", "ng-repeat=~searchResult", String.Format("InnerText=~{0}", iss)), 10000, false);
            ActiveBrowser.Actions.Click(search_iss);

            //пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ, пїЅпїЅпїЅпїЅпїЅ пїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅ
            var phone_glyph = ActiveBrowser.WaitForElement(new HtmlFindExpression("tagname=span", "class=~phone"), 10000, false);
           
           
            Element result_view_phone_number;
            try
            {
                result_view_phone_number = phone_glyph.Parent.Children.Where(el => el.TagName == "span").ToList()[2];
                Assert.AreEqual(iss, result_view_phone_number.InnerMarkup);
            }
            catch(Exception ex)
            {
                //Р»РѕРі
                
            }



        }

    
    }
}
