﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using CommonGenerator.Moj.Enums;
using DataGenerator.Mtb.Tables;
using CommonGenerator;
using CommonGenerator.Mtb;
using CommonGenerator.Moj;
using DataGenerator.Mtb;
using DataGenerator.Moj;
using BLToolkit.Data.DataProvider;
using DataGenerator.Mtb.MtbBuilders;
using DataGenerator.Moj.MojBuilders;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;
using DataGenerator.Mtb.Tables;
using System.Threading;



namespace BrokerImitator
{

    public class DbDataModel
    {
        private MtbDataBuilder _mtbDataBuilder;

        private MojDataBuilder _mojDataBuilder;

        private Random _rand;
        
        public IList<Task> Tasks { get; set; }
        public IList<AutoTask> AutoTasks { get; set; }
        public IList<TaskGrant> TaskGrants { get; set; }



        public DbDataModel(string db_host, string user, string password)
        {
          
            AutoTasks = new List<AutoTask>();
            Tasks = new List<Task>();
            TaskGrants = new List<TaskGrant>();

            _rand = new Random();

            //init data builders
            var _mtb_settings = new MtbSettings()
            {
                Address = db_host,
                Database = "MagTalksBase",
                Login = user,
                Password = password
            };
            var _moj_settings = new MojSettings()
            {
                Address = db_host,
                Database = "MagObjectsJournal",
                Login = user,
                Password = password
            };

            _mtbDataBuilder = new MtbDataBuilder(new MtbDbManager(new SqlDataProvider(), _mtb_settings), MagVersion.ver_8_8);
            _mojDataBuilder = new MojDataBuilder(new MojDbManager(new SqlDataProvider(), _moj_settings));
        }

        public Task GetTaskByCipherParts(string meropr, string vid_object, string task_number, string task_year)
        {
            return Tasks.Where(task => task.Meropr == meropr &&
                task.VidObject == vid_object && task.Task_number == task_number && task.Task_year == task_year).First();
        }


        public List<Task> NonExistedInDbtasks {
            get
            {
                return Tasks.Where(task => task.existsInDB == false).ToList();
            }

        }


        public List<AutoTask> NonExistedInDbAutotasks
        {
            get
            {
                return AutoTasks.Where(a_task => a_task.existsInDB == false).ToList();
            }
        }


        public List<TaskGrant> NonExistedInDbTasksGrants
        {
            get
            {
                return TaskGrants.Where(t_grant => t_grant.existsInDB == false).ToList();
            }
        }


        //очистка моделей
        public void FullDbModelCleanup()
        {
            AutoTasks.Clear();
            Tasks.Clear();
            TaskGrants.Clear();
        }

        //очистка  непосредственно БД
        public void FullDBCleanup()
        {
            _mtbDataBuilder.FullCleanUpMtb();
            _mojDataBuilder.FullCleanUpMoj();

        }

        
        public void ReadTasksAutotasks(List<string> phone_numbers)
        {

            FullDbModelCleanup();

            var _mtb_tasks_list = _mtbDataBuilder.GetTaskList();


            foreach (var _mtb_task in _mtb_tasks_list)
            {
                //часть задания из MagTalksBase
                var task = new Task()
                {
                    //нужен id задания
                    TaskId = _mtb_task.TaskId,
                    IsCompleted = _mtb_task.CategoryId == (int)Categories.Completed ? true : false,
                    Init_organization = _mtb_task.Initiator,
                    Guid = _mtb_task.TaskGuid,
                    //задание существует в базе
                    existsInDB = true

                };

                task.SetCipherParts(_mtb_task.ObjShifr);

                //из MagObjectsJournal нужен номер объекта контроля
                //чтение в цикле - плохо
                var moj_task = _mojDataBuilder.GetTaskByGuid(_mtb_task.TaskGuid);

                if (moj_task != null)
                {
                    task.PhoneControl = moj_task.phone_otm;
                }


                Tasks.Add(task);
            }



            //читаем автозадания, привязки

            var mtb_t_auto_list = _mtbDataBuilder.SelectAutoTasks();
            var t_auto_and_phones = _mtbDataBuilder.GetControlPhonesList();


            foreach (var mtb_t_auto in mtb_t_auto_list)
            {

                var task_auto = new AutoTask()
                {
                    id = mtb_t_auto.TaskAutoId,
                    autotask_number = mtb_t_auto.RegNumber,
                    device_id = mtb_t_auto.DeviceId,
                    channel_id = mtb_t_auto.Channel,
                    creation_date = mtb_t_auto.CreationTime,
                    existsInDB = true
                };

                var control_phone = "";
                //пробуем получить номер контроля
                if (t_auto_and_phones.TryGetValue(mtb_t_auto.TaskAutoId, out control_phone))
                {
                    task_auto.PhoneControl = control_phone;
                }
                else
                {
                    //не удалось найти в БД занчение номера контроля, генерим на ходу
                    task_auto.PhoneControl = phone_numbers[_rand.Next(0, phone_numbers.Count)];
                }

                AutoTasks.Add(task_auto);

            }


            var mtb_tg_list = _mtbDataBuilder.SelectTaskGrants();

            foreach (var mtb_tg in mtb_tg_list)
            {
                var task_grant = new TaskGrant()
                {
                    task = Tasks.First(tsk => tsk.TaskId == mtb_tg.TaskOperId),
                    t_auto = AutoTasks.First(a_tsk => a_tsk.id == mtb_tg.TaskAutoId),
                    auto_start = mtb_tg.AutoStart,
                    existsInDB = true
                };

                TaskGrants.Add(task_grant);

            }


        }


        public void Save()
        {
            foreach (var task in NonExistedInDbtasks)
            {
                var t_moj = _mojDataBuilder.CreateTaskMoj(
                task.Otdel, task.Init_organization, task.Init_podrazd, task.Init_Famini,
                task.Init_phoneA, task.Init_phoneB, task.Meropr, task.VidObject, task.Task_number, task.Task_year,
                task.BeginDate, null, task.PhoneControl, null, null, task.IsCompleted ? Categories.Completed : Categories.Executed, SecurityLevel.Normal,
                "", "ЖИТЕЛЬСТВА", task.TaskSrok, "АСП", "232",
                new List<TaskTypeControl>() { TaskTypeControl.DVO, TaskTypeControl.Place, TaskTypeControl.Sms, TaskTypeControl.Talk, TaskTypeControl.Video });

                ////ориентировку
                _mojDataBuilder.AddOrientation(t_moj, task.Orient);
                ////цель проведения - проверить причастность
                _mojDataBuilder.AddTaskTarget(t_moj, Utils.GetEnumDescription(task.Goal));


                var t_mtb = _mtbDataBuilder.SetTaskMtb(task.BeginDate, task.TaskSrok, task.Meropr, task.VidObject, task.Task_number, task.Task_year, task.IsCompleted ? Categories.Completed : Categories.Executed,
                task.Init_Famini, task.Orient, Utils.GetEnumDescription(task.Goal), DataTypes.Unsigned,
                new List<TaskTypes>() { TaskTypes.DVO, TaskTypes.Place, TaskTypes.Sms, TaskTypes.Talk, TaskTypes.Video },
                SecurityLevel.Normal, _mojDataBuilder.GetTaskGuidById(t_moj.task_id),
                task.ArcKeepPlus, task.ArcKeepMinus, task.ArcKeepNotMark);

                task.TaskId = t_mtb.TaskId;
                task.existsInDB = true;

            }


            foreach (var a_task in NonExistedInDbAutotasks)
            {
                var ta = _mtbDataBuilder.CreateTaskAuto(a_task.autotask_number, SourceTypes.MagFlowBroker, a_task.device_id, a_task.channel_id, a_task.creation_date);
                a_task.id = ta.TaskAutoId;
                a_task.existsInDB = true;
            }


            foreach (var tg in NonExistedInDbTasksGrants)
            {
                var mtb_tg = _mtbDataBuilder.CreateTaskGrant(tg.t_auto.id, tg.task.TaskId, tg.t_auto.creation_date.AddHours(4), null);
                tg.existsInDB = true;

            }




        }


        public string GetRandomStringParametrValueByName(TaskGeneratorStringParameterType param_type)
        {
            var string_list = GetStringParametrByName(param_type);
            return string_list[_rand.Next(0, string_list.Count)];
        }

        public TaskTarget GetRandomTaskTarget()
        {
            var tt = Enum.GetValues(typeof(TaskTarget));
            //Random random = new Random();
            return (TaskTarget)tt.GetValue(_rand.Next(tt.Length));
        }

        public static IList<string> GetStringParametrByName(TaskGeneratorStringParameterType param_type)
        {
            List<string> string_list = new List<string>();

            switch (param_type)
            {
                case TaskGeneratorStringParameterType.Initiator:
                    string_list = new List<string> { "МВД", "ФСБ" };
                    break;
                case TaskGeneratorStringParameterType.Otdel:
                    string_list = new List<string> { "МВД", "ФСБ" };
                    break;
                case TaskGeneratorStringParameterType.Podrazd:
                    string_list = new List<string> { "Отдел Н", "Отдел К" };
                    break;
                case TaskGeneratorStringParameterType.InitFamIni:
                    string_list = new List<string> { "Петров Иван Анатольевич", "Иванов Дмитрий Сергеевич" };
                    break;
                case TaskGeneratorStringParameterType.Phone:
                    string_list = new List<string> { "89130645634", "89130745634" };
                    break;
                //case TaskGeneratorStringParameterType.Meropr:
                //    string_list = new List<string> { "ПТП", "ОТП" };
                //    break;
                //case TaskGeneratorStringParameterType.VidObject:
                //    string_list = new List<string> { "30", "40", "50" };
                //    break;
                case TaskGeneratorStringParameterType.Orient:
                    string_list = new List<string> { "рост средний", "волосы рыжие" };
                    break;
                //case TaskGeneratorStringParameterType.SmsText:
                //    string_list = new List<string> { "привет, все готово", "когда начинаем?" };
                //    break;
                case TaskGeneratorStringParameterType.StenoText:
                    string_list = new List<string> { "ты меня слышишь?", "я полковник на белом коне" };
                    break;
            }

            return string_list;
        }



        public string GetMagFullPath()
        {
            return _mtbDataBuilder.GetDefaultUserOption("beginsDefMagFilePath");
        }




        public int GenerateTasks(List<string> shifr_list, DateTime first_task_date, int task_interval, int task_srok, List<string> phone_numbers)
        {

            var tasks_generated = 0;

            foreach (var task_shifr in shifr_list)
            {

                //проверка, существует ли задание в базе
                if (Tasks.FirstOrDefault(tsk => tsk.TaskShifr == task_shifr) != null)
                    continue;


                var taskBegin = first_task_date.AddDays((shifr_list.IndexOf(task_shifr) - 1) * task_interval);
                
                var task = new Task();
                task.Otdel = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Otdel);
                task.Init_organization = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Initiator);
                task.Init_podrazd = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Podrazd);
                task.Init_Famini = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.InitFamIni);
                task.Init_phoneA = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.Init_phoneB = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Phone);
                task.BeginDate = taskBegin;
                task.TaskSrok = (short)task_srok;
                task.PhoneControl = phone_numbers[_rand.Next(0, phone_numbers.Count)];
                task.IMSI = null;
                task.IMEI = null;
                task.Orient = GetRandomStringParametrValueByName(TaskGeneratorStringParameterType.Orient);
                task.Goal = GetRandomTaskTarget();

                task.IsCompleted =  false;

                task.SetCipherParts(task_shifr);

                //устанавливаем фдаг, т.к. генерируем новое задание, ранее не сохраненное в базу
                task.existsInDB = false;

                Tasks.Add(task);

                tasks_generated++;
            }

            return tasks_generated;
        }

        
        public int GenerateAutoTasks_LinearDistribution(int auto_TasksCount, int interval,
                    DateTime first_creation_date, List<int> devicesList, List<int> channelList, List<string> phones)
        {

            var current_number = 1;
            var a_number = "";

            var autotasks_generated = 0;

            //есть ли задания, которые не существуют в БД,
            //для которых требуется генерить автотаски?
            if (Tasks.Where(task => !task.existsInDB).Count<Task>() == 0)
                auto_TasksCount = 0;

            
            for (int i = 1; i <= Math.Max(auto_TasksCount, phones.Count) ; i++)
            {
                a_number = String.Format("1_{0}", current_number);

                //пропускаем номера существующих в БД автозаданий
                while (AutoTasks.FirstOrDefault(a_tsk => a_tsk.autotask_number == a_number) != null)
                {
                    current_number++;
                    a_number = String.Format("1_{0}", current_number);
                }


                var a_task = new AutoTask();
                a_task.creation_date = first_creation_date.AddHours((i - 1) * interval);

                a_task.autotask_number = a_number;
                a_task.device_id = devicesList[_rand.Next(0, devicesList.Count)];
                a_task.channel_id = channelList[_rand.Next(0, channelList.Count)];


                a_task.PhoneControl = phones[i % phones.Count];

                AutoTasks.Add(a_task);

                autotasks_generated++;
            }

            return autotasks_generated;
        }



        public List<string> GetControlPhones_ForUnsavedTasks()
        {
            return Tasks.Where(task => task.existsInDB == false).Select(task => task.PhoneControl).Distinct().ToList();
        }

        public Dictionary<string, List<AutoTask>> GetAutotaskControlPhones()
        {
            Dictionary<string, List<AutoTask>> res = new Dictionary<string, List<AutoTask>>();

            foreach (var phone in AutoTasks.Select(a_task => a_task.PhoneControl).Distinct())
            {
                res.Add(phone, AutoTasks.Where(a_task => a_task.PhoneControl == phone).ToList());
            }

            return res;

        }


        public int  TaskConnect(int granted_min, int granted_max)
        {
            var not_saved_tasks = Tasks.Where(task => task.existsInDB == false).ToList();

            var autotasks_by_phones = GetAutotaskControlPhones();

            var granted_count = 0;


            foreach (var task in not_saved_tasks)
            {
                var a_tasks = autotasks_by_phones[task.PhoneControl].Take(_rand.Next(granted_min, granted_max));

                foreach (var a_task in a_tasks)
                {
                    TaskGrants.Add(
                    new TaskGrant()
                    {
                        task = task,
                        t_auto = a_task,
                        auto_start = a_task.creation_date.AddHours(4),
                        existsInDB = false
                    });

                    granted_count++;
                }
                
            }

            return granted_count;

        }



        public void CreateTalkWithPhones(int autotask_id, DateTime begin_date, DateTime end_date, TalkPhoneDirection dir, byte[] talk_file, string phone_control, string interlocutor_phone)
        {
            var _talk = _mtbDataBuilder.CreateTalkMtb(autotask_id, begin_date, end_date, Arc_flags.FileWrited, dir, 0, talk_file);
            
            _mtbDataBuilder.CreateTalkPhones(_talk, phone_control, interlocutor_phone);
        }

        public void CreateStatTalkWithPhones(int autotask_id, DateTime begin_date, DateTime end_date, TalkPhoneDirection dir, string phone_control, string interlocutor_phone)
        {
            var _talk = _mtbDataBuilder.CreateTalkMtb(autotask_id, begin_date, end_date, Arc_flags.Statcontrol, dir, 0, new byte[] { });
            
            _mtbDataBuilder.CreateTalkPhones(_talk, phone_control, interlocutor_phone);
        }




    }


    public class Task
    {

        public Task()
        {
            
            IsCompleted = false;

            ArcKeepPlus = 10;

            ArcKeepMinus = 20;

            ArcKeepNotMark = 30;

        }

        public int TaskId { get; set; }

        public Guid Guid { get; set; }

        public bool IsCompleted { get; set; }

        //части шифра
        public string Meropr { get; set; }

        public string VidObject { get; set; }

        public string Task_number { get; set; }

        public string Task_year { get; set; }
        //части шифра

        public DateTime BeginDate { get; set; }

        public short TaskSrok { get; set; }

        public string PhoneControl { get; set; }



        public string Otdel { get; set; }
        public string Init_organization { get; set; }
        public string Init_podrazd { get; set; }
        public string Init_Famini { get; set; }
        public string Init_phoneA { get; set; }
        public string Init_phoneB { get; set; }

        public string IMSI { get; set; }

        public string IMEI { get; set; }

        public string Orient { get; set; }

        public TaskTarget Goal { get; set; }

        public int ArcKeepPlus { get; set; }

        public int ArcKeepMinus { get; set; }

        public int ArcKeepNotMark { get; set; }

        public string TaskShifr { get { return String.Format("{0}-{1}-{2}-{3}", Meropr, VidObject, Task_number, Task_year); } }
        
        //флаг признака существоания в БД
        public bool existsInDB { get; set; }


        public void SetCipherParts(string cipher)
        {
            var parts = cipher.Split('-');

            Meropr = parts[0];
            VidObject = parts[1];
            Task_number = parts[2];
            Task_year = parts[3];
            
        }

    }



    public class AutoTask
    {

        public int id { get; set; }
        public string autotask_number { get; set; }
        public int device_id { get; set; }
        public int channel_id { get; set; }
        public DateTime creation_date { get; set; }
        public List<Talk> talks_list { get; set; }
        //флаг признака существования в БД
        public bool existsInDB { get; set; }
        
        //в данном случае искусственный атрибут, предполагается, что номера
        //объекта контроля у разговоров и у задания совпадают
        public string PhoneControl { get; set; }

        public AutoTask()
        {
            talks_list = new List<Talk>();
        }
    }


    public class TaskGrant
    {
        public Task task { get; set; }
        public AutoTask t_auto { get; set; }
        public DateTime auto_start { get; set; }
        //флаг признака существования в БД
        public bool existsInDB { get; set; }
    }



    public interface ISeance
    {
        //public Seance() { }

    }



    public class Talk : System.Object, ISeance
    {

        public Talk()
        {
            suppress_remove = 0;
        }


        public AutoTask autotask { get; set; }
        public int seanceId { get; set; }
        public DateTime begin { get; set; }
        public DateTime end { get; set; }
        public TalkPhoneDirection dir { get; set; }

        public string ControlObjectPhone { get; set; }
        public string InterlocutorPhone { get; set; }
        //public bool createAudioFile { get; set; }
        
        public Steno Steno { get; set; }

        //0 - удаление разрешено. При значении > 0 - счетчик количества заданий, в рамках которых сеанс должен храниться.
        public byte suppress_remove { get; set; }

        //флаг признака существования в БД
        public bool existsInDB { get; set; }


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            Talk t = obj as Talk;
            if ((System.Object)t == null)
            {
                return false;
            }


            // учитывая, что все генерируемые даты уникальны
            return (autotask == t.autotask)
                && (begin == t.begin);
        }


        public bool Equals(Talk obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // учитывая, что все генерируемые даты уникальны
            return (autotask == obj.autotask)
                && (begin == obj.begin);
        }

        public override int GetHashCode()
        {
            //в данном случае имеем в виду, что все сгенерированные сеансы уникальны по 
            //совокупности набора полей

            byte[] task_asciiBytes = Encoding.ASCII.GetBytes(autotask.ToString());
            byte[] begin_asciiBytes = Encoding.ASCII.GetBytes(begin.ToString());

            int total = 0;

            foreach (var byte_s in task_asciiBytes)
            {
                total += (int)byte_s;
            }

            foreach (var byte_s in begin_asciiBytes)
            {
                total += (int)byte_s;
            }


            return total;


        }



    }


    public class Steno
    {
        
        public Texts8.Texts8State State { get; set; }
        public string text { get; set; }
        
    }


    public enum TaskGeneratorStringParameterType
    {
        Initiator,
        Otdel,
        Podrazd,
        InitFamIni,
        Phone,
        //Meropr,
        //VidObject,
        Orient,
        //SmsText,
        StenoText
    }


}




