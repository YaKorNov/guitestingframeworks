﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices;
using CommonGenerator;
using CommonGenerator.Mtb;
using CommonGenerator.Moj;
using DataGenerator.Mtb;
using DataGenerator.Moj;
using BLToolkit.Data.DataProvider;
using DataGenerator.Mtb.MtbBuilders;
using DataGenerator.Moj.MojBuilders;
using CommonGenerator.Mtb.Enums;
using CommonGenerator.Moj.Enums;
using DataGenerator.Mtb.Tables;




namespace BrokerImitator
{
    class Program
    {

        static bool exitSystem = false;
        
        private DbDataModel _dbDataModel;

        private Random _rand;

        private byte[] talk_file = new byte[] { };
        private byte[] video_file = new byte[] { };

        private string mag_talks_dir;

        private List<string> task_shifrs = new List<string>();



        #region Trap application termination
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            Console.WriteLine("Exiting system due to external CTRL-C, or process kill, or shutdown");

            exitSystem = true;

            //shutdown right away so there are no lingering threads
            Environment.Exit(-1);

            return true;
        }
        #endregion

        static void Main(string[] args)
        {
            // Some biolerplate to react to close window event, CTRL-C, kill, etc
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
            
            if (args.Length < 1)
            {
                throw new ArgumentException("Файл конфигурации не указан в качестве аргумента");
            }

            string cfgFile = args[0];
            string fullName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config", cfgFile);

            Console.WriteLine("Файл конфигурации {0} :", fullName);
            Console.WriteLine("");

            if (!File.Exists(fullName))
            {
                throw new FileNotFoundException("Файл конфигурации не существует. Проверте правильность указания файла.");
            }

            BrokerSettings Settings = BrokerSettings.Create(fullName);
            Settings.Validate();

            Console.WriteLine("используем конфигурацию ");
            Console.WriteLine(Settings.ToString());

            Console.WriteLine("");
            Console.WriteLine("Для продолжения нажмите любую клавишу. Для выхода ESC.");
            ConsoleKeyInfo key = Console.ReadKey();
            if (key.Key == ConsoleKey.Escape)
            {
                return;
            }
            Console.WriteLine("");

            //start your multi threaded program here
            Program p = new Program();
            p.Start(Settings);

            //hold the console so it doesn’t run off the end
            while (!exitSystem)
            {
                Thread.Sleep(500);
            }
        }


        
        public void MagTalksDirCleanup()
        {

            try
            {
                Utils.ClearDirectory(mag_talks_dir);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Неудачная попытка очистить директорию MagTalks {0}. {1}",
                mag_talks_dir, ex.Message));
            }

            //log.Info("Test step. Полностью очистили директорию MagTalks ");


        }


        private void GenerateTasksShifrs_LinearDistribution(int TasksCount, List<string> MeroprList, List<string> VidObjectList, DateTime FirstTaskDate)
        {
            
            var meropr = MeroprList[_rand.Next(0, MeroprList.Count)];
            var vid_object = VidObjectList[_rand.Next(0, MeroprList.Count)];

            for (var i = 1; i <= TasksCount; i++)
            {
                var task_shifr = String.Format("{0}-{1}-{2}-{3}", meropr, vid_object, i.ToString(), FirstTaskDate.Year.ToString());
                
                task_shifrs.Add(task_shifr);
            }
        }




        public void GenerateTalks(List<string> tasks_shifrs, int talks_generation_period, int seances_count, int stat_talks_count, List<string> phone_numbers, byte[] talk_file)
        {
            //в каждый момент по истечении интервала генерим разговоры для всех подключенных автозаданий

            //TODO попробовать через joinы
            var goal_tasks = _dbDataModel.Tasks.Where(task => tasks_shifrs.Contains(task.TaskShifr));
            var connected_autotasks = _dbDataModel.TaskGrants.Where(tg => goal_tasks.Contains(tg.task)).Select(tg => tg.t_auto).Distinct().ToList();

            //1 час
            var seance_duration = 1;



            //Dictionary<AutoTask, DateTime> autotasks_dates = new Dictionary<AutoTask, DateTime>();

            ////первоначальная инициализация 
            //foreach (var _a_task in connected_autotasks)
            //{
            //    //дата первого сеанса = дата подклоючения автотаска + 1 сек
            //    autotasks_dates.Add(_a_task, _a_task.creation_date.AddHours(4).AddSeconds(1));
            //}


            Console.WriteLine(String.Format("Разговоры генерируются для {0} подключенных автотасков", connected_autotasks.Count
                ));


            System.Diagnostics.Stopwatch talks_timer = new System.Diagnostics.Stopwatch();
            talks_timer.Start();


            var sounded_talk_generated_count = 0;
            var stat_talk_generated_count = 0;


            for (var number = 1; number <= seances_count; number++)
            {
                System.Diagnostics.Stopwatch talks_step_timer = new System.Diagnostics.Stopwatch();
                talks_step_timer.Start();


                var generate_stat_talk = _rand.Next(0, 100) % 2 == 0;
                //проверим, какой вариант разговора генерить
                if (generate_stat_talk && (stat_talk_generated_count + 1) > stat_talks_count)
                {
                    generate_stat_talk = false;
                }
                if (!generate_stat_talk && (sounded_talk_generated_count + 1) > seances_count - stat_talks_count)
                {
                    generate_stat_talk = true;
                }
                if (generate_stat_talk)
                {
                    stat_talk_generated_count++;
                }
                else
                {
                    sounded_talk_generated_count++;
                }


                foreach (var a_task in connected_autotasks)
                {
                    var begin_date = a_task.creation_date.AddSeconds(4 * 3600 + 1).AddSeconds((number - 1) * talks_generation_period);
                    var end_date = begin_date.AddHours(_rand.Next(1, seance_duration));
                    var direction = _rand.Next(0, 100) % 2 == 0 ? TalkPhoneDirection.Incoming : TalkPhoneDirection.Outgoing;
                    var interlocutor_phone = phone_numbers[_rand.Next(0, phone_numbers.Count)];


                    //var talk = new Talk()
                    //{
                    //    autotask = a_task_date.Key,
                    //    begin = begin_date,
                    //    end = end_date,
                    //    dir = _rand.Next(0, 100) % 2 == 0 ? TalkPhoneDirection.Incoming : TalkPhoneDirection.Outgoing,
                    //    InterlocutorPhone = PhoneNumbers[_rand.Next(0, PhoneNumbers.Count)],

                    //};
                    
                    if (generate_stat_talk)
                    {
                        _dbDataModel.CreateStatTalkWithPhones(a_task.id, begin_date, end_date, direction, a_task.PhoneControl, interlocutor_phone);
                    }
                    else
                    {
                        _dbDataModel.CreateTalkWithPhones(a_task.id, begin_date, end_date, direction, talk_file, a_task.PhoneControl, interlocutor_phone);
                    }


                    //если будет расширение в сторону генерации стенограм/ и.т.д
                    //talk.seanceId = _talk.TalkId;

                    //if (talk.Steno != null)
                    //{
                    //    _mtbDataBuilder.CreateTexts8(t_mtb, _talk, talk.Steno.State, talk.Steno.text);

                    //}


                }


                //
                if (talks_step_timer.ElapsedMilliseconds < talks_generation_period)
                {
                    Thread.Sleep(talks_generation_period - (int)talks_step_timer.ElapsedMilliseconds);
                }
                else
                {
                    Console.WriteLine(String.Format("{0} Превышено время интервала генерации разговоров на {1} мсек", DateTime.Now, (talks_generation_period - (int)talks_step_timer.ElapsedMilliseconds)));
                }

                talks_step_timer.Stop();


                if (number % 5 == 0)
                {
                    Console.WriteLine(String.Format("{0} Записали {1} разговоров", DateTime.Now, number * connected_autotasks.Count));
                }




            }

            talks_timer.Stop();

            Console.WriteLine(String.Format("Общее время генерации разговоров - {0} мин {1} сек {2} мсек, в среднем - {3} мсек/разговор ", talks_timer.Elapsed.Minutes
                , talks_timer.Elapsed.Seconds, talks_timer.Elapsed.Milliseconds, talks_timer.Elapsed.TotalMilliseconds / (seances_count * connected_autotasks.Count)));


        }



        public void Start(BrokerSettings settings)
        {
            Console.WriteLine("Для прерывания нажмите Ctrl+C.");

            //must to do
            
            var generated = 0;

            //чтение файла разговора
            var _talkFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Samples", "sample_talk.wsd");
            talk_file = FileHelper.LoadFile(_talkFileName);
            
            _rand = new Random();
            _dbDataModel = new DbDataModel(settings.Server, settings.User, settings.Password);

            mag_talks_dir = _dbDataModel.GetMagFullPath();
            if (mag_talks_dir == "" || !Directory.Exists(mag_talks_dir))
            {
                throw new Exception("В настройках МАГа не указан путь до директории MagTalks или она недоступна");
            }

            try
            {

                System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

                timer.Start();

                if (settings.CleanEnv)
                {
                    //очистка директории magtalks
                    Console.WriteLine("[INIT] Расчистка директории MagTalks...");
                    MagTalksDirCleanup();
                    Console.WriteLine("[DONE] Завершено\n");

                    //очистить базу
                    Console.WriteLine("[INIT] Очистка таблиц в БД...");
                    _dbDataModel.FullDBCleanup();
                    Console.WriteLine("[DONE] Завершено\n");
                }

                Console.WriteLine("[INIT] Чтение существующих в БД заданий + автозаданий ...");
                _dbDataModel.ReadTasksAutotasks(settings.PhoneNumbers);
                Console.WriteLine("[DONE] Завершено\n");


                Console.WriteLine("[INIT] Формирование несуществующих в БД заданий...");
                if (settings.GenerationType == "tasks_linear_distrbution")
                {
                    GenerateTasksShifrs_LinearDistribution(settings.TasksCount, settings.MeroprList, settings.VidObjectList, settings.FirstTaskDate);
                    
                }
                else if (settings.GenerationType == "tasks_ciphers")
                {
                    task_shifrs = settings.OtmList;
                }
                Console.WriteLine("Разговоры будут генерироваться по следующему списку заданий :\n {0} ", String.Join(",",task_shifrs));
                generated = _dbDataModel.GenerateTasks(task_shifrs, settings.FirstTaskDate, settings.TasksInterval, settings.TaskSrok, settings.PhoneNumbers);
                Console.WriteLine("Сгенерировано {0} заданий, ранее отсутствующих в базе", generated);
                Console.WriteLine("[DONE] Завершено\n");


                //генерировать список автозаданий на случай, если БД была очищена и связывать автозадания будет
                //не с чем
                Console.WriteLine("[INIT] Формирование несуществующих в БД автозаданий...");
                generated = _dbDataModel.GenerateAutoTasks_LinearDistribution(settings.AutoTasksCount, settings.AutoTasksInterval,
                        settings.FirstAutoTaskCreationDate, settings.DevicesList, settings.ChannelList, _dbDataModel.GetControlPhones_ForUnsavedTasks());
                Console.WriteLine("Сгенерировано {0} автозаданий", generated);
                Console.WriteLine("[DONE] Завершено\n");

                
                Console.WriteLine("[INIT] Подключение тасков...");
                generated =  _dbDataModel.TaskConnect(settings.TaskToAutotaskGrantedCountMin, settings.TaskToAutotaskGrantedCountMax);
                Console.WriteLine("Сгенерировано {0} привязок заданий и автозаданий", generated);
                Console.WriteLine("[DONE] Завершено\n");


                Console.WriteLine("[INIT] Запись тасков + автотасков + привязок в БД ...");
                _dbDataModel.Save();
                Console.WriteLine("[DONE] Завершено\n");


                Console.WriteLine("[INIT] Генерация разговоров...");
                GenerateTalks(task_shifrs, settings.TalksGenerationPeriod, settings.SeancesCount, settings.StatTalksCount, settings.PhoneNumbers, talk_file);
                Console.WriteLine("[DONE] Завершено\n");


                string message = String.Format("{0, -35} {1, 40}", "Время работы генератора", timer.Elapsed);
                Console.WriteLine(" {0} ", message);

                timer.Stop();

                Console.WriteLine();


            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION]");
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
